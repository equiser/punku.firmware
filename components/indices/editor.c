/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file editor.c
 ** @brief Implementacion de la clase para gestion de las ediciones del Arbol B
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.01.18 | evolentini      | Version inicial del archivo para manejo de ediciones    |
 **
 ** @addtogroup indices
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "editor.h"
#include "errores.h"
#include <stddef.h>
#include <string.h>
#include <stdio.h>

/* === Definicion y Macros ===================================================================== */
//! Define la cantidad de entradas en la cola utilizada para las pruebas
#define COLA_ENTRADAS               16

//! Define el nombre del archivo de datos utilizado para las pruebas
#define ARCHIVO_DATOS               "%s.dat"

//! Define el nombre del archivo de transacciones utilizado para las pruebas
#define ARCHIVO_BITACORA            "%s.bit"
    
//! Muestra informacion de los nodos y las operaciones para facilitar la depuracion
#define INFORMACION_DEPURACION      0

//! Macro para simular un reinicio durante las operaciones criticas de escritura durante las pruebas
#ifdef TEST
    #define SimularRenicio(paso)                                            \
    if (paso == abortar) {                                                  \
        self->funciones->CerrarArchivo(self->datos->archivo);               \
        self->funciones->CerrarArchivo(self->bloques->archivo);             \
        InformarPaso("Abortando en el paso %d\r\n", paso);                  \
        abortar = 0;                                                        \
        return true;                                                        \
    }
#else
    #define SimularRenicio(paso)
#endif /* TEST */

//! Macro para mostrar informacion de depuracion durante las pruebas
#if defined(TEST) && (INFORMACION_DEPURACION != 0)
    #define InformarPaso(formato, ...)                                      \
        printf("%03d: " formato, __LINE__, ##__VA_ARGS__)
#else
    #define InformarPaso(formato, ...)
#endif /* TEST && REGISTRAR_PASOS */

/* == Declaraciones de tipos de datos internos ================================================= */

//! Esctructura de datos para almacenar la informacion de un archivo
struct editor_archivo_s {
        char nombre[13];                //!< Nombre del archivo para operaciones de apertura y borrado
        arbol_archivo_t archivo;        //!< Descriptor del archivo para el resto de las operaciones
};

//! Tipo de datos para referencia la información de un archivo
typedef struct editor_archivo_s *  editor_archivo_t;

//! Esctructura de datos para almacenar una instancia de un objeto editor
struct arbol_editor_s {
    editor_archivo_t datos;             //!< Descriptor del archivo que almacena los datos del arbol
    editor_archivo_t bloques;           //!< Estructura para la gestion del archivo de transacciones
    arbol_funciones_t * funciones;      //!< Funciones para el manejo de los archivos
    uint8_t editados;                   //!< Indica la cantidad de bloques editados en la operacion
    uint8_t indices[COLA_ENTRADAS];     //!< Vector con los indices de los bloques editados en la operacion
};

/* === Definiciones de variables internas ====================================================== */
#ifdef TEST
    static uint8_t abortar = 0;
#endif /* TEST */

/* === Declaraciones de funciones internas ===================================================== */

/**
 * @brief Creaa el almacenamiento de nueva instancia de un objeto editor
 * 
 * @return          Puntero al espacio de memoria reservado para la nueva instancia
 */
static arbol_editor_t CrearInstancia(void);

#ifdef TEST
    void AbortarPrimeraEscritura(void);

    void AbortarSegundaEscritura(void);
#endif /* TEST */

/* === Definiciones de funciones internas ====================================================== */

static arbol_editor_t CrearInstancia(void) {
    static struct arbol_editor_s editor;
    static struct arbol_funciones_s funciones;
    static struct editor_archivo_s datos;
    static struct editor_archivo_s bloques;

    memset(&editor, 0, sizeof(struct arbol_editor_s));
    editor.funciones = &funciones;
    editor.datos = &datos;
    editor.bloques = &bloques;
    return &editor;
}

static bool EdicionActiva(arbol_editor_t self) {
    return (self->editados != 0xFF);
}

static bool AplicarCambios(arbol_editor_t self) {
    arbol_bloque_t bloque = &(struct arbol_bloque_s) {0};
    bool resultado = true;

    if (self->funciones->AbrirArchivo(self->bloques->nombre, &self->bloques->archivo, false)) {
        InformarPaso("Se encontro un archivo de transacciones\r\n");
        if (self->funciones->LeerBloque(self->bloques->archivo, 0, bloque)) {
            if ((bloque->datos[0] != 0) && (bloque->datos[0] == bloque->datos[1])) {
                InformarPaso("El archivo contiene transacciones que se deben impactar\r\n");
                Alerta("Se encontro un archivo con transacciones que se deben impactar");
                self->editados = bloque->datos[0];
            } else {
                Registrar(INFORMACION, "Se encontro un archivo con transacciones incompletas");
                self->editados = 0;
            }
        }
        while (self->editados > 0) {
            self->editados--;
            InformarPaso("Escribiendo bloques diferidos\r\n");
            resultado = self->funciones->LeerBloque(self->bloques->archivo, self->editados + 1, bloque);
            InformarPaso("Desencolando bloque %d con 0x%02X en la posicion %d\r\n", bloque->indice, bloque->datos[0], self->editados + 1);
            if (resultado) self->funciones->EscribirBloque(self->datos->archivo, bloque->indice, bloque);
            InformarPaso("Resultado: %d\r\n", resultado);
            if (!resultado) break;
        };
        resultado = self->funciones->EscribirArchivo(self->datos->archivo);
        if (resultado) resultado = self->funciones->CerrarArchivo(self->bloques->archivo);
        if (resultado) self->funciones->BorrarArchivo(self->bloques->nombre);
        
        if (resultado) self->funciones->CerrarArchivo(self->datos->archivo);
        if (resultado) self->funciones->AbrirArchivo(self->datos->nombre, &self->datos->archivo, false);
        
        if (resultado) {
            Registrar(INFORMACION, "Las transacciones pendiente se completaron correctamente");
        } else {
            Error("No se pudo completar las transacciones pedientes");
        }
    }
    if (resultado) self->editados = 0xFF;
    return resultado;
}

#ifdef TEST
    void AbortarPrimeraEscritura(void) {
        abortar = 1;
    }

    void AbortarSegundaEscritura(void) {
        abortar = 2;
    }
#endif /* TEST */

/* === Definiciones de funciones externas ====================================================== */

arbol_editor_t EditorCrear(const arbol_nombre_t nombre, const arbol_funciones_t * funciones) {
    arbol_editor_t self = NULL;
    
    self = CrearInstancia();
    sprintf(self->datos->nombre, ARCHIVO_DATOS, nombre);
    sprintf(self->bloques->nombre, ARCHIVO_BITACORA, nombre);
    memcpy(self->funciones, funciones, sizeof(arbol_funciones_t));    

    return self;
}

bool EditorAbrir(arbol_editor_t self, bool nuevo) {
    bool resultado = (self != NULL);

    if (resultado) {
        InformarPaso("Abriendo el archivo de datos\r\n");
        resultado = self->funciones->AbrirArchivo(self->datos->nombre, &self->datos->archivo, nuevo);
        AplicarCambios(self);
    }
    return resultado;
}

bool EditorLeer(arbol_editor_t self, uint8_t indice, arbol_bloque_t bloque) {
    bool resultado = ((self != NULL) && (bloque != NULL));
    uint8_t editado = 0xFF;

    if (resultado) {
        if (EdicionActiva(self)) {
            InformarPaso("Editados %d\r\n", self->editados);
            for (editado = 0; editado < self->editados; editado++) {
                InformarPaso("Buscado %d, Posicion %d, Bloque %d\r\n", indice, editado, self->indices[editado]);
                if (self->indices[editado] == indice) break;
            }
        }
        if (editado < self->editados) {
            InformarPaso("Leyendo bloque editado\r\n");
            resultado = self->funciones->LeerBloque(self->bloques->archivo, editado + 1, bloque);
        } else {
            InformarPaso("Leyendo bloque original\r\n");
            resultado = self->funciones->LeerBloque(self->datos->archivo, indice, bloque);
        }
    }
    return resultado;
}

bool EditorComenzar(arbol_editor_t self) {
    bool resultado = (self != NULL);
    if (resultado) resultado = (self->editados == 0xFF);
    if (resultado) {
        resultado = self->funciones->AbrirArchivo(self->bloques->nombre, &self->bloques->archivo, true);
        if (resultado) {
            self->editados = 0x00;
        } else {
            self->funciones->CerrarArchivo(self->bloques->archivo);
            self->funciones->BorrarArchivo(self->bloques->nombre);
        }
    }
    return resultado;
}

bool EditorConfirmar(arbol_editor_t self) {    
    bool resultado = (self != NULL);
    if (resultado) resultado = (self->editados != 0xFF);
    if (resultado) {
        InformarPaso("Confirmando modificaciones en disco\r\n");
        arbol_bloque_t bloque = &(struct arbol_bloque_s) {0};
        bloque->indice = 0xFF;
        bloque->datos[0] = self->editados;
        resultado = self->funciones->EscribirBloque(self->bloques->archivo, 0, bloque);

        SimularRenicio(1);

        if (resultado) {
            bloque->datos[1] = self->editados;
            resultado = self->funciones->EscribirBloque(self->bloques->archivo, 0, bloque);
            SimularRenicio(2);
        }

        if (resultado) {
            resultado = self->funciones->EscribirArchivo(self->bloques->archivo);
        }
        self->funciones->CerrarArchivo(self->bloques->archivo);
    }
    if (resultado) AplicarCambios(self);
    return resultado;
}

bool EditorEscribir(arbol_editor_t self, const arbol_bloque_t bloque) {
    bool resultado = ((self != NULL) && (bloque != NULL));
    
    if (resultado) resultado = (self->editados != 0xFF);
    if (resultado) {
        uint8_t editado;
        for (editado = 0; editado < self->editados; editado++) {
            InformarPaso("Buscado %d, Posicion %d, Bloque %d\r\n", bloque->indice, editado, self->indices[editado]);
            if (self->indices[editado] == bloque->indice) break;
        }
    
        if (editado < self->editados) {
            InformarPaso("Modificando el bloque %d con 0x%02X encolado en la posicion %d\r\n", bloque->indice, bloque->datos[0], self->editados + 1);
            resultado = self->funciones->EscribirBloque(self->bloques->archivo, editado + 1, bloque);
        } else {
            InformarPaso("Encolando Escritura bloque %d con 0x%02X en la posicion %d\r\n", bloque->indice, bloque->datos[0], self->editados + 1);
            self->indices[self->editados] = bloque->indice;
            resultado = self->funciones->EscribirBloque(self->bloques->archivo, self->editados + 1, bloque);
            self->editados++;
        }
        // if (resultado) resultado = self->funciones->EscribirArchivo(self->archivo);
    }
    return resultado;
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

