/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef ARBOLB_H   /*! @cond    */
#define ARBOLB_H   /*! @endcond */

/** @file arbolb.h
 ** @brief Declaraciones de la clase que proporciona un indice basado en un Arbol B
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.06.30 | evolentini      | Version inicial del archivo de gestion del Arbol B      |
 **|   2 | 2020.01.20 | evolentini      | Soporte para edicion del Arbol B con transacciones      |
 **
 ** @addtogroup indices
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include <stdbool.h>
#include <stdint.h>
#include "editor.h"

/* === Cabecera C++ ============================================================================ */

#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos para almacenar la clave de un elemento de un Arbol B
typedef uint32_t arbol_clave_t;

//! Tipo de datos para almacenar el valor de un elemento de un Arbol B
typedef uint8_t arbol_valor_t;

//! Tipo de datos para almacenar el par clave valor de un elemento de un Arbol B
typedef struct {
    arbol_clave_t clave;    //!< Clave de busqueda del par
    arbol_valor_t valor;    //!< Valor asociado a la clave del par
} arbol_par_t;

//! Tipo de datos para almacenar un descriptor a un Arbol B
typedef struct arbol_s * arbol_t;

//! Tipo de datos para obtener las estadisticas actuales del Arbol B
typedef struct arbol_estadisticas_s {
    uint8_t libres;                     //!< Cantidad de bloques libres en el archivo
    uint8_t ocupados;                   //!< Cantidad de bloques ocupados en el archivo
    uint8_t niveles;                    //!< Cantidad de niveles en el arbol
    uint8_t elementos;                  //!< Cantidad maxima de elementos en cada nodo del arbol
    uint16_t almacenados;               //!< Cantidad de elementos almacenados en el arbol
} arbol_estadisticas_t;

//! Prototipo de la funcion para procesar cada entrada al listar un Arbol B
typedef bool (*arbol_listar_t)(uint16_t indice, arbol_clave_t clave, arbol_valor_t valor, void * referencia);

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Funcion para crear una instancia de memoria de un Arbol B nuevo o existente
 * 
 * @param[in]  editor      Referencia un objeto editor responsable de controlar las operaciones
 *                         de lectura y escritura en disco.
 * @param[in]  elementos   Parametro para indicar que se desea crear un nuevo arbol en disco.
 *                         Tambien permite fijar la cantidad maxima de elentos en el nodo para
 *                         facilitar las pruebas. En produccion se puede utilizar los macros
 *                         true y false de la stdbool independientemente de su definicion.
 * @return                 Puntero al descriptor de la nueva instancia creada. Si el arbol no
 *                         existe en el disco entonces la funcion devuleve NULL
 */
arbol_t ArbolCrear(const arbol_editor_t editor, int elementos);

/**
 * @brief Funcion para listar las claves contenidas en un Arbol B
 * 
 * @param[in]  arbol       Puntero al descriptor obtenido al crear la instancia del arbol
 * @param[in]  procesador  Referencia a la funcion externa responsable de procesar cada elemento del listado
 * @param[in]  referencia  Referencia del usuario para enviar como parametro a la funcion externa
 * @return     True        El procesamiento de todos los elementos del arbol se completo sin errores
 * @return     False       La funcion externa de procesamiento del listado devolvio error en por lo menos un elemento
 */
bool ArbolListar(arbol_t arbol, arbol_listar_t procesador, void * referencia);

/**
 * @brief Funcion para agregar un par clave:valor en el Arbol B
 * 
 * @param[in]  arbol       Puntero al descriptor obtenido al crear la instancia del arbol
 * @param[in]  clave       Clave que se utiliza para indexar el par
 * @param[in]  valor       Valor que se desea almacenar relacionado con la clave
 */
bool ArbolInsertar(arbol_t arbol, arbol_clave_t clave, arbol_valor_t valor);

/**
 * @brief Funcion para buscar un valor a partir de una clave
 * 
 * @param[in]  arbol       Puntero al descriptor obtenido al crear la instancia del arbol
 * @param[in]  clave       Clave que se utiliza para buscar en el indice
 * @param[in]  valor       Valor que se desea recuperar a partir de la clave
 * @return     True        La clave se encontro en el arbol y el valor devuelto es valido
 * @return     False       La clave no se encontro en el arbol y el valor devuelto no es valido
 */
bool ArbolBuscar(arbol_t arbol, arbol_clave_t clave, arbol_valor_t * valor);

/**
 * @brief Funcion para eliminar un valor del Arbol B a partir de una clave
 * 
 * @param[in]  arbol       Puntero al descriptor obtenido al crear la instancia del arbol
 * @param[in]  clave       Clave que se utiliza para elimianar el par en el indic
 * @return     True        La clave se encontro en el arbol y el valor devuelto es valido
 * @return     False       La clave no se encontro en el arbol y el valor devuelto no es valido
 */
bool ArbolEliminar(arbol_t arbol, arbol_clave_t clave);

/**
 * @brief Funcion para obtener las estadisticas actuales del Arbol B
 * 
 * @param[in]  arbol       Puntero al descriptor obtenido al crear la instancia del arbol
 * @param[out] resultado   Puntero a la estructura donde se deben cargar las estadisticas
 */
void ArbolEstadisticas(arbol_t arbol, arbol_estadisticas_t * resultado);

/**
 * @brief Elimina todos los datos del Arbol B para volver a un indice vacio
 * 
 * @param[in]  arbol       Puntero al descriptor obtenido al crear la instancia del arbol
 * @return     True        La operacion de borrado se completo sin errores y el arbol esta vacio
 * @return     False       No se pudo completar la operacion de borrado del arbol
 */
bool ArbolLimpiar(arbol_t arbol);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* ARBOLB_H */
