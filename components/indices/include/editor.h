/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef EDITOR_H   /*! @cond    */
#define EDITOR_H   /*! @endcond */

/** @file editor.h
 ** @brief Declaraciones de la clase para gestion de las ediciones del Arbol B
 **
 ** Una operación de actualizacion en el Arbol B eventualmente implica la modificación de varios 
 ** nodos y por lo tanto una secuencia de escrituras en disco. Si por alguna razon esta secuencia
 ** se interrumpe el Arbol queda en un estado invalido imposible de reparar. Para evitar esto
 ** el objeto editor proporciona una mecanismo de transacciones que permite asegurar que cuando
 ** se efectua una edicion del Arbol la misma se impacta completa en el disco o no se impacta 
 ** en lo absoluto, evitando de esta forma la corrupción del indice.
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.01.18 | evolentini      | Version inicial del archivo para manejo de ediciones    |
 **
 ** @addtogroup indices
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include <stdbool.h>
#include <stdint.h>

/* === Cabecera C++ ============================================================================ */

#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

//! Tamaño del bloque que se lee y escribe en el almacenamiento
#define ESPACIO_BLOQUE          (512)

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos para almacenar el nombre de un Arbol B
typedef char arbol_nombre_t[8];

//! Tipo de datos para almacenar una referencia al almacenamiento de un Arbol B
typedef void * arbol_archivo_t;

//! TEstructura para almacenar un bloque de datos en disco
struct arbol_bloque_s {
    uint8_t indice;                         //!< Numero logico del bloque de datos
    uint8_t datos[ESPACIO_BLOQUE - 1];      //!< Datos almacenados en el bloque
}  __attribute__((packed)); 

//! Tipo de datos para referenciar un bloque de datos en disco
typedef struct arbol_bloque_s * arbol_bloque_t;

//! Prototipo de la funcion para apertura o creacion del archivo que almacena un Arbol B
typedef bool (*arbol_obtener_archivo_t)(const char * nombre, arbol_archivo_t * archivo, bool nuevo);

//! Prototipo de la funcion para la lectura de la cabecera de un nodo del Arbol B
typedef bool (*arbol_lectura_bloque_t)(arbol_archivo_t archivo, uint8_t indice, arbol_bloque_t bloque);

//! Prototipo de la funcion para la escritura de la cabecera de un nodo del Arbol B
typedef bool (*arbol_escritura_bloque_t)(arbol_archivo_t archivo, uint8_t indice, const arbol_bloque_t bloque);

//! Prototipo de la funcion para informar un evento de modificación del contenido del Arbol B
typedef bool (*arbol_operacion_archivo_t)(arbol_archivo_t archivo);

//! Prototipo de la funcion para informar un evento de modificación del contenido del Arbol B
typedef bool (*arbol_operacion_nombre_t)(const char * nombre);

//! Tipo de datos para definir las funciones de lectura y escritura del arbol
typedef struct arbol_funciones_s {
    arbol_obtener_archivo_t AbrirArchivo;       //!< Funcion para la apertura deun archivo en disco
    arbol_operacion_archivo_t CerrarArchivo;       //!< Funcion para el cierre de un archivo en disco
    arbol_operacion_archivo_t EscribirArchivo;     //!< Funcion para completar la escritura diferida en disco
    arbol_operacion_nombre_t BorrarArchivo;       //!< Funcion para el borrado de un archivo en disco
    arbol_lectura_bloque_t LeerBloque;          //!< Funcion para la lectura de un bloque del archivo
    arbol_escritura_bloque_t EscribirBloque;    //!< Funcion para la escritura de un bloque del archivo
} arbol_funciones_t;

//! Tipo de datos para almacenar un monitor de ediciones de un Arbol B
typedef struct arbol_editor_s * arbol_editor_t;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Funcion para crear una nueva instancia a un objeto editor del Arbol B
 * 
 * @param   nombre      Identificador unico del Arbol B que se crea o recupera de disco
 * @param   funciones   Estructura con las funciones para el manejo del almacenamiento
 * @return              Puntero al descriptor de la nueva instancia creada.
 */
arbol_editor_t EditorCrear(const arbol_nombre_t nombre, const arbol_funciones_t * funciones);

/**
 * @brief Funcion para abrir el archivo de almacenamiento de un Arbol B
 * 
 * @param   editor      Referencia al objeto editor que controla la operacion
 * @param   nuevo       Indica si se debe crear un nuevo archivo o recuperar uno existente
 * @return  true        La apertura del archivo del arbol se pudo completar sin errores
 * @return  false       No se pudo realizar la apertura del archivo del arbol
 */
bool EditorAbrir(arbol_editor_t editor, bool nuevo);

/**
 * @brief Funcion para comenzar una operacion de edicion en el Arbol B
 * 
 * @param   editor      Referencia al objeto editor que controla la operacion
 * @return  true        La nueva operacion de edicion se pudo comenzar sin errores
 * @return  false       No se pudo comenzar la operacion de edicion en el arbol
 */
bool EditorComenzar(arbol_editor_t editor);

/**
 * @brief Funcion para conpletar una operacion de edicion en el Arbol B
 * 
 * @param   editor      Referencia al objeto editor que controla la operacion
 * @return  true        La nueva operacion de edicion se pudo completar sin errores
 * @return  false       No se pudo completar la operacion de edicion en el arbol
 */
bool EditorConfirmar(arbol_editor_t editor);

/**
 * @brief Funcion para la escritura de un bloque en disco
 * 
 * @param   editor      Referencia al objeto editor que controla la operacion
 * @param   indice      Indice del bloque que se desea leer
 * @param   bloque      Puntero a la variable donde se almacenaran los datos leidos
 * @return  true        La lectura se pudo completar sin errores
 * @return  false       No se pudo completar la lectura del bloque en disco
 */
bool EditorLeer(arbol_editor_t editor, uint8_t indice, arbol_bloque_t bloque);

/**
 * @brief Funcion para la escritura de un bloque en disco
 * 
 * @param   editor      Referencia al objeto editor que controla la operacion
 * @param   bloque      Puntero a la variable con los datos que se deben escribir
 * @return  true        La escritura se pudo completar sin errores
 * @return  false       No se pudo completar la escritura del bloque en disco
 */
bool EditorEscribir(arbol_editor_t editor, const arbol_bloque_t bloque);

/* === Ciere de documentacion ================================================================== */

#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* EDITOR_H */
