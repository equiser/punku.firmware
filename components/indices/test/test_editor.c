/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_editor.c
 ** @brief Puerbas unitarias de la clase gestiona de las ediciones del Arbol B
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.01.18 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup indices
 ** @{ */

/**
 * @todo Crear una instancia de un arbol que ya existe en disco pero esta corrupto
 */

/* === Inclusiones de cabeceras ================================================================ */
#include <stdbool.h>
#include "unity.h"
#include "editor.h"
#include "mock_errores.h"

/* === Definicion y Macros ===================================================================== */

//! Nombre del indice utilizado en las pruebas
#define NOMBRE              "prueba" 

//! Nombre del archivo de datos para el indice utilizado en las pruebas
#define ARCHIVO_DATOS       NOMBRE ".dat"

//! Nombre del archivo de transacciones para el indice utilizado en las pruebas
#define ARCHIVO_BITACORA    NOMBRE ".bit"

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Declaraciones de funciones internas ===================================================== */

//! Funcion para la apertura o creacion de un archivo en disco
bool AbrirArchivo(const char * nombre, arbol_archivo_t * archivo, bool nuevo);

//! Funcion para la completar la escritura diferida de un archivo en disco
bool EscribirArchivo(arbol_archivo_t archivo);

//! Funcion para el cierre de un archivo en disco
bool CerrarArchivo(arbol_archivo_t archivo);

//! Funcion para el borrado de un archivo en disco
bool BorrarArchivo(const char * nombre);

//! Funcion para la lectura de un sector de disco
bool LeerBloque(arbol_archivo_t archivo, uint8_t indice, arbol_bloque_t bloque);

//! Funcion para la escritura de un sector de disco
bool EscribirBloque(arbol_archivo_t archivo, uint8_t indice, const arbol_bloque_t bloque);

//! Funcion para simular un reinicio del sistema durante una de las escrituras criticas
extern void AbortarPrimeraEscritura(void);

//! Funcion para simular un reinicio del sistema durante una de las escrituras criticas
extern void AbortarSegundaEscritura(void);

/* === Definiciones de variables internas ====================================================== */

//! Constante con las funciones para la gestion del almacenamiento
static const arbol_funciones_t funciones = {
    .AbrirArchivo = AbrirArchivo,
    .EscribirArchivo = EscribirArchivo,
    .CerrarArchivo = CerrarArchivo,
    .BorrarArchivo = BorrarArchivo,
    .LeerBloque = LeerBloque,
    .EscribirBloque = EscribirBloque,
};

//! Constante con un bloque da datos de ejemplo para las pruebas
static const arbol_bloque_t cero_a = &(struct arbol_bloque_s) {
    .indice = 0, .datos = { [0 ... 510] = 0xAA}
};

//! Constante con un bloque da datos de ejemplo para las pruebas
static const arbol_bloque_t cero_b = &(struct arbol_bloque_s) {
    .indice = 0, .datos = { [0 ... 510] = 0x55}
};

//! Constante con un bloque da datos de ejemplo para las pruebas
static const arbol_bloque_t cero_c = &(struct arbol_bloque_s) {
    .indice = 0, .datos = { [0 ... 510] = 0xA5}
};

//! Constante con un bloque da datos de ejemplo para las pruebas
static const arbol_bloque_t uno_a = &(struct arbol_bloque_s) {
    .indice = 1, .datos = { [0 ... 510] = 0xAA}
};

//! Constante con un bloque da datos de ejemplo para las pruebas
static const arbol_bloque_t uno_b = &(struct arbol_bloque_s) {
    .indice = 1, .datos = { [0 ... 510] = 0x55}
};

//! Variable global que permite simular un reinicio durante las escrituras criticas
extern uint8_t abortar;

/* === Definiciones de funciones internas ====================================================== */

bool AbrirArchivo(const char * nombre, arbol_archivo_t * archivo, bool nuevo) {
    if (nuevo) {
        *archivo = fopen(nombre, "w+");
    } else {
        *archivo = fopen(nombre, "r+");
    }
    return (*archivo != NULL);
}

bool EscribirArchivo(arbol_archivo_t archivo) {
    return (fflush(archivo) == 0);
}

bool CerrarArchivo(arbol_archivo_t archivo) {
    return (fclose(archivo) == 0);
}

bool BorrarArchivo(const char * nombre) {
    return (remove(nombre) == 0);
}

bool LeerBloque(arbol_archivo_t archivo, uint8_t indice, arbol_bloque_t bloque) {
    fseek(archivo, ESPACIO_BLOQUE * indice , SEEK_SET);
    fread(bloque, ESPACIO_BLOQUE, 1, archivo);
    return true;
}

bool EscribirBloque(arbol_archivo_t archivo, uint8_t indice, const arbol_bloque_t bloque) {
    fseek(archivo, ESPACIO_BLOQUE * indice, SEEK_SET);
    fwrite(bloque, ESPACIO_BLOQUE, 1, archivo);
    return true;
}

/* === Definiciones de funciones externas ====================================================== */
void setUp(void) {

}

void tearDown(void) {
    remove(ARCHIVO_DATOS);
    remove(ARCHIVO_BITACORA);
}

//! @test Crear una instancia de un editor
void test_crear_instancia(void) {
    arbol_editor_t editor = NULL;

    // Cuando llamo a la función para reabrir un Arbol B preexitente
    editor = EditorCrear(NOMBRE, &funciones);
    
    // Entonces not recupero un descriptor valido para operar sobre el arbol creado
    TEST_ASSERT_NOT_NULL(editor);
}

//! @test Todas las operaciones fallan si se envia una referencia nula
void test_operaciones_revisar_refrerencias_nulas(void) {
    arbol_bloque_t bloque = &(struct arbol_bloque_s) {0};
    arbol_editor_t editor = EditorCrear(NOMBRE, &funciones);
    
    // Cuando llamo a la función para abrir un archivo con un editor nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(EditorAbrir(NULL, true));

    // Cuando llamo a la función para leer un archivo con un editor nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(EditorLeer(NULL, 0, bloque));

    // Cuando llamo a la función para leer un archivo con un bloque nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(EditorLeer(editor, 0, NULL));

    // Cuando llamo a la función para escribir un archivo con un editor nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(EditorEscribir(NULL, bloque));

    // Cuando llamo a la función para escribir un archivo con un bloque nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(EditorEscribir(editor, NULL));

    // Cuando llamo a la función para comenzar una edicion con un editor nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(EditorComenzar(NULL));

    // Cuando llamo a la función para completar una edicion con un editor nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(EditorConfirmar(NULL));
}

//! @test Crear un nuevo Arbol B en disco
void test_crear_nuevo_arbol(void) {
    arbol_bloque_t bloque = &(struct arbol_bloque_s) {
        .indice = 0, .datos = {0},
    };
    
    // Dado un editor para un Arbol B
    arbol_editor_t editor = EditorCrear(NOMBRE, &funciones);

    // Y un sistema donde no existe el archivo de datos del Arbol B
    remove(ARCHIVO_DATOS);

    // Y un sistema donde no existe un archivo de transacciones del Arbol B
    remove(ARCHIVO_BITACORA);

    // Cuando llamo a la función para crear un nuevo Arbol B
    TEST_ASSERT(EditorAbrir(editor, true));

    // Entonces existe un archivo de datos para el Arbol B
    arbol_archivo_t archivo = fopen(ARCHIVO_DATOS, "r");
    fclose(archivo);
    TEST_ASSERT_NOT_NULL(archivo);
}

//! @test Crear no se puede realizar una escritura sin antes empezar una modificacion
void test_no_se_puede_escribir_sin_iniciar_modificacion(void) {
    arbol_bloque_t bloque = &(struct arbol_bloque_s) {0};
    
    // Dado un editor para un Arbol B
    arbol_editor_t editor = EditorCrear(NOMBRE, &funciones);

    // Cuando llamo a la función para crear un nuevo Arbol B
    TEST_ASSERT(EditorAbrir(editor, true));

    // Entonces no puedo realizar una escritura sin antes empezar una modificacion
    TEST_ASSERT_FALSE(EditorEscribir(editor, bloque));
}

//! @test Crear no se puede realizar una escritura despues de completar una modificacion
void test_no_se_puede_escribir_despues_de_completar_una_modificacion(void) {
    arbol_bloque_t bloque = &(struct arbol_bloque_s) {0};
    
    // Dado un editor para un Arbol B
    arbol_editor_t editor = EditorCrear(NOMBRE, &funciones);

    // Cuando llamo a la función para crear un nuevo Arbol B
    TEST_ASSERT(EditorAbrir(editor, true));

    // Y comienzo una operacion de edicion
    TEST_ASSERT(EditorComenzar(editor));

    // Y confirmo la operacion de edicion
    TEST_ASSERT(EditorConfirmar(editor));

    // Entonces no puedo realizar una escritura despues de terminar la operacion de edicion
    TEST_ASSERT_FALSE(EditorEscribir(editor, bloque));
}

//! @test Crear realizar una operacion de modificacion completa
void test_operacion_escritura_correcta(void) {
    arbol_bloque_t bloque = &(struct arbol_bloque_s) {0};
    
    // Dado un editor para un Arbol B
    arbol_editor_t editor = EditorCrear(NOMBRE, &funciones);

    // Cuando llamo a la función para crear un nuevo Arbol B
    TEST_ASSERT(EditorAbrir(editor, true));

    // Y comienzo una operacion de edicion
    TEST_ASSERT(EditorComenzar(editor));

    // Y escribo un bloque de datos en el archivo
    TEST_ASSERT(EditorEscribir(editor, cero_a));

    // Y confirmo la operacion de edicion
    TEST_ASSERT(EditorConfirmar(editor));

    // Entonces al leer el mismo bloque recupero los mismos datos
    TEST_ASSERT(EditorLeer(editor, cero_a->indice, bloque));
    TEST_ASSERT_EQUAL_MEMORY(cero_a, bloque, ESPACIO_BLOQUE);
}


//! @test Crear revetir una operacion de modificacion incompleta
void test_operacion_escritura_incompleta(void) {
    arbol_bloque_t bloque = &(struct arbol_bloque_s) {0};

    // Dado un editor para un Arbol B
    arbol_editor_t editor = EditorCrear(NOMBRE, &funciones);

    // Cuando llamo a la función para crear un nuevo Arbol B
    TEST_ASSERT(EditorAbrir(editor, true));

    // Y escribo correctamente un bloque de datos en el archivo
    TEST_ASSERT(EditorComenzar(editor));
    TEST_ASSERT(EditorEscribir(editor, cero_a));
    TEST_ASSERT(EditorConfirmar(editor));

    // Y realiza una modificacion incompleta del bloque de datos en el archivo
    TEST_ASSERT(EditorComenzar(editor));
    TEST_ASSERT(EditorEscribir(editor, cero_b));

    // Entonces al leer el mismo bloque recupero los datos actualizados
    TEST_ASSERT(EditorLeer(editor, cero_b->indice, bloque));
    TEST_ASSERT_EQUAL_MEMORY(cero_b, bloque, ESPACIO_BLOQUE);

    // Cuando llamo a la funcion para recuperar el arbol B existente
    TEST_ASSERT(EditorAbrir(editor, false));

    // Entonces al leer el mismo bloque recupero los previos a la modificacion incompleta
    TEST_ASSERT(EditorLeer(editor, cero_a->indice, bloque));
    TEST_ASSERT_EQUAL_MEMORY(cero_a, bloque, ESPACIO_BLOQUE);
}


//! @test Lectura de la ultima version del bloque modificado
void test_lectura_ultima_version_bloque_modificado(void) {
    arbol_bloque_t bloque = &(struct arbol_bloque_s) {0};

    // Dado un editor para un Arbol B
    arbol_editor_t editor = EditorCrear(NOMBRE, &funciones);

    // Cuando llamo a la función para crear un nuevo Arbol B
    TEST_ASSERT(EditorAbrir(editor, true));

    // Y escribo correctamente un bloque de datos en el archivo
    TEST_ASSERT(EditorComenzar(editor));
    TEST_ASSERT(EditorEscribir(editor, cero_a));

    // Entonces al leer el mismo bloque recupero los datos de la primera actualizacion
    TEST_ASSERT(EditorLeer(editor, cero_a->indice, bloque));
    TEST_ASSERT_EQUAL_MEMORY(cero_a, bloque, ESPACIO_BLOQUE);

    // Cuando se realiza una nueva esccritura del mismo bloque don datos diferentes
    TEST_ASSERT(EditorEscribir(editor, cero_b));

    // Entonces al leer el mismo bloque recupero los datos de la segunda actualizacion
    TEST_ASSERT(EditorLeer(editor, cero_b->indice, bloque));
    TEST_ASSERT_EQUAL_MEMORY(cero_b, bloque, ESPACIO_BLOQUE);

    // Cuando confirmo la operacion de modificacion del arhcivo
    TEST_ASSERT(EditorEscribir(editor, cero_b));

    // Entonces al leer el mismo bloque recupero los datos de la segunda actualizacion
    TEST_ASSERT(EditorLeer(editor, cero_b->indice, bloque));
    TEST_ASSERT_EQUAL_MEMORY(cero_b, bloque, ESPACIO_BLOQUE);
}

//! @test Un reinicio antes de completar la confirmacion en disco reversa los datos escritos
void test_reinicio_antes_de_confirmar_los_cambios_en_disco(void) {
    arbol_bloque_t bloque = &(struct arbol_bloque_s) {0};

    // Dado un editor para un archivo con un bloque de datos conocido
    arbol_editor_t editor = EditorCrear(NOMBRE, &funciones);
    TEST_ASSERT(EditorAbrir(editor, true));
    TEST_ASSERT(EditorComenzar(editor));
    TEST_ASSERT(EditorEscribir(editor, cero_a));
    TEST_ASSERT(EditorConfirmar(editor));

    // Cuando empiezo una nueva modificación 
    TEST_ASSERT(EditorComenzar(editor));

    // Y realizo una actualizacion del bloque de datos conocido
    TEST_ASSERT(EditorEscribir(editor, cero_b));

    // Y se produce un reinicio antes de escrirbir la confirmación de las modificaciones
    AbortarPrimeraEscritura();
    TEST_ASSERT(EditorConfirmar(editor));

    // Cuando llamo a la funcion para recuperar el arbol B existente
    TEST_ASSERT(EditorAbrir(editor, false));

    // Entonces al leer el mismo bloque recupero los previos a la modificacion incompleta
    TEST_ASSERT(EditorLeer(editor, cero_a->indice, bloque));
    TEST_ASSERT_EQUAL_MEMORY(cero_a, bloque, ESPACIO_BLOQUE);
}

//! @test Un reinicio despues de completar la confirmacion en disco conserva los datos escritos
void test_reinicio_despues_de_confirmar_los_cambios_en_disco(void) {
    arbol_bloque_t bloque = &(struct arbol_bloque_s) {0};

    // Dado un editor para un archivo con un bloque de datos conocido
    arbol_editor_t editor = EditorCrear(NOMBRE, &funciones);
    TEST_ASSERT(EditorAbrir(editor, true));
    TEST_ASSERT(EditorComenzar(editor));
    TEST_ASSERT(EditorEscribir(editor, cero_a));
    TEST_ASSERT(EditorConfirmar(editor));

    // Cuando empiezo una nueva modificación 
    TEST_ASSERT(EditorComenzar(editor));

    // Y realizo una actualizacion del bloque de datos conocido
    TEST_ASSERT(EditorEscribir(editor, cero_b));

    // Y se produce un reinicio despues de escrirbir la confirmación de las modificaciones
    AbortarSegundaEscritura();
    TEST_ASSERT(EditorConfirmar(editor));

    // Cuando llamo a la funcion para recuperar el arbol B existente
    TEST_ASSERT(EditorAbrir(editor, false));

    // Entonces al leer el mismo bloque recupero los posteriores modificados por la operacion incompleta
    TEST_ASSERT(EditorLeer(editor, cero_a->indice, bloque));
    TEST_ASSERT_EQUAL_MEMORY(cero_b, bloque, ESPACIO_BLOQUE);
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

