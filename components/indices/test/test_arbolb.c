/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_arbolb.c
 ** @brief Puerbas unitarias de la clase que proporciona un indice basado en un Arbol B
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.04.03 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup indices
 ** @{ */

/**
 * @todo Agregar un dato a un arbol, cerrarlo y volver a recuperar el dato agregado
 * @todo Agregar dos veces el mismo datos en un arbol
 * @todo Eliminar un dato que no esta en el arbol
 * @todo Crear una instancia de un arbol que ya existe en disco y buscar datos existentes
 * @todo Crear una instancia de un arbol que ya existe en disco pero esta corrupto
 */

/* === Inclusiones de cabeceras ================================================================ */
#include <stdbool.h>
#include "unity.h"
#include "arbolb.h"
#include "editor.h"
#include "mock_errores.h"

/* === Definicion y Macros ===================================================================== */

//! Nombre del indice utilizado para las pruebas
#define NOMBRE          "prueba" 

//! Nombre del archivo de datos utilizado para las pruebas
#define NOMBRE_ARCHVO   "prueba.dat"

//! Verifica que se pueden encontrar las claves y que los valores coinciden
#define TEST_ASSERT_FIND_VALUES_MESSAGE(arbol, contenido, mensaje) {                            \
    uint16_t indice;                                                                            \
    arbol_valor_t valor;                                                                        \
    char completo[128];                                                                         \
    for(indice = 0; indice < sizeof(contenido) / sizeof(arbol_par_t); indice++) {               \
        sprintf(completo, "No se encontro la clave. Indice: %d, Clave: %d. %s",                 \
            indice, contenido[indice].clave, mensaje);                                          \
        TEST_ASSERT_MESSAGE(ArbolBuscar(arbol, contenido[indice].clave, &valor), completo);     \
        sprintf(completo, "Indice: %d, Clave: %d. %s",                                          \
             indice, contenido[indice].clave, mensaje);                                         \
        TEST_ASSERT_EQUAL_MESSAGE(contenido[indice].valor, valor, completo);                    \
    }                                                                                           \
}

//! Verifica que se pueden encontrar las claves y que los valores coinciden
#define TEST_ASSERT_FIND_VALUES(arbol, contenido)                                               \
    TEST_ASSERT_FIND_VALUES_MESSAGE(arbol, contenido, "")

//! Verifica que el contenido del arbol coincide con una lista ordenada
#define TEST_ASSERT_EQUAL_ARBOL(arbol, contenido) {                                             \
    const uint8_t CANTIDAD = sizeof(contenido) / sizeof(arbol_par_t);                           \
    arbol_par_t pares[sizeof(contenido) / sizeof(arbol_par_t)];                                 \
    struct lista_s lista = {.pares = pares, .cantidad = CANTIDAD};                              \
    uint16_t indice;                                                                            \
    char mensaje[64];                                                                           \
    TEST_ASSERT_TRUE(ArbolListar(arbol, ListarPar, &lista));                                    \
    for(indice = 0; indice < sizeof(contenido) / sizeof(arbol_par_t); indice++) {               \
        sprintf(mensaje, "Indice: %d", indice);                                                 \
        TEST_ASSERT_EQUAL_MESSAGE(contenido[indice].clave, pares[indice].clave, mensaje);       \
        TEST_ASSERT_EQUAL_MESSAGE(contenido[indice].valor, pares[indice].valor, mensaje);       \
    }                                                                                           \
}

//! Verifica que el contenido del arbol esta ordenado
#define TEST_ASSERT_ARBOL_ORDENADO_MESSAGE(arbol, cantidad_, mensaje) {                          \
    arbol_par_t pares[cantidad_];                                                                \
    struct lista_s lista = {.pares = pares, .cantidad = cantidad_};                              \
    uint8_t indice;                                                                             \
    TEST_ASSERT_TRUE(ArbolListar(arbol, ListarPar, &lista));                                    \
    for(indice = 0; indice < cantidad_ - 1; indice++) {                                          \
        TEST_ASSERT_GREATER_THAN_MESSAGE(pares[indice].clave, pares[indice + 1].clave,          \
            mensaje);                                                                           \
    }                                                                                           \
}

//! Verifica que el contenido del arbol esta ordenado
#define TEST_ASSERT_ARBOL_ORDENADO(arbol, cantidad)                                             \
    TEST_ASSERT_ARBOL_ORDENADO_MESSAGE(arbol, cantidad, "")                                     \

//! Verifica que el las estadisticas del arbol coinciden con las esperadas
#define TEST_ASSERT_ARBOL_ESTADISTICAS_MESSAGE(arbol, esperado, mensaje) {                      \
    arbol_estadisticas_t estadisticas;                                                          \
    char completo[128];                                                                         \
    ArbolEstadisticas(arbol, &estadisticas);                                                    \
    sprintf(completo, "No coincide la cantidad de bloques libres. %s", mensaje);                \
    TEST_ASSERT_EQUAL_MESSAGE(esperado.libres, estadisticas.libres, mensaje);                   \
    sprintf(completo, "No coincide la cantidad de bloques ocupados. %s", mensaje);              \
    TEST_ASSERT_EQUAL_MESSAGE(esperado.ocupados, estadisticas.ocupados, mensaje);               \
    sprintf(completo, "No coincide la niveles del arbol. %s", mensaje);                         \
    TEST_ASSERT_EQUAL_MESSAGE(esperado.niveles, estadisticas.niveles, mensaje);                 \
    sprintf(completo, "No coincide la cantidad de elementos del arbol. %s", mensaje);           \
    TEST_ASSERT_EQUAL_MESSAGE(esperado.almacenados, estadisticas.almacenados, mensaje);         \
}

//! Verifica que el las estadisticas del arbol coinciden con las esperadas
#define TEST_ASSERT_ARBOL_ESTADISTICAS(arbol, esperado)                                         \
    TEST_ASSERT_ARBOL_ESTADISTICAS_MESSAGE(arbol, esperado, "")

//! Verifica se puedan insertas todas las claves en el arbol
#define TEST_ASSERT_INSERTAR_VALORES_MESSAGE(arbol, pares, inicio, cantidad, mensaje) {         \
    uint16_t indice;                                                                            \
    char completo[128];                                                                         \
    for(indice = 0; indice < cantidad; indice++) {                                              \
        sprintf(completo, "No se pudo insertar la clave %d. %s",                                \
            pares[inicio + indice].clave, mensaje);                                             \
        TEST_ASSERT_MESSAGE(ArbolInsertar(arbol, pares[inicio + indice].clave,                  \
            pares[inicio + indice].valor), completo);                                           \
    }                                                                                           \
}

//! Verifica se puedan insertas todas las claves en el arbol
#define TEST_ASSERT_INSERTAR_VALORES(arbol, pares, inicio, cantidad)                            \
    TEST_ASSERT_INSERTAR_VALORES_MESSAGE(arbol, pares, inicio, cantidad, "")

//! Verifica que se pueden encontrar las claves y que los valores coinciden
#define TEST_ASSERT_ARBOL_ELIMINADOS_MESSAGE(arbol, contenido, eliminados, mensaje) {           \
    uint16_t indice;                                                                            \
    uint8_t ultimo = 0;                                                                         \
    arbol_valor_t valor;                                                                        \
    char completo[128];                                                                         \
    for(indice = 0; indice < sizeof(contenido) / sizeof(arbol_par_t); indice++) {               \
        if (indice == eliminados[ultimo]) {                                                     \
            sprintf(completo, "Se encontro la clave. Indice: %d, Clave: %d. %s",                \
                indice, contenido[indice].clave, mensaje);                                      \
            TEST_ASSERT_FALSE_MESSAGE(ArbolBuscar(arbol, contenido[indice].clave, &valor),      \
                 completo);                                                                     \
            ultimo++;                                                                           \
        } else {                                                                                \
            sprintf(completo, "No se encontro la clave. Indice: %d, Clave: %d. %s",             \
                indice, contenido[indice].clave, mensaje);                                      \
            TEST_ASSERT_MESSAGE(ArbolBuscar(arbol, contenido[indice].clave, &valor), completo); \
            sprintf(completo, "Indice: %d, Clave: %d. %s",                                      \
                indice, contenido[indice].clave, mensaje);                                      \
            TEST_ASSERT_EQUAL_MESSAGE(contenido[indice].valor, valor, completo);                \
        }                                                                                       \
    }                                                                                           \
}

/* == Declaraciones de tipos de datos internos ================================================= */

typedef struct lista_s {
    arbol_par_t * pares;
    uint16_t cantidad;
} * lista_t;

/* === Declaraciones de funciones internas ===================================================== */

//! Completa un arreglo de pares con claves y valores generados en forma secuencial
void GenerarClaves(arbol_par_t * pares, uint16_t cantidad, arbol_clave_t clave, uint16_t paso);

//! Completa un arreglo de pares con claves provistas en una lista y valores generados secuencialmente
void CargarClaves(arbol_par_t * pares, uint8_t cantidad, const arbol_clave_t * claves);

//! Funcion para la apertura o creacion de un archivo en disco
bool AbrirArchivo(const char * nombre, arbol_archivo_t * archivo, bool nuevo);

//! Funcion para la completar la escritura diferida de un archivo en disco
bool EscribirArchivo(arbol_archivo_t archivo);

//! Funcion para el cierre de un archivo en disco
bool CerrarArchivo(arbol_archivo_t archivo);

//! Funcion para el borrado de un archivo en disco
bool BorrarArchivo(const char * nombre);

//! Funcion para la lectura de un sector de disco
bool LeerBloque(arbol_archivo_t archivo, uint8_t indice, arbol_bloque_t bloque);

//! Funcion para la escritura de un sector de disco
bool EscribirBloque(arbol_archivo_t archivo, uint8_t indice, const arbol_bloque_t bloque);

//! Declaracion de la funcion interna para imprimir el arbol durante las pruebas
extern void ArbolImprimir(arbol_t self, bool estructura, bool contenido);

//! Declaracion de la funcion interna para verificar el arbol durante las pruebas
extern bool ArbolVerificar(arbol_t self);

/* === Definiciones de variables internas ====================================================== */

//! Constante con las funciones para la gestion del almacenamiento
static const arbol_funciones_t funciones = {
    .AbrirArchivo = AbrirArchivo,
    .EscribirArchivo = EscribirArchivo,
    .CerrarArchivo = CerrarArchivo,
    .BorrarArchivo = BorrarArchivo,
    .LeerBloque = LeerBloque,
    .EscribirBloque = EscribirBloque,
};

/* === Definiciones de funciones internas ====================================================== */
void GenerarClaves(arbol_par_t * pares, uint16_t cantidad, arbol_clave_t clave, uint16_t paso) {
    uint16_t indice;

    for(indice = 0; indice < cantidad; indice++) {
        pares[indice].clave = clave + paso * indice;
        pares[indice].valor = (uint8_t) indice;
    }
}

void CargarClaves(arbol_par_t * pares, uint8_t cantidad, const arbol_clave_t * claves) {
    uint8_t indice;

    for(indice = 0; indice < cantidad; indice++) {
        pares[indice].clave = claves[indice];
        pares[indice].valor = indice;
    }
}

bool AbrirArchivo(const char * nombre, arbol_archivo_t * archivo, bool nuevo) {
    if (nuevo) {
        *archivo = fopen(nombre, "w+");
    } else {
        *archivo = fopen(nombre, "r+");
    }
    return (*archivo != NULL);
}

bool EscribirArchivo(arbol_archivo_t archivo) {
    return (fflush(archivo) == 0);
}

bool CerrarArchivo(arbol_archivo_t archivo) {
    return (fclose(archivo) == 0);
}

bool BorrarArchivo(const char * nombre) {
    return (remove(nombre) == 0);
}

bool LeerBloque(arbol_archivo_t archivo, uint8_t indice, arbol_bloque_t bloque) {
    fseek(archivo, ESPACIO_BLOQUE * indice , SEEK_SET);
    fread(bloque, ESPACIO_BLOQUE, 1, archivo);
    return true;
}

bool EscribirBloque(arbol_archivo_t archivo, uint8_t indice, const arbol_bloque_t bloque) {
    fseek(archivo, ESPACIO_BLOQUE * indice, SEEK_SET);
    fwrite(bloque, ESPACIO_BLOQUE, 1, archivo);
    return true;
}

bool ListarPar(uint16_t indice, arbol_clave_t clave, arbol_valor_t valor, void * referencia) {
    lista_t lista = referencia;
    bool resultado = (indice < lista->cantidad);
    if (resultado) {
        lista->pares[indice].clave = clave;
        lista->pares[indice].valor = valor;
    }
    return resultado;
}

/* === Definiciones de funciones externas ====================================================== */

//! Funcion que se ejecuta automaticamente al inicio de cada caso de prueba
void setUp(void) {
    remove(NOMBRE_ARCHVO);
}

//! Funcion que se ejecuta automaticamente al final de cada caso de prueba
void tearDown(void) {
    remove(NOMBRE_ARCHVO);
}

//! @test Crear una instancia de un arbol nuevo
void test_crear_nuevo_arbol(void) {
    const arbol_estadisticas_t ESTADISTICAS = {
        .libres = 254, .ocupados = 2, .niveles = 1, .almacenados = 0,
    };
    arbol_t arbol = NULL;

    // Dado un sistema donde no existe previamente un Arbol B
    remove(NOMBRE_ARCHVO);

    // Cuando llamo a la función para crear un nuevo arbol B
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), true);
    
    // Entonces recupero un descriptor valido para operar sobre el arbol creado
    TEST_ASSERT_NOT_NULL(arbol);

    // Y las estadisticas del arbol vacio son las esperadas
    TEST_ASSERT_ARBOL_ESTADISTICAS(arbol, ESTADISTICAS);

}

//! @test Crear una instancia de un arbol que no existe en disco
void test_crear_instancia_de_arbol_inexistente(void) {
    arbol_t arbol = NULL;

    // Dado un sistema donde no existe previamente un Arbol B
    remove(NOMBRE_ARCHVO);
    // Cuando llamo a la función para reabrir un Arbol B preexitente
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), false);
    
    // Entonces not recupero un descriptor valido para operar sobre el arbol creado
    TEST_ASSERT_NULL(arbol);
}

//! @test Listar el contenido de un arbol vacio
void test_listar_contenido_arbol_vacio(void) {
    arbol_t arbol;
    arbol_par_t pares[1];
    int cantidad;

    // Dado un arbol recien creado
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), true);

    // Cuando llamo a la funcion para listar las claves del arbol
    struct lista_s lista = {
        .pares = pares, .cantidad = sizeof(pares) / sizeof(arbol_par_t)
    };
    TEST_ASSERT_TRUE(ArbolListar(arbol, ListarPar, &lista));

    // Entonces el arbol esta vacio
    // TEST_ASSERT_EQUAL(0, cantidad);
}

//! @test Las funciones no operan sobre un arbol invalico
void test_las_funciones_no_operan_sobre_un_arbol_invalido(void) {
    arbol_t arbol;
    arbol_valor_t resultado;

    // Dado un arbol invalido
    arbol = NULL;

    // Cuando quiero intertar una clave obtengo un error
    TEST_ASSERT_FALSE(ArbolInsertar(arbol, 15, 115));
    
    // Cuando quiero buscar una clave obtengo un error
    TEST_ASSERT_FALSE(ArbolBuscar(arbol, 15, &resultado));

    // Y el no hay errores de ejecucion por uso de punteros invalidos
}
//! @test Agregar un dato a un arbol vacio
void test_agregar_un_dato_a_un_arbol_vacio(void) {
    const arbol_par_t CONTENIDO[] = {
        { .clave = 15, .valor = 115 },
    };
    const arbol_estadisticas_t ESTADISTICAS = {
        .libres = 254, .ocupados = 2, .niveles = 1, .almacenados = 1,
    };
    arbol_t arbol;
    arbol_valor_t resultado;

    // Dado un arbol recien creado
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), true);

    // Cuando agrego una pareja clave valor al arbol
    TEST_ASSERT_INSERTAR_VALORES(arbol, CONTENIDO, 0, 1);

    // Entonces puedo recuperar el valor a partir de la clave
    TEST_ASSERT(ArbolBuscar(arbol, 15, &resultado));
    TEST_ASSERT_EQUAL(115, resultado);

    // Y el dato agregado es el unico par almacenado en el arbol
    TEST_ASSERT_EQUAL_ARBOL(arbol, CONTENIDO);

    // Y las estadisticas del arbol vacio son las esperadas
    TEST_ASSERT_ARBOL_ESTADISTICAS(arbol, ESTADISTICAS);
}

//! @test Reabrir una instancia de un Arbol B existente en disco
void test_reabrir_un_arbol_existente_en_disco(void) {
    const arbol_par_t CONTENIDO[] = {
        { .clave = 15, .valor = 115 },
    };
    const arbol_estadisticas_t ESTADISTICAS = {
        .libres = 254, .ocupados = 2, .niveles = 1, .almacenados = 1,
    };
    arbol_t arbol;
    arbol_valor_t resultado;

    // Dado un arbol recien creado
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), true);

    // Cuando agrego una pareja clave valor al arbol
    TEST_ASSERT_INSERTAR_VALORES(arbol, CONTENIDO, 0, 1);

    // Y se vuelve a crear una instancia al arbol existente en disco
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), false);

    // Entonces puedo recuperar el valor a partir de la clave
    TEST_ASSERT(ArbolBuscar(arbol, 15, &resultado));
    TEST_ASSERT_EQUAL(115, resultado);

    // Y el dato agregado es el unico par almacenado en el arbol
    TEST_ASSERT_EQUAL_ARBOL(arbol, CONTENIDO);

    // Y las estadisticas del arbol vacio son las esperadas
    TEST_ASSERT_ARBOL_ESTADISTICAS(arbol, ESTADISTICAS);
}

//! @test Agregar dos datos en orden creciente a un arbol vacio
void test_agregar_dos_datos_en_orden_creciente_a_un_arbol_vacio(void) {
    const arbol_par_t CONTENIDO[] = {
        { .clave = 15, .valor = 115 },
        { .clave = 25, .valor = 125 },
    };
    const arbol_estadisticas_t ESTADISTICAS = {
        .libres = 254, .ocupados = 2, .niveles = 1, .almacenados = 2,
    };
    arbol_t arbol;

    // Dado un arbol recien creado
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), true);

    // Cuando agrego dos pares clave valor con las claves en orden creciente
    TEST_ASSERT_INSERTAR_VALORES(arbol, CONTENIDO, 0, sizeof(CONTENIDO) / sizeof(arbol_par_t));

    // Entonces puedo recuperar todos los valores a partir de las claves insertadas
    TEST_ASSERT_FIND_VALUES(arbol, CONTENIDO);

    // Y el contenido del arbol son las claves y valores insertados en orden creciente
    TEST_ASSERT_EQUAL_ARBOL(arbol, CONTENIDO);

    // Y las estadisticas del arbol vacio son las esperadas
    TEST_ASSERT_ARBOL_ESTADISTICAS(arbol, ESTADISTICAS);
}

//! @test Agregar dos datos en orden decreciente a un arbol vacio
void test_agregar_dos_datos_en_orden_decreciente_a_un_arbol_vacio(void) {
    const arbol_par_t OPERACIONES[] = {
        { .clave = 25, .valor = 125 },
        { .clave = 15, .valor = 115 },
    };
    const arbol_par_t CONTENIDO[] = {
        { .clave = 15, .valor = 115 },
        { .clave = 25, .valor = 125 },
    };
    arbol_t arbol;

    // Dado un arbol recien creado
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), true);

    // Cuando agrego dos pares clave valor con las claves en orden decreciente
    TEST_ASSERT_INSERTAR_VALORES(arbol, OPERACIONES, 0, sizeof(OPERACIONES) / sizeof(arbol_par_t));

    // Entonces puedo recuperar todos los valores a partir de las claves insertadas
    TEST_ASSERT_FIND_VALUES(arbol, OPERACIONES);

    // Y el contenido del arbol son las claves y valores insertados en orden creciente
    TEST_ASSERT_EQUAL_ARBOL(arbol, CONTENIDO);
}

//! @test Agregar agregar datos hasta obtener un arbol de dos niveles
void test_agregar_datos_hasta_obtener_un_arbol_de_dos_niveles(void) {
    const arbol_estadisticas_t ESTADISTICAS = {
        .libres = 252, .ocupados = 4, .niveles = 2, .almacenados = 5,
    };
    const arbol_par_t EJEMPLOS[] = {
        {.clave = 5, .valor = 100},
        {.clave = 15, .valor = 100},
        {.clave = 25, .valor = 100},
        {.clave = 35, .valor = 100},
        {.clave = 45, .valor = 100},
    };
    arbol_par_t contenido[4];
    arbol_valor_t valor;
    arbol_t arbol;
    int ejemplo;
    char mensaje[64];

    for(ejemplo = 0; ejemplo < sizeof(EJEMPLOS) / sizeof(arbol_par_t); ejemplo++) {
        sprintf(mensaje, "Ejemplo %d", ejemplo);

        // Dado un arbol que tiene la raiz completa
        arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
        
        GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 10, 10);
        TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

        // Cuando agrego un nuevo par que provoca la division de la raiz
        TEST_ASSERT_INSERTAR_VALORES(arbol, EJEMPLOS, ejemplo, 1);

        // Entonces puedo recuperar todos los valores al inicio
        TEST_ASSERT_FIND_VALUES_MESSAGE(arbol, contenido, mensaje);
        
        // Y puedo recuperar tambien el valor del ejemplo
        TEST_ASSERT_MESSAGE(ArbolBuscar(arbol, EJEMPLOS[ejemplo].clave, &valor), mensaje);
        TEST_ASSERT_EQUAL_MESSAGE(EJEMPLOS[ejemplo].valor, valor, mensaje);

        // Y el contenido del arbol es el esperado y esta ordenado
        TEST_ASSERT_ARBOL_ORDENADO_MESSAGE(arbol, sizeof(contenido) / sizeof(arbol_par_t) + 1, mensaje);

        // Y las estadisticas del arbol vacio son las esperadas
        TEST_ASSERT_ARBOL_ESTADISTICAS_MESSAGE(arbol, ESTADISTICAS, mensaje);
    }
}

//! @test Provocar la division de una hoja en un arbol de dos niveles
void test_provocar_la_division_de_una_hoja_en_un_arbol_de_dos_niveles(void) {
    const arbol_estadisticas_t ESTADISTICAS[] = {
        {.libres = 252, .ocupados = 4, .niveles = 2, .almacenados = 8},
        {.libres = 251, .ocupados = 5, .niveles = 2, .almacenados = 8},
    };
    const arbol_par_t EJEMPLOS[] = {
        {.clave = 5, .valor = 100},
        {.clave = 15, .valor = 100},
        {.clave = 25, .valor = 100},
        {.clave = 35, .valor = 100},
        {.clave = 75, .valor = 100},
    };
    arbol_par_t contenido[7];
    arbol_valor_t valor;
    arbol_t arbol;
    int ejemplo;
    char mensaje[64];

    for(ejemplo = 0; ejemplo < sizeof(EJEMPLOS) / sizeof(arbol_par_t); ejemplo++) {
        sprintf(mensaje, "Ejemplo %d", ejemplo);

        // Dado un arbol de dos niveles con un nodo saturado
        arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
        
        GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 10, 10);
        TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

        // Cuando agrego un nuevo par que provoca la del nodo saturado
        TEST_ASSERT_INSERTAR_VALORES(arbol, EJEMPLOS, ejemplo, 1);
    
        // Entonces puedo recuperar todos los valores al inicio
        TEST_ASSERT_FIND_VALUES_MESSAGE(arbol, contenido, mensaje);
        
        // Y puedo recuperar tambien el valor del ejemplo
        TEST_ASSERT_MESSAGE(ArbolBuscar(arbol, EJEMPLOS[ejemplo].clave, &valor), mensaje);
        TEST_ASSERT_EQUAL_MESSAGE(EJEMPLOS[ejemplo].valor, valor, mensaje);

        // Y el contenido del arbol es el esperado y esta ordenado
        TEST_ASSERT_ARBOL_ORDENADO_MESSAGE(arbol, 
            sizeof(contenido) / sizeof(arbol_par_t) + 1, mensaje);

        // Y las estadisticas del arbol vacio son las esperadas
        TEST_ASSERT_ARBOL_ESTADISTICAS_MESSAGE(arbol, ESTADISTICAS[ejemplo < 3 ? 0 : 1], mensaje);
    }
}

//! @test Agregar agregar datos hasta obtener un arbol de tres niveles
void test_agregar_datos_hasta_obtener_un_arbol_de_tres_niveles(void) {
    const arbol_estadisticas_t ESTADISTICAS = {
        .libres = 246, .ocupados = 10, .niveles = 3, .almacenados = 25,
    };
    const arbol_clave_t CLAVES[] = {
        100, 110, 140, 150, 160, 190, 200, 210, 240, 250, 260, 290, 300, 310, 320, 330,
        120, 130, 170, 180, 220, 230, 270, 280
    };
    arbol_par_t contenido[24], ejemplo[1];
    arbol_valor_t valor;
    arbol_t arbol;
    int indice;
    char mensaje[64];

    for(indice = 0; indice < sizeof(CLAVES) / sizeof(arbol_clave_t) + 1; indice++) {
        ejemplo->clave = 5 + 10 * indice;
        ejemplo->valor = 100;
        sprintf(mensaje, "Ejemplo %d", ejemplo->clave);

        // Dado un arbol de dos niveles con todos sus nodos completos
        arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
        CargarClaves(contenido, sizeof(CLAVES) / sizeof(arbol_clave_t), CLAVES);        
        TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

        // Cuando agrego un nuevo par que provoca la division de cualquiera de los nodos
        TEST_ASSERT_INSERTAR_VALORES(arbol, ejemplo, 0, 1);

        // Entonces puedo recuperar todos los valores al inicio
        TEST_ASSERT_FIND_VALUES_MESSAGE(arbol, contenido, mensaje);
        
        // Y puedo recuperar tambien el valor del ejemplo
        TEST_ASSERT_MESSAGE(ArbolBuscar(arbol, ejemplo->clave, &valor), mensaje);
        TEST_ASSERT_EQUAL_MESSAGE(ejemplo->valor, valor, mensaje);

        // Y el contenido del arbol es el esperado y esta ordenado
        TEST_ASSERT_ARBOL_ORDENADO_MESSAGE(arbol, 
            sizeof(contenido) / sizeof(arbol_par_t) + 1, mensaje);

        // Y las estadisticas del arbol vacio son las esperadas
        TEST_ASSERT_ARBOL_ESTADISTICAS_MESSAGE(arbol, ESTADISTICAS, mensaje);
    }
}

//! @test Agregar datos hasta llenar el almacenamiento disponible
void test_agregar_datos_hasta_llenar_el_almacenamiento_disponible(void) {
    arbol_par_t contenido[502];
    arbol_t arbol;

    // Dado un arbol al que le quedan libres tantos bloques como niveles
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 1, 1);
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    // Entonces no puedo agregar un nuevo par que provoca la division de cualquiera de los nodos
    TEST_ASSERT_FALSE(ArbolInsertar(arbol, 503, 100));
    // ArbolImprimir(arbol, true, true);

    // Y puedo recuperar todos los valores insertados al inicio
    TEST_ASSERT_FIND_VALUES(arbol, contenido);
}

//! @test Eliminar un dato en un arbol con solo un dato
void test_eliminar_un_dato_en_un_arbol_con_un_solo_elemento(void) {
    const arbol_estadisticas_t ESTADISTICAS = {
        .libres = 254, .ocupados = 2, .niveles = 1, .almacenados = 0,
    };
    arbol_t arbol;

    // Dado un arbol con un solo elemento almacenado
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    TEST_ASSERT(ArbolInsertar(arbol, 10, 100));

    // Cuando elimino la unica claves almacenadas en el arbol
    TEST_ASSERT(ArbolEliminar(arbol, 10));

    // Entonces el arbol mantiene su estructura correctamente
    TEST_ASSERT(ArbolVerificar(arbol));

    // Y las estadisticas del arbol vacio son las esperadas
    TEST_ASSERT_ARBOL_ESTADISTICAS_MESSAGE(arbol, ESTADISTICAS, "");
}

//! @test Eliminar un dato en un arbol con solo la raiz
void test_eliminar_un_dato_en_un_arbol_con_solo_la_raiz(void) {
    const arbol_estadisticas_t ESTADISTICAS = {
        .libres = 254, .ocupados = 2, .niveles = 1, .almacenados = 3,
    };
    arbol_par_t contenido[4];
    uint8_t eliminado[2] = {0};
    arbol_t arbol;
    uint8_t ejemplo;
    char mensaje[64];

   for(ejemplo = 0; ejemplo <  sizeof(contenido) / sizeof(arbol_par_t); ejemplo++) {
        sprintf(mensaje, "Ejemplo %d", ejemplo);

        // Dado un arbol con la raiz completa
        arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
        GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 1, 1);
        TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));
   
        // Cuando elimino una las claves almacenadas en el arbol
        TEST_ASSERT(ArbolEliminar(arbol, contenido[ejemplo].clave));

        // Entonces puedo recuperar todos los valores al inicio excepto la clave eliminada
        eliminado[0] = ejemplo;
        TEST_ASSERT_ARBOL_ELIMINADOS_MESSAGE(arbol, contenido, eliminado, mensaje);
        
        // Y el contenido del arbol esta ordenado
        TEST_ASSERT_ARBOL_ORDENADO_MESSAGE(arbol, 
            sizeof(contenido) / sizeof(arbol_par_t) - 1, mensaje);

        // Y las estadisticas del arbol vacio son las esperadas
        TEST_ASSERT_ARBOL_ESTADISTICAS_MESSAGE(arbol, ESTADISTICAS, mensaje);
    }
}

//! @test Eliminar datos de la raiz en un arbol de dos niveles
void test_borrar_datos_en_la_raiz_de_un_arbol_de_dos_niveles(void) {
    const arbol_par_t CONTENIDO[] = {
        { .clave = 10, .valor = 110 }, { .clave = 20, .valor = 120 }, { .clave = 30, .valor = 130 },
        { .clave = 40, .valor = 140 }, { .clave = 50, .valor = 150 }, { .clave = 60, .valor = 160 },
        { .clave = 70, .valor = 170 }, { .clave = 80, .valor = 180 }, { .clave = 15, .valor = 115 }, 
        { .clave = 25, .valor = 125 }, { .clave = 45, .valor = 145 }, { .clave = 55, .valor = 155 },
        { .clave = 75, .valor = 175 }, { .clave = 85, .valor = 185 },
    };
    const arbol_par_t RESULTADO[] = {
        { .clave = 10, .valor = 110 }, { .clave = 15, .valor = 115 }, { .clave = 20, .valor = 120 },
        { .clave = 25, .valor = 125 }, { .clave = 40, .valor = 140 }, { .clave = 45, .valor = 145 },
        { .clave = 50, .valor = 150 }, { .clave = 55, .valor = 155 }, { .clave = 60, .valor = 160 },
        { .clave = 70, .valor = 170 }, { .clave = 75, .valor = 175 }, { .clave = 80, .valor = 180 },
        { .clave = 85, .valor = 185 },
    };

    arbol_t arbol;
    
    // Dado un arbol recien creado
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);

    // Cuando agrego dos pares clave valor con las claves en orden creciente
    TEST_ASSERT_INSERTAR_VALORES(arbol, CONTENIDO, 0, sizeof(CONTENIDO) / sizeof(arbol_par_t));

    // Cuando elimino una las claves almacenadas en la raiz el arbol
    TEST_ASSERT(ArbolEliminar(arbol, 30));

    // Entonces puedo recuperar todos los valores a partir de las claves insertadas
    TEST_ASSERT_FIND_VALUES(arbol, RESULTADO);

    // Y el contenido del arbol son las claves y valores insertados en orden creciente
    TEST_ASSERT_EQUAL_ARBOL(arbol, RESULTADO);

    TEST_ASSERT(ArbolVerificar(arbol));
}

//! @test Eliminar datos para tomar prestado un elemento del hermano derecho
void test_borrar_datos_para_tomar_prestado_un_elemento_del_hermano_derecho(void) {
    const arbol_clave_t CLAVES[] = {
        10, 20, 30, 40, 50, 60, 70, 80, 45, 55
    };

    arbol_par_t contenido[10];
    arbol_t arbol;

    // Dado un arbol de dos niveles con un nodo con un nodo con el minimo de elementos 
    // y un nodo hermano a la derecha con por lo menos un elemento mas del mínimo
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    CargarClaves(contenido, sizeof(CLAVES) / sizeof(arbol_clave_t), CLAVES);        
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    // Cuando se elimina una clave del nodo con la cantidad minima de elementos
    TEST_ASSERT(ArbolEliminar(arbol, 20));

    // Entonces el nodo debe obtener un elemento de su hermano a la derecha
    TEST_ASSERT(ArbolVerificar(arbol));
}

//! @test Eliminar datos para tomar prestado un elemento del hermano izquierdo
void test_borrar_datos_para_tomar_prestado_un_elemento_del_hermano_izquierdo(void) {
    const arbol_clave_t CLAVES[] = {
        10, 20, 30, 40, 50, 60, 70, 80, 45, 55
    };

    arbol_par_t contenido[10];
    arbol_t arbol;

    // Dado un arbol de dos niveles con un nodo con un nodo con el minimo de elementos 
    // y un nodo hermano a la izquierda con por lo menos un elemento mas del mínimo
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    CargarClaves(contenido, sizeof(CLAVES) / sizeof(arbol_clave_t), CLAVES);        
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    // Cuando se elimina una clave del nodo con la cantidad minima de elementos
    TEST_ASSERT(ArbolEliminar(arbol, 70));

    // Entonces el nodo debe obtener un elemento de su hermano a la izquierda
    TEST_ASSERT(ArbolVerificar(arbol));
}

//! @test Eliminar datos para provocar provocar la fusion con el hermano derecho
void test_borrar_datos_para_provocar_la_fusion_con_el_hermano_derecho(void) {
    const arbol_clave_t CLAVES[] = {
        10, 20, 30, 40, 50, 60, 70, 80,
    };

    arbol_par_t contenido[8];
    arbol_t arbol;

    // Dado un arbol de dos niveles con toos los nodos con el minimo de elementos 
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    CargarClaves(contenido, sizeof(CLAVES) / sizeof(arbol_clave_t), CLAVES);        
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    // Cuando se elimina una clave de un nodo con la cantidad minima de elementos
    TEST_ASSERT(ArbolEliminar(arbol, 20));

    // Entonces el nodo debe debe absorver al hermano derecho
    TEST_ASSERT(ArbolVerificar(arbol));
}

//! @test Eliminar datos para provocar provocar la fusion con el hermano izquierdo
void test_borrar_datos_para_provocar_la_fusion_con_el_hermano_izquierdo(void) {
    const arbol_clave_t CLAVES[] = {
        10, 20, 30, 40, 50, 60, 70, 80,
    };

    arbol_par_t contenido[8];
    arbol_t arbol;

    // Dado un arbol de dos niveles con toos los nodos con el minimo de elementos 
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    CargarClaves(contenido, sizeof(CLAVES) / sizeof(arbol_clave_t), CLAVES);        
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    // Cuando se elimina una clave del ultimo nodo a la derecha
    TEST_ASSERT(ArbolEliminar(arbol, 70));

    // Entonces el nodo debe debe absorver al hermano izquierdo
    TEST_ASSERT(ArbolVerificar(arbol));
}

//! @test Eliminar datos para provocar provocar la reduccion de la altura del arbol eliminado el mayor
void test_borrar_datos_para_reducir_la_altura_del_arbol_por_derecha(void) {
    const arbol_estadisticas_t ESTADISTICAS = {
        .libres = 249, .ocupados = 7, .niveles = 2, .almacenados = 16,
    };

    arbol_par_t contenido[17];
    arbol_t arbol;

    // Dado un arbol de tres niveles con toos los nodos con el minimo de elementos 
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 10, 2);
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    // Cuando se elimina la ultima clave del ultimo nodo a la derecha
    TEST_ASSERT(ArbolEliminar(arbol, 42));

    // Entonces el nodo debe debe absorver al hermano izquierdo
    TEST_ASSERT(ArbolVerificar(arbol));
    // Y las estadisticas del arbol son las esperadas
    TEST_ASSERT_ARBOL_ESTADISTICAS_MESSAGE(arbol, ESTADISTICAS, "");
}

//! @test Eliminar datos para provocar provocar la reduccion de la altura del arbol eliminando el menor
void test_borrar_datos_para_reducir_la_altura_del_arbol_uno_por_izuqierda(void) {
    const arbol_estadisticas_t ESTADISTICAS = {
        .libres = 249, .ocupados = 7, .niveles = 2, .almacenados = 16,
    };

    arbol_par_t contenido[17];
    arbol_t arbol;

    // Dado un arbol de dos niveles con toos los nodos con el minimo de elementos 
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 10, 2);
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));
    
    // Cuando se elimina una primera clave del nodo a la izquierda
    TEST_ASSERT(ArbolEliminar(arbol, 10));

    // Entonces el nodo debe debe absorver al hermano derecho
    TEST_ASSERT(ArbolVerificar(arbol));

    // Y las estadisticas del arbol son las esperadas
    TEST_ASSERT_ARBOL_ESTADISTICAS_MESSAGE(arbol, ESTADISTICAS, "");
}

//! @test Eliminar datos para provocar provocar la combinacion con el hermano derecho en tres niveles
void test_borrar_datos_para_fusionar_con_hermano_derecho_en_tres_niveles(void) {
    arbol_par_t contenido[20];
    arbol_t arbol;

    // Dado un arbol de tres niveles con un nodo intermedio con mas elements que el minimo
    // Y el resto de los nodos con la cantidad mínima de elemntos
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 10, 2);
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    // Cuando se elimina una primera clave del primer nodo de la izquierda
    TEST_ASSERT(ArbolEliminar(arbol, 10));

    // Entonces el nodo debe debe absorver al hermano derecho
    TEST_ASSERT(ArbolVerificar(arbol));
}

//! @test Eliminar datos para provocar provocar la combinacion con el hermano izquierdo en tres niveles
void test_borrar_datos_para_fusionar_con_hermano_izquierdo_en_tres_niveles(void) {
    const arbol_clave_t CLAVES[] = {
        10, 12, 14, 16, 18, 20, 22, 24, 32, 34, 36, 38, 40, 42, 44, 48, 50, 26, 28, 30, 
    };
    arbol_par_t contenido[20];
    arbol_t arbol;

    // Dado un arbol de tres niveles con un nodo intermedio con mas elements que el minimo
    // Y el resto de los nodos con la cantidad mínima de elemntos
    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    CargarClaves(contenido, sizeof(CLAVES) / sizeof(arbol_clave_t), CLAVES);        
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    // Cuando se elimina una primera clave del primer nodo de la izquierda
    TEST_ASSERT(ArbolEliminar(arbol, 50));

    // Entonces el nodo debe debe absorver al hermano derecho
    TEST_ASSERT(ArbolVerificar(arbol));
}
//! @test Eliminar datos por la derecha en un nodo con una cantidad par de elementos en cada nodo
void test_agregar_y_borrar_datos_por_derecha_nodos_cantidad_elementos_par(void) {
    arbol_par_t contenido[53];
    arbol_t arbol;
    char mensaje[64];

    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 10, 2);
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    for(int indice = 0; indice < sizeof(contenido) / sizeof(arbol_par_t); indice++) {
        sprintf(mensaje, "Indice %d", indice);
        TEST_ASSERT(ArbolEliminar(arbol, contenido[indice].clave));
        TEST_ASSERT_MESSAGE(ArbolVerificar(arbol), mensaje);
    }
}

//! @test Eliminar datos por la izquierda en un nodo con una cantidad par de elementos en cada nodo
void test_agregar_y_borrar_datos_por_izquierda_nodos_cantidad_elementos_par(void) {
    arbol_par_t contenido[53];
    arbol_t arbol;
    char mensaje[64];

    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 4);
    GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 10, 2);
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    for(int indice = sizeof(contenido) / sizeof(arbol_par_t); indice > 0 ; indice--) {
        sprintf(mensaje, "Indice %d", indice);
        TEST_ASSERT(ArbolEliminar(arbol, contenido[indice - 1].clave));
        TEST_ASSERT_MESSAGE(ArbolVerificar(arbol), mensaje);
    }
}

//! @test Eliminar datos por la derecha en un nodo con una cantidad impar de elementos en cada nodo
void test_agregar_y_borrar_datos_por_derecha_nodos_cantidad_elementos_impar(void) {
    arbol_par_t contenido[66];
    arbol_t arbol;
    char mensaje[64];

    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 5);
    GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 10, 2);
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    for(int indice = 0; indice < sizeof(contenido) / sizeof(arbol_par_t); indice++) {
        sprintf(mensaje, "Indice %d", indice);
        TEST_ASSERT(ArbolEliminar(arbol, contenido[indice].clave));
        TEST_ASSERT_MESSAGE(ArbolVerificar(arbol), mensaje);
    }
}

//! @test Eliminar datos por la izquierda en un nodo con una cantidad impar de elementos en cada nodo
void test_agregar_y_borrar_datos_por_izquierda_nodos_cantidad_elementos_impar(void) {
    arbol_par_t contenido[66];
    arbol_t arbol;
    char mensaje[64];

    arbol = ArbolCrear(EditorCrear(NOMBRE, &funciones), 5);
    GenerarClaves(contenido, sizeof(contenido) / sizeof(arbol_par_t), 10, 2);
    TEST_ASSERT_INSERTAR_VALORES(arbol, contenido, 0, sizeof(contenido) / sizeof(arbol_par_t));

    for(int indice = sizeof(contenido) / sizeof(arbol_par_t); indice > 0 ; indice--) {
        sprintf(mensaje, "Indice %d", indice);
        TEST_ASSERT(ArbolEliminar(arbol, contenido[indice - 1].clave));
        TEST_ASSERT_MESSAGE(ArbolVerificar(arbol), mensaje);
    }
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

