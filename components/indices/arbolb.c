/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file arbolb.c
 ** @brief Implementación de la clase que proporciona un indice basado en un Arbol B
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.06.30 | evolentini      | Version inicial del archivo de gestion del ArbolB       |
 **|   2 | 2020.01.17 | evolentini      | Soporte completo para eliminacion de datos              |
 **|   3 | 2020.01.20 | evolentini      | Soporte para edicion del Arbol B con transacciones      |
 **
 ** @addtogroup indices
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "arbolb.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>  //!< @todo Eliminar dependcia utilizar para depuracion

/* === Definicion y Macros ===================================================================== */

//! Tamaño de cada elemento que se almacena en el nodo del arbol
#define ESPACIO_ELEMENTO        (uint32_t)(sizeof(elemento_t))

//! Tamaño de la cabecera de cada nodo del arbol
#define ESPACIO_CABECERA_NODO   (3 * (uint32_t) sizeof(uint8_t))

//! Cantidad de elementos que se pueden almacenar en un nodo del arbol
#define ELEMENTOS               (uint8_t)((ESPACIO_BLOQUE - ESPACIO_CABECERA_NODO)/ ESPACIO_ELEMENTO)

//! Espacio ocupado por los elementos de un nodo
#define ESPACIO_ELEMENTOS       (ELEMENTOS * ESPACIO_ELEMENTO)

//! Espacio de relleno en el nodo para completar el tamaño del bloque del almacenamiento
#define RELLENO_NODO            (ESPACIO_BLOQUE - ESPACIO_CABECERA_NODO - ESPACIO_ELEMENTOS)

//! Cantidad total de bloques disponibles en el almacenamiento
#define CANTIDAD_BLOQUES        (256)

//! Espacio utilizado por la mascara para identificar la ocupacion de los bloques del almacenamiento
#define ESPACIO_MASCARA         (CANTIDAD_BLOQUES / 8)

//! Espacio de relleno en la cabecera del arbol para completar el tamaño del bloque del almacenamiento
#define RELLENO_CABECERA        (ESPACIO_BLOQUE - ESPACIO_MASCARA - \
    (12 * (uint32_t) sizeof(uint8_t)) - (1 * (uint32_t) sizeof(uint16_t)))

//! Identificador de numero de bloque invalido
#define BLOQUE_INVALIDO         0x00

//! Muestra informacion de los nodos y las operaciones para facilitar la depuracion
#define INFORMACION_DEPURACION  0

/* == Declaraciones de tipos de datos internos ================================================= */

//! Esctructura de datos para almacenar la información de un elemento del nodo del Arbol B 
struct elemento_s {
    uint8_t referencia;             //!< Nodo que contiene los elmentos menores a la clave
    arbol_clave_t clave;            //!< Clave del elemento almacenado 
    arbol_valor_t valor;            //!< Valor asociado a la clave del elemento almacenado
} __attribute__((packed));

//! Tipo de datos para almacenar un elemento del nodo del Arbol B 
typedef struct elemento_s elemento_t;

//! Esctructura de datos para almacenar la información de un nodo del Arbol B 
struct arbol_nodo_s {
    uint8_t indice;                     //!< Numero de bloque donde esta almacenado 
    uint8_t ocupados;                   //!< Cantidad de elementos ocupados en el nodo
    uint8_t referencia;                 //!< Nodo que contiene los elementos mayores a la ultima clave
    elemento_t elementos[ELEMENTOS];    //!< Elementos almacenados en el nodo
    uint8_t relleno[RELLENO_NODO];      //!< Bytes para completar el tamaño del bloque de disco
} __attribute__((packed));

//! Tipo de datos para almacenar un nodo del nodo del Arbol B 
typedef struct arbol_nodo_s arbol_nodo_t;

//! Esctructura de datos para almacenar la cabecera de un Arbol B 
struct arbol_cabecera_s {
    uint8_t indice;                     //!< Numero de bloque donde esta almacenada 
    uint8_t raiz;                       //!< Indice del bloque con contiene a la raiz del arbol
    uint8_t libres;                     //!< Cantidad de bloques libres en el archivo
    uint8_t ocupados;                   //!< Cantidad de bloques ocupados en el archivo
    uint8_t niveles;                    //!< Cantidad de niveles en el arbol
    uint8_t elementos;                  //!< Cantidad maxima de elementos en cada nodo del arbol
    uint16_t almacenados;               //!< Cantidad de elementos almacenados en el arbol
    uint8_t recorrido[7];               //!< Indice de los nodos recorridos para llegar al nodo actual
    uint8_t profundidad;                //!< Nivel de profundidad del nodo actual
    uint8_t bloques[ESPACIO_MASCARA];   //!< Bits para indicar el estado de cada uno de los bloques
    uint8_t relleno[RELLENO_CABECERA];  //!< Bytes para completar el tamaño del bloque de disco
} __attribute__((packed));

//! Tipo de datos para almacenar la cebecera del nodo del Arbol B 
typedef struct arbol_cabecera_s arbol_cabecera_t;

//! Esctructura de datos para almacenar una instancia de un Arbol B 
struct arbol_s {
    arbol_editor_t editor;                      //!< Descriptor del objeto editor del arbol
    arbol_cabecera_t * cabecera;                //!< Puntero al bloque con la cabecera del arbol
    arbol_nodo_t * nodo;                        //!< Puntero al cache del nodo actual del arbol
    arbol_listar_t procesador;
    void * referencia;
};

/* === Declaraciones de funciones internas ===================================================== */

/**
 * @brief Ocupa un bloque del almacenamiento del Arbol B
 * 
 * @param[in]  self        Puntero al descriptor del Arbol B
 * @return                 Numero del bloque asignado
 */
static uint8_t BloqueOcupar(arbol_t self);

/**
 * @brief Librera un bloque del almacenamiento del Arbol B
 * 
 * @param[in]  self        Puntero al descriptor del Arbol B
 * @param[in]  indice      Numero del bloque desocupado
 */
static void BloqueLiberar(arbol_t self, uint8_t indice);

/**
 * @brief Copia una cantidad de elementos de un nodo a otro
 * 
 * @param[in]  origen      Puntero al nodo que contiene los elementos a copiar
 * @param[in]  inicio      Indice del primer elemento en el nodo origen
 * @param[in]  destino     Puntero al nodo donde se desean copiar los elementos
 * @param[in]  posicion    Indice del primer elemento en el nodo destino
 */
static void ElementosCopiar(arbol_nodo_t * origen, uint8_t inicio, arbol_nodo_t * destino, uint8_t posicion);

/**
 * @brief Inserta un espacio en blanco en los elementos de unodo
 * 
 * @param[in]  nodo        Puntero al nodo que contiene los elementos
 * @param[in]  posicion    Posición en la que se desea insertar el espacio
 */
static void ElementosInsertar(arbol_nodo_t * nodo, uint8_t posicion);

/**
 * @brief Determina la posicion de una referencia en en un nodo determinado
 * 
 * La funcion permite determinar la posicion de una referencia a un nodo entre los elementos almacenados 
 * en otro  nodo. Cuando el elemento es encontrado se retorna el indice del elemento que contiene a
 * la clave buscada. Cuando el elemento no se ecuentra entonces se retorna el valor menos uno.
 * 
 * @param[in]  nodo        Puntero al nodo donde se desea buscar la clave
 * @param[in]  rererencia  Valor de la referencia buscada en el nodo
 * @return                 Valor del indice donde se encuentra la raferecnia o menos uno si no se encuentra
 */
static int ReferenciaBuscar(arbol_nodo_t * nodo, uint8_t referencia);

/**
 * @brief Crea un nuevo nodo en un Arbol B
 * 
 * @param[in]  self        Puntero al descriptor del Arbol B que contiene al nodo
 * @param[in]  nodo        Puntero al espacio de almacenamiento para el nuevo nodo
 */
static void NodoCrear(arbol_t self, arbol_nodo_t * nodo);

/**
 * @brief Determina la posicion de una clave en el nodo actual
 * 
 * La funcion permite determinar la posicion de una clave entre los elementos almacenados en 
 * un nodo. Cuando el elemento es encontrado se retorna el indice del elemento que contiene a
 * la clave buscada. Cuando el elemento no se ecuentra entonces se retorna el indice del elemento
 * que debería contener a la clave buscada para facilitar su eventual inserción.
 * 
 * @param[in]  nodo        Puntero al nodo donde se desea buscar la clave
 * @param[in]  clave       Valor de la clave que se busca entre los elementos del nodo
 * @param[out] resultado   Valor del indice donde se encuentra la clave o donde deberia ubicarse
 * @return                 La clave buscada se encuentra en el elemento con el indice resultado
 * @return                 La clave buscada debería ubicarse en elemento con el indice resultado
 */
static bool NodoBuscar(arbol_nodo_t * nodo, arbol_clave_t clave, uint8_t * resultado);

/**
 * @brief Inserta un elemento en el nodo que esta actualmente cargado en memoria
 * 
 * @param[in]  nodo        Puntero al nodo donde se desea insertar el nuevo elemento
 * @param[in]  clave       Valor de la clave del nuevo elemento que se inserta
 * @param[in]  valor       Valor asociado a la clave del nuevo elemento que se inserta
 * @param[in]  referencia  Referencia al nodo que almacena las claves mayores al nuevo elemento
 */
static void NodoInsertar(arbol_nodo_t * nodo, arbol_clave_t clave, arbol_valor_t valor, uint8_t referencia);

/**
 * @brief Divide el nodo cargado en memoria e inserta un valor en la mitad correspondiente
 * 
 * @param[in]  self        Puntero al descriptor del Arbol B que contiene al nodo a dividir
 * @param[in]  clave       Valor de la clave del nuevo elemento que se inserta
 * @param[in]  valor       Valor asociado a la clave del nuevo elemento que se inserta
 */
static void NodoDividir(arbol_t self, arbol_clave_t clave, arbol_valor_t valor);

/**
 * @brief Divide el nodo cargado en memoria e inserta un valor en la mitad correspondiente
 * 
 * @param[in]  self        Puntero al descriptor del Arbol B que contiene al nodo a dividir
 * @param[in]  clave       Valor de la clave del nuevo elemento que se inserta
 * @param[in]  valor       Valor asociado a la clave del nuevo elemento que se inserta
 */
static void NodoRotar(arbol_t self, uint8_t posicion);

static void NodoCorregir(arbol_t self);

/**
 * @brief Lista recursivamente el contenido de un nodo mediante una funcion externa
 * 
 * @param[in]     self        Puntero al descriptor del Arbol B que contiene al nodo a listar
 * @param[in,out] posicion    Indice del vector donde debe almacenarse el primer resultado
 * @param[in]     indice      Indice del bloque que contiene al nodo a lista
 */
static bool NodoListar(arbol_t self, uint16_t * posicion, uint8_t indice);

/**
 * @brief Imprime recursivamente el contenido de un nodo del Arbol B
 * 
 * @param[in]  self        Puntero al descriptor del Arbol B que contiene al nodo
 * @param[in]  indice      Numero de bloque que contiene al nodo que se quiere imprimir
 * @param[in]  nivel       Nivel de anidamiento en el cual se encuentra el nodo a imprimir
 */
#if (INFORMACION_DEPURACION != 0)
static void NodoImprimir(arbol_t self, uint8_t indice, uint8_t nivel);
#endif

/**
 * @brief Creaa el almacenamiento de nueva instancia de un Arbol B
 * 
 * @return                 Puntero al espacio de memoria reservado para la nueva instancia
 */
static arbol_t CrearInstancia(void);

/**
 * @brief Inicializar la cabecera y la raiz de un Arbol B vacio
 * 
 * @param[in]  self        Puntero al descriptor del Arbol B que se inicializa
 * @param[in]  elementos   Cantidad de elementos en cada nodo del arbol
 */
static void ArbolInicializar(arbol_t self, int elementos);

/**
 * @brief Busca un elemento en todo el Arbol B empezando por la raiz
 * 
 * @param[in]  self        Puntero al Arbol B donde se desea buscar la clave
 * @param[in]  clave       Valor de la clave que se busca entre los elementos del nodo
 * @param[out] posicion    Valor del indice donde se encuentra la clave o donde deberia ubicarse
 * @return                 La clave buscada se encuentra en el elemento con el indice resultado
 * @return                 La clave buscada debería ubicarse en elemento con el indice resultado
*/
static bool BuscarElemento(arbol_t self, arbol_clave_t clave, uint8_t * posicion);

#if (INFORMACION_DEPURACION != 0)
/**
 * @brief Imprime por pantalla el contenido de un Arbol B
 * 
 * @param[in]  self        Puntero al Arbol B donde se desea imprimir
 * @param[in]  estructura  Imprimir la estructura del arbol
 * @param[in]  contenido   Imprimir el contenido del arbol
 */
void ArbolImprimir(arbol_t self, bool estructura, bool contenido);
#endif

/* === Definiciones de variables internas ====================================================== */

/* === Definiciones de funciones internas ====================================================== */

static uint8_t BloqueOcupar(arbol_t self) {
    uint16_t indice;
    uint8_t resultado = BLOQUE_INVALIDO;

    if (self->cabecera->libres > 0) {
        for(indice = 0; indice < CANTIDAD_BLOQUES; indice++) {
            if ((self->cabecera->bloques[indice >> 3] & (1 << (indice & 0x07))) == 0) {
                self->cabecera->bloques[indice >> 3] |= (1 << (indice & 0x07));
                self->cabecera->libres--;
                self->cabecera->ocupados++;
                resultado = indice;
                break;
            }
        }
    }
    return resultado;
}

static void BloqueLiberar(arbol_t self, uint8_t indice) {
    if ((self->cabecera->bloques[indice >> 3] & (1 << (indice & 0x07))) != 0) {
        self->cabecera->bloques[indice >> 3] &= ~(1 << (indice & 0x07));
        self->cabecera->libres++;
        self->cabecera->ocupados--;
    }
}

static void ElementosCopiar(arbol_nodo_t * origen, uint8_t inicio, arbol_nodo_t * destino, uint8_t posicion) {
    memcpy(&destino->elementos[posicion], &origen->elementos[inicio], 
        sizeof(elemento_t) * (origen->ocupados - inicio));
}

static void ElementosInsertar(arbol_nodo_t * nodo, uint8_t posicion) {
    memcpy(&nodo->elementos[posicion + 1], &nodo->elementos[posicion], 
        sizeof(elemento_t) * (nodo->ocupados - posicion));
}

static void ElementosEliminar(arbol_nodo_t * nodo, uint8_t posicion) {
    memcpy(&nodo->elementos[posicion], &nodo->elementos[posicion + 1], 
        sizeof(elemento_t) * (nodo->ocupados - posicion - 1));
}

static int ReferenciaBuscar(arbol_nodo_t * nodo, uint8_t referencia) {
    int resultado = -1;
    int indice;

    for(indice = 0; indice < nodo->ocupados; indice++) {
        if (nodo->elementos[indice].referencia == referencia) {
            resultado = indice;
            break;
        }
    }
    return resultado;
}

static bool NodoSubocupado(arbol_t self, arbol_nodo_t * nodo) {
    return (nodo->ocupados < (self->cabecera->elementos >> 1));
}

static void NodoCrear(arbol_t self, arbol_nodo_t * nodo) {
    //! @todo Eliminar la instruccion, se utilizar para simplificar la depuracion
    memset(nodo, 0, sizeof(arbol_nodo_t));
    nodo->indice = BloqueOcupar(self);
    nodo->ocupados = 0;
    nodo->referencia = BLOQUE_INVALIDO;
}

static bool NodoBuscar(arbol_nodo_t * nodo, arbol_clave_t clave, uint8_t * resultado) {
    bool encontrado = false;
    uint8_t indice;

    *resultado = nodo->ocupados;
    for(indice = 0; indice < nodo->ocupados; indice++) {
        if (nodo->elementos[indice].clave == clave) {
            *resultado = indice;
            encontrado = true;
            break;
        } else if (nodo->elementos[indice].clave > clave) {
            *resultado = indice;
            break;
        }
    }
    return encontrado;
}

static void NodoInsertar(arbol_nodo_t * nodo, arbol_clave_t clave, arbol_valor_t valor, uint8_t referencia) {
    uint8_t posicion;

    NodoBuscar(nodo, clave, &posicion);
    if (posicion < nodo->ocupados) {
        ElementosInsertar(nodo, posicion);
    }
    nodo->ocupados++;
    nodo->elementos[posicion].clave = clave;
    nodo->elementos[posicion].valor = valor;
    nodo->elementos[posicion].referencia = referencia;
}

static void NodoDividir(arbol_t self, arbol_clave_t clave, arbol_valor_t valor) {
    arbol_nodo_t nuevo;
    elemento_t medio;
    uint8_t posicion, izquierdo, derecho, referencia;
    bool repetir = false;

    referencia = BLOQUE_INVALIDO;
    do {    
        posicion = self->cabecera->elementos / 2;
        NodoCrear(self, &nuevo);
        izquierdo = self->nodo->indice;
        derecho = nuevo.indice;

        if (clave < self->nodo->elementos[posicion - 1].clave) {
            memcpy(&medio, &self->nodo->elementos[posicion - 1], sizeof(elemento_t));
            ElementosCopiar(self->nodo, posicion, &nuevo, 0);
            nuevo.ocupados = self->nodo->ocupados - posicion;
            self->nodo->ocupados = posicion - 1;
            NodoInsertar(self->nodo, clave, valor, referencia);
        } else if (clave > self->nodo->elementos[posicion].clave) {
            memcpy(&medio, &self->nodo->elementos[posicion], sizeof(elemento_t));
            ElementosCopiar(self->nodo, posicion + 1, &nuevo, 0);
            nuevo.ocupados = self->nodo->ocupados - posicion - 1;
            self->nodo->ocupados = posicion;
            NodoInsertar(&nuevo, clave, valor, referencia);
        } else {
            medio.clave = clave;
            medio.valor = valor;
            medio.referencia = referencia;
            ElementosCopiar(self->nodo, posicion, &nuevo, 0);
            nuevo.ocupados = self->nodo->ocupados - posicion;
            self->nodo->ocupados = posicion;
        }
        nuevo.referencia = medio.referencia;
        EditorEscribir(self->editor, (arbol_bloque_t) &nuevo);
        EditorEscribir(self->editor, (arbol_bloque_t) self->nodo);

        if (self->nodo->indice == self->cabecera->raiz) {
            NodoCrear(self, self->nodo);
            self->nodo->referencia = izquierdo;
            self->cabecera->raiz = self->nodo->indice;
            self->cabecera->niveles++;
            NodoInsertar(self->nodo, medio.clave, medio.valor, derecho);
            repetir = false;
        } else {
            self->cabecera->profundidad--;
            EditorLeer(self->editor, self->cabecera->recorrido[self->cabecera->profundidad - 1], (arbol_bloque_t) self->nodo);
            if (self->nodo->ocupados < self->cabecera->elementos) {
                NodoInsertar(self->nodo, medio.clave, medio.valor, derecho);
                repetir = false;
            } else {
                clave = medio.clave;
                valor = medio.valor;
                referencia = derecho;
                repetir = true;
            }
        }
    } while (repetir);
}

static bool NodoListar(arbol_t self, uint16_t * posicion, uint8_t indice) {
    bool resultado = true;
    arbol_nodo_t nodo;
    uint16_t contador;

    EditorLeer(self->editor, indice, (arbol_bloque_t) &nodo);
    if (nodo.referencia == BLOQUE_INVALIDO) {
        for(contador = 0; contador < nodo.ocupados; contador++) {
            resultado = self->procesador(*posicion, nodo.elementos[contador].clave, nodo.elementos[contador].valor, self->referencia);
            if (!resultado) break;
            (*posicion)++;
        }
    } else {
        resultado = NodoListar(self, posicion, nodo.referencia);
        if (resultado) {
            for(contador = 0; contador < nodo.ocupados; contador++) {
                resultado = self->procesador(*posicion, nodo.elementos[contador].clave, nodo.elementos[contador].valor, self->referencia);
                if (!resultado) break;
                (*posicion)++;
                resultado = NodoListar(self, posicion, nodo.elementos[contador].referencia);
                if (!resultado) break;
            }
        }
    }
    return resultado;
}

static bool NodoVerificar(arbol_t self, uint8_t indice, arbol_clave_t * menor, arbol_clave_t * mayor) {
    arbol_nodo_t nodo;
    arbol_clave_t izquierdo, medio, derecho;
    uint16_t contador;
    bool resultado = true;

    EditorLeer(self->editor, indice, (arbol_bloque_t) &nodo);
    if ((indice != self->cabecera->raiz) && (NodoSubocupado(self, &nodo))) {
        printf("Nodo %d subocupado\r\n", nodo.indice);
        resultado = false;
    } else if (nodo.referencia == BLOQUE_INVALIDO) {
        for(contador = 1; contador < nodo.ocupados - 1; contador++) {
            if (nodo.elementos[contador].clave >= nodo.elementos[contador + 1].clave) {
                printf("Nodo %d elemento %d desordenado\r\n", nodo.indice, contador);
                resultado = false;
                break;
            }
        }
        *menor = nodo.elementos[0].clave;
        *mayor = nodo.elementos[nodo.ocupados - 1].clave;
    } else {
        resultado = NodoVerificar(self, nodo.referencia, menor, &izquierdo);
        for(contador = 0; contador < nodo.ocupados; contador++) {
            resultado = NodoVerificar(self, nodo.elementos[contador].referencia, &medio, &derecho);
            if (nodo.elementos[contador].clave <= izquierdo) {
                printf("Nodo %d hijo izquierdo %d desordenado\r\n", nodo.indice, contador);
                resultado = false;
                break;                
            } else if (nodo.elementos[contador].clave >= medio) {
                printf("Nodo %d hijo derecho %d desordenado\r\n", nodo.indice, contador);
                resultado = false;
                break;
            }
            izquierdo = derecho;
        }
        *mayor = derecho;
    }
    return resultado;
}

#if (INFORMACION_DEPURACION != 0)
static void NodoContenido(char * prefijo, arbol_nodo_t * nodo) {
    printf("%s in: %d, e: %d, r: %d", prefijo, nodo->indice, nodo->ocupados, nodo->referencia);
    for(int indice = 0; indice < nodo->ocupados; indice++) {
        printf(" (c: %d, v: %d, r: %d)", nodo->elementos[indice].clave, 
            nodo->elementos[indice].valor, nodo->elementos[indice].referencia);
    }
    printf("\r\n");
}

static void NodoImprimir(arbol_t self, uint8_t indice, uint8_t nivel) {
    arbol_nodo_t nodo;
    char identado[9] = "        ";
    
    identado[(2 * nivel) + 1] = 0;
    EditorLeer(self->editor, indice, (arbol_bloque_t) &nodo);
    NodoContenido(identado, &nodo);
    if (nodo.referencia != BLOQUE_INVALIDO) {
        NodoImprimir(self, nodo.referencia, nivel + 1);
        for(indice = 0; indice < nodo.ocupados; indice++) {
            NodoImprimir(self, nodo.elementos[indice].referencia, nivel + 1);
        }
    }
}
#endif

static void NodoRotar(arbol_t self, uint8_t posicion) {
    arbol_nodo_t nodo;

    self->cabecera->recorrido[self->cabecera->profundidad] = self->nodo->elementos[posicion].referencia;
    self->cabecera->profundidad++;
    EditorLeer(self->editor, self->nodo->elementos[posicion].referencia, (arbol_bloque_t) &nodo);
    self->nodo->elementos[posicion].clave = nodo.elementos[0].clave;
    self->nodo->elementos[posicion].valor = nodo.elementos[0].valor;
    EditorEscribir(self->editor, (arbol_bloque_t) self->nodo);
    memcpy(self->nodo, &nodo, sizeof(arbol_nodo_t));
}

static void NodoRebalancear(arbol_t self, int posicion, arbol_nodo_t * padre, arbol_nodo_t * hermano) {
    arbol_nodo_t * nodo = self->nodo;
    
    if (posicion < (nodo->ocupados - 1)) {
        nodo->elementos[nodo->ocupados].clave = padre->elementos[posicion + 1].clave;
        nodo->elementos[nodo->ocupados].valor = padre->elementos[posicion + 1].valor;
        nodo->elementos[nodo->ocupados].referencia = hermano->referencia;
        nodo->ocupados++;
        
        padre->elementos[posicion + 1].clave = hermano->elementos[0].clave;
        padre->elementos[posicion + 1].valor = hermano->elementos[0].valor;
        hermano->referencia = hermano->elementos[0].referencia;
        ElementosEliminar(hermano, 0);
        hermano->ocupados--;
    } else {
        ElementosInsertar(nodo, 0);
        nodo->elementos[0].clave = padre->elementos[posicion].clave;
        nodo->elementos[0].valor = padre->elementos[posicion].valor;
        nodo->elementos[0].referencia = nodo->referencia;
        nodo->ocupados++;

        padre->elementos[posicion].clave = hermano->elementos[hermano->ocupados - 1].clave;
        padre->elementos[posicion].valor = hermano->elementos[hermano->ocupados - 1].valor;
        nodo->referencia = hermano->elementos[hermano->ocupados - 1].referencia;
        hermano->ocupados--;
    }
    EditorEscribir(self->editor, (arbol_bloque_t) hermano);
    EditorEscribir(self->editor, (arbol_bloque_t) nodo);
    // NodoContenido("Hermano: ", hermano);
    // NodoContenido("Nodo: ", nodo);
}

static void NodoFusionar(arbol_t self, int posicion, arbol_nodo_t * padre, arbol_nodo_t * hermano) {
    arbol_nodo_t * nodo = self->nodo;

    if (posicion < (nodo->ocupados - 1)) {
        nodo->elementos[nodo->ocupados].clave = padre->elementos[posicion + 1].clave;
        nodo->elementos[nodo->ocupados].valor = padre->elementos[posicion + 1].valor;
        nodo->elementos[nodo->ocupados].referencia = hermano->referencia;
        nodo->ocupados++;
        ElementosCopiar(hermano, 0, nodo, nodo->ocupados);
        nodo->ocupados += hermano->ocupados;
        BloqueLiberar(self, hermano->indice);
        ElementosEliminar(padre, posicion + 1);
        padre->ocupados--;
        EditorEscribir(self->editor, (arbol_bloque_t) nodo);
        // NodoContenido("Nodo: ", nodo);
    } else {
        ElementosInsertar(nodo, 0);
        hermano->elementos[hermano->ocupados].clave = padre->elementos[posicion].clave;
        hermano->elementos[hermano->ocupados].valor = padre->elementos[posicion].valor;
        hermano->elementos[hermano->ocupados].referencia = nodo->referencia;
        hermano->ocupados++;
        ElementosCopiar(nodo, 0, hermano, hermano->ocupados);
        hermano->ocupados += nodo->ocupados;
        BloqueLiberar(self, nodo->indice);
        ElementosEliminar(padre, posicion);
        padre->ocupados--;
        EditorEscribir(self->editor, (arbol_bloque_t) hermano);
        // NodoContenido("Hermano: ", hermano);
    }
}

static void NodoCorregir(arbol_t self) {
    arbol_nodo_t * nodo = self->nodo;
    arbol_nodo_t * padre = &(arbol_nodo_t) {0};
    arbol_nodo_t * hermano = &(arbol_nodo_t) {0};
    int posicion;
    
    self->cabecera->profundidad--;
    EditorLeer(self->editor, self->cabecera->recorrido[self->cabecera->profundidad - 1], (arbol_bloque_t) padre);
   
    posicion = ReferenciaBuscar(padre, nodo->indice);
    if (posicion < (nodo->ocupados - 1)) {
        EditorLeer(self->editor, padre->elementos[posicion + 1].referencia, (arbol_bloque_t) hermano);
    } else if (posicion > 0) {
        EditorLeer(self->editor, padre->elementos[posicion - 1].referencia, (arbol_bloque_t) hermano);
    } else {
        EditorLeer(self->editor, padre->referencia, (arbol_bloque_t) hermano);
    }
    // printf("Indice %d, hermano %d, posicion en el padre %d\r\n", nodo->indice, hermano->indice, posicion);
    if ((nodo->ocupados + hermano->ocupados) >= self->cabecera->elementos) {
        NodoRebalancear(self, posicion, padre, hermano);
    } else {
        NodoFusionar(self, posicion, padre, hermano);
    }
    // NodoContenido("Padre: ", padre);
    memcpy(nodo, padre, sizeof(arbol_nodo_t));
}

static arbol_t CrearInstancia(void) {
    static arbol_cabecera_t cabecera;
    static arbol_nodo_t nodo;
    static struct arbol_s arbol;

    memset(&cabecera, 0, sizeof(arbol_cabecera_t));
    memset(&nodo, 0, sizeof(arbol_nodo_t));
    memset(&arbol, 0, sizeof(struct arbol_s));
    arbol.cabecera = &cabecera;
    arbol.nodo = &nodo;

    return &arbol;
}

static void ArbolInicializar(arbol_t self, int elementos) {
    memset(self->nodo, 0, sizeof(arbol_nodo_t));
    self->nodo->indice = 1;

    memset(self->cabecera, 0, sizeof(arbol_cabecera_t));
    self->cabecera->ocupados = 2;
    self->cabecera->libres = CANTIDAD_BLOQUES - 2;
    self->cabecera->niveles = 1;
    self->cabecera->raiz = 1;
    self->cabecera->bloques[0] = (1 << 1) | (1 << 0);

    if ((elementos > 3) && (elementos < ELEMENTOS)) {
        self->cabecera->elementos = elementos;
    } else {
        self->cabecera->elementos = ELEMENTOS;
    }
}

static bool BuscarElemento(arbol_t self, arbol_clave_t clave, uint8_t * posicion) {
    bool encontrado;
    uint8_t indice;

    self->cabecera->profundidad = 1;
    self->cabecera->recorrido[0] = self->cabecera->raiz;
    if (self->nodo->indice != self->cabecera->raiz) {
        EditorLeer(self->editor, self->cabecera->raiz, (arbol_bloque_t) self->nodo);
    }

    do {
        encontrado = NodoBuscar(self->nodo, clave, &indice);
        if (encontrado) {
            break;
        } else if (self->nodo->referencia != BLOQUE_INVALIDO) {
            if (indice == self->nodo->ocupados) {
                indice--;
            } 
            if (clave > self->nodo->elementos[indice].clave) {
                self->cabecera->recorrido[self->cabecera->profundidad] = self->nodo->elementos[indice].referencia;
                self->cabecera->profundidad++;
                EditorLeer(self->editor, self->nodo->elementos[indice].referencia, (arbol_bloque_t) self->nodo);
            } else if (indice > 0) {
                self->cabecera->recorrido[self->cabecera->profundidad] = self->nodo->elementos[indice - 1].referencia;
                self->cabecera->profundidad++;
                EditorLeer(self->editor, self->nodo->elementos[indice - 1].referencia, (arbol_bloque_t) self->nodo);
            } else {
                self->cabecera->recorrido[self->cabecera->profundidad] = self->nodo->referencia;
                self->cabecera->profundidad++;
                EditorLeer(self->editor, self->nodo->referencia, (arbol_bloque_t) self->nodo);
            }
        } else {
            break;
        }
    } while (!encontrado);

    *posicion = indice;
    return encontrado;
}

#if (INFORMACION_DEPURACION != 0)
void ArbolImprimir(arbol_t self, bool estructura, bool contenido) {
    if (estructura) {
        printf("Cantidad bloques ocupados en el archivo: %d\r\n", self->cabecera->ocupados);
        printf("Cantidad bloques libres en el archivo: %d\r\n", self->cabecera->libres);
        printf("Cantidad niveles en el arbol: %d\r\n", self->cabecera->niveles);
        printf("Cantidad elementos almacenados en el arbol: %d\r\n", self->cabecera->almacenados);
        printf("Cantidad de bytes ocupados por la cabecera del arbol: %u\r\n\r\n", (uint32_t) sizeof(arbol_cabecera_t));

        printf("Cantidad de bytes por elemento: %u\r\n", ESPACIO_ELEMENTO);
        printf("Cantidad de elementos por nodo: %d\r\n", self->cabecera->elementos);
        printf("Cantidad de bytes en la cabecera del nodo: %u\r\n", ESPACIO_CABECERA_NODO);
        printf("Cantidad de bytes en elementos del nodo: %u\r\n", ESPACIO_ELEMENTOS);
        printf("Cantidad de bytes de relleno en el nodo: %u\r\n", RELLENO_NODO);
        printf("Cantidad de bytes ocupados por cada nodo: %u\r\n\r\n", (uint32_t) sizeof(arbol_nodo_t));
        printf("Indice de nodo raiz actual del arbol: %d\r\n", self->cabecera->raiz);
    }
    if (contenido && self->cabecera->almacenados) {
        NodoImprimir(self, self->cabecera->raiz, 0);
    }
}
#endif

/* === Definiciones de funciones externas ====================================================== */

arbol_t ArbolCrear(const arbol_editor_t editor, int elementos) { 
    arbol_t self = NULL;
    
    self = CrearInstancia();
    self->editor = editor;
    if (EditorAbrir(self->editor, elementos != 0)) {
        if (elementos != 0) {
            ArbolInicializar(self, elementos);
            EditorComenzar(self->editor);
            EditorEscribir(self->editor, (arbol_bloque_t) self->cabecera);
            EditorEscribir(self->editor, (arbol_bloque_t) self->nodo);
            EditorConfirmar(self->editor);
        } else {
            EditorLeer(self->editor, 0, (arbol_bloque_t) self->cabecera);
        }
    } else {
        self = NULL;
    }
    return self;
}

bool ArbolInsertar(arbol_t self, arbol_clave_t clave, arbol_valor_t valor) {
    bool resultado = false;
    uint8_t posicion;

    if (self) {
        resultado = true;
    }
    if (resultado && !BuscarElemento(self, clave, &posicion)) {
        EditorComenzar(self->editor);
        if (self->nodo->ocupados < self->cabecera->elementos) {
            NodoInsertar(self->nodo, clave, valor, BLOQUE_INVALIDO);
        } else if (self->cabecera->libres > self->cabecera->niveles) {
            NodoDividir(self, clave, valor);
        } else {
            resultado = false;
        }
        EditorEscribir(self->editor, (arbol_bloque_t) self->nodo);

        self->cabecera->almacenados++;
        EditorEscribir(self->editor, (arbol_bloque_t) self->cabecera);
        EditorConfirmar(self->editor);
    }
    return resultado;
}

bool ArbolBuscar(arbol_t self, arbol_clave_t clave, arbol_valor_t * valor) {
    bool resultado = false;
    uint8_t posicion;

    if (self) {
        resultado = BuscarElemento(self, clave, &posicion);
    }
    if (resultado && valor) {
        *valor = self->nodo->elementos[posicion].valor;
    }
    return resultado;
}

bool ArbolEliminar(arbol_t self, arbol_clave_t clave) {
    bool resultado = false;
    uint8_t posicion;

    if (self) {
        resultado = BuscarElemento(self, clave, &posicion);
    }
    if (resultado) {
        EditorComenzar(self->editor);
        while (self->nodo->referencia != BLOQUE_INVALIDO) {
            NodoRotar(self, posicion);
            posicion = 0;
        }
        if (posicion < self->nodo->ocupados - 1) {
            ElementosEliminar(self->nodo, posicion);
        }
        self->nodo->ocupados--;

        while ((self->nodo->indice != self->cabecera->raiz) && NodoSubocupado(self, self->nodo)) {
            NodoCorregir(self);
        }
        if (self->nodo->ocupados == 0) {
            self->cabecera->raiz = self->nodo->referencia;
            BloqueLiberar(self, self->nodo->indice);
            self->cabecera->niveles--;
        } else {
            EditorEscribir(self->editor, (arbol_bloque_t) self->nodo);
        }

        self->cabecera->almacenados--;
        if (self->cabecera->almacenados == 0) ArbolInicializar(self, self->cabecera->elementos);
        EditorEscribir(self->editor, (arbol_bloque_t) self->cabecera);
        EditorConfirmar(self->editor);
    }
    return resultado;
}

bool ArbolListar(arbol_t self, arbol_listar_t procesador, void * referencia) {
    uint16_t posicion = 0;
    self->referencia = referencia;
    self->procesador = procesador;

    bool resultado = (self->procesador != NULL);
    if (resultado) resultado = NodoListar(self, &posicion, self->cabecera->raiz);
    return resultado;
}

bool ArbolVerificar(arbol_t self) {
    arbol_clave_t izquierdo, derecho;
    bool resultado = true;

    if (self->cabecera->almacenados > 0) {
        resultado = NodoVerificar(self, self->cabecera->raiz, &izquierdo, &derecho);
        resultado = resultado && (izquierdo <= derecho);
        // printf("izquierdo %d, derecho %d, resultado %d\r\n", izquierdo, derecho, resultado);
    }
    return resultado;
}

void ArbolEstadisticas(arbol_t self, arbol_estadisticas_t * resultado) {
    resultado->libres = self->cabecera->libres;
    resultado->ocupados = self->cabecera->ocupados;
    resultado->niveles = self->cabecera->niveles;
    resultado->almacenados = self->cabecera->almacenados;
}

bool ArbolLimpiar(arbol_t self) {
    bool resultado = (self != NULL);
    if (resultado) resultado = EditorComenzar(self->editor);
    if (resultado) ArbolInicializar(self, self->cabecera->elementos);
    if (resultado) resultado = EditorEscribir(self->editor, (arbol_bloque_t) self->cabecera);
    if (resultado) resultado = EditorEscribir(self->editor, (arbol_bloque_t) self->nodo);
    if (resultado) resultado = EditorConfirmar(self->editor);
    return resultado;
}
/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */
