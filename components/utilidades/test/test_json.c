/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_json.c
 ** @brief Pruebas unitarias de la libreria para gestion de parametros JSON_NESTED
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.02.25 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup utilidades
 ** @{ */


/* === Inclusiones de cabeceras ================================================================ */
#include <string.h>
#include <stdbool.h>
#include <stddef.h>
#include "unity.h"
#include "json.h"

/* === Definicion y Macros ===================================================================== */

/* == Declaraciones de tipos de datos internos ================================================= */

typedef struct estructura_prueba_s {
    int8_t con_signo_corto;
    uint8_t sin_signo_corto;
    int16_t con_signo;
    uint16_t sin_signo;
    int32_t con_signo_largo;
    uint32_t sin_signo_largo;
    bool logico;
    char cadena_corta[8];
    char cadena_larga[16];
} * estructura_prueba_t;

typedef struct estructura_simple_s {
    int entero;
    bool logico;
    char cadena[8];
}  * estructura_simple_t;

typedef struct estructura_anidada_s {
    int entero;
    bool logico;
    char cadena[8];
    struct estructura_simple_s simple;
    struct estructura_prueba_s prueba;
}  * estructura_anidada_t;

/* === Definiciones de variables internas ====================================================== */
static const struct json_entrada_s JSON_ESTRUCTURA_SIMPLE[] = {
    JSON_CAMPO(estructura_simple_s, entero, JSON_SIGNED),
    JSON_CAMPO(estructura_simple_s, logico, JSON_BOOLEAN),
    JSON_CAMPO(estructura_simple_s, cadena, JSON_STRING),
    JSON_FINAL()
};

static const struct json_entrada_s JSON_ESTRUCTURA_PRUEBA[] = {
    {
        .tipo = JSON_SIGNED,
        .etiqueta = "con_signo_corto",
        .desplazamiento = offsetof(struct estructura_prueba_s, con_signo_corto),
        .espacio = sizeof(((estructura_prueba_t)0)->con_signo_corto),
    },{
        .tipo = JSON_UNSIGNED,
        .etiqueta = "sin_signo_corto",
        .desplazamiento = offsetof(struct estructura_prueba_s, sin_signo_corto),
        .espacio = sizeof(((estructura_prueba_t)0)->sin_signo_corto),
    },{
        .tipo = JSON_SIGNED,
        .etiqueta = "con signo",
        .desplazamiento = offsetof(struct estructura_prueba_s, con_signo),
        .espacio = sizeof(((estructura_prueba_t)0)->con_signo),
    },{
        .tipo = JSON_UNSIGNED,
        .etiqueta = "sin signo",
        .desplazamiento = offsetof(struct estructura_prueba_s, sin_signo),
        .espacio = sizeof(((estructura_prueba_t)0)->sin_signo),
    },{
        .tipo = JSON_SIGNED,
        .etiqueta = "ConSignoLargo",
        .desplazamiento = offsetof(struct estructura_prueba_s, con_signo_largo),
        .espacio = sizeof(((estructura_prueba_t)0)->con_signo_largo),
    },{
        .tipo = JSON_UNSIGNED,
        .etiqueta = "SinSignoLargo",
        .desplazamiento = offsetof(struct estructura_prueba_s, sin_signo_largo),
        .espacio = sizeof(((estructura_prueba_t)0)->sin_signo_largo),
    },{
        .tipo = JSON_BOOLEAN,
        .etiqueta = "logico",
        .desplazamiento = offsetof(struct estructura_prueba_s, logico),
        .espacio = sizeof(((estructura_prueba_t)0)->logico),
    },{
        .tipo = JSON_STRING,
        .etiqueta = "cadena_corta",
        .desplazamiento = offsetof(struct estructura_prueba_s, cadena_corta),
        .espacio = sizeof(((estructura_prueba_t)0)->cadena_corta),
    },{
        .tipo = JSON_STRING,
        .etiqueta = "cadena larga",
        .desplazamiento = offsetof(struct estructura_prueba_s, cadena_larga),
        .espacio = sizeof(((estructura_prueba_t)0)->cadena_larga),
    },{
        .tipo = JSON_END_DATA,
    }
};

static const struct json_entrada_s JSON_ESTRUCTURA_ANIDADA[] = {
    JSON_CAMPO(estructura_anidada_s, entero, JSON_SIGNED),
    JSON_CAMPO(estructura_anidada_s, logico, JSON_BOOLEAN),
    JSON_CAMPO(estructura_anidada_s, cadena, JSON_STRING),
    JSON_ANIDADO(estructura_anidada_s, simple, JSON_ESTRUCTURA_SIMPLE),
    JSON_ANIDADO(estructura_anidada_s, prueba, JSON_ESTRUCTURA_PRUEBA),
    JSON_FINAL(),
};

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de funciones internas ====================================================== */

/* === Definiciones de funciones externas ====================================================== */

//! Fija las condiciones iniciales comunes a todas las pruebas
void setUp(void) {
}

//! Limpia los resultados y objetos de todas las pruebas
void tearDown(void) {
}

//! @test Deserializacion de una estrutura simple
void test_deserializar_estructura_simple(void) {
    char EJEMPLO[] = "{"
        "\"entero\": 37345,"
        "\"logico\": false,"
        "\"cadena\": \"prueba\","
    "}";

    struct estructura_simple_s resultado = {
        .entero = 37345,
        .logico = false,
        .cadena = "prueba",
    };
    
    struct estructura_simple_s valores[1] = {0};

    DeserializarJSON(EJEMPLO, valores, JSON_ESTRUCTURA_SIMPLE);
    TEST_ASSERT_EQUAL_INT(resultado.entero, valores->entero);
    TEST_ASSERT_EQUAL(resultado.logico, valores->logico);
    TEST_ASSERT_EQUAL_STRING(resultado.cadena, valores->cadena);
}

//! @test Serializacion de una estrutura simple
void test_serializar_estructura_simple(void) {
    const char RESULTADO[] = "{"
        "\"entero\":37935,"
        "\"logico\":false,"
        "\"cadena\":\"prueba\""
    "}";

    struct estructura_simple_s valores = {
        .entero = 37935,
        .logico = false,
        .cadena = "prueba",
    };
    char cadena[256];

    SerializarJSON(cadena, &valores, JSON_ESTRUCTURA_SIMPLE, sizeof(cadena));
    TEST_ASSERT_EQUAL_STRING(RESULTADO, cadena);
}

void test_comprimir_cadena(void) {
    const char RESULTADO[] = "{"
        "\"entero\":37935,"
        "\"logico\":false,"
        "\"cadena\":\"prueba\""
    "}";

    char cadena[128] = "{\r\n"
    "   \"entero\": 37935,\r\n"
    "   \"logico\" : false,\r\n"
    "   \"cadena\":\"prueba\"\r\n"
    "}";
    
    ComprimirJSON(cadena);
    TEST_ASSERT_EQUAL_STRING(RESULTADO, cadena);
}

void test_serializar_estructura_completa(void) {
    char EJEMPLO[] = "{"
        "\"con_signo_corto\": -120,"
        "\"sin_signo_corto\": 230,"
        "\"con signo\": -13470,"
        "\"sin signo\": 19345,"
        "\"ConSignoLargo\": -1234070,"
        "\"SinSignoLargo\": \"0xFB183FFB\","
        "\"logico\": true,"
        "\"cadena_corta\": \"corta\","
        "\"cadena larga\": \"cadena larga\""
    "}";

    struct estructura_prueba_s resultado = {
        .con_signo_corto = -120,
        .sin_signo_corto = 230,
        .con_signo = -13470,
        .sin_signo = 19345,
        .con_signo_largo = -1234070,
        .sin_signo_largo = 0xFB183FFB,
        .logico = true,
        .cadena_corta = "corta",
        .cadena_larga = "cadena larga",
    };
    
    struct estructura_prueba_s valores[1];

    DeserializarJSON(EJEMPLO, valores, JSON_ESTRUCTURA_PRUEBA);
    TEST_ASSERT_EQUAL_INT8(resultado.con_signo_corto, valores->con_signo_corto);
    TEST_ASSERT_EQUAL_UINT8(resultado.sin_signo_corto, valores->sin_signo_corto);
    TEST_ASSERT_EQUAL_INT16(resultado.con_signo, valores->con_signo);
    TEST_ASSERT_EQUAL_UINT16(resultado.sin_signo, valores->sin_signo);
    TEST_ASSERT_EQUAL_INT32(resultado.con_signo_largo, valores->con_signo_largo);
    TEST_ASSERT_EQUAL_UINT32(resultado.sin_signo_largo, valores->sin_signo_largo);
    TEST_ASSERT_EQUAL(resultado.logico, valores->logico);
    TEST_ASSERT_EQUAL_STRING(resultado.cadena_corta, valores->cadena_corta);
    TEST_ASSERT_EQUAL_STRING(resultado.cadena_larga, valores->cadena_larga);
}

void test_cadena_complicadas(void) {
    char EJEMPLO[] = "{"
        "\"basura\": \"a.b,c:e{f}\","
        "\"cadena_corta\": \"a.b,c:e{f}\","
        "\"cadena larga\": \"\\\"a.b,c:e{f}g[h]\""
    "}";

    struct estructura_prueba_s resultado = {
        .cadena_corta = "a.b,c:e",
        .cadena_larga = "\"a.b,c:e{f}g[h]",
    };    
    struct estructura_prueba_s valores[1];

    DeserializarJSON(EJEMPLO, valores, JSON_ESTRUCTURA_PRUEBA);
    TEST_ASSERT_EQUAL_STRING(resultado.cadena_corta, valores->cadena_corta);
    TEST_ASSERT_EQUAL_STRING(resultado.cadena_larga, valores->cadena_larga);
}

void test_serializar_estructura_anidada(void) {
    char EJEMPLO[] = "{"
        "\"entero\": 379345,"
        "\"logico\": false,"
        "\"cadena\": \"prueba\","
        "\"simple\": {"
            "\"entero\": -67543,"
            "\"logico\": true,"
            "\"cadena\": \"simple\""
        "},"
        "\"prueba\": {"
            "\"con_signo_corto\": -120,"
            "\"sin_signo_corto\": 230,"
            "\"con signo\": -13470,"
            "\"sin signo\": 19345,"
            "\"ConSignoLargo\": -1234070,"
            "\"SinSignoLargo\": \"0xFB183FFB\","
            "\"logico\": true,"
            "\"cadena_corta\": \"corta\","
            "\"cadena larga\": \"cadena larga\""
        "}"
    "}";

    struct estructura_anidada_s resultado = {
        .entero = 379345,
        .logico = false,
        .cadena = "prueba",
        .simple = {
            .entero = -67543,
            .logico = true,
            .cadena = "simple",
        },
        .prueba = {
            .con_signo_corto = -120,
            .sin_signo_corto = 230,
            .con_signo = -13470,
            .sin_signo = 19345,
            .con_signo_largo = -1234070,
            .sin_signo_largo = 0xFB183FFB,
            .logico = true,
            .cadena_corta = "corta",
            .cadena_larga = "cadena larga",
        }
    };
    
    struct estructura_anidada_s valores = {0};
    DeserializarJSON(EJEMPLO, &valores, JSON_ESTRUCTURA_ANIDADA);
    
    TEST_ASSERT_EQUAL_INT(resultado.entero, valores.entero);
    TEST_ASSERT_EQUAL(resultado.logico, valores.logico);
    TEST_ASSERT_EQUAL_STRING(resultado.cadena, valores.cadena);

    TEST_ASSERT_EQUAL_INT(resultado.simple.entero, valores.simple.entero);
    TEST_ASSERT_EQUAL(resultado.simple.logico, valores.simple.logico);
    TEST_ASSERT_EQUAL_STRING(resultado.simple.cadena, valores.simple.cadena);

    TEST_ASSERT_EQUAL_INT8(resultado.prueba.con_signo_corto, valores.prueba.con_signo_corto);
    TEST_ASSERT_EQUAL_UINT8(resultado.prueba.sin_signo_corto, valores.prueba.sin_signo_corto);
    TEST_ASSERT_EQUAL_INT16(resultado.prueba.con_signo, valores.prueba.con_signo);
    TEST_ASSERT_EQUAL_UINT16(resultado.prueba.sin_signo, valores.prueba.sin_signo);
    TEST_ASSERT_EQUAL_INT32(resultado.prueba.con_signo_largo, valores.prueba.con_signo_largo);
    TEST_ASSERT_EQUAL_UINT32(resultado.prueba.sin_signo_largo, valores.prueba.sin_signo_largo);
    TEST_ASSERT_EQUAL(resultado.prueba.logico, valores.prueba.logico);
    TEST_ASSERT_EQUAL_STRING(resultado.prueba.cadena_corta, valores.prueba.cadena_corta);
    TEST_ASSERT_EQUAL_STRING(resultado.prueba.cadena_larga, valores.prueba.cadena_larga);
}

void test_deserializar_estructura_anidada(void) {
    char RESULTADO[] = "{"
        "\"entero\":379345,"
        "\"logico\":false,"
        "\"cadena\":\"prueba\","
        "\"simple\":{"
            "\"entero\":-67543,"
            "\"logico\":true,"
            "\"cadena\":\"simple\""
        "},"
        "\"prueba\":{"
            "\"con_signo_corto\":-120,"
            "\"sin_signo_corto\":230,"
            "\"con signo\":-13470,"
            "\"sin signo\":19345,"
            "\"ConSignoLargo\":-1234070,"
            "\"SinSignoLargo\":4212670459,"
            "\"logico\":true,"
            "\"cadena_corta\":\"corta\","
            "\"cadena larga\":\"cadena larga\""
        "}"
    "}";

    const struct estructura_anidada_s ejemplo = {
        .entero = 379345,
        .logico = false,
        .cadena = "prueba",
        .simple = {
            .entero = -67543,
            .logico = true,
            .cadena = "simple",
        },
        .prueba = {
            .con_signo_corto = -120,
            .sin_signo_corto = 230,
            .con_signo = -13470,
            .sin_signo = 19345,
            .con_signo_largo = -1234070,
            .sin_signo_largo = 0xFB183FFB,
            .logico = true,
            .cadena_corta = "corta",
            .cadena_larga = "cadena larga",
        }
    };
    
    char cadena[512] = {0};

    TEST_ASSERT_TRUE(SerializarJSON(cadena, (void *) &ejemplo, JSON_ESTRUCTURA_ANIDADA, sizeof(cadena)));
    TEST_ASSERT_EQUAL_STRING(RESULTADO, cadena);
}
/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

