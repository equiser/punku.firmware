
/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file json.c
 ** @brief Implementacion de la libreria para gestion de parametros JSON
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.02.25 | evolentini      | Version inicial del archivo                             |
 **|   2 | 2020.02.25 | evolentini      | Control del espacio maximo en la variable de destino    |
 **
 ** @addtogroup utilidades
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "json.h"

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

/* === Definicion y Macros ===================================================================== */

//! Muestra informacion de los nodos y las operaciones para facilitar la depuracion
#define INFORMACION_DEPURACION              0

//! Macro para mostrar informacion de depuracion durante las pruebas
#if defined(TEST) && (INFORMACION_DEPURACION != 0)
    #define Informar(formato, ...)                                      \
        printf("%03d: " formato, __LINE__, ##__VA_ARGS__)
#else
    #define Informar(formato, ...)
#endif /* TEST && REGISTRAR_PASOS */

/* == Declaraciones de tipos de estructuras internos ================================================= */

//! Tipo de datos para almacenar el estado de la maquina que analiza el json
typedef enum estado_e {
    BUSCANDO_ETIQUETA,
    TERMINANDO_ETIQUETA,
    BUSCANDO_CAMPO,
    TERMINANDO_CAMPO,
    COMILLAS_ABIERTAS,
    OBJETO_ABIERTO,
    OBJETO_CERRADO,
    ARREGLO_ABIERTO,
} estado_t;

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de variables internas ====================================================== */

/* === Definiciones de funciones internas ====================================================== */

json_entrada_t BuscarCampo(char * etiqueta, json_entrada_t campos) {
    json_entrada_t resultado = NULL;

    for(json_entrada_t campo = campos; campo->tipo != JSON_END_DATA; campo++) {
        if (strcmp(etiqueta, campo->etiqueta) == 0) {
            resultado = campo;
            break;
        }
    }
    return resultado;
}

void DeserializarConSigno(json_entrada_t campo, char * const valor, void * const estructura) {
    if (campo->espacio == 1) {
        *(int8_t *)(estructura + campo->desplazamiento) = strtol(valor, NULL, 0);
        Informar("campo %s, desplazamiento %d, espacio %d, asignado %s, resultado %d\r\n", 
            campo->etiqueta, campo->desplazamiento, campo->espacio, valor, *(int8_t *)(estructura + campo->desplazamiento));
    } else if (campo->espacio == 2) {
        *(int16_t *)(estructura + campo->desplazamiento) = strtol(valor, NULL, 0);
        Informar("campo %s, desplazamiento %d, espacio %d, asignado %s, resultado %d\r\n", 
            campo->etiqueta, campo->desplazamiento, campo->espacio, valor, *(int16_t *)(estructura + campo->desplazamiento));
    } else {
        *(int32_t *)(estructura + campo->desplazamiento) = strtol(valor, NULL, 0);
        Informar("campo %s, desplazamiento %d, espacio %d, asignado %s, resultado %d\r\n", 
            campo->etiqueta, campo->desplazamiento, campo->espacio, valor, *(int32_t *)(estructura + campo->desplazamiento));
    }
}
void DeserializarSinSigno(json_entrada_t campo, char * const valor, void * const estructura) {
    if (campo->espacio == 1) {
        *(uint8_t *)(estructura + campo->desplazamiento) = strtoul(valor, NULL, 0);
        Informar("campo %s, desplazamiento %d, espacio %d, asignado %s, resultado %d\r\n", 
            campo->etiqueta, campo->desplazamiento, campo->espacio, valor, *(uint8_t *)(estructura + campo->desplazamiento));
    } else if (campo->espacio == 2) {
        *(uint16_t *)(estructura + campo->desplazamiento) = strtoul(valor, NULL, 0);
        Informar("campo %s, desplazamiento %d, espacio %d, asignado %s, resultado %d\r\n", 
            campo->etiqueta, campo->desplazamiento, campo->espacio, valor, *(uint16_t *)(estructura + campo->desplazamiento));
    } else {
        *(uint32_t *)(estructura + campo->desplazamiento) = strtoul(valor, NULL, 0);
        Informar("campo %s, desplazamiento %d, espacio %d, asignado %s, resultado %d\r\n", 
            campo->etiqueta, campo->desplazamiento, campo->espacio, valor, *(uint32_t *)(estructura + campo->desplazamiento));
    }
}

void DeserializarCadena(json_entrada_t campo, char * const valor, void * const estructura) {
    char * origen, * destino;
    for(origen = destino = valor; *destino != 0; origen++, destino++) {
        if (*origen == '\"') destino--;
        if (origen != destino) *destino = *origen;
    }

    memset(estructura + campo->desplazamiento, 0, campo->espacio);
    strncpy(estructura + campo->desplazamiento, valor, campo->espacio - 1);
    Informar("campo %s, desplazamiento %d, espacio %d, asignado %s, resultado %s\r\n", 
        campo->etiqueta, campo->desplazamiento, campo->espacio, valor, (estructura + campo->desplazamiento));
}

void DeserializarLogico(json_entrada_t campo, char * const valor, void * const estructura) {
    if (strcmp("true", valor) == 0) {
        *(bool *)(estructura + campo->desplazamiento) = true;
    } else {
        *(bool *)(estructura + campo->desplazamiento) = false;
    }
    Informar("campo %s, desplazamiento %d, espacio %d, asignado %s, resultado %d\r\n", 
        campo->etiqueta, campo->desplazamiento, campo->espacio, valor, *(bool *)(estructura + campo->desplazamiento));
}

void SerializarConSigno(json_entrada_t campo, char * const cadena, void const * const estructura) {
    if (campo->espacio == 1) {
        sprintf(cadena, "\"%s\":%d,", campo->etiqueta, *(int8_t *) (estructura + campo->desplazamiento));
    } else if (campo->espacio == 2) {
        sprintf(cadena, "\"%s\":%d,", campo->etiqueta, *(int16_t *) (estructura + campo->desplazamiento));
    } else {
        sprintf(cadena, "\"%s\":%d,", campo->etiqueta, *(int32_t *) (estructura + campo->desplazamiento));
    }
}

void SerializarSinSigno(json_entrada_t campo, char * const cadena, void const * const estructura) {
    if (campo->espacio == 1) {
        sprintf(cadena, "\"%s\":%u,", campo->etiqueta, *(uint8_t *) (estructura + campo->desplazamiento));
    } else if (campo->espacio == 2) {
        sprintf(cadena, "\"%s\":%u,", campo->etiqueta, *(uint16_t *) (estructura + campo->desplazamiento));
    } else {
        sprintf(cadena, "\"%s\":%u,", campo->etiqueta, *(uint32_t *) (estructura + campo->desplazamiento));
    }
}

void SerializarCadena(json_entrada_t campo, char * const cadena, void const * const estructura) {
    // *(char *)(estructura + campo->desplazamiento + campo->espacio) = 0;
    sprintf(cadena, "\"%s\":\"%s\",", campo->etiqueta, (char *) (estructura + campo->desplazamiento));
}

void SerializarLogico(json_entrada_t campo, char * const cadena, void const * const estructura) {
    if (*(bool *)(estructura + campo->desplazamiento)) {
        sprintf(cadena, "\"%s\":true,", campo->etiqueta);
    } else {
        sprintf(cadena, "\"%s\":false,", campo->etiqueta);
    }
}

void CopiarValor(json_entrada_t campo, char * const valor, void * const estructura) {
    switch (campo->tipo) {
        case JSON_SIGNED:
            DeserializarConSigno(campo, valor, estructura);
        break;
        case JSON_UNSIGNED:
            DeserializarSinSigno(campo, valor, estructura);
        break;
        case JSON_STRING:
            DeserializarCadena(campo, valor, estructura);
        break;
        case JSON_BOOLEAN:
            DeserializarLogico(campo, valor, estructura);
        break;
        case JSON_END_DATA:
        break;
        case JSON_NESTED:
        break;
    }
}

/* === Definiciones de funciones externas ====================================================== */

void DeserializarJSON(char * cadena, void * estructura, const json_entrada_t campos) {
    json_entrada_t campo;
    int indice, etiqueta, valor;
    int longitud = strlen(cadena);
    int llaves = 0;
    estado_t estado = BUSCANDO_ETIQUETA;

    for(indice = 0; indice < longitud; indice++) {
        // printf("caracter %c, estado inicial %d, llaves inicial %d", cadena[indice], estado, llaves);
        switch (estado) {
            case BUSCANDO_ETIQUETA:
                if (cadena[indice] == '\"') {
                    etiqueta = indice + 1;
                    estado = TERMINANDO_ETIQUETA;
                }
            break;
            case TERMINANDO_ETIQUETA:
                if (cadena[indice] == '\"') {
                    cadena[indice] = 0;
                    estado = BUSCANDO_CAMPO;
                }
            break;
            case BUSCANDO_CAMPO:
                if (cadena[indice] == '\"') {
                    valor = indice + 1;
                    estado = COMILLAS_ABIERTAS;
                } else if (cadena[indice] == '{') {
                    valor = indice;
                    llaves = 0;
                    estado = OBJETO_ABIERTO;
                // } else if (cadena[indice] == '[') {
                //     valor = indice + 1;
                //     estado = ARREGLO_ABIERTO;
                } else if ((cadena[indice] != ':') && (cadena[indice] != ' ')) {
                    valor = indice;
                    estado = TERMINANDO_CAMPO;
                }
            break;
            case TERMINANDO_CAMPO:
                if ((cadena[indice] == ',') || (cadena[indice] == '}')) {
                    cadena[indice] = 0;
                    if (valor) {
                        campo = BuscarCampo(&cadena[etiqueta], campos);
                        if (campo) CopiarValor(campo, &cadena[valor], estructura);
                    } 
                    etiqueta = 0;
                    valor = 0;
                    estado = BUSCANDO_ETIQUETA;
                }
            break;
            case COMILLAS_ABIERTAS:
                if ((cadena[indice] == '\"') && (cadena[indice - 1] != '\\')){
                    cadena[indice] = 0;
                    estado = TERMINANDO_CAMPO;
                }
            break;
            case OBJETO_ABIERTO:
                if (cadena[indice] == '{') {
                    llaves++;
                } else if (cadena[indice] == '}') {
                    if (llaves == 0) {
                        estado = OBJETO_CERRADO;
                    } else {
                        llaves--;
                    }
                };
            break;
            case OBJETO_CERRADO:
                if ((cadena[indice] == ',') || (cadena[indice] == '}')) {
                    cadena[indice] = 0;
                    if (valor) {
                        campo = BuscarCampo(&cadena[etiqueta], campos);
                        if (campo) {
                            if (campo->tipo == JSON_NESTED) {
                                DeserializarJSON(&cadena[valor], estructura + campo->desplazamiento, campo->campos);
                            }
                        }
                    } 
                    etiqueta = 0;
                    valor = 0;
                    estado = BUSCANDO_ETIQUETA;
                }
            break;
            case ARREGLO_ABIERTO:
                if (cadena[indice] == ']') {
                    cadena[indice] = 0;
                    etiqueta = 0;
                    valor = 0;
                    estado = BUSCANDO_ETIQUETA;
                }
            break;
        }
        // printf(", estado final, llaves final %d %d\r\n", estado, llaves);
    }
}

bool SerializarJSON(char * cadena, void const * estructura, const json_entrada_t campos, int espacio) {
    int posicion = 0;
    bool resultado = true;

    cadena[posicion] = '{';
    posicion++;
    for(json_entrada_t campo = campos; (campo->tipo != JSON_END_DATA) && (resultado); campo++) {
        // sprintf(&cadena[posicion], "\"%s\":", campo->etiqueta);
        switch (campo->tipo) {
            case JSON_SIGNED:
                if (posicion + 4 + strlen(campo->etiqueta) + 3 * campo->espacio < espacio) {
                    SerializarConSigno(campo, &cadena[posicion], estructura);
                } else {
                    resultado = false;
                }
            break;
            case JSON_UNSIGNED:
                if (posicion + 4 + strlen(campo->etiqueta) + 3 * campo->espacio < espacio) {
                    SerializarSinSigno(campo, &cadena[posicion], estructura);
                } else {
                    resultado = false;
                }
            break;
            case JSON_STRING:
                if (posicion + 4 + strlen(campo->etiqueta) + 2 + campo->espacio < espacio) {
                    SerializarCadena(campo, &cadena[posicion], estructura);
                } else {
                    resultado = false;
                }
                break;
            case JSON_BOOLEAN:
                if (posicion + 4 + strlen(campo->etiqueta) + 5) {
                    SerializarLogico(campo, &cadena[posicion], estructura);
                } else {
                    resultado = false;
                }
                break;
            break;
            case JSON_END_DATA:
            break;
            case JSON_NESTED:
                if (posicion + 4 + strlen(campo->etiqueta)) {
                    sprintf(&cadena[posicion], "\"%s\":", campo->etiqueta);
                    posicion += strlen(&cadena[posicion]);
                    resultado = SerializarJSON(cadena + posicion, estructura + campo->desplazamiento, campo->campos, espacio - posicion);
                    posicion += strlen(&cadena[posicion]);
                    sprintf(&cadena[posicion], ",");
                } else {
                    resultado = false;
                }
            break;
        }
        posicion += strlen(&cadena[posicion]);
    }
    cadena[posicion - 1] = '}';
    cadena[posicion] = 0;

    // printf("%s\r\n", cadena);
    return resultado;
}

void ComprimirJSON(char * cadena) {
    for(char const * origen = cadena; *origen; origen++) {
        *cadena = *origen;
        if ((*origen != ' ') && (*origen != '\r') && (*origen != '\n')) cadena++;
    };
    *cadena = 0;
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

