/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef JSON_H   /*! @cond    */
#define JSON_H   /*! @endcond */

/** @file json.h
 ** @brief Declaraciones de la libreria para gestion de parametros JSON
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.04.03 | evolentini      | Version inicial del archivo                             |
 **|   2 | 2020.02.25 | evolentini      | Control del espacio maximo en la variable de destino    |
 **
 ** @addtogroup utilidades
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/**
 * @brief Macro para generar la definicion de un campo json a partir de un campo de estructura
 * 
 * @param   estructura_     Nombre de la estructura que contiene el campo a declarar
 * @param   campo_          Nombre del campo en la estructura que se desea declarar
 * @param   tipo_           Tipo de datos al que contiene el campo a declarar
 */
#define JSON_CAMPO(estructura_, campo_, tipo_) {                \
    .tipo = tipo_,                                              \
    .etiqueta = #campo_,                                        \
    .desplazamiento = offsetof(struct estructura_, campo_),     \
    .espacio = sizeof(((struct estructura_ *)0)->campo_),       \
}

/**
 * @brief Macro para generar la definicion de un campo json anidado a partir de un campo de estructura
 * 
 * @param   estructura_     Nombre de la estructura que contiene el campo a declarar
 * @param   campo_          Nombre del campo en la estructura que se desea declarar
 * @param   descriptor_     Nombre de la estructura con la descripcion de JSON del campo
 */
#define JSON_ANIDADO(estructura_, campo_, descriptor_) {        \
    .tipo = JSON_NESTED,                                        \
    .campos = descriptor_,                                      \
    .etiqueta = #campo_,                                        \
    .desplazamiento = offsetof(struct estructura_, campo_),     \
    .espacio = sizeof(((struct estructura_ *)0)->campo_),       \
}

//! Macro para terminar la definicion de una lista de campos json
#define JSON_FINAL() {                                          \
    .tipo = JSON_END_DATA,                                      \
}                                                               \

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipos de datos almacenados por los campos de la estructura a convertir
typedef enum json_tipos_e {
    JSON_END_DATA = 0,                  //!< Marca el final de la definicion de campos
    JSON_STRING,                      //!< El campo contiene una cadena de caracteres
    JSON_BOOLEAN,                     //!< El campo contiene un valor logico
    JSON_SIGNED,           //!< El campo contiene un numero entero con signo
    JSON_UNSIGNED,           //!< El campo contiene un numero entero sin signo
    JSON_NESTED,                       //!< El campo una estructura anidada o un objeto json
} json_tipos_t;

//! Tipo de datos para almacenar una referencia a una estructura de campos json
typedef struct json_entrada_s const * json_entrada_t;

//! Estructura para describir la conversion de un campo json en un campo de estructura c
struct json_entrada_s {
    json_tipos_t tipo;          //!< Tipo de datos almacenado en el campo de la estructura
    json_entrada_t campos;      //!< Referencia a la definicion de campos en un json anidado
    const char * etiqueta;      //!< Cadena de texto con la etiqueta esperada en el json
    uint8_t desplazamiento;     //!< Desplazamiento del campo dentro de la estructura c
    uint8_t espacio;            //!< Cantidad de bytes reservados para el campo en la estructura c
};

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Funcion para deserializar una cadena con un objeto JSON_NESTED en una estructura C
 * 
 * @param   cadena          Cadena de caracteres que contiene el objeto JSON_NESTED
 * @param   estructura      Puntero a la estructura de datos que recibe los valores del JSON_NESTED
 * @param   campos          Puntero a la lista de los dscriptores de los campos JSON_NESTED
 */
void DeserializarJSON(char * cadena, void * estructura, const json_entrada_t campos);

/**
 * @brief Funcion para serializar una escturura C en una cadena con un objeto JSON_NESTED 
 * 
 * @param   cadena          Cadena de caracteres que recibe el objeto JSON_NESTED
 * @param   estructura      Puntero a la estructura de datos que provee los valores del JSON_NESTED
 * @param   campos          Puntero a la lista de los dscriptores de los campos JSON_NESTED
 * @param   espacio         Espacio diponible en la cadena que recibe el objeto JSON_NESTED
 */
bool SerializarJSON(char * cadena, void const * estructura, const json_entrada_t campos, int espacio);

/**
 * @brief Funcion para comprimir una cadena JSON_NESTED eliminando espacios y retornos de carro
 * 
 * @param   cadena          Cadena de caracteres que contiene el objeto JSON_NESTED
 */
void ComprimirJSON(char * cadena);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* JSON_H */
