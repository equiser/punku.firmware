/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file pcd.c
 ** @brief Implementacion de la libreria para la comunicacion con las tarjetas RFID ISO 14443A
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2015.12.28 | evolentini      | Version inicial del archivo portado desde arduino       |
 **|   2 | 2019.08.08 | evolentini      | Reorganizacion del codigo en dos capas                  |
 **
 ** \addtogroup mifare
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "pcd.h"

/* === Definicion y Macros ===================================================================== */

/* == Declaraciones de tipos de datos internos ================================================= */


/* === Definiciones de variables internas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de funciones internas ====================================================== */

/* === Definiciones de funciones externas ====================================================== */

const char * GetStatusCodeName(StatusCode code) {
    switch (code) {
        case STATUS_OK:                 return "Success.";
        case STATUS_ERROR:              return "Error in communication.";
        case STATUS_COLLISION:          return "Collission detected.";
        case STATUS_TIMEOUT:            return "Timeout in communication.";
        case STATUS_NO_ROOM:            return "A buffer is not big enough.";
        case STATUS_INTERNAL_ERROR:     return "Internal error in the code. Should not happen.";
        case STATUS_INVALID:            return "Invalid argument.";
        case STATUS_CRC_WRONG:          return "The CRC_A does not match.";
        case STATUS_MIFARE_NACK:        return "A MIFARE PICC responded with NAK.";
        default:                        return "Unknown error";
    }
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

