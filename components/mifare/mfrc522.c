/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file mifare.h
 ** @brief Implementacion del contolador para el integrado de lectura RFID MFRC-522 de NXP 
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2015.12.28 | evolentini      | Version inicial del archivo portado desde arduino       |
 **|   2 | 2019.08.08 | evolentini      | Reorganizacion del codigo en dos capas                  |
 **
 ** \addtogroup mifare
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "mfrc522.h"
#include "driver/spi_master.h"

#include <stdio.h>
#include <string.h>

/* === Definicion y Macros ===================================================================== */

/* == Declaraciones de tipos de datos internos ================================================= */

//! Tipo de datos para mantener la configuracion del chip MFRC522
typedef struct {
    //! Enlace de comunicacion SPI con el chip
    spi_dispositivo_t enlace;
    //! Pin connected to MFRC522's reset and power down input
    digital_salida_t reset;
} pcd_t;

/* === Definiciones de variables internas ====================================================== */

//! Size of the MFRC522 FIFO
static const uint8_t FIFO_SIZE = 64;       //!< The FIFO is 64 uint8_ts.

//! Library configuration values
static pcd_t * self = &(pcd_t) { .enlace = NULL, .reset = NULL };

/*
 Firmware data for self-test
 Reference values based on firmware version
 Hint: if needed, you can remove unused self-test data to save flash memory
 */
#ifdef MFRC522_SELF_TEST
    /**
     @brief Data for self-test Firmware Version 0.0 (0x90)
    Philips Semiconductors; Preliminary Specification Revision 2.0 - 01 August 2005; 16.1 Sefttest
    */
    static const uint8_t MFRC522_firmware_referenceV0_0[] = {
        0x00, 0x87, 0x98, 0x0f, 0x49, 0xFF, 0x07, 0x19,
        0xBF, 0x22, 0x30, 0x49, 0x59, 0x63, 0xAD, 0xCA,
        0x7F, 0xE3, 0x4E, 0x03, 0x5C, 0x4E, 0x49, 0x50,
        0x47, 0x9A, 0x37, 0x61, 0xE7, 0xE2, 0xC6, 0x2E,
        0x75, 0x5A, 0xED, 0x04, 0x3D, 0x02, 0x4B, 0x78,
        0x32, 0xFF, 0x58, 0x3B, 0x7C, 0xE9, 0x00, 0x94,
        0xB4, 0x4A, 0x59, 0x5B, 0xFD, 0xC9, 0x29, 0xDF,
        0x35, 0x96, 0x98, 0x9E, 0x4F, 0x30, 0x32, 0x8D
    };

    /**
     @brief Data for self-test Firmware Version 1.0 (0x91)
    NXP Semiconductors; Rev. 3.8 - 17 September 2014; 16.1.1 Self test
    */
    static const uint8_t MFRC522_firmware_referenceV1_0[] = {
        0x00, 0xC6, 0x37, 0xD5, 0x32, 0xB7, 0x57, 0x5C,
        0xC2, 0xD8, 0x7C, 0x4D, 0xD9, 0x70, 0xC7, 0x73,
        0x10, 0xE6, 0xD2, 0xAA, 0x5E, 0xA1, 0x3E, 0x5A,
        0x14, 0xAF, 0x30, 0x61, 0xC9, 0x70, 0xDB, 0x2E,
        0x64, 0x22, 0x72, 0xB5, 0xBD, 0x65, 0xF4, 0xEC,
        0x22, 0xBC, 0xD3, 0x72, 0x35, 0xCD, 0xAA, 0x41,
        0x1F, 0xA7, 0xF3, 0x53, 0x14, 0xDE, 0x7E, 0x02,
        0xD9, 0x0F, 0xB5, 0x5E, 0x25, 0x1D, 0x29, 0x79
    };

    /**
     @brief Data for self-test Firmware Version 2.0 (0x92)
    NXP Semiconductors; Rev. 3.8 - 17 September 2014; 16.1.1 Self test
    */
    static const uint8_t MFRC522_firmware_referenceV2_0[] = {
        0x00, 0xEB, 0x66, 0xBA, 0x57, 0xBF, 0x23, 0x95,
        0xD0, 0xE3, 0x0D, 0x3D, 0x27, 0x89, 0x5C, 0xDE,
        0x9D, 0x3B, 0xA7, 0x00, 0x21, 0x5B, 0x89, 0x82,
        0x51, 0x3A, 0xEB, 0x02, 0x0C, 0xA5, 0x00, 0x49,
        0x7C, 0x84, 0x4D, 0xB3, 0xCC, 0xD2, 0x1B, 0x81,
        0x5D, 0x48, 0x76, 0xD5, 0x71, 0x61, 0x21, 0xA9,
        0x86, 0x96, 0x83, 0x38, 0xCF, 0x9D, 0x5B, 0x6D,
        0xDC, 0x15, 0xBA, 0x3E, 0x7D, 0x95, 0x3B, 0x2F
    };

    /**
     @brief Data for self-test Firmware Clone
    Fudan Semiconductor FM17522 (0x88)
    */
    static const uint8_t FM17522_firmware_reference[] = {
        0x00, 0xD6, 0x78, 0x8C, 0xE2, 0xAA, 0x0C, 0x18,
        0x2A, 0xB8, 0x7A, 0x7F, 0xD3, 0x6A, 0xCF, 0x0B,
        0xB1, 0x37, 0x63, 0x4B, 0x69, 0xAE, 0x91, 0xC7,
        0xC3, 0x97, 0xAE, 0x77, 0xF4, 0x37, 0xD7, 0x9B,
        0x7C, 0xF5, 0x3C, 0x11, 0x8F, 0x15, 0xC3, 0xD7,
        0xC1, 0x5B, 0x00, 0x2A, 0xD0, 0x75, 0xDE, 0x9E,
        0x51, 0x64, 0xAB, 0x3E, 0xE9, 0x15, 0xB5, 0xAB,
        0x56, 0x9A, 0x98, 0x82, 0x26, 0xEA, 0x2A, 0x62
    };
#endif

/* === Declaraciones de funciones internas ===================================================== */

//! Writes a uint8_t to the specified register in the MFRC522 chip.
/*!
 The interface is described in the datasheet section 8.1.2.
 @param[in] reg The register to write to.
 @param[in] value The value to write.
 */
static void PCD_WriteRegister(const PCD_Register reg, const uint8_t value);

//! Writes a number of uint8_ts to the specified register in the MFRC522 chip.
/*!
 The interface is described in the datasheet section 8.1.2.
 @param[in] reg The register to write to.
 @param[in] count The number of uint8_ts to write to the register.
 @param[in] values uint8_t array with the values to write.
 */
static void PCD_WriteString(const PCD_Register reg, const uint8_t count, const uint8_t *values);

//! Reads a uint8_t from the specified register in the MFRC522 chip.
/*!
 The interface is described in the datasheet section 8.1.2.
 @param[in] reg The register to read from.
 */
static uint8_t PCD_ReadRegister(const PCD_Register reg);

//! Reads a number of uint8_ts from the specified register in the MFRC522 chip.
/*!
 The interface is described in the datasheet section 8.1.2.
 @param[in] reg The register to read from.
 @param[in] count The number of uint8_ts to read from the register.
 @param[in] values uint8_t array to store the values in.
 @param[in] rxAlign	Only bit positions rxAlign..7 in values[0] are updated.
 */
static void PCD_ReadString(const PCD_Register reg, uint8_t count, uint8_t *values,
                           const uint8_t rxAlign);

//! Sets the bits given in mask in register reg.
/*!
 @param[in] reg The register to update.
 @param[in] mask The bits to set.
 */
static void PCD_SetRegisterBitMask(const PCD_Register reg, const uint8_t mask);

//! Clear the bits given in mask in register reg.
/*!
 @param[in] reg The register to update.
 @param[in] mask The bits to clear.
 */
static void PCD_ClearRegisterBitMask(const PCD_Register reg, const uint8_t mask);

static void delay(uint32_t value);
/* === Definiciones de funciones internas ====================================================== */

static void PCD_WriteRegister(const PCD_Register reg, const uint8_t value) {
    uint8_t datos[2];

    // MSB == 0 is for writing. LSB is not used in address. Datasheet section 8.1.2.3.
    datos[0] = reg & 0x7E;
    datos[1] = value;
   
    SpiTransferir(self->enlace, datos, 2);
}

static void PCD_WriteString(const PCD_Register reg, const uint8_t count, const uint8_t * values) {
    uint8_t datos[FIFO_SIZE + 1];

    if ((count == 0) || (count > FIFO_SIZE)) {
        return;
    }

    // MSB == 0 is for writing. LSB is not used in address. Datasheet section 8.1.2.3.
    datos[0] = reg & 0x7E;
    memcpy(&datos[1], values, count);

    SpiTransferir(self->enlace, datos, count + 1);
}

static uint8_t PCD_ReadRegister(const PCD_Register reg) {
    uint8_t datos[2];

    // MSB == 1 is for reading. LSB is not used in address. Datasheet section 8.1.2.3.
    datos[0] = reg | 0x80;
    // Read the value back. Send 0 to stop reading.
    datos[1] = 0;

    SpiTransferir(self->enlace, datos, 2);
    return datos[1];
}

static void PCD_ReadString(const PCD_Register reg, uint8_t count, uint8_t *values, const uint8_t rxAlign) {

    if ((count == 0) || (count > FIFO_SIZE)) {
        return;
    }

#ifdef DEBUG_SERIAL_PCD
    printf("Reading %d bytes from register.\r\n", count);
#endif
    uint8_t trama[FIFO_SIZE + 1];
    
    // MSB == 1 is for reading. LSB is not used in address. Datasheet section 8.1.2.3.
    memset(trama, reg | 0x80, count);
    // Read the last value back. Send 0 to stop reading.
    trama[count + 1] = 0;

    SpiTransferir(self->enlace, trama, count + 1);

    if (rxAlign) {
        // Only update bit positions rxAlign..7 in values[0]
        // Create bit mask for bit positions rxAlign..7
        uint8_t mask = 0;
        uint8_t i;
        for (i = rxAlign; i <= 7; i++) {
            mask |= (1 << i);
        }
        values[0] = (values[0] & ~mask) | (trama[1] & mask);
        memcpy(&values[1], &trama[2], count - 1);
    } else {
        memcpy(&values[0], &trama[1], count);
    }
}

static void PCD_SetRegisterBitMask(const PCD_Register reg, const uint8_t mask) {
    uint8_t tmp;

    tmp = PCD_ReadRegister(reg);
    // set bit mask
    PCD_WriteRegister(reg, tmp | mask);
}

static void PCD_ClearRegisterBitMask(const PCD_Register reg, const uint8_t mask) {
    uint8_t tmp;

    tmp = PCD_ReadRegister(reg);
    // clear bit mask
    PCD_WriteRegister(reg, tmp & (~mask));
}

static void delay(uint32_t value) {
    uint32_t index;
    uint32_t delay;

    for(index = 0; index < value; index++) {
        for(delay = 0; delay < 0x8000; delay++) {
        }
    }
}

/* === Definiciones de funciones externas ====================================================== */

StatusCode PCD_CalculateCRC(const uint8_t *data, const uint8_t length, uint8_t *result) {
    // Stop any active command.
    PCD_WriteRegister(CommandReg, PCD_Idle);
    // Clear the CRCIRq interrupt request bit
    PCD_WriteRegister(DivIrqReg, 0x04);
    // FlushBuffer = 1, FIFO initialization
    PCD_SetRegisterBitMask(FIFOLevelReg, 0x80);
    // Write data to the FIFO
    PCD_WriteString(FIFODataReg, length, data);
    // Start the calculation
    PCD_WriteRegister(CommandReg, PCD_CalcCRC);

    // Wait for the CRC calculation to complete. Each iteration of the while-loop takes 17.73us.
    uint16_t i = 5000;
    uint8_t n;
    while (1) {
        // DivIrqReg[7..0] bits: Set2 reserved reserved MfinActIRq reserved CRCIRq reserved reserved
        n = PCD_ReadRegister(DivIrqReg);
        // CRCIRq bit set - calculation done
        if (n & 0x04) {
            break;
        }
        // The emergency break. We will eventually terminate on this one after 89ms.
        // Communication with the MFRC522 might be down.
        if (--i == 0) {
            return STATUS_TIMEOUT;
        }
    }
    // Stop calculating CRC for new content in the FIFO.
    PCD_WriteRegister(CommandReg, PCD_Idle);

    // Transfer the result from the registers to the result buffer
    result[0] = PCD_ReadRegister(CRCResultRegL);
    result[1] = PCD_ReadRegister(CRCResultRegH);
    return STATUS_OK;
}

void PCD_Init(const spi_dispositivo_t enlace, const digital_salida_t reset) {
    // Set the port and pins configuration
    self->enlace = enlace;
    self->reset = reset;

    SalidaApagar(self->reset);
    delay(150);
    SalidaPrender(self->reset);
    delay(150);

    /*
     When communicating with a PICC we need a timeout if something goes wrong.
     f_timer = 13.56 MHz / (2*TPreScaler+1) where TPreScaler = [TPrescaler_Hi:TPrescaler_Lo].
     TPrescaler_Hi are the four low bits in TModeReg. TPrescaler_Lo is TPrescalerReg.
    */

    // TAuto=1; timer starts automatically at the end of the transmission
    // in all communication modes at all speeds
    PCD_WriteRegister(TModeReg, 0x80);

    // TPreScaler = TModeReg[3..0]:TPrescalerReg, ie 0x0A9 = 169 => f_timer=40kHz,
    // ie a timer period of 25us.
    PCD_WriteRegister(TPrescalerReg, 0xA9);

    // Reload timer with 0x3E8 = 1000, ie 25ms before timeout.
    PCD_WriteRegister(TReloadRegH, 0x03);
    PCD_WriteRegister(TReloadRegL, 0xE8);

    // Default 0x00. Force a 100 % ASK modulation independent of the ModGsPReg register setting
    PCD_WriteRegister(TxASKReg, 0x40);

    // Default 0x3F. Set the preset value for the CRC coprocessor
    // for the CalcCRC command to 0x6363 (ISO 14443-3 part 6.2.4)
    PCD_WriteRegister(ModeReg, 0x3D);

    // Enable the antenna driver pins TX1 and TX2 (they were disabled by the reset)
    PCD_AntennaOn();
}

/* --- Functions for manipulating the MFRC522 -------------------------------------------------- */

void PCD_Reset(void) {
    // Issue the SoftReset command.
    PCD_WriteRegister(CommandReg, PCD_SoftReset);
    // The datasheet does not mention how long the SoftRest command takes to complete.
    // But the MFRC522 might have been in soft power-down mode (triggered by bit 4 of CommandReg)
    // Section 8.8.2 in the datasheet says the oscillator start-up time is the start up time
    // of the crystal + 37,74us. Let us be generous: 50ms.

    // delay(50);
    // Wait for the PowerDown bit in CommandReg to be cleared
    while (PCD_ReadRegister(CommandReg) & (1<<4)) {
        // PCD still restarting - unlikely after waiting 50ms, but better safe than sorry.
    }
}

void PCD_AntennaOn(void) {
    uint8_t value = PCD_ReadRegister(TxControlReg);
    if ((value & 0x03) != 0x03) {
        PCD_WriteRegister(TxControlReg, value | 0x03);
    }
    value = PCD_ReadRegister(TxControlReg);
}

void PCD_AntennaOff(void) {
    PCD_ClearRegisterBitMask(TxControlReg, 0x03);
}

PCD_RxGain PCD_GetAntennaGain(void ) {
    return PCD_ReadRegister(RFCfgReg) & (0x07<<4);
}

void PCD_SetAntennaGain(const PCD_RxGain value) {
    // only bother if there is a change
    if (PCD_GetAntennaGain() != value) {
        // clear needed to allow 000 pattern
        PCD_ClearRegisterBitMask(RFCfgReg, (0x07<<4));
        // only set RxGain[2:0] bits
        PCD_SetRegisterBitMask(RFCfgReg, ((uint8_t) value) & (0x07<<4));
    }
}

#ifdef MFRC522_SELF_TEST
bool PCD_PerformSelfTest(void) {
    // This follows directly the steps outlined in 16.1.1
    // 1. Perform a soft reset.
    PCD_Reset();

    // 2. Clear the internal buffer by writing 25 uint8_ts of 00h
    uint8_t ZEROES[25] = {0x00};
    // flush the FIFO buffer
    PCD_SetRegisterBitMask(FIFOLevelReg, 0x80);
    // write 25 uint8_ts of 00h to FIFO
    PCD_WriteString(FIFODataReg, 25, ZEROES);
    // transfer to internal buffer
    PCD_WriteRegister(CommandReg, PCD_Mem);

    // 3. Enable self-test
    PCD_WriteRegister(AutoTestReg, 0x09);

    // 4. Write 00h to FIFO buffer
    PCD_WriteRegister(FIFODataReg, 0x00);

    // 5. Start self-test by issuing the CalcCRC command
    PCD_WriteRegister(CommandReg, PCD_CalcCRC);

    // 6. Wait for self-test to complete
    uint16_t i;
    uint8_t n;
    for (i = 0; i < 0xFF; i++) {
        // DivIrqReg[7..0] bits: Set2 reserved reserved MfinActIRq reserved CRCIRq reserved reserved
        n = PCD_ReadRegister(DivIrqReg);
        // CRCIRq bit set - calculation done
        if (n & 0x04) {
            break;
        }
    }
    // Stop calculating CRC for new content in the FIFO.
    PCD_WriteRegister(CommandReg, PCD_Idle);

    // 7. Read out resulting 64 uint8_ts from the FIFO buffer.
    uint8_t result[64];
    PCD_ReadString(FIFODataReg, 64, result, 0);

    // Auto self-test done
    // Reset AutoTestReg register to be 0 again. Required for normal operation.
    PCD_WriteRegister(AutoTestReg, 0x00);

    // Determine firmware version (see section 9.3.4.8 in spec)
    uint8_t version = PCD_ReadRegister(VersionReg);

    // Pick the appropriate reference values
    const uint8_t *reference;
    switch (version) {
            // Fudan Semiconductor FM17522 clone
        case 0x88:
            reference = FM17522_firmware_reference;
            break;
            // Version 0.0
        case 0x90:
            reference = MFRC522_firmware_referenceV0_0;
            break;
            // Version 1.0
        case 0x91:
            reference = MFRC522_firmware_referenceV1_0;
            break;
            // Version 2.0
        case 0x92:
            reference = MFRC522_firmware_referenceV2_0;
            break;
            // Unknown version
        default:
            return false;
    }

    // Verify that the results match up to our expectations
    for (i = 0; i < 64; i++) {
        if (result[i] != (reference[i])) {
            return false;
        }
    }

    // Test passed; all is good.
    return true;
}
#endif

/* --- Functions for communicating with PICCs -------------------------------------------------- */

StatusCode PCD_CommunicateWithPICC(const PCD_Command command, const uint8_t waitIRq,
                                   const uint8_t *sendData, const uint8_t sendLen,
                                   uint8_t *backData, uint8_t *backLen, uint8_t *validBits,
                                   const uint8_t rxAlign, const bool checkCRC) {
    uint8_t n, _validBits = 0;
    unsigned int i;

    // Prepare values for BitFramingReg
    uint8_t txLastBits = validBits ? *validBits : 0;
    // RxAlign = BitFramingReg[6..4]. TxLastBits = BitFramingReg[2..0]
    uint8_t bitFraming = (rxAlign << 4) + txLastBits;

    // Stop any active command.
    PCD_WriteRegister(CommandReg, PCD_Idle);
    // Clear all seven interrupt request bits
    PCD_WriteRegister(ComIrqReg, 0x7F);
    // FlushBuffer = 1, FIFO initialization
    PCD_SetRegisterBitMask(FIFOLevelReg, 0x80);
    // Write sendData to the FIFO
    PCD_WriteString(FIFODataReg, sendLen, sendData);
    // Bit adjustments
    PCD_WriteRegister(BitFramingReg, bitFraming);
    // Execute the command
    PCD_WriteRegister(CommandReg, command);
    if (command == PCD_Transceive) {
        // StartSend=1, transmission of data starts
        PCD_SetRegisterBitMask(BitFramingReg, 0x80);
    }

    // Wait for the command to complete.
    // In PCD_Init() we set the TAuto flag in TModeReg. This means the timer automatically starts
    // when the PCD stops transmitting. Each iteration of the do-while-loop takes 17.86us.
    i = 2000;
    while (1) {
        // ComIrqReg[7..0] bits are: Set1 TxIRq RxIRq IdleIRq HiAlertIRq LoAlertIRq ErrIRq TimerIRq
        n = PCD_ReadRegister(ComIrqReg);
        // One of the interrupts that signal success has been set.
        if (n & waitIRq) {
            break;
        }
        // Timer interrupt - nothing received in 25ms
        if (n & 0x01) {
            return STATUS_TIMEOUT;
        }
        // The emergency break. If all other condions fail we will eventually terminate on this one
        // after 35.7ms. Communication with the MFRC522 might be down.
        if (--i == 0) {
            return STATUS_TIMEOUT;
        }
    }

    // Stop now if any errors except collisions were detected.
    // ErrorReg[7..0] bits: WrErr TempErr reserved BufferOvfl CollErr CRCErr ParityErr ProtocolErr
    uint8_t errorRegValue = PCD_ReadRegister(ErrorReg);
    // BufferOvfl ParityErr ProtocolErr
    if (errorRegValue & 0x13) {
        return STATUS_ERROR;
    }

    // If the caller wants data back, get it from the MFRC522.
    if (backData && backLen) {
        // Number of uint8_ts in the FIFO
        n = PCD_ReadRegister(FIFOLevelReg);
        if (n > *backLen) {
            return STATUS_NO_ROOM;
        }
        // Number of uint8_ts returned
        *backLen = n;
        // Get received data from FIFO
        PCD_ReadString(FIFODataReg, n, backData, rxAlign);
        // RxLastBits[2:0] indicates the number of valid bits in the last received uint8_t.
        // If this value is 000b, the whole uint8_t is valid.
        _validBits = PCD_ReadRegister(ControlReg) & 0x07;
        if (validBits) {
            *validBits = _validBits;
        }
    }

    // Tell about collisions
    if (errorRegValue & 0x08) {
        // CollErr
        return STATUS_COLLISION;
    }

    // Perform CRC_A validation if requested.
    if (backData && backLen && checkCRC) {
        // In this case a MIFARE Classic NAK is not OK.
        if (*backLen == 1 && _validBits == 4) {
            return STATUS_MIFARE_NACK;
        }
        // We need at least the CRC_A value and all 8 bits of the last uint8_t must be received.
        if (*backLen < 2 || _validBits != 0) {
            return STATUS_CRC_WRONG;
        }
        // Verify CRC_A - do our own calculation and store the control in controlBuffer.
        uint8_t controlBuffer[2];
        StatusCode status = PCD_CalculateCRC(&backData[0], *backLen - 2, &controlBuffer[0]);
        if (status != STATUS_OK) {
            return status;
        }
        if ((backData[*backLen - 2] != controlBuffer[0])
            || (backData[*backLen - 1] != controlBuffer[1])) {
            return STATUS_CRC_WRONG;
        }
    }

    return STATUS_OK;
}

StatusCode PCD_TransceiveData(const uint8_t *sendData, const uint8_t sendLen,uint8_t *backData,
                              uint8_t *backLen, uint8_t *validBits, const uint8_t rxAlign,
                              const bool checkCRC) {
    // RxIRq and IdleIRq
    uint8_t waitIRq = 0x30;
    return PCD_CommunicateWithPICC(PCD_Transceive, waitIRq, sendData, sendLen, backData, backLen,
                                   validBits, rxAlign, checkCRC);
}

/* --- Functions for communicating with MIFARE PICCs ------------------------------------------- */

StatusCode PCD_Authenticate(const PICC_Command command, const uint8_t blockAddr,
                            const MIFARE_Key key, const PICC_UID_TYPE *uid) {
    // IdleIRq
    uint8_t waitIRq = 0x10;
    uint8_t i;
    
    // Build command buffer
    uint8_t sendData[12];
    sendData[0] = command;
    sendData[1] = blockAddr;
    // 6 key uint8_ts
    for (i = 0; i < MF_KEY_SIZE; i++) {
        sendData[2+i] = key[i];
    }
    // The first 4 uint8_ts of the UID
    for (i = 0; i < 4; i++) {
        sendData[8+i] = uid->data[i];
    }

    // Start the authentication.
    return PCD_CommunicateWithPICC(PCD_MFAuthent, waitIRq, &sendData[0], sizeof(sendData),
                                   NULL, NULL, NULL, 0, false);
}

void PCD_StopCrypto1(void) {
    // Clear MFCrypto1On bit
    // Status2Reg[7..0] bits: TempSensClear I2CForceHS reserved reserved MFCrypto1On ModemState[2:0]
    PCD_ClearRegisterBitMask(Status2Reg, 0x08);
}


StatusCode PICC_REQA_or_WUPA(const PICC_Command command, uint8_t *bufferATQA, uint8_t *bufferSize) {
    uint8_t validBits;
    StatusCode status;

    // The ATQA response is 2 uint8_ts long.
    if (bufferATQA == NULL || *bufferSize < 2) {
        return STATUS_NO_ROOM;
    }
    // ValuesAfterColl=1 => Bits received after collision are cleared.
    PCD_ClearRegisterBitMask(CollReg, 0x80);
    // For REQA and WUPA we need the short frame format - transmit only 7 bits of the last
    // (and only) uint8_t. TxLastBits = BitFramingReg[2..0]
    validBits = 7;
    status = PCD_TransceiveData((uint8_t *) &command, 1, bufferATQA, bufferSize, &validBits, 0, false);
    if (status != STATUS_OK) {
        return status;
    }
    if (*bufferSize != 2 || validBits != 0) {
        // ATQA must be exactly 16 bits.
        return STATUS_ERROR;
    }
    return STATUS_OK;
}

StatusCode PICC_RequestA(uint8_t *bufferATQA, uint8_t *bufferSize) {
    return PICC_REQA_or_WUPA(PICC_CMD_REQA, bufferATQA, bufferSize);
}

StatusCode PICC_WakeupA(uint8_t *bufferATQA, uint8_t *bufferSize) {
    return PICC_REQA_or_WUPA(PICC_CMD_WUPA, bufferATQA, bufferSize);
}

StatusCode PICC_Select(PICC_UID_TYPE *uid, uint8_t validBits) {

    bool uidComplete;
    bool selectDone;
    bool useCascadeTag;
    uint8_t cascadeLevel = 1;
    StatusCode result;
    uint8_t count;
    uint8_t index;
    // The first index in uid->data[] that is used in the current Cascade Level.
    uint8_t uidIndex;
    // The number of known UID bits in the current Cascade Level.
    int8_t currentLevelKnownBits;
    // The SELECT/ANTICOLLISION commands uses a 7 uint8_t standard frame + 2 uint8_ts CRC_A
    uint8_t buffer[9];
    // The number of uint8_ts used in the buffer, ie the number of uint8_ts to transfer to the FIFO.
    uint8_t bufferUsed;
    // Used in BitFramingReg. Defines the bit position for the first bit received.
    uint8_t rxAlign;
    // Used in BitFramingReg. The number of valid bits in the last transmitted uint8_t.
    uint8_t txLastBits;
    uint8_t *responseBuffer;
    uint8_t responseLength;

    /*
     Description of buffer structure:
     uint8_t 0: SEL                 Indicates the Cascade Level: PICC_CMD_SEL_CL1,
     PICC_CMD_SEL_CL2 or PICC_CMD_SEL_CL3
     uint8_t 1: NVB                 Number of Valid Bits (in complete command, not just the UID):
     High nibble: complete uint8_ts, Low nibble: Extra bits.
     uint8_t 2: UID-data or CT      See explanation below. CT means Cascade Tag.
     uint8_t 3: UID-data
     uint8_t 4: UID-data
     uint8_t 5: UID-data
     uint8_t 6: BCC                 Block Check Character - XOR of uint8_ts 2-5
     uint8_t 7: CRC_A
     uint8_t 8: CRC_A
     The BCC and CRC_A is only transmitted if we know all the UID bits of the current Cascade Level.

     Description of uint8_ts 2-5: (Section 6.5.4 of the ISO/IEC 14443-3 draft:
     UID contents and cascade levels)
     UID size    Cascade level    uint8_t2    uint8_t3    uint8_t4    uint8_t5
     ========    =============    =====    =====    =====    =====
     4 uint8_ts        1            uid0      uid1    uid2    uid3
     7 uint8_ts        1              CT      uid0    uid1    uid2
     2            uid3      uid4    uid5    uid6
     10 uint8_ts        1              CT      uid0    uid1    uid2
     2              CT      uid3    uid4    uid5
     3            uid6      uid7    uid8    uid9
     */

    // Sanity checks
    if (validBits > 80) {
        return STATUS_INVALID;
    }

    // Prepare MFRC522
    // ValuesAfterColl=1 => Bits received after collision are cleared.
    PCD_ClearRegisterBitMask(CollReg, 0x80);

    // Repeat Cascade Level loop until we have a complete UID.
    uidComplete = false;
    while (!uidComplete) {
        // Set the Cascade Level in the SEL uint8_t, find out if we need to use the Cascade Tag in uint8_t 2.
        switch (cascadeLevel) {
            case 1:
                buffer[0] = PICC_CMD_SEL_CL1;
                uidIndex = 0;
                // When we know that the UID has more than 4 uint8_ts
                useCascadeTag = validBits && uid->size > 4;
                break;

            case 2:
                buffer[0] = PICC_CMD_SEL_CL2;
                uidIndex = 3;
                // When we know that the UID has more than 7 uint8_ts
                useCascadeTag = validBits && uid->size > 7;
                break;

            case 3:
                buffer[0] = PICC_CMD_SEL_CL3;
                uidIndex = 6;
                // Never used in CL3.
                useCascadeTag = false;
                break;

            default:
                return STATUS_INTERNAL_ERROR;
                break;
        }

        // How many UID bits are known in this Cascade Level?
        currentLevelKnownBits = validBits - (8 * uidIndex);
        if (currentLevelKnownBits < 0) {
            currentLevelKnownBits = 0;
        }
        // Copy the known bits from uid->data[] to buffer[]
        index = 2; // destination index in buffer[]
        if (useCascadeTag) {
            buffer[index++] = PICC_CMD_CT;
        }
        // The number of uint8_ts needed to represent the known bits for this level.
        uint8_t uint8_tsToCopy = currentLevelKnownBits / 8 + (currentLevelKnownBits % 8 ? 1 : 0);
        if (uint8_tsToCopy) {
            // Max 4 uint8_ts in each Cascade Level. Only 3 left if we use the Cascade Tag
            uint8_t maxuint8_ts = useCascadeTag ? 3 : 4;
            if (uint8_tsToCopy > maxuint8_ts) {
                uint8_tsToCopy = maxuint8_ts;
            }
            for (count = 0; count < uint8_tsToCopy; count++) {
                buffer[index++] = uid->data[uidIndex + count];
            }
        }
        // Now that the data has been copied we need to include
        // the 8 bits in CT in currentLevelKnownBits
        if (useCascadeTag) {
            currentLevelKnownBits += 8;
        }

        // Repeat anti collision loop until we can transmit all UID bits + BCC
        // and receive a SAK - max 32 iterations.
        selectDone = false;
        while (!selectDone) {
            // Find out how many bits and uint8_ts to send and receive.
            // All UID bits in this Cascade Level are known. This is a SELECT.
            if (currentLevelKnownBits >= 32) {
#ifdef DEBUG_SERIAL_PCD
                printf("SELECT: currentLevelKnownBits=%d\r\n", currentLevelKnownBits);
#endif
                // NVB - Number of Valid Bits: Seven whole uint8_ts
                buffer[1] = 0x70;
                // Calculate BCC - Block Check Character
                buffer[6] = buffer[2] ^ buffer[3] ^ buffer[4] ^ buffer[5];
                // Calculate CRC_A
                result = PCD_CalculateCRC(buffer, 7, &buffer[7]);
                if (result != STATUS_OK) {
                    return result;
                }
                // 0 => All 8 bits are valid.
                txLastBits        = 0;
                bufferUsed        = 9;
                // Store response in last 3 uint8_ts of buffer (BCC and CRC_A, not needed after tx)
                responseBuffer    = &buffer[6];
                responseLength    = 3;
            } else {
                // This is an ANTICOLLISION.
#ifdef DEBUG_SERIAL_PCD
                printf("ANTICOLLISION: currentLevelKnownBits=%d\r\n", currentLevelKnownBits);
#endif
                txLastBits        = currentLevelKnownBits % 8;
                count            = currentLevelKnownBits / 8;
                // Number of whole uint8_ts in the UID part.
                index            = 2 + count;
                // Number of whole uint8_ts: SEL + NVB + UIDs
                buffer[1]        = (index << 4) + txLastBits;
                // NVB - Number of Valid Bits
                bufferUsed        = index + (txLastBits ? 1 : 0);
                // Store response in the unused part of buffer
                responseBuffer    = &buffer[index];
                responseLength    = sizeof(buffer) - index;
            }

            // Set bit adjustments
            rxAlign = txLastBits;
            // Having a seperate variable is overkill. But it makes the next line easier to read.
            // RxAlign = BitFramingReg[6..4]. TxLastBits = BitFramingReg[2..0]
            PCD_WriteRegister(BitFramingReg, (rxAlign << 4) + txLastBits);

            // Transmit the buffer and receive the response.
            result = PCD_TransceiveData(buffer, bufferUsed, responseBuffer, &responseLength,
                                        &txLastBits, rxAlign, false);
            // More than one PICC in the field => collision.
            if (result == STATUS_COLLISION) {
                // CollReg[7..0] bits are: ValuesAfterColl reserved CollPosNotValid CollPos[4:0]
                uint8_t valueOfCollReg = PCD_ReadRegister(CollReg);
                // CollPosNotValid
                if (valueOfCollReg & 0x20) {
                    // Without a valid collision position we cannot continue
                    return STATUS_COLLISION;
                }
                // Values 0-31, 0 means bit 32.
                uint8_t collisionPos = result & 0x1F;
                if (collisionPos == 0) {
                    collisionPos = 32;
                }
                // No progress - should not happen
                if (collisionPos <= currentLevelKnownBits) {
                    return STATUS_INTERNAL_ERROR;
                }
                // Choose the PICC with the bit set.
                currentLevelKnownBits = collisionPos;
                // The bit to modify
                count = (currentLevelKnownBits - 1) % 8;
                // First uint8_t is index 0.
                index = 1 + (currentLevelKnownBits / 8) + (count ? 1 : 0);
                buffer[index] |= (1 << count);
            } else if (result != STATUS_OK) {
                return result;
            } else {
                // STATUS_OK
                if (currentLevelKnownBits >= 32) {
                    // This was a SELECT.  No more anticollision
                    selectDone = true;
                    // We continue below outside the while.
                } else {
                    // This was an ANTICOLLISION.
                    // We now have all 32 bits of the UID in this Cascade Level
                    currentLevelKnownBits = 32;
                    // Run loop again to do the SELECT.
                }
            }
        }   // End of while (!selectDone)

        // We do not check the CBB - it was constructed by us above.

        // Copy the found UID uint8_ts from buffer[] to uid->data[]
        // source index in buffer[]
        index = (buffer[2] == PICC_CMD_CT) ? 3 : 2;
        uint8_tsToCopy  = (buffer[2] == PICC_CMD_CT) ? 3 : 4;
        for (count = 0; count < uint8_tsToCopy; count++) {
            uid->data[uidIndex + count] = buffer[index++];
        }

        // Check response SAK (Select Acknowledge)
        // SAK must be exactly 24 bits (1 uint8_t + CRC_A).
        if (responseLength != 3 || txLastBits != 0) {
            return STATUS_ERROR;
        }
        // Verify CRC_A - do our own calculation and store the control in buffer[2..3]
        // those uint8_ts are not needed anymore.
        result = PCD_CalculateCRC(responseBuffer, 1, &buffer[2]);
        if (result != STATUS_OK) {
            return result;
        }
        if ((buffer[2] != responseBuffer[1]) || (buffer[3] != responseBuffer[2])) {
            return STATUS_CRC_WRONG;
        }
        // Cascade bit set - UID not complete yes
        if (responseBuffer[0] & 0x04) {
            cascadeLevel++;
        } else {
            uidComplete = true;
            uid->sak = responseBuffer[0];
        }
    }   // End of while (!uidComplete)

    // Set correct uid->size
    uid->size = 3 * cascadeLevel + 1;

    return STATUS_OK;
}

StatusCode PICC_HaltA(void) {
    StatusCode result;
    uint8_t buffer[4];

    // Build command buffer
    buffer[0] = PICC_CMD_HLTA;
    buffer[1] = 0;
    // Calculate CRC_A
    result = PCD_CalculateCRC(buffer, 2, &buffer[2]);
    if (result != STATUS_OK) {
        return result;
    }

    /*
     Send the command. The standard says:
     If the PICC responds with any modulation during a period of 1 ms after the end of the
     frame containing the HLTA command, this response shall be interpreted as 'not acknowledge'.
     We interpret that this way: Only STATUS_TIMEOUT is an success.
     */

    result = PCD_TransceiveData(buffer, sizeof(buffer), NULL, 0, 0, 0, false);
    if (result == STATUS_TIMEOUT) {
        return STATUS_OK;
    }
    // That is ironically NOT ok in this case ;-)
    if (result == STATUS_OK) {
        return STATUS_ERROR;
    }
    return result;
}

PICC_Type PICC_GetType(uint8_t sak) {
    /*
     http://www.nxp.com/documents/application_note/AN10833.pdf
     3.2 Coding of Select Acknowledge (SAK) ignore 8-bit (iso14443 starts with LSBit = bit 1)
     fixes wrong type for manufacturer Infineon (http://nfc-tools.org/index.php?title=ISO14443A)
     */
    sak &= 0x7F;
    switch (sak) {
        // UID not complete
        case 0x04:      return PICC_TYPE_NOT_COMPLETE;
        case 0x09:      return PICC_TYPE_MIFARE_MINI;
        case 0x08:      return PICC_TYPE_MIFARE_1K;
        case 0x18:      return PICC_TYPE_MIFARE_4K;
        case 0x00:      return PICC_TYPE_MIFARE_UL;
        case 0x10:
        case 0x11:      return PICC_TYPE_MIFARE_PLUS;
        case 0x01:      return PICC_TYPE_TNP3XXX;
        case 0x20:      return PICC_TYPE_ISO_14443_4;
        case 0x40:      return PICC_TYPE_ISO_18092;
        default:        return PICC_TYPE_UNKNOWN;
    }
}

const char * GetPiccTypeName(PICC_Type piccType) {
    switch (piccType) {
        case PICC_TYPE_ISO_14443_4:     return "PICC compliant with ISO/IEC 14443-4";
        case PICC_TYPE_ISO_18092:       return "PICC compliant with ISO/IEC 18092 (NFC)";
        case PICC_TYPE_MIFARE_MINI:     return "MIFARE Mini, 320 uint8_ts";
        case PICC_TYPE_MIFARE_1K:       return "MIFARE 1KB";
        case PICC_TYPE_MIFARE_4K:       return "MIFARE 4KB";
        case PICC_TYPE_MIFARE_UL:       return "MIFARE Ultralight or Ultralight C";
        case PICC_TYPE_MIFARE_PLUS:     return "MIFARE Plus";
        case PICC_TYPE_TNP3XXX:         return "MIFARE TNP3XXX";
        case PICC_TYPE_NOT_COMPLETE:    return "SAK indicates UID is not complete.";
        case PICC_TYPE_UNKNOWN:
        default:                        return "Unknown type";
    }
}

void PICC_DumpToSerial(PICC_UID_TYPE *uid) {
    // All keys are set to FFFFFFFFFFFFh at chip delivery from the factory.
    const MIFARE_Key key = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    PICC_Type piccType;
    uint8_t i;
    // UID
    printf("Card UID:");
    for (i = 0; i < uid->size; i++) {
        printf(" %02X", uid->data[i]);
    }
    printf("\r\n");
    
    // PICC type
    piccType = PICC_GetType(uid->sak);
    printf("PICC type: %s \r\n", GetPiccTypeName(piccType));
    
    // Dump contents
    switch (piccType) {
        case PICC_TYPE_MIFARE_MINI:
        case PICC_TYPE_MIFARE_1K:
        case PICC_TYPE_MIFARE_4K:
            PICC_DumpMifareClassicToSerial(uid, piccType, key);
            break;
            
        case PICC_TYPE_MIFARE_UL:
            PICC_DumpMifareUltralightToSerial();
            break;
            
        case PICC_TYPE_ISO_14443_4:
        case PICC_TYPE_ISO_18092:
        case PICC_TYPE_MIFARE_PLUS:
        case PICC_TYPE_TNP3XXX:
            printf("Dumping memory contents not implemented for that PICC type.\r\n");
            break;
            
        case PICC_TYPE_UNKNOWN:
        case PICC_TYPE_NOT_COMPLETE:
        default:
            // No memory dump here
            break;
    }
    
    printf("\r\n");
    // Already done if it was a MIFARE Classic PICC.
    PICC_HaltA();
}

void PICC_DumpMifareClassicToSerial(PICC_UID_TYPE *uid, PICC_Type piccType, const uint8_t * key) {
    uint8_t no_of_sectors = 0;
    int8_t i;
    
    switch (piccType) {
        case PICC_TYPE_MIFARE_MINI:
            // Has 5 sectors * 4 blocks/sector * 16 uint8_ts/block = 320 uint8_ts.
            no_of_sectors = 5;
            break;
            
        case PICC_TYPE_MIFARE_1K:
            // Has 16 sectors * 4 blocks/sector * 16 uint8_ts/block = 1024 uint8_ts.
            no_of_sectors = 16;
            break;
            
        case PICC_TYPE_MIFARE_4K:
            // Has 256 block * 16 uint8_ts/block = 4096 uint8_ts.
            // (32 sectors * 4 blocks/sector + 8 sectors * 16 blocks/sector)
            no_of_sectors = 40;
            break;
            
        default: // Should not happen. Ignore.
            break;
    }
    
    // Dump sectors, highest address first.
    if (no_of_sectors) {
        printf("Sector Block   0  1  2  3   4  5  6  7   8  9 10 11  12 13 14 15  AccessBits\r\n");
        for (i = no_of_sectors - 1; i >= 0; i--) {
            PICC_DumpMifareClassicSectorToSerial(uid, key, i);
        }
    }
    // Halt the PICC before stopping the encrypted session.
    PICC_HaltA();
    PCD_StopCrypto1();
}

void PICC_DumpMifareClassicSectorToSerial(PICC_UID_TYPE *uid, const uint8_t * key, uint8_t sector) {
    StatusCode status;
    // Address of lowest address to dump actually last block dumped)
    uint8_t firstBlock;
    // Number of blocks in sector
    uint8_t no_of_blocks;
    // Set to true while handling the "last" (ie highest address) in the sector.
    bool isSectorTrailer;
    
    int8_t blockOffset;
    uint8_t index;
    
    /*
     The access bits are stored in a peculiar fashion.
     There are four groups:
     g[3] Access bits for the sector trailer, block 3 (for sectors 0-31) or block 15 (for sectors 32-39)
     g[2] Access bits for block 2 (for sectors 0-31) or blocks 10-14 (for sectors 32-39)
     g[1] Access bits for block 1 (for sectors 0-31) or blocks 5-9 (for sectors 32-39)
     g[0] Access bits for block 0 (for sectors 0-31) or blocks 0-4 (for sectors 32-39)
     Each group has access bits [C1 C2 C3]. In this code C1 is MSB and C3 is LSB.
     The four CX bits are stored together in a nible cx and an inverted nible cx_.
     */
    
    // Nibbles
    uint8_t c1, c2, c3;
    // Inverted nibbles
    uint8_t c1_, c2_, c3_;
    // True if one of the inverted nibbles did not match
    bool invertedError = false;
    // Access bits for each of the four groups.
    uint8_t g[4];
    // 0-3 - active group for access bits
    uint8_t group;
    // True for the first block dumped in the group
    bool firstInGroup;
    
    // Determine position and size of sector.
    if (sector < 32) {
        // Sectors 0..31 has 4 blocks each
        no_of_blocks = 4;
        firstBlock = sector * no_of_blocks;
    } else if (sector < 40) {
        // Sectors 32-39 has 16 blocks each
        no_of_blocks = 16;
        firstBlock = 128 + (sector - 32) * no_of_blocks;
    } else {
        // Illegal input, no MIFARE Classic PICC has more than 40 sectors.
        return;
    }
    
    // Dump blocks, highest address first.
    uint8_t uint8_tCount;
    uint8_t buffer[18];
    uint8_t blockAddr;
    isSectorTrailer = true;
    for (blockOffset = no_of_blocks - 1; blockOffset >= 0; blockOffset--) {
        blockAddr = firstBlock + blockOffset;
        // Sector number - only on first line
        if (isSectorTrailer) {
            printf("%4d   ", sector);
        } else {
            printf("       ");
        }
        // Block number
        printf("%4d  ", blockAddr);
        
        // Establish encrypted communications before reading the first block
        if (isSectorTrailer) {
            status = PCD_Authenticate(PICC_CMD_MF_AUTH_KEY_A, firstBlock, key, uid);
            if (status != STATUS_OK) {
                printf("PCD_Authenticate() failed: %s\r\n", GetStatusCodeName(status));
                return;
            }
        }
        // Read block
        uint8_tCount = sizeof(buffer);
        status = MIFARE_Read(blockAddr, buffer, &uint8_tCount);
        if (status != STATUS_OK) {
            printf("MIFARE_Read() failed: %s\r\n", GetStatusCodeName(status));
            continue;
        }
        // Dump data
        for (index = 0; index < 16; index++) {
            printf(" %02X ", buffer[index]);
            if ((index % 4) == 3) printf(" ");
        }
        // Parse sector trailer data
        if (isSectorTrailer) {
            c1  = buffer[7] >> 4;
            c2  = buffer[8] & 0xF;
            c3  = buffer[8] >> 4;
            c1_ = buffer[6] & 0xF;
            c2_ = buffer[6] >> 4;
            c3_ = buffer[7] & 0xF;
            invertedError = (c1 != (~c1_ & 0xF)) || (c2 != (~c2_ & 0xF)) || (c3 != (~c3_ & 0xF));
            g[0] = ((c1 & 1) << 2) | ((c2 & 1) << 1) | ((c3 & 1) << 0);
            g[1] = ((c1 & 2) << 1) | ((c2 & 2) << 0) | ((c3 & 2) >> 1);
            g[2] = ((c1 & 4) << 0) | ((c2 & 4) >> 1) | ((c3 & 4) >> 2);
            g[3] = ((c1 & 8) >> 1) | ((c2 & 8) >> 2) | ((c3 & 8) >> 3);
            isSectorTrailer = false;
        }
        
        // Which access group is this block in?
        if (no_of_blocks == 4) {
            group = blockOffset;
            firstInGroup = true;
        } else {
            group = blockOffset / 5;
            firstInGroup = (group == 3) || (group != (blockOffset + 1) / 5);
        }
        
        if (firstInGroup) {
            // Print access bits
            printf(" [ %d %d %d ]", (g[group] >> 2) & 1, (g[group] >> 1) & 1, g[group] & 1);
            if (invertedError) printf(" Inverted access bits did not match! ");
        }
        
        if (group != 3 && (g[group] == 1 || g[group] == 6)) {
            // Not a sector trailer, a value block
            long value = buffer[0];
            value |= ((long)buffer[1]) << 8;
            value |= ((long)buffer[2]) << 16;
            value |= ((long)buffer[3]) << 24;
            printf(" Value=0x%lX  Adr=0x%02X", value, buffer[12]);
        }
        printf("\r\n");
    }
    
    return;
}

void PICC_DumpMifareUltralightToSerial() {
    StatusCode status;
    uint8_t uint8_tCount;
    uint8_t buffer[18];
    uint8_t page, offset, index, i;
    
    
    printf("Page  0  1  2  3 \r\n");
    // Try the mpages of the original Ultralight. Ultralight C has more pages.
    for (page = 0; page < 16; page +=4) { // Read returns data for 4 pages at a time.
        // Read pages
        uint8_tCount = sizeof(buffer);
        status = MIFARE_Read(page, buffer, &uint8_tCount);
        if (status != STATUS_OK) {
            printf("MIFARE_Read() failed: %s\r\n", GetStatusCodeName(status));
            break;
        }
        // Dump data
        for (offset = 0; offset < 4; offset++) {
            i = page + offset;
            printf("%3d ", i);
            for (index = 0; index < 4; index++) {
                i = 4 * offset + index;
                printf(" %02X", buffer[i]);
            }
            printf("\r\n");
        }
    }
}

/* --- Convenience functions - does not add extra functionality -------------------------------- */

bool PICC_IsNewCardPresent() {
    uint8_t bufferATQA[2];
    uint8_t bufferSize = sizeof(bufferATQA);
    uint8_t result = PICC_RequestA(bufferATQA, &bufferSize);
    return (result == STATUS_OK || result == STATUS_COLLISION);
}


bool PICC_ReadCardSerial(PICC_UID_TYPE * uid) {
    uint8_t result = PICC_Select(uid, 0);
    return (result == STATUS_OK);
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

