/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef MFRC522_H   /*! @cond    */
#define MFRC522_H   /*! @endcond */

/** @file mfrc522.h
 ** @brief Declaraciones del contolador para el integrado de lectura RFID MFRC-522 de NXP 
 **
 ** Based on Library for ARDUINO RFID MODULE KIT 13.56 MHZ WITH TAGS SPI W AND R BY COOQROBOT.
 ** Based on code Dr.Leong   ( WWW.B2CQSHOP.COM )
 ** Created by Miguel Balboa (circuitito.com), Jan, 2012.
 ** Rewritten by Søren Thing Andersen (access.thing.dk), fall of 2013 (Translation to English,
 ** refactored, comments, anti collision, cascade levels.)
 ** Extended by Tom Clement with functionality to write to sector 0 of UID changeable Mifare cards.
 ** Released into the public domain.
 **
 ** Please read this file for an overview and then MFRC522.c for comments on the specific functions.
 ** Search for "mf-rc522" on ebay.com to purchase the MF-RC522 board.
 **
 ** There are three hardware components involved:
 ** 1) The microcontroller: (An Arduino in original file)
 ** 2) The PCD (short for Proximity Coupling Device): NXP MFRC522 Contactless Reader IC
 ** 3) The PICC (short for Proximity Integrated Circuit Card): A card or tag using the
 **    ISO 14443A interface, eg Mifare or NTAG203.
 **
 ** The microcontroller and card reader uses SPI for communication.
 ** The protocol is described in the MFRC522 datasheet:
 ** http://www.nxp.com/documents/data_sheet/MFRC522.pdf
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2015.12.28 | evolentini      | Version inicial del archivo portado desde arduino       |
 **|   2 | 2019.08.08 | evolentini      | Reorganizacion del codigo en dos capas                  |
 **
 ** \addtogroup mifare
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "pcd.h"
#include "picc.h"
#include "mifare.h"

#include "digitales.h"
#include "spi.h"

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/** @brief Initializes the library and MFRC522 chip.
 * 
 * Initializes the library's operation variables and the MFRC522 chip. The SPI port and GPIO pins
 * must be configured and inicializated before call this function
 * 
 * @param[in]   enlace  SPI port connected to PCD
 * @param[in]   reset   GPIO pin connected reset and power down input
 */
void PCD_Init(const spi_dispositivo_t enlace, const digital_salida_t reset);


//! Performs a soft reset on the MFRC522 chip and waits for it to be ready again.
void PCD_Reset(void);

//! Turns the antenna on by enabling pins TX1 and TX2.
/*!
 After a reset these pins are disabled.
 */
void PCD_AntennaOn(void);

//! Turns the antenna off by disabling pins TX1 and TX2.
void PCD_AntennaOff(void);

//! Get the current MFRC522 Receiver Gain (RxGain[2:0]) value.
/*!
 See 9.3.3.6 / table 98 in http://www.nxp.com/documents/data_sheet/MFRC522.pdf
 NOTE: Return value scrubbed with (0x07<<4)=01110000b as RCFfgReg may use reserved bits.
 @return Value of the RxGain, scrubbed to the 3 bits used.
 */
PCD_RxGain PCD_GetAntennaGain(void);

//! Set the MFRC522 Receiver Gain (RxGain) to value specified by given mask.
/*!
 See 9.3.3.6 / table 98 in http://www.nxp.com/documents/data_sheet/MFRC522.pdf
 @note Given value is scrubbed with (0x07<<4)=01110000b as RCFfgReg may use reserved bits.
 @param[in] value Value of gain to set.
 */
void PCD_SetAntennaGain(const PCD_RxGain value);

/* --- Functions for communicating with PICCs -------------------------------------------------- */


// StatusCode PCD_CommunicateWithPICC(uint8_t command, uint8_t waitIRq, uint8_t *sendData, uint8_t sendLen,
//                                    uint8_t *backData = NULL, uint8_t *backLen = NULL,
//                                    uint8_t *validBits = NULL, uint8_t rxAlign = 0, bool checkCRC = false);

//! Executes the Transceive command.
/*!
 CRC validation can only be done if backData and backLen are specified.
 
 @param[in]         sendData    Pointer to the data to transfer to the FIFO.
 @param[in]         sendLen     Number of bytes to transfer to the FIFO.
 @param[out]        backData    Pointer to buffer if data should be read back 
                                after executing the command.
 @param[in,out]     backLen     @li In: Max number of bytes to write to backData.
                                @li Out: The number of bytes returned.
 @param[in,out]     validBits   The number of valid bits in the last uint8_t. 0 for 8 valid bits.
 @param[in]         rxAlign     Defines the bit position in backData[0] for the first bit received.
                                Default value is 0.
 @param[in]         checkCRC    The last two bytes of the response is assumed to be a CRC_A 
                                that must be validated.
 @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode PCD_TransceiveData(const uint8_t *sendData, const uint8_t sendLen,uint8_t *backData,
                              uint8_t *backLen, uint8_t *validBits, const uint8_t rxAlign,
                              const bool checkCRC);
// StatusCode PCD_TransceiveData(uint8_t *sendData, uint8_t sendLen, uint8_t *backData, uint8_t *backLen,
//                               uint8_t *validBits = NULL, uint8_t rxAlign = 0, bool checkCRC = false);



/* --- Functions for communicating with MIFARE PICCs ------------------------------------------- */

//! Executes the MFRC522 MFAuthent command.
/*!
 This command manages MIFARE authentication to enable a secure communication to any MIFARE Mini,
 MIFARE 1K and MIFARE 4K card. The authentication is described in the MFRC522 datasheet
 section 10.3.1.9 and http://www.nxp.com/documents/data_sheet/MF1S503x.pdf section 10.1.
 For use with MIFARE Classic PICCs.
 The PICC must be selected - ie in state ACTIVE(*) - before calling this function.
 Remember to call PCD_StopCrypto1() after communicating with the authenticated PICC - otherwise
 no new communications can start.
 All keys are set to FFFFFFFFFFFFh at chip delivery.
 
 @param[in]     command     PICC_CMD_MF_AUTH_KEY_A or PICC_CMD_MF_AUTH_KEY_B
 @param[in]     blockAddr   The block number. See numbering in the comments.
 @param[in]     key         Pointer to the Crypto1 key to use (6 bytes)
 @param[in]     uid         Pointer to Uid struct. The first 4 bytes of the UID is used.
 @return                    STATUS_OK on success, STATUS_??? otherwise. 
                            Probably STATUS_TIMEOUT if the key is wrong.
 */
StatusCode PCD_Authenticate(const PICC_Command command, const uint8_t blockAddr,
                            const MIFARE_Key key, const PICC_UID_TYPE *uid);

//! Used to exit the PCD from its authenticated state.
/*!
 Remember to call this function after communicating with an authenticated PICC
 otherwise no new communications can start.
 */
void PCD_StopCrypto1(void);

#ifdef MFRC522_SELF_TEST
//! Performs a self-test of the MFRC522
/*!
 See 16.1.1 in http://www.nxp.com/documents/data_sheet/MFRC522.pdf
 @return Whether or not the test passed.
 */
bool PCD_PerformSelfTest(void);
#endif

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* MFRC522_H */
