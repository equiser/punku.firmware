/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef MIFARE_H   /*! @cond    */
#define MIFARE_H   /*! @endcond */

/** @file mifare.h
 ** @brief Declaraciones de la libreria para comunicacion con tarjetas Mifare
 **
 ** Based on Library for ARDUINO RFID MODULE KIT 13.56 MHZ WITH TAGS SPI W AND R BY COOQROBOT.
 ** Based on code Dr.Leong   ( WWW.B2CQSHOP.COM )
 ** Created by Miguel Balboa (circuitito.com), Jan, 2012.
 ** Rewritten by Søren Thing Andersen (access.thing.dk), fall of 2013 (Translation to English,
 ** refactored, comments, anti collision, cascade levels.)
 ** Extended by Tom Clement with functionality to write to sector 0 of UID changeable Mifare cards.
 ** Released into the public domain.
 **
 ** There are three hardware components involved:
 ** 1) The microcontroller: (An Arduino in original file)
 ** 2) The PCD (short for Proximity Coupling Device): NXP MFRC522 Contactless Reader IC
 ** 3) The PICC (short for Proximity Integrated Circuit Card): A card or tag using the
 **    ISO 14443A interface, eg Mifare or NTAG203.
 **
 ** The card reader and the tags communicate using a 13.56MHz electromagnetic field.
 ** The protocol is defined in ISO/IEC 14443-3 Identification cards -- Contactless integrated
 ** circuit cards -- Proximity cards -- Part 3: Initialization and anticollision".
 **  A free version of the final draft can be found at
 ** http://wg8.de/wg8n1496_17n3613_Ballot_FCD14443-3.pdf
 ** Details are found in chapter 6, Type A – Initialization and anticollision.
 **
 ** If only the PICC UID is wanted, the above documents has all the needed information. To read
 ** and write from MIFARE PICCs, the MIFARE protocol is used after the PICC has been selected.
 ** The MIFARE Classic chips and protocol is described in the datasheets:
 **        1K:   http://www.nxp.com/documents/data_sheet/MF1S503x.pdf
 **        4K:   http://www.nxp.com/documents/data_sheet/MF1S703x.pdf
 **        Mini: http://www.idcardmarket.com/download/mifare_S20_datasheet.pdf
 ** The MIFARE Ultralight chip and protocol is described in the datasheets:
 **        Ultralight:   http://www.nxp.com/documents/data_sheet/MF0ICU1.pdf
 **         Ultralight C: http://www.nxp.com/documents/short_data_sheet/MF0ICU2_SDS.pdf
 **
 ** MIFARE Classic 1K (MF1S503x):
 **     Has 16 sectors * 4 blocks/sector * 16 bytes/block = 1024 bytes.
 **     The blocks are numbered 0-63.
 **     Block 3 in each sector is the Sector Trailer.
 **     See http://www.nxp.com/documents/data_sheet/MF1S503x.pdf sections 8.6 and 8.7:
 **             bytes 0-5:   Key A
 **             bytes 6-8:   Access Bits
 **             bytes 9:     User data
 **             bytes 10-15: Key B (or user data)
 **     Block 0 is read-only manufacturer data.
 **     To access a block, an authentication using a key from the block's sector must be performed
 **     first.
 **        Example: To read from block 10, first authenticate using a key from sector 3 (blocks 8-11).
 **        All keys are set to FFFFFFFFFFFFh at chip delivery.
 **     Warning: Please read section 8.7 "Memory Access". It includes this text: if the PICC
 **     detects a format violation the whole sector is irreversibly blocked.
 **        To use a block in "value block" mode (for Increment/Decrement operations) you need to
 **     change the sector trailer. Use PICC_SetAccessBits() to calculate the bit patterns.
 ** MIFARE Classic 4K (MF1S703x):
 **        Has (32 sectors * 4 blocks/sector + 8 sectors * 16 blocks/sector) = 256 blocks
 **     Has 256 blocks * 16 bytes/block 4096 bytes.
 **        The blocks are numbered 0-255.
 **        The last block in each sector is the Sector Trailer like above.
 ** MIFARE Classic Mini (MF1 IC S20):
 **        Has 5 sectors * 4 blocks/sector * 16 bytes/block = 320 bytes.
 **        The blocks are numbered 0-19.
 **        The last block in each sector is the Sector Trailer like above.
 **
 ** MIFARE Ultralight (MF0ICU1):
 **        Has 16 pages of 4 bytes = 64 bytes.
 **        Pages 0 + 1 is used for the 7-uint8_t UID.
 **        Page 2 contains the last check digit for the UID, one uint8_t manufacturer internal data, and
 **     the lock bytes (see http://www.nxp.com/documents/data_sheet/MF0ICU1.pdf section 8.5.2)
 **        Page 3 is OTP, One Time Programmable bits. Once set to 1 they cannot revert to 0.
 **        Pages 4-15 are read/write unless blocked by the lock bytes in page 2.
 ** MIFARE Ultralight C (MF0ICU2):
 **        Has 48 pages of 4 bytes = 192 bytes.
 **        Pages 0 + 1 is used for the 7-uint8_t UID.
 **        Page 2 contains the last check digit for the UID, one uint8_t manufacturer internal data, and
 **     the lock bytes (see http://www.nxp.com/documents/data_sheet/MF0ICU1.pdf section 8.5.2)
 **        Page 3 is OTP, One Time Programmable bits. Once set to 1 they cannot revert to 0.
 **        Pages 4-39 are read/write unless blocked by the lock bytes in page 2.
 **        Page 40 Lock bytes
 **        Page 41 16 bit one way counter
 **        Pages 42-43 Authentication configuration
 **        Pages 44-47 Authentication key
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2015.12.28 | evolentini      | Version inicial del archivo portado desde arduino       |
 **|   2 | 2019.08.08 | evolentini      | Reorganizacion del codigo en dos capas                  |
 **
 ** \addtogroup mifare
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include <stdbool.h>
#include <stdint.h>
#include "pcd.h"

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

//! A Mifare Crypto1 key is 6 bytes.
#define MF_KEY_SIZE     6


/* === Declaraciones de tipos de datos ========================================================= */

//! A struct used for passing a MIFARE Crypto1 key
typedef uint8_t MIFARE_Key[MF_KEY_SIZE];
// typedef struct {
//     uint8_t key[MF_KEY_SIZE];
// } MIFARE_Key;

//! A struct used for passing the UID of a PICC.
typedef struct {
    uint8_t size;           //!< Number of bytes in the UID. 4, 7 or 10.
    uint8_t data[10];       //!< UID number storage
    uint8_t sak;            //!< The SAK (Select acknowledge) uint8_t returned from the PICC
                            //!< after successful selection.
} MIFARE_UID_TYPE;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/** Reads 16 bytes (+ 2 bytes CRC_A) from the active PICC.
 * For MIFARE Classic the sector containing the block must be authenticated before calling this
 * function.
 * 
 * For MIFARE Ultralight only addresses 00h to 0Fh are decoded.
 * 
 * The MF0ICU1 returns a NAK for higher addresses.
 * The MF0ICU1 responds to the READ command by sending 16 bytes starting from the page address
 * defined by the command argument.
 * 
 * For example; if blockAddr is 03h then pages 03h, 04h, 05h, 06h are returned.
 * A roll-back is implemented: If blockAddr is 0Eh, then the contents of pages 
 * 0Eh, 0Fh, 00h and 01h are returned.
 * 
 * The buffer must be at least 18 bytes because a CRC_A is also returned.
 * Checks the CRC_A before returning STATUS_OK.
 * 
 * @param[in]       blockAddr     @li MIFARE Classic: The block (0-0xff) number.
 *                                @li MIFARE Ultralight: The first page to return data from.
 * @param[out]      buffer        The buffer to store the data in.
 * @param[in,out]   bufferSize    Buffer size, at least 18 bytes, and number of bytes returned.
 * @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode MIFARE_Read(const uint8_t blockAddr, uint8_t *buffer, uint8_t *bufferSize);

/** @brief Writes 16 bytes to the active PICC.
 * 
 * For MIFARE Classic the sector containing the block must be authenticated before calling
 * this function.
 * 
 * For MIFARE Ultralight the operation is called "COMPATIBILITY WRITE".
 * Even though 16 bytes are transferred to the Ultralight PICC, only the least significant 4 
 * bytes (bytes 0 to 3) are written to the specified address. It is recommended to set the
 * remaining bytes 04h to 0Fh to all logic 0.
 * 
 * @param[in]       blockAddr     @li MIFARE Classic: The block (0-0xff) number.
 *                                @li MIFARE Ultralight: The first page to return data from.
 * @param[out]      buffer        The 16 bytes to write to the PICC.
 * @param[in]       bufferSize    Buffer size, must be at least 16 bytes.
 *                                First 16 bytes are written.
 * @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode MIFARE_Write(const uint8_t blockAddr, const uint8_t *buffer, const uint8_t bufferSize);

/** @brief Writes a 4 uint8_t page to the active MIFARE Ultralight PICC.
 * 
 * @param[in]       page          The page (2-15) to write to.
 * @param[in]       buffer        The 4 bytes to write to the PICC
 * @param[in]       bufferSize    Buffer size, must be at least 4 bytes.
 *                                Exactly 4 bytes are written.
 * @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode MIFARE_Ultralight_Write(uint8_t page, uint8_t *buffer, uint8_t bufferSize);

/** @brief Helper routine to read the current value from a Value Block.
 * 
 * Only for MIFARE Classic and only for blocks in "value block" mode, that is: with access bits
 * [C1 C2 C3] = [110] or [001]. The sector containing  the block must be authenticated before calling
 * this function.
 * 
 * @param[in]       blockAddr     The block (0x00-0xff) number.
 * @param[out]      value         Current value of the Value Block.
 * @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode MIFARE_GetValue(uint8_t blockAddr, long *value);

/** @brief Helper routine to write a specific value into a Value Block.
 * 
 * Only for MIFARE Classic and only for blocks in "value block" mode, that is: with access bits
 * [C1 C2 C3] = [110] or [001]. The sector containing the block must be authenticated before calling
 * this function.
 * 
 * @param[in]       blockAddr     The block (0x00-0xff) number.
 * @param[in]       value         New value of the Value Block.
 * @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode MIFARE_SetValue(uint8_t blockAddr, long value);

/** brief MIFARE Transfer writes the value stored in the volatile memory into one MIFARE Classic block.
 * 
 * MIFARE Transfer writes the value stored in the volatile memory into one MIFARE Classic block.
 * For MIFARE Classic only. The sector containing the block must be authenticated before calling this
 * function. Only for blocks in "value block" mode, ie with access bits [C1 C2 C3] = [110] or [001].
 * 
 * @param[in]       blockAddr     The block (0-0xff) number.
 * @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode MIFARE_Transfer(uint8_t blockAddr);

/** @brief MIFARE Decrement subtracts the delta from the value of the addressed block.
 *
 * MIFARE Decrement subtracts the delta from the value of the addressed block, and stores the result
 * in a volatile memory. For MIFARE Classic only. The sector containing the block must be
 * authenticated before calling this function. Only for blocks in "value block" mode, ie with access
 * bits [C1 C2 C3] = [110] or [001]. Use MIFARE_Transfer() to store the result in a block.
 * 
 * @param[in]       blockAddr     The block (0-0xff) number.
 * @param[in]       delta         This number is subtracted from the value of block blockAddr.
 * 
 * @return STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode MIFARE_Decrement(uint8_t blockAddr, long delta);

/** @brief MIFARE Increment adds the delta from the value of the addressed block.
 *
 * MIFARE Increment adds the delta from the value of the addressed block, and stores the result
 * in a volatile memory. For MIFARE Classic only. The sector containing the block must be
 * authenticated before calling this function. Only for blocks in "value block" mode, ie with access
 * bits [C1 C2 C3] = [110] or [001]. Use MIFARE_Transfer() to store the result in a block.
 * 
 * @param[in]       blockAddr     The block (0-0xff) number.
 * @param[in]       delta         This number is subtracted from the value of block blockAddr.
 * 
 * @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode MIFARE_Increment(uint8_t blockAddr, long delta);

/** @brief MIFARE Restore copies the value of the addressed block into a volatile memory.
 *
 * For MIFARE Classic only. The sector containing the block must be authenticated before calling this
 * function. Only for blocks in "value block" mode, ie with access bits [C1 C2 C3] = [110] or [001]. 
 * Use MIFARE_Transfer() to store the result in a block.
 * 
 * @param[in]       blockAddr     The block (0-0xff) number.
 * @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode MIFARE_Restore(uint8_t blockAddr);

/* --- Support functions ----------------------------------------------------------------------- */

//! Calculates the bit pattern needed for the specified access bits.
/*!
 In the [C1 C2 C3] tupples C1 is MSB (=4) and C3 is LSB (=1).
 
 @param[out]      accessBuffer          Pointer to bytes 6 to 8 in the sector trailer.
                                        Bytes [0..2] will be set.
 @param[in]       g0                    Access bits [C1 C2 C3] for block 0 (for sectors 0-31) 
                                        or blocks 0-4 (for sectors 32-39)
 @param[in]       g1                    Access bits C1 C2 C3] for block 1 (for sectors 0-31) 
                                        or blocks 5-9 (for sectors 32-39)
 @param[in]       g2                    Access bits C1 C2 C3] for block 2 (for sectors 0-31) 
                                        or blocks 10-14 (for sectors 32-39)
 @param[in]       g3                    Access bits C1 C2 C3] for the sector trailer, 
                                        block 3 (for sectors 0-31) or block 15 (for sectors 32-39)
 */
void MIFARE_SetAccessBits(uint8_t *accessBuffer, uint8_t g0, uint8_t g1, uint8_t g2, uint8_t g3);

//! Performs the "magic sequence" needed to get Chinese UID changeable
/*!
 Mifare cards to allow writing to sector 0, where the card UID is stored.
 Note that you do not need to have selected the card through REQA or WUPA,
 this sequence works immediately when the card is in the reader vicinity.
 This means you can use this method even on "bricked" cards that your reader does
 not recognise anymore (see MIFARE_UnbrickUidSector).
 Of course with non-bricked devices, you're free to select them before calling this function.

 @param[in]     logErrors               If true errors on secuence are printing in sdtout.
 */
bool MIFARE_OpenUidBackdoor(bool logErrors);

//! Updates entire block 0, including all manufacturer data.
/*!
 Reads entire block 0, including all manufacturer data, and overwrites that block with the new UID,
 a freshly calculated BCC, and the original manufacturer data.
 It assumes a default KEY A of 0xFFFFFFFFFFFF.
 Make sure to have selected the card before this function is called.

 @param[in]     newUid                  Array with new uid to save in block 0 of card.
 @param[in]     uidSize                 Size of new uid to save.
 @param[in]     logErrors               If true errors on secuence are printing in sdtout.
*/
bool MIFARE_SetUid(uint8_t *newUid, uint8_t uidSize, bool logErrors);


//! Resets entire sector 0 to zeroes, so the card can be read again by readers.
/*!
 @param[in]     logErrors               If true errors on secuence are printing in sdtout.
 */
bool MIFARE_UnbrickUidSector(bool logErrors);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* MIFARE_H */
