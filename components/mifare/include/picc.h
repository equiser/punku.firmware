/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef PICC_H   /*! @cond    */
#define PICC_H   /*! @endcond */

/** @file picc.h
 ** @brief Declaraciones de la libreria para la comunicacion con las tarjetas RFID ISO 14443A
 **
 ** Based on Library for ARDUINO RFID MODULE KIT 13.56 MHZ WITH TAGS SPI W AND R BY COOQROBOT.
 ** Based on code Dr.Leong   ( WWW.B2CQSHOP.COM )
 ** Created by Miguel Balboa (circuitito.com), Jan, 2012.
 ** Rewritten by Søren Thing Andersen (access.thing.dk), fall of 2013 (Translation to English,
 ** refactored, comments, anti collision, cascade levels.)
 ** Extended by Tom Clement with functionality to write to sector 0 of UID changeable Mifare cards.
 ** Released into the public domain.
 **
 ** There are three hardware components involved:
 ** 1) The microcontroller: (An Arduino in original file)
 ** 2) The PCD (short for Proximity Coupling Device): NXP MFRC522 Contactless Reader IC
 ** 3) The PICC (short for Proximity Integrated Circuit Card): A card or tag using the
 **    ISO 14443A interface, eg Mifare or NTAG203.
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2015.12.28 | evolentini      | Version inicial del archivo portado desde arduino       |
 **|   2 | 2019.08.08 | evolentini      | Reorganizacion del codigo en dos capas                  |
 **
 ** \addtogroup mifare
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "pcd.h"
#include "mifare.h"
#include <stdbool.h>

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

/** @brief PICC types we can detect.
 *
 * Remember to update PICC_GetTypeName() if you add more.
 */
typedef enum {
    PICC_TYPE_UNKNOWN        = 0,
    PICC_TYPE_ISO_14443_4    = 1,   //!< PICC compliant with ISO/IEC 14443-4
    PICC_TYPE_ISO_18092      = 2,   //!< PICC compliant with ISO/IEC 18092 (NFC)
    PICC_TYPE_MIFARE_MINI    = 3,   //!< MIFARE Classic protocol, 320 bytes
    PICC_TYPE_MIFARE_1K      = 4,   //!< MIFARE Classic protocol, 1KB
    PICC_TYPE_MIFARE_4K      = 5,   //!< MIFARE Classic protocol, 4KB
    PICC_TYPE_MIFARE_UL      = 6,   //!< MIFARE Ultralight or Ultralight C
    PICC_TYPE_MIFARE_PLUS    = 7,   //!< MIFARE Plus
    PICC_TYPE_TNP3XXX        = 8,   //!< Only mentioned in NXP AN 10833
                                    //!< MIFARE Type Identification Procedure
    PICC_TYPE_NOT_COMPLETE   = 255  //!< SAK indicates UID is not complete.
} PICC_Type;

//! A struct used for passing the UID of a PICC.
typedef struct {
    uint8_t size;           //!< Number of bytes in the UID. 4, 7 or 10.
    uint8_t data[10];       //!< UID number storage
    uint8_t sak;            //!< The SAK (Select acknowledge) uint8_t returned from the PICC
                            //!< after successful selection.
} PICC_UID_TYPE;

//! Commands sent to the PICC.
typedef enum {
    // The commands used by the PCD to manage communication with several PICCs
    // (ISO 14443-3, Type A, section 6.4)
    ///REQuest command, Type A
    PICC_CMD_REQA           = 0x26, //!< REQuest command, Type A. Invites PICCs in state IDLE
                                    //!< to go to READY and prepare for anticollision or selection.
                                    //!< 7 bit frame.
    PICC_CMD_WUPA           = 0x52, //!< Wake-UP command, Type A. Invites PICCs in state IDLE and
                                    //!< HALT to go to READY(*) and prepare for anticollision or
                                    //!< selection. 7 bit frame.
    PICC_CMD_CT             = 0x88, //!< Cascade Tag. Not really a command, but used during
                                    //!< anti collision.
    PICC_CMD_SEL_CL1        = 0x93, //!< Anti collision/Select, Cascade Level 1
    PICC_CMD_SEL_CL2        = 0x95, //!< Anti collision/Select, Cascade Level 2
    PICC_CMD_SEL_CL3        = 0x97, //!< Anti collision/Select, Cascade Level 3
    PICC_CMD_HLTA           = 0x50, //!< HaLT command, Type A. Instructs an ACTIVE PICC to go to
                                    //!< state HALT.

    // The commands used for MIFARE Classic
    // (from http://www.nxp.com/documents/data_sheet/MF1S503x.pdf, Section 9)
    // Use PCD_MFAuthent to authenticate access to a sector, then use these commands to
    // read/write/modify the blocks on the sector.
    // The read/write commands can also be used for MIFARE Ultralight.
    PICC_CMD_MF_AUTH_KEY_A  = 0x60, //!< Perform authentication with Key A
    PICC_CMD_MF_AUTH_KEY_B  = 0x61, //!< Perform authentication with Key B
    PICC_CMD_MF_READ        = 0x30, //!< Reads one 16 uint8_t block from the authenticated sector of
                                    //!< the PICC. Also used for MIFARE Ultralight.
    PICC_CMD_MF_WRITE       = 0xA0, //!< Writes one 16 uint8_t block to the authenticated sector of
                                    //!< PICC. Called "COMPATIBILITY WRITE" for MIFARE Ultralight.
    PICC_CMD_MF_DECREMENT   = 0xC0, //!< Decrements the contents of a block and stores the result
                                    //!< in the internal data register.
    PICC_CMD_MF_INCREMENT   = 0xC1, //!< Increments the contents of a block and stores the result
                                    //!< in the internal data register.
    PICC_CMD_MF_RESTORE     = 0xC2, //!< Reads the contents of a block into the internal data register.
    PICC_CMD_MF_TRANSFER    = 0xB0, //!< Writes the contents of the internal data register to a block.

    // The commands used for MIFARE Ultralight
    // (from http://www.nxp.com/documents/data_sheet/MF0ICU1.pdf, Section 8.6)
    // The PICC_CMD_MF_READ and PICC_CMD_MF_WRITE can also be used for MIFARE Ultralight.
    PICC_CMD_UL_WRITE        = 0xA2  //!< Writes one 4 uint8_t page to the PICC.
} PICC_Command;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

//! Transmits REQA or WUPA commands.
/*!
 Beware: When two PICCs are in the field at the same time I often get STATUS_TIMEOUT,
 probably due do bad antenna design.
 
 @param[in]         command         The command to send - PICC_CMD_REQA or PICC_CMD_WUPA.
 @param[out]        bufferATQA      The buffer to store the ATQA (Answer to request).
 @param[in,out]     bufferSize      Buffer size, at least two bytes. 
                                    Number of bytes returned if STATUS_OK.
 @return                            STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode PICC_REQA_or_WUPA(const PICC_Command command, uint8_t *bufferATQA, uint8_t *bufferSize );

//! Transmits a REQuest command, Type A.
/*!
 Invites PICCs in state IDLE to go to READY and prepare for anticollision or selection. 7 bit frame.
 Beware: When two PICCs are in the field at the same time I often get STATUS_TIMEOUT,
 probably due do bad antenna design.
 
 @param[out]        bufferATQA      The buffer to store the ATQA (Answer to request) in
 @param[in,out]     bufferSize      Buffer size, at least two bytes. 
                                    Number of bytes returned if STATUS_OK.
 @return                            STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode PICC_RequestA(uint8_t *bufferATQA, uint8_t *bufferSize);

//! Transmits a Wake-UP command, Type A.
/*!
 Invites PICCs in state IDLE and HALT to go to READY(*) and prepare for anticollision or selection.
 7 bit frame. Beware: When two PICCs are in the field at the same time I often get STATUS_TIMEOUT,
 probably due do bad antenna design.
 
 @param[out]        bufferATQA      The buffer to store the ATQA (Answer to request) in
 @param[in,out]     bufferSize      Buffer size, at least two bytes. 
                                    Number of bytes returned if STATUS_OK.
 @return                            STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode PICC_WakeupA(uint8_t *bufferATQA, uint8_t *bufferSize);

//! Transmits SELECT/ANTICOLLISION commands to select a single PICC.
/*!
 Before calling this function the PICCs must be placed in the READY(*) state by calling
 PICC_RequestA() or PICC_WakeupA().
 On success:
 
 - The chosen PICC is in state ACTIVE(*) and all other PICCs have returned to state IDLE/HALT.
 (Figure 7 of the ISO/IEC 14443-3 draft.)
 
 - The UID size and value of the chosen PICC is returned in *uid along with the SAK.
 A PICC UID consists of 4, 7 or 10 bytes.
 Only 4 bytes can be specified in SELECT command, for longer UIDs two or three iterations are used:
 
 UID size    Number of UID bytes        Cascade levels        Example of PICC
 ========    ===================        ==============        ===============
 single              4                        1                MIFARE Classic
 double              7                        2                MIFARE Ultralight
 triple             10                        3                Not currently in use?
 
 @param[in,out]   uid           Pointer to Uid struct. Normally output, but can be used to supply
                                a known UID.
 @param[in]       validBits     The number of known UID bits supplied. Normally 0.
                                If set you must also supply uid->size.
 @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode PICC_Select(PICC_UID_TYPE *uid, uint8_t validBits);

//! Instructs a PICC in state ACTIVE(*) to go to state HALT.
/*!
 @return STATUS_OK on success, STATUS_??? otherwise.
 */
StatusCode PICC_HaltA(void);

//! Translates the SAK (Select Acknowledge) to a PICC type.
/*!
 @param[in]       sak                   The SAK uint8_t returned from PICC_Select().
 @return                                One of the PICC_Type enums.
 */
PICC_Type PICC_GetType(uint8_t sak);

//! Returns a pointer to the PICC type name.
/*!
 @param[in]       piccType              One of the PICC_Type enums.
 @return                                Pointer to string with tag type name description
 */
const char * GetPiccTypeName(PICC_Type piccType);

//! Dumps debug info about the selected PICC to Serial.
/*!
 On success the PICC is halted after dumping the data.
 For MIFARE Classic the factory default key of 0xFFFFFFFFFFFF is tried.
 
 @param[in]       uid                   Pointer to Uid struct returned from a successful selecttion.
 */
void PICC_DumpToSerial(PICC_UID_TYPE *uid);

//! Dumps memory contents of a MIFARE Classic PICC.
/*!
 On success the PICC is halted after dumping the data.

 @param[in]       uid                   Pointer to Uid struct returned from a successful selecttion.
 @param[in]       piccType              One of the PICC_Type enums.
 @param[in]       key                   Key A used for all sectors.
 */
void PICC_DumpMifareClassicToSerial(PICC_UID_TYPE *uid, PICC_Type piccType, const uint8_t * key);

//! Dumps memory contents of a sector of a MIFARE Classic PICC.
/*!
 Uses PCD_Authenticate(), MIFARE_Read() and PCD_StopCrypto1. Always uses PICC_CMD_MF_AUTH_KEY_A 
 because only Key A can always read the sector trailer access bits.

 @param[in]       uid                   Pointer to Uid struct returned from a successful selecttion.
 @param[in]       key                   Key A for the sector.
 @param[in]       sector                The sector to dump, a value from 0 to 39.
*/
void PICC_DumpMifareClassicSectorToSerial(PICC_UID_TYPE *uid, const uint8_t * key, uint8_t sector);

//! Dumps memory contents of a MIFARE Ultralight PICC.
void PICC_DumpMifareUltralightToSerial()
;
/* --- Convenience functions - does not add extra functionality -------------------------------- */

//! Returns true if a PICC responds to PICC_CMD_REQA.
/*!
 Only "new" cards in state IDLE are invited. Sleeping cards in state HALT are ignored.
 
 @return                                Returns true if new card are selected, false on other case.
 */
bool PICC_IsNewCardPresent(void);

//! Simple wrapper around PICC_Select.
/*!
 Remember to call PICC_IsNewCardPresent(), PICC_RequestA() or PICC_WakeupA() first.
 The read UID is available in the class variable uid.

 @param[out]      uid                   UID struct returned from a successful selection.
 @return                                Returns true if a UID could be read.
 */
bool PICC_ReadCardSerial(PICC_UID_TYPE * uid);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* PICC_H */
