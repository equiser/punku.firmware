/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file mifare.h
 ** @brief Implementacion de la libreria para comunicacion con tarjetas Mifare
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2015.12.28 | evolentini      | Version inicial del archivo portado desde arduino       |
 **|   2 | 2019.08.08 | evolentini      | Reorganizacion del codigo en dos capas                  |
 **
 ** \addtogroup mifare
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "mifare.h"
#include "picc.h"
#include "mfrc522.h"

#include <stdio.h>
#include <string.h>

/* === Definicion y Macros ===================================================================== */

//! The MIFARE Classic uses a 4 bit ACK/NAK. Any other value than 0xA is NAK.
#define MF_ACK          0xA

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

//! Helper function for the two-step MIFARE Classic protocol operations.
/*!
 Helper function for the two-step MIFARE Classic protocol operations Decrement, Increment and Restore.
 
 @param[in]       command       The command to use.
 @param[in]       blockAddr     The block (0-0xff) number.
 @param[in]       data          The data to transfer in second step.
 @return                        STATUS_OK on success, STATUS_??? otherwise.
 */
static StatusCode MIFARE_TwoStepHelper(uint8_t command, uint8_t blockAddr, long data);

//! Wrapper for MIFARE protocol communication.
/*!
 Adds CRC_A, executes the Transceive command and checks that the response is MF_ACK or a timeout.
 
 @param[in]       sendData          Pointer to the data to transfer to the FIFO.
 Do NOT include the CRC_A.
 @param[in]       sendLen           Number of uint8_ts in sendData.
 @param[in]       acceptTimeout     if true then a timeout is also success
 @return                            STATUS_OK on success, STATUS_??? otherwise.
 */
static StatusCode PCD_MIFARE_Transceive(const uint8_t *sendData, uint8_t sendLen, bool acceptTimeout);

/* === Definiciones de funciones internas ====================================================== */

StatusCode MIFARE_TwoStepHelper(uint8_t command, uint8_t blockAddr, long data) {
    StatusCode result;
    // We only need room for 2 uint8_ts.
    uint8_t cmdBuffer[2];
    
    // Step 1: Tell the PICC the command and block address
    cmdBuffer[0] = command;
    cmdBuffer[1] = blockAddr;
    result = PCD_MIFARE_Transceive(cmdBuffer, 2, false);
    // Adds CRC_A and checks that the response is MF_ACK.
    if (result != STATUS_OK) {
        return result;
    }
    
    // Step 2: Transfer the data
    result = PCD_MIFARE_Transceive((uint8_t *)&data, 4, true);
    // Adds CRC_A and accept timeout as success.
    if (result != STATUS_OK) {
        return result;
    }
    
    return STATUS_OK;
}

static StatusCode PCD_MIFARE_Transceive(const uint8_t *sendData, uint8_t sendLen, bool acceptTimeout) {
    StatusCode result;
    // We need room for 16 uint8_ts data and 2 uint8_ts CRC_A.
    uint8_t cmdBuffer[18];
    
    // Sanity check
    if (sendData == NULL || sendLen > 16) {
        return STATUS_INVALID;
    }
    
    // Copy sendData[] to cmdBuffer[] and add CRC_A
    memcpy(cmdBuffer, sendData, sendLen);
    result = PCD_CalculateCRC(cmdBuffer, sendLen, &cmdBuffer[sendLen]);
    if (result != STATUS_OK) {
        return result;
    }
    sendLen += 2;
    
    // Transceive the data, store the reply in cmdBuffer[]
    uint8_t waitIRq = 0x30;        // RxIRq and IdleIRq
    uint8_t cmdBufferSize = sizeof(cmdBuffer);
    uint8_t validBits = 0;
    result = PCD_CommunicateWithPICC(PCD_Transceive, waitIRq, cmdBuffer, sendLen, cmdBuffer,
                                     &cmdBufferSize, &validBits, 0, false);
    if (acceptTimeout && result == STATUS_TIMEOUT) {
        return STATUS_OK;
    }
    if (result != STATUS_OK) {
        return result;
    }
    // The PICC must reply with a 4 bit ACK
    if (cmdBufferSize != 1 || validBits != 4) {
        return STATUS_ERROR;
    }
    if (cmdBuffer[0] != MF_ACK) {
        return STATUS_MIFARE_NACK;
    }
    return STATUS_OK;
}

/* === Definiciones de funciones externas ====================================================== */


void MIFARE_SetAccessBits(uint8_t *accessBuffer, uint8_t g0, uint8_t g1, uint8_t g2, uint8_t g3) {
    uint8_t c1 = ((g3 & 4) << 1) | ((g2 & 4) << 0) | ((g1 & 4) >> 1) | ((g0 & 4) >> 2);
    uint8_t c2 = ((g3 & 2) << 2) | ((g2 & 2) << 1) | ((g1 & 2) << 0) | ((g0 & 2) >> 1);
    uint8_t c3 = ((g3 & 1) << 3) | ((g2 & 1) << 2) | ((g1 & 1) << 1) | ((g0 & 1) << 0);
    
    accessBuffer[0] = (~c2 & 0xF) << 4 | (~c1 & 0xF);
    accessBuffer[1] =          c1 << 4 | (~c3 & 0xF);
    accessBuffer[2] =          c3 << 4 | c2;
}

bool MIFARE_OpenUidBackdoor(bool logErrors) {
    /*
     Magic sequence:
     -> 50 00 57 CD (HALT + CRC)
     -> 40 (7 bits only)
     <- A (4 bits only)
     -> 43
     <- A (4 bits only)
     Then you can write to sector 0 without authenticating
     */
    
    // 50 00 57 CD
    PICC_HaltA();
    
    uint8_t cmd = 0x40;
    // Our command is only 7 bits. After receiving card response,
    // this will contain amount of valid response bits. */
    uint8_t validBits = 7;
    
    // Card's response is written here
    uint8_t response[32];
    uint8_t received;
    // 40
    StatusCode status = PCD_TransceiveData(&cmd, 1, response, &received, &validBits, 0, false);
    if(status != STATUS_OK) {
        if(logErrors) {
            printf("Card did not respond to 0x40 after HALT command."
                   "Are you sure it is a UID changeable one? \r\n"
                   "Error name: %s \r\n", GetStatusCodeName(status));
        }
        return false;
    }
    if (received != 1 || response[0] != 0x0A) {
        if (logErrors) {
            printf("Got bad response on backdoor 0x40 command: "
                   " %02X (%d valid bits)\r\n", response[0], validBits);
        }
        return false;
    }
    
    cmd = 0x43;
    validBits = 8;
    status = PCD_TransceiveData(&cmd, (uint8_t)1, response, &received, &validBits, (uint8_t)0, false); // 43
    if(status != STATUS_OK) {
        if(logErrors) {
            printf("Error in communication at command 0x43, after successfully executing 0x40\r\n"
                   "Error name: %s \r\n", GetStatusCodeName(status));
        }
        return false;
    }
    if (received != 1 || response[0] != 0x0A) {
        if (logErrors) {
            printf("Got bad response on backdoor 0x43 command: "
                   " %02X (%d valid bits)\r\n", response[0], validBits);
        }
        return false;
    }
    
    // You can now write to sector 0 without authenticating!
    return true;
}

bool MIFARE_SetUid(uint8_t *newUid, uint8_t uidSize, bool logErrors) {
    const MIFARE_Key key = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    PICC_UID_TYPE uid;
    
    // UID + BCC uint8_t can not be larger than 16 together
    if (!newUid || !uidSize || uidSize > 15) {
        if (logErrors) {
            printf("New UID buffer empty, size 0, or size > 15 given\r\n");
        }
        return false;
    }
    
    // Authenticate for reading
    StatusCode status = PCD_Authenticate(PICC_CMD_MF_AUTH_KEY_A, (uint8_t)1, key, &uid);
    if (status != STATUS_OK) {
        
        if (status == STATUS_TIMEOUT) {
            // We get a read timeout if no card is selected yet, so let's select one
            
            // Wake the card up again if sleeping
            //              uint8_t atqa_answer[2];
            //              uint8_t atqa_size = 2;
            //              PICC_WakeupA(atqa_answer, &atqa_size);
            
            if (!PICC_IsNewCardPresent() || !PICC_ReadCardSerial(&uid)) {
                printf("No card was previously selected, and none are available."
                       "Failed to set UID.\r\n");
                return false;
            }
            
            status = PCD_Authenticate(PICC_CMD_MF_AUTH_KEY_A, (uint8_t)1, key, &uid);
            if (status != STATUS_OK) {
                // We tried, time to give up
                if (logErrors) {
                    printf("Failed to authenticate to card for reading, could not set UID: %s\r\n",
                           GetStatusCodeName(status));
                }
                return false;
            }
        }
        else {
            if (logErrors) {
                printf("PCD_Authenticate() failed: %s ", GetStatusCodeName(status));
            }
            return false;
        }
    }
    
    // Read block 0
    uint8_t block0_buffer[18];
    uint8_t uint8_tCount = sizeof(block0_buffer);
    status = MIFARE_Read((uint8_t)0, block0_buffer, &uint8_tCount);
    if (status != STATUS_OK) {
        if (logErrors) {
            printf("MIFARE_Read() failed: %s\r\n", GetStatusCodeName(status));
            printf("Are you sure your KEY A for sector 0 is 0xFFFFFFFFFFFF?\r\n");
        }
        return false;
    }
    
    // Write new UID to the data we just read, and calculate BCC uint8_t
    uint8_t bcc = 0;
    int i;
    for (i = 0; i < uidSize; i++) {
        block0_buffer[i] = newUid[i];
        bcc ^= newUid[i];
    }
    
    // Write BCC uint8_t to buffer
    block0_buffer[uidSize] = bcc;
    
    // Stop encrypted traffic so we can send raw uint8_ts
    PCD_StopCrypto1();
    
    // Activate UID backdoor
    if (!MIFARE_OpenUidBackdoor(logErrors)) {
        if (logErrors) {
            printf("Activating the UID backdoor failed.\r\n");
        }
        return false;
    }
    
    // Write modified block 0 back to card
    status = MIFARE_Write(0, block0_buffer, 16);
    if (status != STATUS_OK) {
        if (logErrors) {
            printf("MIFARE_Write() failed: %s \r\n", GetStatusCodeName(status));
        }
        return false;
    }
    
    // Wake the card up again
    uint8_t atqa_answer[2];
    uint8_t atqa_size = 2;
    PICC_WakeupA(atqa_answer, &atqa_size);
    
    return true;
}

bool MIFARE_UnbrickUidSector(bool logErrors) {
    MIFARE_OpenUidBackdoor(logErrors);
    
    uint8_t block0_buffer[] = {0x01, 0x02, 0x03, 0x04, 0x04, 0x00, 0x00, 0x00,
                               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    
    // Write modified block 0 back to card
    StatusCode status = MIFARE_Write(0, block0_buffer, 16);
    if (status != STATUS_OK) {
        if (logErrors) {
            printf("MIFARE_Write() failed: %s \r\n", GetStatusCodeName(status));
        }
        return false;
    }
    return true;
}

StatusCode MIFARE_Read(const uint8_t blockAddr, uint8_t *buffer, uint8_t *bufferSize) {
    StatusCode result;

    // Sanity check
    if (buffer == NULL || *bufferSize < 18) {
        return STATUS_NO_ROOM;
    }

    // Build command buffer
    buffer[0] = PICC_CMD_MF_READ;
    buffer[1] = blockAddr;
    // Calculate CRC_A
    result = PCD_CalculateCRC(buffer, 2, &buffer[2]);
    if (result != STATUS_OK) {
        return result;
    }

    // Transmit the buffer and receive the response, validate CRC_A.
    return PCD_TransceiveData(buffer, 4, buffer, bufferSize, NULL, 0, true);
}

StatusCode MIFARE_Write(const uint8_t blockAddr, const uint8_t *buffer, const uint8_t bufferSize) {
    StatusCode result;

    // Sanity check
    if (buffer == NULL || bufferSize < 16) {
        return STATUS_INVALID;
    }

    // Mifare Classic protocol requires two communications to perform a write.
    // Step 1: Tell the PICC we want to write to block blockAddr.
    uint8_t cmdBuffer[2];
    cmdBuffer[0] = PICC_CMD_MF_WRITE;
    cmdBuffer[1] = blockAddr;
    // Adds CRC_A and checks that the response is MF_ACK.
    result = PCD_MIFARE_Transceive(cmdBuffer, 2, false);
    if (result != STATUS_OK) {
        return result;
    }

    // Step 2: Transfer the data
    // Adds CRC_A and checks that the response is MF_ACK.
    result = PCD_MIFARE_Transceive(buffer, bufferSize, false);
    if (result != STATUS_OK) {
        return result;
    }

    return STATUS_OK;
}

StatusCode MIFARE_Ultralight_Write(uint8_t page, uint8_t *buffer, uint8_t bufferSize) {
    StatusCode result;
    
    // Sanity check
    if (buffer == NULL || bufferSize < 4) {
        return STATUS_INVALID;
    }
    
    // Build commmand buffer
    uint8_t cmdBuffer[6];
    cmdBuffer[0] = PICC_CMD_UL_WRITE;
    cmdBuffer[1] = page;
    memcpy(&cmdBuffer[2], buffer, 4);
    
    // Perform the write
    result = PCD_MIFARE_Transceive(cmdBuffer, 6, false);
    // Adds CRC_A and checks that the response is MF_ACK.
    if (result != STATUS_OK) {
        return result;
    }
    return STATUS_OK;
}

StatusCode MIFARE_GetValue(uint8_t blockAddr, long *value) {
    StatusCode status;
    uint8_t buffer[18];
    uint8_t size = sizeof(buffer);
    
    // Read the block
    status = MIFARE_Read(blockAddr, buffer, &size);
    if (status == STATUS_OK) {
        // Extract the value
        *value = buffer[0];
        *value |= ((long)buffer[1]) << 8;
        *value |= ((long)buffer[2]) << 16;
        *value |= ((long)buffer[3]) << 24;
    }
    return status;
}

StatusCode MIFARE_SetValue(uint8_t blockAddr, long value) {
    uint8_t buffer[18];
    
    // Translate the long into 4 uint8_ts; repeated 2x in value block
    buffer[0] = buffer[ 8] = (value & 0xFF);
    buffer[1] = buffer[ 9] = (value & 0xFF00) >> 8;
    buffer[2] = buffer[10] = (value & 0xFF0000) >> 16;
    buffer[3] = buffer[11] = (value & 0xFF000000) >> 24;
    // Inverse 4 uint8_ts also found in value block
    buffer[4] = ~buffer[0];
    buffer[5] = ~buffer[1];
    buffer[6] = ~buffer[2];
    buffer[7] = ~buffer[3];
    // Address 2x with inverse address 2x
    buffer[12] = buffer[14] = blockAddr;
    buffer[13] = buffer[15] = ~blockAddr;
    
    // Write the whole data block
    return MIFARE_Write(blockAddr, buffer, 16);
}

StatusCode MIFARE_Transfer(uint8_t blockAddr) {
    StatusCode result;
    // We only need room for 2 uint8_ts.
    uint8_t cmdBuffer[2];
    
    // Tell the PICC we want to transfer the result into block blockAddr.
    cmdBuffer[0] = PICC_CMD_MF_TRANSFER;
    cmdBuffer[1] = blockAddr;
    result = PCD_MIFARE_Transceive(cmdBuffer, 2, false);
    // Adds CRC_A and checks that the response is MF_ACK.
    if (result != STATUS_OK) {
        return result;
    }
    return STATUS_OK;
}

StatusCode MIFARE_Decrement(uint8_t blockAddr, long delta) {
    return MIFARE_TwoStepHelper(PICC_CMD_MF_DECREMENT, blockAddr, delta);
}

StatusCode MIFARE_Increment(uint8_t blockAddr, long delta) {
    return MIFARE_TwoStepHelper(PICC_CMD_MF_INCREMENT, blockAddr, delta);
}

StatusCode MIFARE_Restore(uint8_t blockAddr) {
    // The datasheet describes Restore as a two step operation, but does not explain what data
    // to transfer in step 2. Doing only a single step does not work, so I chose to transfer 0L
    return MIFARE_TwoStepHelper(PICC_CMD_MF_RESTORE, blockAddr, 0L);
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

