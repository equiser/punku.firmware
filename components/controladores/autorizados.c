/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file autorizados.c
 ** @brief Declaraciones de la libreria para la gestion de lista tarjetas autorizadas
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.01.21 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "autorizados.h"
#include "arbolb.h"
#include "editor.h"
#include "archivos.h"
#include "errores.h"

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#ifdef TEST
#include "FreeRTOS.h"
#include "semphr.h"
#else
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#endif

/* === Definicion y Macros ===================================================================== */

//! Nombre del arbol B que contiene a la lista de tarjetas autorizadas
#define NOMBRE                  "tarjetas"

//! Formato de los nombres de archivo para almacenar un arbol B
#define NOMBRE_ARCHIVO          "./%s.bin"

//! Tiempo maximo de espera para obtener el bloqueo de exclusividad
#define ESPERA_MAXIMA           pdMS_TO_TICKS(100)

/* == Declaraciones de tipos de datos internos ================================================= */

//! Estrcutura que describe la información de una puerta
struct autorizados_s {
    arbol_t indice;                     //!< Indice de las tarjetas autorizadas
    SemaphoreHandle_t mutex;            //!< Mutex para coordinar lecturas y modificaciones
    autorizados_listar_t procesador;    //!< Funcion para procesar cada tarjeta listada
    void * referencia;                  //!< Referencia del usuario para los listados
};

/* === Declaraciones de funciones internas ===================================================== */

/** @brief Busca un nuevo descriptor de entrada sin usar
 *
 * @return Referencia al descriptor del nuevo objeto autorizados 
 */
static autorizados_t CrearInstancia(void);

//! Funcion para la apertura o creacion de un archivo en disco
bool AbrirArchivo(const char * nombre, arbol_archivo_t * archivo, bool nuevo);

//! Funcion para la lectura de un sector de disco
bool LeerBloque(arbol_archivo_t archivo, uint8_t indice, arbol_bloque_t bloque);

//! Funcion para la escritura de un sector de disco
bool EscribirBloque(arbol_archivo_t archivo, uint8_t indice, const arbol_bloque_t bloque);

bool EnventoListado(uint16_t indice, arbol_clave_t clave, arbol_valor_t valor, void * referencia);

/* === Definiciones de variables internas ====================================================== */

//! Constante con las funciones para la gestion del almacenamiento
static const arbol_funciones_t funciones = {
    .AbrirArchivo = AbrirArchivo,
    .EscribirArchivo = (bool(*)(arbol_archivo_t)) ArchivoCompletar,
    .CerrarArchivo = (bool(*)(arbol_archivo_t)) ArchivoCerrar,
    .BorrarArchivo = ArchivoBorrar,
    .LeerBloque = LeerBloque,
    .EscribirBloque = EscribirBloque,
};

/* === Definiciones de funciones internas ====================================================== */

static autorizados_t CrearInstancia(void) {
    static struct autorizados_s autorizados = {0};
 
    if (autorizados.mutex == NULL) {
        autorizados.mutex = xSemaphoreCreateMutex();
    }
    return &autorizados;
}

bool AbrirArchivo(const char * nombre, arbol_archivo_t * archivo, bool nuevo) {
    if (nuevo) {
        *archivo = ArchivoAbrir(nombre, "w+");
    } else {
        *archivo = ArchivoAbrir(nombre, "r+");
    }
    return (*archivo != NULL);
}

bool LeerBloque(arbol_archivo_t archivo, uint8_t indice, arbol_bloque_t bloque) {
    bool resultado;
    resultado = ArchivoMover(archivo, ESPACIO_BLOQUE * indice , SEEK_SET);
    if (resultado) resultado = ArchivoLeer(archivo, bloque, ESPACIO_BLOQUE);
    return resultado;
}

bool EscribirBloque(arbol_archivo_t archivo, uint8_t indice, const arbol_bloque_t bloque) {
    bool resultado;
    resultado = ArchivoMover(archivo, ESPACIO_BLOQUE * indice , SEEK_SET);
    if (resultado) resultado = ArchivoEscribir(archivo, bloque, ESPACIO_BLOQUE);
    return resultado;
}

bool EnventoListado(uint16_t indice, arbol_clave_t clave, arbol_valor_t valor, void * referencia) {
    autorizados_t self = referencia;

    return self->procesador(indice, clave, self->referencia);
}

#ifdef TEST
bool TomarMutex(autorizados_t self) {
    return xSemaphoreTake(self->mutex, 0) == pdTRUE;
}

bool LiberarMutex(autorizados_t self) {
    return xSemaphoreGive(self->mutex) == pdTRUE;
}
#endif

/* === Definiciones de funciones externas ====================================================== */

autorizados_t AutorizadosCrear(void) {
    autorizados_t self = CrearInstancia();

    if (self != NULL) {
        self->indice = ArbolCrear(EditorCrear(NOMBRE, &funciones), false);
        if (self->indice == NULL) {
            Alerta("Creando un nuevo archivo de personas autorizadas");
            self->indice = ArbolCrear(EditorCrear(NOMBRE, &funciones), true);
        } 
    }
    return self;
}

bool AutorizadosValidar(autorizados_t self, tarjeta_t tarjeta) {
    bool resultado = (self != NULL);
    if (resultado) resultado = (xSemaphoreTake(self->mutex, ESPERA_MAXIMA) == pdTRUE);
    if (resultado) {
        resultado = ArbolBuscar(self->indice, tarjeta, NULL);
        xSemaphoreGive(self->mutex);
    }
    return resultado;
}

bool AutorizadosAgregar(autorizados_t self, tarjeta_t tarjeta) {
    bool resultado = (self != NULL);
    if (resultado) resultado = (xSemaphoreTake(self->mutex, ESPERA_MAXIMA) == pdTRUE);
    if (resultado) {
        resultado = ArbolInsertar(self->indice, tarjeta, 0);
        xSemaphoreGive(self->mutex);
    }
    return resultado;
}

bool AutorizadosBorrar(autorizados_t self, tarjeta_t tarjeta) {
    bool resultado = (self != NULL);
    if (resultado) resultado = (xSemaphoreTake(self->mutex, ESPERA_MAXIMA) == pdTRUE);
    if (resultado) {
        resultado = ArbolEliminar(self->indice, tarjeta);
        xSemaphoreGive(self->mutex);
    }
    return resultado;
}

bool AutorizadosLimpiar(autorizados_t self) {
    bool resultado = (self != NULL);
    if (resultado) resultado = ArbolLimpiar(self->indice);
    return resultado;
}

bool AutorizadosListar(autorizados_t self, autorizados_listar_t procesador, void * referencia) {
    self->procesador = procesador;
    self->referencia = referencia;
    bool resultado = (self->procesador != NULL);
    if (resultado) resultado = ArbolListar(self->indice, EnventoListado, self);
    return resultado;
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

