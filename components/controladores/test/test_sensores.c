/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_sensores.c
 ** @brief Pruebas unitarias de la clase para gestion de los sensores digitales
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.02.09 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "unity.h"
#include "sensores.h"
#include "mock_digitales.h"

/* === Definicion y Macros ===================================================================== */

//! Terminal digital utilizado para las pruebas
#define GPIO                1

//! Valor de histeresis del sensor utilizado en las pruebas
#define HISTERESIS          4

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

//! Variable global con la referencia al objeto puerta bajo prueba
static sensor_t sensor = NULL;

//! Variable global con una referencia a utilizar en los eventos generados por la puerta
static void * referencia = (void *) 4;

//! Variable global para capturar los parametros de las llamadas a la funcion de notificación
static struct {
    bool ejecutada;
    bool estado;
    bool estable;
    sensor_t sensor;
    void * referencia;
} llamada = {0};

/* === Declaraciones de funciones internas ===================================================== */

//! Funcion para capturar las notificaciones de un sensor
void capturar_evento(bool estado, bool estable, sensor_t sensor, void * referencia);

//! Declaracion para simular una notificacion de la entrada al sensor
extern void SensorEventoEntrada(bool valor, digital_entrada_t entrada, void * referencia);

/* === Definiciones de funciones internas ====================================================== */

void capturar_evento(bool estado, bool estable, sensor_t sensor, void * referencia) {
    llamada.estado = estado;
    llamada.estable = estable;
    llamada.sensor = sensor;
    llamada.referencia = referencia;
    llamada.ejecutada = true;
}

/* === Definiciones de funciones externas ====================================================== */

//! Fija las condiciones iniciales comunes a todas las pruebas
void setUp(void) {
    if (sensor == NULL) {
        sensor = SensorCrear(NULL, HISTERESIS);
    }
}

//! Limpia los resultados y objetos de todas las pruebas
void tearDown(void) {
}

//! @test Un sensor inicia en estado inactivo y estable
void test_sensor_inicia_inactivo(void) {
    // Dado un sensor recien creado
    sensor_t sensor = SensorCrear(NULL, HISTERESIS);
    // Entonces el descriptor devuelto es valido
    TEST_ASSERT_NOT_NULL(sensor);
    // Y el sensor no se encuentra inactivo
    TEST_ASSERT_FALSE(SensorActivo(sensor));
    // Y el sensor se encuentra en reposo
    TEST_ASSERT_TRUE(SensorReposo(sensor));
    // Y el sensor se encuentra en estado estable
    TEST_ASSERT_TRUE(SensorEstable(sensor));
}

//! @test El sensor cambia a inestable con una lectura diferente de la entrada
void test_sensor_cambia_a_inestable_con_una_lectura_diferente(void) {
    // Dado un sensor en estado inactivo
    EntradaLeer_fake.return_val = false;
    for(int indice = 0; indice < HISTERESIS + 1; indice++){
        SensorEspera(sensor);
    }
    TEST_ASSERT_FALSE(SensorActivo(sensor));

    // Y al cual se pidio la notificacion de eventos
    SensorNotificar(sensor, capturar_evento, referencia);
    llamada.ejecutada = false;

    // Cuando se lee la entrada digital como activa una vez
    EntradaLeer_fake.return_val = true;
    SensorEspera(sensor);

    // Entonces el sensor permanece inactivo
    TEST_ASSERT_FALSE(SensorActivo(sensor));
    
    // Y el sensor se encuentra en estado estable
    TEST_ASSERT_FALSE(SensorEstable(sensor));

    // Y el sensor ejecutó la funcion de notificacion con los parametros esperados
    TEST_ASSERT_TRUE(llamada.ejecutada);
    TEST_ASSERT_FALSE(llamada.estado);
    TEST_ASSERT_FALSE(llamada.estable);
    TEST_ASSERT_EQUAL(sensor, llamada.sensor);
    TEST_ASSERT_EQUAL(referencia, llamada.referencia);
}

/**
 *  @test Una cantidad de cambios en la entrada digital asociada
 *  igual al valor de  histeresis no provoca el cambio del sensor"
 */
void test_sensor_cambios_menores_hiteresis_permanece_inactivo(void) {
    // Dado un sensor en estado inactivo
    EntradaLeer_fake.return_val = false;
    for(int indice = 0; indice < HISTERESIS + 1; indice++){
        SensorEspera(sensor);
    }
    TEST_ASSERT_FALSE(SensorActivo(sensor));

    // Cuando se lee la entrada digital como activa una cantidad de veces menor que el valor de hsteresis
    EntradaLeer_fake.return_val = true;
    for(int indice = 0; indice < HISTERESIS; indice++){
        SensorEspera(sensor);
    }
    // Entonces el sensor permanece inactivo
    TEST_ASSERT_FALSE(SensorActivo(sensor));
}

/**
 *  @test Una cantidad de cambios en la entrada digital asociada
 *  mayor al valor de histeresis provoca el cambio del sensor
 */
void test_sensor_cambios_mayores_hiteresis_genera_un_cambio(void) {
    // Dado un sensor en estado inactivo
    EntradaLeer_fake.return_val = false;
    for(int indice = 0; indice < HISTERESIS + 1; indice++){
        SensorEspera(sensor);
    }
    TEST_ASSERT_FALSE(SensorActivo(sensor));

    // Y al cual se pidio la notificacion de eventos
    SensorNotificar(sensor, capturar_evento, referencia);
    llamada.ejecutada = false;

    // Cuando se lee la entrada digital como activa una cantidad de veces igual que el valor de hsteresis
    EntradaLeer_fake.return_val = true;
    for(int indice = 0; indice < HISTERESIS + 1; indice++){
        SensorEspera(sensor);
    }
    // Entonces el sensor permanece inactivo
    TEST_ASSERT_TRUE(SensorActivo(sensor));
    // Entonces el sensor no se encuentra en reposo
    TEST_ASSERT_FALSE(SensorReposo(sensor));

    // Y el sensor ejecutó la funcion de notificacion con los parametros esperados
    TEST_ASSERT_TRUE(llamada.ejecutada);
    TEST_ASSERT_TRUE(llamada.estado);
    TEST_ASSERT_TRUE(llamada.estable);
    TEST_ASSERT_EQUAL(sensor, llamada.sensor);
    TEST_ASSERT_EQUAL(referencia, llamada.referencia);
}

/**
 *  @test Una cantidad de cambios igual que la histeresis en la entrada
 *  digital asociada al sensor se descartan si son seguidos de una cantidad de lecturas mayor
 *  que la histeresis con el mismo valor que el estado actual del sensor
 */
void test_sensor_cambios_iguales_a_histeresis_se_descartan(void) {
    // Dado un sensor en estado inactivo
    EntradaLeer_fake.return_val = false;
    for(int indice = 0; indice < HISTERESIS + 1; indice++){
        SensorEspera(sensor);
    }
    TEST_ASSERT_FALSE(SensorActivo(sensor));

    // Cuando se lee la entrada digital como activa una cantidad de veces menor que el valor de histeresis
    EntradaLeer_fake.return_val = true;
    for(int indice = 0; indice < HISTERESIS; indice++){
        SensorEspera(sensor);
        
        // Entonces el sensor permanece inactivo
        TEST_ASSERT_FALSE(SensorActivo(sensor));
    }

    EntradaLeer_fake.return_val = false;
    for(int indice = 0; indice < HISTERESIS + 1; indice++){
        SensorEspera(sensor);
        
        // Entonces el sensor permanece inactivo
        TEST_ASSERT_FALSE(SensorActivo(sensor));
    }

    // Entonces el sensor permanece inactivo
    TEST_ASSERT_FALSE(SensorActivo(sensor));
    // Entonces el sensor permanece inactivo
    TEST_ASSERT_TRUE(SensorEstable(sensor));
}

//! @test Una sensor debe informar los cambios de estado a un gestor externo
void test_notificaciones_de_la_entrada_digital(void) {
    // Dado un sensor en estado inactivo
    EntradaLeer_fake.return_val = false;
    for(int indice = 0; indice < HISTERESIS + 1; indice++){
        SensorEspera(sensor);
    }
    TEST_ASSERT_FALSE(SensorActivo(sensor));

    // Y al cual se pidio la notificacion de eventos
    SensorNotificar(sensor, capturar_evento, referencia);
    llamada.ejecutada = false;

    // Cuando se lee la entrada digital como activa una cantidad de veces igual que el valor de hsteresis
    SensorEventoEntrada(true, NULL, (void *) sensor);

    // Entonces el sensor permanece inactivo
    TEST_ASSERT_FALSE(SensorActivo(sensor));

    // Y el sensor ejecutó la funcion de notificacion con los parametros esperados
    TEST_ASSERT_TRUE(llamada.ejecutada);
    TEST_ASSERT_FALSE(llamada.estado);
    TEST_ASSERT_FALSE(llamada.estable);
    TEST_ASSERT_EQUAL(sensor, llamada.sensor);
    TEST_ASSERT_EQUAL(referencia, llamada.referencia);
}
/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

