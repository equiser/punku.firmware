/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_autorizados.c
 ** @brief Pruebas unitarias del componente para gestion de la lista de autorizados
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.01.21 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "unity.h"
#include "autorizados.h"
#include "archivos.h"
#include "arbolb.h"
#include "editor.h"
#include "mock_errores.h"

#include "FreeRTOS.h"
#include "semphr.h"

/* === Definicion y Macros ===================================================================== */

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

//! Variable global con la referencia al objeto puerta bajo prueba
static autorizados_t autorizados;

//! Variable global con una referencia a utilizar en los eventos generados por la puerta
static void * referencia = (void *) 4;

static const tarjeta_t tarjetas[] = {
    12345678, 87654321, 1234321, 8765678, 18273645, 81726354, 43215678, 56784321
};

/* === Declaraciones de funciones internas ===================================================== */

/**
 * @brief Funcion auxiliar para probar el uso de mutex en las operaciones
 * 
 * @param   self        Referencia al objeto autorizados que se debe bloquear
 * @return  true        El bloqueo se pudo completar correctamente
 * @return  false       No se pudo completar el bloqueo del objeto autorizados
 */
extern bool TomarMutex(autorizados_t self);

/**
 * @brief Funcion auxiliar para probar el uso de mutex en las operaciones
 * 
 * @param   self        Referencia al objeto autorizados que se debe liberar
 * @return  true        El bloqueo se pudo liberar correctamente
 * @return  false       No se pudo liberar el bloqueo del objeto autorizados
 */
extern bool LiberarMutex(autorizados_t self);

bool ListarTarjetas(uint16_t indice, tarjeta_t tarjeta, void * referencia);

/* === Definiciones de funciones internas ====================================================== */

bool ListarTarjetas(uint16_t indice, tarjeta_t tarjeta, void * referencia) {
    char * cadena = referencia;
    if (strlen(cadena) == 0) {
        sprintf(referencia, "%d", tarjeta);
    } else {
        sprintf(referencia, "%s, %d", referencia, tarjeta);
    }
    return true;
}

/* === Definiciones de funciones externas ====================================================== */

//! Fija las condiciones iniciales comunes a todas las pruebas
void setUp(void) {
    autorizados = AutorizadosCrear();
}

//! Limpia los resultados y objetos de todas las pruebas
void tearDown(void) {

}
//! @test Todas las operaciones fallan si se envia una referencia nula
void test_operaciones_revisar_refrerencias_nulas(void) {    
    // Cuando llamo a la función para validar una tarjeta con un objeto nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(AutorizadosValidar(NULL, tarjetas[0]));

    // Cuando llamo a la función para agregar una tarjeta con un objeto nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(AutorizadosAgregar(NULL, tarjetas[0]));

    // Cuando llamo a la función para borrar una tarjeta con un objeto nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(AutorizadosBorrar(NULL, tarjetas[0]));

    // Cuando llamo a la función para limpiar la lista con un objeto nulo
    // Entonces la operacion devuelve un error
    TEST_ASSERT_FALSE(AutorizadosLimpiar(NULL));
}

//! @test No se pueden realizar dos operaciones simultaneamente
void test_las_operaciones_son_mutuamente_excluyentes(void) {
    // Dada una lista de autorizados con tarjetas conocidas
    TEST_ASSERT(AutorizadosLimpiar(autorizados));
    TEST_ASSERT(AutorizadosAgregar(autorizados, tarjetas[0]));

    // Cuando se esta realizando una operacion sobre la misma
    TEST_ASSERT(TomarMutex(autorizados));

    // Entonces no se pude relizar ninguna otra operacion 
    TEST_ASSERT_FALSE(AutorizadosAgregar(autorizados, tarjetas[1]));
    TEST_ASSERT_FALSE(AutorizadosValidar(autorizados, tarjetas[0]));
    TEST_ASSERT_FALSE(AutorizadosValidar(autorizados, tarjetas[1]));
    TEST_ASSERT_FALSE(AutorizadosBorrar(autorizados, tarjetas[0]));
    TEST_ASSERT_FALSE(AutorizadosBorrar(autorizados, tarjetas[1]));
    TEST_ASSERT(AutorizadosLimpiar(autorizados));

    TEST_ASSERT(LiberarMutex(autorizados));
}

//! @test No se autoriza el ingreso de una tarjeta cuando la lista esta vacia
void test_ingreso_denegado_en_lista_vacia(void) {
    // Dada una lista de autorizados vacia
    TEST_ASSERT(AutorizadosLimpiar(autorizados));

    char mensaje[32];
    for (int tarjeta = 0; tarjeta < sizeof(tarjetas) / sizeof(tarjeta_t); tarjeta++){
        sprintf(mensaje, "tarjeta %d", tarjetas[tarjeta]);

        // Entonces no se autoriza el ingreso de cualquier tarjeta
        TEST_ASSERT_FALSE_MESSAGE(AutorizadosValidar(autorizados, tarjetas[tarjeta]), mensaje);
    }
}

//! @test Se autoriza el ingreso al agregar una tarjeta en una lista vacia
void test_ingreso_autorizado_despues_de_agregar_en_lista_vacia(void) {
    char mensaje[32];
    for (int tarjeta = 0; tarjeta < sizeof(tarjetas) / sizeof(tarjeta_t); tarjeta++){
        sprintf(mensaje, "tarjeta %d", tarjetas[tarjeta]);

        // Dada una lista de autorizados vacia
        TEST_ASSERT(AutorizadosLimpiar(autorizados));
        
        // Cuando se agrega una tarjeta a la lista de autorizados
        TEST_ASSERT(AutorizadosAgregar(autorizados, tarjetas[tarjeta]));

        // Entonces se autoriza el ingreso de esa tarjeta
        TEST_ASSERT(AutorizadosValidar(autorizados, tarjetas[tarjeta]));
    }
}

//! @test Se autoriza el ingreso al agregar una tarjeta en una lista con datos
void test_ingreso_autorizado_despues_de_agregar_en_lista_con_datos(void) {
    // Dada una lista de autorizados vacia
    TEST_ASSERT(AutorizadosLimpiar(autorizados));
        
    char mensaje[32];
    for (int tarjeta = 0; tarjeta < sizeof(tarjetas) / sizeof(tarjeta_t); tarjeta++){
        sprintf(mensaje, "tarjeta %d", tarjetas[tarjeta]);

        // Cuando se agrega una tarjeta a la lista de autorizados
        TEST_ASSERT(AutorizadosAgregar(autorizados, tarjetas[tarjeta]));

        // Entonces se autoriza el ingreso de esa tarjeta
        TEST_ASSERT(AutorizadosValidar(autorizados, tarjetas[tarjeta]));
    }
    // Y todas las tarjetas agregadas tienen el acceso autorizado
    for (int tarjeta = 0; tarjeta < sizeof(tarjetas) / sizeof(tarjeta_t); tarjeta++){
        sprintf(mensaje, "tarjeta %d", tarjetas[tarjeta]);
        TEST_ASSERT(AutorizadosValidar(autorizados, tarjetas[tarjeta]));
    }
}

//! @test No se autoriza el ingreso de una tarjeta cuando se la borra de una lista con datos
void test_ingreso_denegado_despues_de_borrar_en_lista_con_datos(void) {
    // Dada una lista de autorizados con tarjetas conocidas
    TEST_ASSERT(AutorizadosLimpiar(autorizados));
    for (int tarjeta = 0; tarjeta < sizeof(tarjetas) / sizeof(tarjeta_t); tarjeta++){
        TEST_ASSERT(AutorizadosAgregar(autorizados, tarjetas[tarjeta]));
    }

    char mensaje[32];
    for (int tarjeta = 0; tarjeta < sizeof(tarjetas) / sizeof(tarjeta_t); tarjeta++){
        sprintf(mensaje, "tarjeta %d", tarjetas[tarjeta]);

        // Cuando se borra una tarjeta de la lista
        TEST_ASSERT(AutorizadosBorrar(autorizados, tarjetas[tarjeta]));

        // Entonces no se autoriza el ingreso de esa tarjeta
        TEST_ASSERT_FALSE(AutorizadosValidar(autorizados, tarjetas[tarjeta]));
    }
}

//! @test Se puede recuperar una lista de las tarjetas autorizadas
void test_lista_tarjetas_autorizadas(void) {
    static const tarjeta_t TARJETAS[] = {
        12, 23, 34, 45, 56, 67, 78, 89
    };
    static const char RESULTADO[] = "12, 23, 34, 45, 56, 67, 78, 89";
    char resultado[128];

    // Dada una lista de autorizados con tarjetas conocidas
    TEST_ASSERT(AutorizadosLimpiar(autorizados));
    for (int tarjeta = 0; tarjeta < sizeof(TARJETAS) / sizeof(tarjeta_t); tarjeta++){
        TEST_ASSERT(AutorizadosAgregar(autorizados, TARJETAS[tarjeta]));
    }

    memset(resultado, 0, sizeof(resultado));
    AutorizadosListar(autorizados, ListarTarjetas, resultado);
    TEST_ASSERT_EQUAL_STRING(RESULTADO, resultado);
}

//! @todo @test Cuando se borra una tarjeta de la lista el resto continuan autorizadas

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

