/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_puertas.c
 ** @brief Pruebas unitarias de la clase para gestion de la puerta
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.04.03 | evolentini      | Version inicial del archivo                             |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                   |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "unity.h"
#include "puertas.h"
#include "mock_digitales.h"
#include "mock_sensores.h"

/* === Definicion y Macros ===================================================================== */

//! Tiempo de liberacion de la cerradura utilizado en las pruebas
#define TIEMPO_LIBERACION       10

//! Tiempo de apertura de la puerta utilizado en las pruebas
#define TIEMPO_APERTURA         30

//! Tiempo de cierra de la puerta utilizado en las pruebas
#define TIEMPO_CIERRE           50

//! Macro para asegurar que la puerta genera un evento correctamente
#define TEST_ASSERT_EVENT(mensaje)                                                                  \
    TEST_ASSERT_TRUE_MESSAGE(evento.valido, "No se genero el evento");                              \
    TEST_ASSERT_EQUAL_MESSAGE(puerta, evento.puerta, "El objeto puerta no es el esperado");         \
    TEST_ASSERT_EQUAL_MESSAGE(referencia, evento.referencia, "La referencia no es la esperada");    \
    evento.valido = false

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

//! Variable global con una referencia a utilizar en los eventos generados por la puerta
static void * referencia = (void *) 4;

//! Variable global con la referencia al objeto puerta bajo prueba
static puerta_t puerta = NULL;

//! Variable global para simular el estado actual de los sensores
static struct {
    bool abierta;
    bool liberada;
    bool bloqueada;
    bool directa;
    bool inversa;
    bool alarma;
} gpio = {0};

//! Variable global para capturar los parametros de las llamadas a la funcion de notificación
static struct {
    bool valido;
    puerta_mensajes_t mensaje;
    puerta_t puerta;
    void * referencia;
} evento = {0};

static const puerta_recursos_t recursos = &(struct puerta_recursos_s) {
    .abierta = (sensor_t) &gpio.abierta,
    .liberada = (sensor_t) &gpio.liberada,
    .bloqueada = (sensor_t) &gpio.bloqueada,
    .directa = (digital_salida_t) &gpio.directa,
    .inversa = (digital_salida_t) &gpio.inversa,
    .alarma = (digital_salida_t) &gpio.alarma,
};

static struct puerta_configuracion_s configuracion = {
    .opciones = {
        .sensor = false, .inversor = false, .mecanismo = false
    },
    .tiempos = {
        .liberacion = 10, .apertura = 30, .cierre = 50  
    },
};

/* === Declaraciones de funciones internas ===================================================== */

/**
 * @brief Funcion para simular el resultado de una lectura en un sensor digital
 * 
 * @param   sensor      Referencia al objeto sensor digital que se quiere leer
 * @return  true        La entrada digital leida se encuentra activa
 * @return  false       La entrada digital leida se encuentra en reposo
 */
bool simular_activo(sensor_t sensor);

/**
 * @brief Funcion para simular el resultado de una lectura en un sensor digital
 * 
 * @param   sensor      Referencia al objeto sensor digital que se quiere leer
 * @return  true        La entrada digital leida se encuentra en reposo
 * @return  false       La entrada digital leida se encuentra activa
 */
bool simular_reposo(sensor_t sensor);

/**
 * @brief Funcion para simular la escritura en un terminal digital de salida
 * 
 * @param   salida      Referencia al objeto entrada digital que se quiere escribir
 * @param   estado      Nuevo estado que se desea asignar al terminal de salida
 */
void simular_escritura(digital_salida_t salida, bool estado);

/**
 * @brief Funcion para simular la activacion en un terminal digital de salida
 * 
 * @param   salida      Referencia al objeto entrada digital que se quiere activar
 */
void simular_prender(digital_salida_t salida);

/**
 * @brief Funcion para simular el apagado en un terminal digital de salida
 * 
 * @param   salida      Referencia al objeto entrada digital que se quiere apagar
 */
void simular_apagar(digital_salida_t salida);

/**
 * @brief Funcion para capturar las notificaciones de eventos de una puerta
 * 
 * @param   mensaje     Mensaje de estado generado por la puerta
 * @param   puerta      Referencia al objeto puerta que envia la notificacion
 * @param   referencia  Referencia del usuario definida cuando se registra el gestor de eventos
 */
void evento_puerta(puerta_mensajes_t mensaje, puerta_t puerta, void * referencia);

/**
 * @brief Función para generar una secuencia de eventos que simula el paso del tiempo
 * 
 * @param   cantidad    Cantidad de tiempo que se desea esperar en decimas de segundo
 */
static void esperar_tiempo(uint32_t cantidad);

/* === Definiciones de funciones internas ====================================================== */

bool simular_activo(sensor_t sensor) {
    return *((bool *)sensor);
}

bool simular_reposo(sensor_t sensor) {
    return !*((bool *)sensor);
}

void simular_escritura(digital_salida_t salida, bool estado) {
    *((bool *)salida) = estado;
}

void simular_prender(digital_salida_t salida) {
    *((bool *)salida) = true;
}

void simular_apagar(digital_salida_t salida) {
    *((bool *)salida) = false;
}

void evento_puerta(puerta_mensajes_t mensaje, puerta_t puerta, void * referencia) {
    evento.mensaje = mensaje;
    evento.puerta = puerta;
    evento.referencia = referencia;
    evento.valido = true;
}

static void esperar_tiempo(uint32_t cantidad) {
    int contador;

    for(contador = 0; contador < 10 * cantidad; contador++) {
        PuertaEspera(puerta);
        // printf("Contador %u -----\r\n", contador);
    }
}

/* === Definiciones de funciones externas ====================================================== */

//! Fija las condiciones iniciales comunes a todas las pruebas
void setUp(void) {
    SensorActivo_fake.custom_fake = simular_activo;
    SensorReposo_fake.custom_fake = simular_reposo;
    SalidaEscribir_fake.custom_fake = simular_escritura;
    SalidaPrender_fake.custom_fake = simular_prender;
    SalidaApagar_fake.custom_fake = simular_apagar;

    if (puerta == NULL) {
        puerta = PuertaCrear(recursos, &configuracion);
    }
    PuertaNotificar(puerta, evento_puerta, referencia);
}

//! Limpia los resultados y objetos de todas las pruebas
void tearDown(void) {

}

//! @test Se debe activar la salida de alarma cuando la puerta se abre sin autorizacion del equipo
void test_puerta_forzada(void) {
    // Dado un equipo con el sensor de puerta instalado
    configuracion.opciones.sensor = true;
    PuertaConfigurar(puerta, &configuracion);
    // Cuando la puerta se abre sin haber sin haber sido liberada
    gpio.abierta = true;
    esperar_tiempo(2);
    // Entonces se activa la salida de alarma
    TEST_ASSERT_TRUE(gpio.alarma);
    // Y se genera un evento informando que la puerta fue forzada
    TEST_ASSERT_EVENT(PUERTA_FORZADA);
    // TEST_ASSERT_EVENT(EVENTO_ENVIAR_MENSAJE, PUERTA_FORZADA, NULL, referencia);
    // Cuando despues de un tiempo arbitrario se cierra la puerta
    esperar_tiempo(5);
    gpio.abierta = false;
    esperar_tiempo(2);
    // Entonces se apaga la salida de alarma
    TEST_ASSERT_FALSE(gpio.alarma);
    // Y se genera un evento informando el cierre de la puerta
    TEST_ASSERT_EVENT(PUERTA_CERRO);
}

//! @test Liberacion de una puerta sin inversion y sin sensor de apertura
void test_sin_inversion_sin_sensor_entrada(void) {
    // Dada una puerta con una configuración conocida
    configuracion.opciones.sensor = false;
    configuracion.tiempos.apertura = TIEMPO_APERTURA;
    PuertaConfigurar(puerta, &configuracion);
    // Cuando se envia el comando para liberar la cerradura
    PuertaLiberar(puerta);
    // Entonces se activa la salida para liberar la cerradura
    TEST_ASSERT_TRUE(gpio.directa);
    // Cuando se completa el tiempo de espera
    esperar_tiempo(TIEMPO_APERTURA);
    // Entonces se apaga la salida para volver a bloquear la cerradura
    TEST_ASSERT_FALSE(gpio.directa);
    // Y se genera un evento informando que alguien entro por la puerta
    TEST_ASSERT_EVENT(PUERTA_ENTRO);
}

//! @test Liberacion sin entrada de una puerta sin inversion y con sensor de apertura
void test_sin_inversion_con_sensor_liberada_sin_entrar(void) {
    // Dada una puerta con una configuración conocida
    configuracion.opciones.sensor = true;
    configuracion.tiempos.apertura = TIEMPO_APERTURA;
    PuertaConfigurar(puerta, &configuracion);
    // Cuando se envia el comando para liberar la cerradura
    PuertaLiberar(puerta);
    // Entonces se activa la salida para liberar la cerradura
    TEST_ASSERT_TRUE(gpio.directa);
    // Cuando la puerta no se abre antes del tiempo maximo de espera
    esperar_tiempo(TIEMPO_APERTURA);
    // Entonces se apaga la salida para volver a bloquear la cerradura
    TEST_ASSERT_FALSE(gpio.directa);
    // Y se genera un evento informando que la puerta no se abrio
    TEST_ASSERT_EVENT(PUERTA_NO_ABRIO);
}

//! @test Apertura y cierre de una puerta sin inversion y con sensor de apertura
void test_sin_inversion_con_sensor_abrio_y_cerro(void) {
    // Dada una puerta con una configuración conocida
    configuracion.opciones.sensor = true;
    configuracion.tiempos.apertura = TIEMPO_APERTURA;
    configuracion.tiempos.cierre = TIEMPO_CIERRE;
    PuertaConfigurar(puerta, &configuracion);
    // Cuando se envia el comando para liberar la cerradura
    PuertaLiberar(puerta);
    // Entonces se activa la salida para liberar la cerradura
    TEST_ASSERT_TRUE(gpio.directa);
    // Cuando se abre la puerta antes del tiempo maximo de espera
    esperar_tiempo(TIEMPO_APERTURA - 5);
    gpio.abierta = true;
    esperar_tiempo(2);
    // Entonces se apaga la salida para volver a bloquear la cerradura
    TEST_ASSERT_FALSE(gpio.directa);
    // Cuando antes del tiempo maximo de espera se cierra la puerta
    esperar_tiempo(TIEMPO_CIERRE - 5);
    gpio.abierta = false;
    esperar_tiempo(2);
    // Entonces se genera un evento informando que alguien entro por la puerta
    TEST_ASSERT_EVENT(PUERTA_ENTRO);
}

//! @test Apertura sin cierre de una puerta sin inversion y con sensor de apertura
void test_sin_inversion_con_sensor_abrio_y_deja_puerta_abierta(void) {
    // Dada una puerta con una configuración conocida
    configuracion.opciones.sensor = true;
    configuracion.tiempos.apertura = TIEMPO_APERTURA;
    configuracion.tiempos.cierre = TIEMPO_CIERRE;
    PuertaConfigurar(puerta, &configuracion);
    // Cuando se envia el comando para liberar la cerradura
    PuertaLiberar(puerta);
    // Entonces se activa la salida para liberar la cerradura
    TEST_ASSERT_TRUE(gpio.directa);
    // Cuando se abre la puerta antes del tiempo maximo de espera
    esperar_tiempo(TIEMPO_APERTURA - 5);
    gpio.abierta = true;
    esperar_tiempo(2);
    // Entonces se apaga la salida para volver a bloquear la cerradura
    TEST_ASSERT_FALSE(gpio.directa);
    // Cuando despues del tiempo maximo de espera la puerta sigue abierta
    esperar_tiempo(TIEMPO_CIERRE);
    // Entonces se activa la salida de alarma
    TEST_ASSERT_TRUE(gpio.alarma);
    // Y se genera un evento informando que la puerta no se cerro
    TEST_ASSERT_EVENT(PUERTA_NO_CERRO);
    // Cuando despues de un tiempo arbitrario se cierra la puerta
    esperar_tiempo(5);
    gpio.abierta = false;
    esperar_tiempo(2);
    // Entonces se apaga la salida de alarma
    TEST_ASSERT_FALSE(gpio.alarma);
    // Y se egenra un evento informando el cierre de la puerta
    TEST_ASSERT_EVENT(PUERTA_CERRO);
}

//! @test Liberacion sin entrada de una puerta con inversion y con sensor de apertura
void test_con_inversion_con_sensor_liberada_sin_entrar(void) {
    // Dada una puerta con una configuración conocida
    configuracion.opciones.sensor = true;
    configuracion.opciones.inversor = true;
    configuracion.tiempos.liberacion = TIEMPO_LIBERACION;
    configuracion.tiempos.apertura = TIEMPO_APERTURA;
    PuertaConfigurar(puerta, &configuracion);
    // Cuando se envia el comando para liberar la cerradura
    PuertaLiberar(puerta);
    // Entonces se activa la salida para liberar la cerradura
    TEST_ASSERT_TRUE(gpio.directa);
    // Cuando transcurre el tiempo de liberacion
    esperar_tiempo(TIEMPO_LIBERACION);
    // Entonces se apaga la salida para liberar la cerradur
    TEST_ASSERT_FALSE(gpio.directa);
    // Cuando la puerta no se abre antes del tiempo maximo de espera
    esperar_tiempo(TIEMPO_APERTURA);
    // Entonces se activa la salida para bloquear la cerradura
    TEST_ASSERT_TRUE(gpio.inversa);
    // Cuando transcurre el tiempo de liberacion
    esperar_tiempo(TIEMPO_LIBERACION);
    // Entonces se apaga la salida para bloquear la cerradur
    TEST_ASSERT_FALSE(gpio.inversa);
    // Y se genera un evento informando que la puerta no se abrio
    TEST_ASSERT_EVENT(PUERTA_NO_ABRIO);
}

// //! @test pertura y cierre de una puerta con inversion y con sensor de apertura
// void test_con_inversion_con_sensor_abrio_y_cerro(void) {
//     // Dada una puerta con una configuración conocida
//     configuracion.opciones.sensor = true;
//     configuracion.opciones.inversor = true;
//     configuracion.tiempos.liberacion = TIEMPO_LIBERACION;
//     configuracion.tiempos.apertura = TIEMPO_APERTURA;
//     configuracion.tiempos.cierre = TIEMPO_CIERRE;
//     PuertaConfigurar(puerta, &configuracion);
//     // Cuando se envia el comando para liberar la cerradura
//     PuertaLiberar(puerta);
//     // Entonces se activa la salida para liberar la cerradura
//     TEST_ASSERT_TRUE(gpio.directa);
//     // Cuando transcurre el tiempo de liberacion
//     esperar_tiempo(TIEMPO_LIBERACION);
//     // Entonces se apaga la salida para liberar la cerradur
//     TEST_ASSERT_FALSE(gpio.directa);

//     // Cuando se abre la puerta antes del tiempo maximo de espera
//     esperar_tiempo(TIEMPO_APERTURA - 5);
//     gpio.abierta = true;
//     esperar_tiempo(2);

//     // Y antes del tiempo maximo de espera se cierra la puerta
//     esperar_tiempo(TIEMPO_CIERRE - 5);
//     gpio.abierta = false;
//     esperar_tiempo(2);

//     // Entonces se activa la salida para bloquear la cerradura
//     TEST_ASSERT_TRUE(gpio.inversa);
//     // Cuando transcurre el tiempo de liberacion
//     esperar_tiempo(TIEMPO_LIBERACION);
//     // Entonces se apaga la salida para bloquear la cerradur
//     TEST_ASSERT_FALSE(gpio.inversa);
//     // Y se genera un evento informando que la puerta no se abrio
//     TEST_ASSERT_EVENT(PUERTA_ENTRO);
// }
/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

