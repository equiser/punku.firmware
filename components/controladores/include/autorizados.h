/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef AUTORIZADOS_H   /*! @cond    */
#define AUTORIZADOS_H   /*! @endcond */

/** @file autorizados.h
 ** @brief Declaraciones de la libreria para la gestion de lista tarjetas autorizadas
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.01.21 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "tarjeta.h"
#include <stdbool.h>

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos para almacenar una referencia a una lista de tarjetas autorizadas
typedef struct autorizados_s * autorizados_t;

//! Prototipo de la funcion para procesar cada entrada al listar los autorizados
typedef bool (*autorizados_listar_t)(uint16_t indice, tarjeta_t tarjeta, void * referencia);

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear un objeto que gestiona la lista de tarjetas autorizadas
 * 
 * @return  autorizados_t   Referencia al objeto creado
 */
autorizados_t AutorizadosCrear(void);

/**
 * @brief Función para consultar si una tarjeta esta autorizada a ingresar
 * 
 * @param   autorizados     Referencia al objeto que gestiona la lista devuelta por el constructor 
 * @param   tarjeta         Numero de la tarjeta que se desea consultar
 * @return  true            La tarjeta esta autorizada a ingresar
 * @return  false           La tarjeta esta no autorizada a ingresar
 */
bool AutorizadosValidar(autorizados_t autorizados, tarjeta_t tarjeta);

/**
 * @brief Funcion para agregar una tarjeta a la lista de autorizados
 * 
 * @param   autorizados     Referencia al objeto que gestiona la lista devuelta por el constructor 
 * @param   tarjeta         Numero de la tarjeta que se desea agregar
 * @return  true            La tarjeta se pudo agregar correctamente y queda autorizada a ingresar
 * @return  false           No se pudo agregar correctamente la tarjeta soliticada
 */
bool AutorizadosAgregar(autorizados_t autorizados, tarjeta_t tarjeta);

/**
 * @brief Funcion para borrar una tarjeta a la lista de autorizados
 * 
 * @param   autorizados     Referencia al objeto que gestiona la lista devuelta por el constructor 
 * @param   tarjeta         Numero de la tarjeta que se desea borrar
 * @return  true            La tarjeta se pudo borrar correctamente y ya no puede ingresar
 * @return  false           No se pudo borrar correctamente la tarjeta soliticada
 */
bool AutorizadosBorrar(autorizados_t autorizados, tarjeta_t tarjeta);

/**
 * @brief Funcion para limpiar la lista de tarjetas autorizadas
 * 
 * @param   autorizados     Referencia al objeto que gestiona la lista devuelta por el constructor 
 * @return  true            La lista de tarjetas autorizadas se limpio correctamente
 * @return  false           No se pudo limpiar la lista de tarjetas autorizadas
 */
bool AutorizadosLimpiar(autorizados_t autorizados);

bool AutorizadosListar(autorizados_t autorizados, autorizados_listar_t procesador, void * referencia);


/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* BITACORA_H */
