/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef PUERTAS_H   /*! @cond    */
#define PUERTAS_H   /*! @endcond */

/** @file puertas.h
 ** @brief Declaraciones de la libreria para gestion de la puerta
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.04.03 | evolentini      | Version inicial del archivo                             |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                   |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include <stdbool.h>
#include "digitales.h"
#include "sensores.h"

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos para almacenar una referencia a un objeto puerta
typedef struct puerta_s * puerta_t;

//! Tipo de datos con los eventos enviados por la puerta
typedef enum {
    PUERTA_NO_ABRIO,        //!< La puerta se liberó pero no se abrió en el tiempo esperado
    PUERTA_ENTRO,           //!< La puerta se liberó, se abrió y se cerró correctamente
    PUERTA_NO_CERRO,        //!< La puerta se liberó, se abrió pero no se cerro en el tiempo esperado
    PUERTA_CERRO,           //!< La puerta se cerró despues de ser forzada o quedar abierta
    PUERTA_FORZADA,         //!< La puerta se abrió sin una liberación previa del equipo
} puerta_mensajes_t;

//! Tipo de datos para almacenar una referencia a una funcion que gestiona un evento de sensor
typedef void (*puerta_evento_t)(puerta_mensajes_t mensaje, puerta_t puerta, void * referencia);

//! Tipo de datos para la definicion de un tiempo de espera
typedef uint16_t tiempo_t;

//! Tipo de datos para almacenar los tiempos de operacion de una puerta
typedef struct puerta_tiempos_s {                    
    tiempo_t liberacion;            //!< Tiempo para el motor libere el mecanismo (PNK-PO005)
    tiempo_t apertura;              //!< Tiempo para que el usuario abra la puerta (PNK-PO004)
    tiempo_t cierre;                //!< Tiempo para que el usuario cierre la puerta (PNK-PO006)
} * puerta_tiempos_t;

//! Tipo de datos para almacenar las opciones de funcionamiento de una puerta
typedef struct puerta_opciones_s {                    
    bool sensor;                    //!< El sensor de puerta esta conectado (PNK-PO007)        
    bool inversor;                  //!< El actuador requiere inversion de polaridad (PNK-PO008)        
    bool mecanismo;                 //!< Los sensores del mecanismos estan conectados (PNK-PO009)
} * puerta_opciones_t;

//! Tipo de datos para almacenar la configuración de una puerta
typedef struct puerta_configuracion_s {
    struct puerta_opciones_s opciones;      //!< Estructura con las opciones de funcionamiento
    struct puerta_tiempos_s tiempos;        //!< Estructura con los tiempos de operacion
} const * puerta_configuracion_t;

//! Tipo de datos para almacenar los recursos de una puerta
typedef struct puerta_recursos_s {
    sensor_t abierta;               //!< Sensor digital para comprobar la apertura de la puerta
    sensor_t liberada;              //!< Sensor digital para comprobar la liberacion de la cerradura
    sensor_t bloqueada;             //!< Sensor digital para comprobar el bloqueo de la cerradura
    digital_salida_t directa;       //!< Salida digital para liberar la cerradura
    digital_salida_t inversa;       //!< Salida digital para bloquear la cerradura
    digital_salida_t alarma;        //!< Salida ditital para informar alarma
} const * puerta_recursos_t;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear una nueva instancia de un objeto puerta
 * 
 * @param   terminales      Referencia a la estructura con los terminales utilizados en la puerta
 * @param   configuracion   Referencia a la estructura con la configuracion inical de la puerta
 * @return  puerta_t        Referencia al nuevo objeto puerta creado
 */
puerta_t PuertaCrear(puerta_recursos_t recursos, puerta_configuracion_t configuracion);

/**
 * @brief Función para pedir a la puerta notificación de los eventos que genera
 * 
 * @param   puerta          Referencia al objeto puerta que debe geenrar los eventos
 * @param   gestor          Función de callback para informar de los eventos
 * @param   referencia      Referencia de usuario para enviar en los eventos generados
 */
void PuertaNotificar(puerta_t puerta, puerta_evento_t gestor, void * referencia);

/**
 * @brief Función para configurar los tiempos y opciones de una puerta
 * 
 * @param   puerta          Referencia al objeto puerta que se desea configurar
 * @param   configuracion   Referencia a la estructura con la configuracion inical de la puerta
 * @return  true            La puerta se configuro correctamente con los nuevos valores
 * @return  false           No se pudo configurar la puerta porque alguno de los valores es erroneo
 */
bool PuertaConfigurar(puerta_t puerta, puerta_configuracion_t configuracion);

/**
 * @brief Función para gesionar la espera de un tiempo en la puerta
 * 
 * @param   puerta          Referencia al objeto puerta que se desea actualizar
 */
void PuertaEspera(puerta_t puerta);

/**
 * @brief Función para liberar la puerta y permitir un ingreso
 * 
 * @param   puerta          Referencia al objeto puerta que se desea liberar
 */
void PuertaLiberar(puerta_t puerta);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* PUERTA_H */
