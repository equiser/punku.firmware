/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef SENSORES_H   /*! @cond    */
#define SENSORES_H   /*! @endcond */

/** @file sensores.h
 ** @brief Declaraciones de la clase para gestion de los sensores digitales
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.02.05 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "digitales.h"
#include <stdbool.h>

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

//! Tipo de datos para almacenar una referencia a un objeto de sensor digital
typedef struct sensor_s * sensor_t;

/**
 * @brief Referencia a una funcion para gestionar los eventos de cambios en las sensored digitales
 * 
 * @param[in]   estado      Indica el valor actual del sensor despues del cambio
 * @param[in]   estable     Indica si el valor del sensor se encuentra estable
 * @param[in]   sensor      Referencia al objeto sensor que notifica el evento de cambio
 * @param[in]   referencia  Referencia del usuario definida al registrar el gestor de eventos
 */
typedef void (*sensor_evento_t)(bool estado, bool estable, sensor_t sensor, void * referencia);

/* === Declaraciones de tipos de datos ========================================================= */

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear una nueva instancia de un sensor digital
 * 
 * @param[in]  entrada      Referencia al objeto entrada digital conectada al sensor
 * @param[in]  histeresis   Cantidad de muestras sucesivas que produce un cambio de estado       
 * @return     sensor_t     Referencia al nuevo objeto sensor digital creado
 */
sensor_t SensorCrear(digital_entrada_t entrada, uint8_t histeresis);

/**
 * @brief Función para solicitar a un objeto sensor digital la notificación de eventos
 * 
 * @param[in]   sensor      Referencia al objeto de terminal digital
 * @param[in]   gestor      Referencia a la funcion para informar de los eventos
 * @param[in]   referencia  Referencia de usuario para enviar con los eventos generados
 */
void SensorNotificar(sensor_t sensor, sensor_evento_t gestor, void * referencia);

/**
 * @brief Funcion para procesar los cambios de un sensor despues de una unidad de espera
 * 
 * @param[in]   sensor      Referencia al objeto sensor digital a procesar
 * @return      true        El sensor se encuentra en una transicion y hay una espera activa
 * @return      false       El sensor se encuentra estable y no hay una espera activa
 */
bool SensorEspera(sensor_t sensor);

/**
 * @brief Función para consultar si un sensor digital esta activo
 * 
 * @param[in]   sensor      Referencia al objeto sensor digital a consultar
 * @return      true        El sensor se encuentra activo
 * @return      false       El sensor se encuentra en reposo
 */
bool SensorActivo(sensor_t sensor);

/**
 * @brief Función para consultar si un sensor digital esta en reposo
 * 
 * @param[in]   sensor      Referencia al objeto sensor digital a consultar
 * @return      true        El sensor se encuentra reposo
 * @return      false       El sensor se encuentra en activo
 */
bool SensorReposo(sensor_t sensor);

/**
 * @brief Función para consultar si el estado de un sensor digital es estable
 * 
 * @param[in]   sensor      Referencia al objeto sensor digital a consultar
 * @return      true        El sensor se encuentra estable
 * @return      false       El sensor se encuentra en transicion
 */
bool SensorEstable(sensor_t sensor);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* SENSORES_H */
