/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef TARJETA_H   /*! @cond    */
#define TARJETA_H   /*! @endcond */

/** @file tarjeta.h
 ** @brief Declaraciones de la libreria para la gestion de las tarjetas RFID
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.09.14 | evolentini      | Version inicial del archivo para manejo de las tarjetas |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include <stdint.h>

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

#define ORIGEN_PULSADOR     0xFFFFFFFF

#define ORIGEN_EXTERNO      0x00000000

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos para almacenar el numero de una tarjeta de RFID
typedef uint32_t tarjeta_t;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* BITACORA_H */
