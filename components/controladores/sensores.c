/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file sensores.c
 ** @brief Implementacion de la clase para gestion de los sensores digitales
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.02.05 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "sensores.h"
#include <stddef.h>
#include <string.h>

/* === Definicion y Macros ===================================================================== */

//! Cantidad maxima de sensores digitales que se reservan en forma estatica
#ifndef SENSORES_CANTIDAD
    #define SENSORES_CANTIDAD       4
#endif

//! Macro para permitir la visibilidad de funciones protegidas durante las pruebas unitarias
#ifdef TEST
    #define protected 
#else
    #define protected static
#endif

/* == Declaraciones de tipos de datos internos ================================================= */

//! Estrutura para alamcenar los atributos de una instancia de un sensor digital
struct sensor_s {
    digital_entrada_t entrada;      //!< Referencia al objeto entrada digital del sensor
    sensor_evento_t gestor;         //!< Funcion para informar un evento de puerta
    void * referencia;              //!< Puntero para enviar una referencia al gestor de eventos
    uint32_t espera;                //!< Tiempo de espera para filtrado en las transiciones
    uint8_t histeresis;             //!< Cantidad de muestras que produce un cambio
    bool estado;                    //!< Estado actual del sensor
    bool ultimo;                    //!< Ultima lectura del valor del sensor
};

/* === Definiciones de variables internas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/** @brief Busca un nuevo descriptor de entrada sin usar
 *
 * @return Referencia al descriptor del nuevo objeto sensor 
 */
static sensor_t CrearInstancia(void);

/**
 * @brief Funcion para analizar una muestra de la entrada y generar los cambios en el estado del sensor
 * 
 * @param   self        Referencia al objeto sensor que se desea analizar
 * @param   activa      Estado actual de la entrada digital asociada al sensor
 * @return  true        El sensor se encuentra en un estado estable
 * @return  false       El sensor se encuentra en una transicion
 */
static bool SensorCambios(sensor_t  self, bool activa);

/**
 * @brief Funcion para gestionar el evento de cambio por interupciones de una entrada digital
 * 
 * @param   valor       Valor actual de la entrada digital que genera el evento
 * @param   entrada     Referencia al objeto entrada digital que genera el evento
 * @param   referencia  Referencia del usuario enviada al registrar el gesto de eventos
 */
protected void SensorEventoEntrada(bool valor, digital_entrada_t entrada, void * referencia);

/**
 * @brief Funcion para informar un evento de cambio en el estado del sensor a un gestor externo
 * 
 * @param   self        Referencia al objeto sensor que se genera el evento de cambio
 */
inline static void NotificarEvento(sensor_t self) {
    if (self->gestor) {
        self->gestor(self->estado, (self->espera == 0), self, self->referencia);
    }
}

/* === Definiciones de funciones internas ====================================================== */

sensor_t CrearInstancia(void) {
    sensor_t resultado = NULL;

    static int usados = 0;
    static struct sensor_s sensores[SENSORES_CANTIDAD] = {0};
    if (usados < sizeof(sensores) / sizeof(struct sensor_s)) {
        resultado = &sensores[usados];
        usados++;
    }

    return resultado;
}

bool SensorCambios(sensor_t  self, bool activa) {

    if (self->estado != activa) {
        if (self->espera == 0) {
            self->espera = self->histeresis;
            NotificarEvento(self);
        } else {
            self->espera--;
            if (self->espera == 0) {
                self->estado = activa;
                NotificarEvento(self);
            }
        }
    } else if (self->espera != 0) {
        if (self->ultimo != activa) {
            self->espera = self->histeresis;
        } else {
            self->espera--;
            if (self->espera == 0) {
                self->estado = activa;
                NotificarEvento(self);
            }
        }
    }
    self->ultimo = activa;
    return (self->espera == 0);
}

void SensorEventoEntrada(bool valor, digital_entrada_t entrada, void * referencia) {
    sensor_t self = referencia;
    SensorCambios(self, valor);
}

/* === Definiciones de funciones externas ====================================================== */

sensor_t SensorCrear(digital_entrada_t entrada, uint8_t histeresis) {
    sensor_t self = CrearInstancia();
    if (self != NULL) {
        memset(self, 0, sizeof(struct sensor_s));
        self->entrada = entrada;
        self->histeresis = histeresis;
    }
    return self;
}

void SensorNotificar(sensor_t self, sensor_evento_t gestor, void * referencia) {
    self->gestor = gestor;
    self->referencia = referencia;
    EntradaNotificar(self->entrada, SensorEventoEntrada, (void *) self);
}

bool SensorEspera(sensor_t self) {
    return SensorCambios(self, EntradaLeer(self->entrada));
}

bool SensorActivo(sensor_t sensor) {
    return (sensor->estado);
}

bool SensorReposo(sensor_t sensor) {
    return !(sensor->estado);
}

bool SensorEstable(sensor_t sensor) {
    return (sensor->espera == 0);
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

