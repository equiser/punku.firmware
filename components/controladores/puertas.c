/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file puertas.h
 ** @brief Declaraciones de la libreria para gestion de la puerta
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.04.03 | evolentini      | Version inicial del archivo                             |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                  |
 **|   3 | 2020.02.07 | evolentini      | Merjora en el manejo de los sensores                    |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
// #include "configuracion.h"
#include "puertas.h"
#include "errores.h"
#include "sensores.h"
#include <string.h>

/* === Definicion y Macros ===================================================================== */

//! Cantidad maxima de sensores digitales que se reservan en forma estatica
#ifndef PUERTAS_CANTIDAD
    #define PUERTAS_CANTIDAD                1
#endif

//! Muestra informacion de los nodos y las operaciones para facilitar la depuracion
#define INFORMACION_DEPURACION              0

//! Macro para mostrar informacion de depuracion durante las pruebas
#if defined(TEST) && (INFORMACION_DEPURACION != 0)
    #define InformarEstado(formato, ...)                                      \
        printf("%03d: " formato, __LINE__, ##__VA_ARGS__)
#else
    #define InformarEstado(formato, ...)
#endif /* TEST && REGISTRAR_PASOS */

/* == Declaraciones de tipos de datos internos ================================================= */

//! Estados de de una puerta
typedef enum puerta_estado_e {
    PUERTA_CERRADA,                 //!< La puerta esta cerrada y la cerradura bloqueada
    PUERTA_LIBERANDO,               //!< Se activo la salida y la cerradura se esta liberando
    PUERTA_LIBERADA,                //!< La puerta esta cerrada y la cerradura liberada
    PUERTA_BLOQUEANDO,              //!< Se activo la salida y la cerradura se esta bloqueando
    PUERTA_ESPERA,                  //!< Se espera que el usuario cierre la puerta
    PUERTA_ABIERTA,                 //!< La puerta permanece abierta mas tiempo de esperado
    PUERTA_ALARMA,                  //!< La puerta fué abierta sin liberar la cerradura
} puerta_estado_t;

//! Estructura que describe la información de una puerta
struct puerta_s {
    puerta_estado_t estado;         //!< Estado actual de la puerta
    puerta_evento_t gestor;         //!< Referencia a la funcion a la que se deben notificar los eventos
    void * referencia;              //!< Puntero para enviar una referencia al gestor de eventos
    tiempo_t espera;                //!< Valor de espera para volver a actualizar el estado
    puerta_tiempos_t tiempos;       //!< Referencia a la estructura con las opciones de funcionamiento
    puerta_opciones_t opciones;     //!< Referencia a la estructura con los tiempos de operacion
    sensor_t abierta;               //!< Referencia al objeto sensor de puerta abierta
    sensor_t liberada;              //!< Referencia al objeto sensor de puerta liberada
    sensor_t bloqueada;             //!< Referencia al objeto sensor de puerta bloqueada
    digital_salida_t directa;       //!< Referencia al objeto salida digital para liberar la puerta
    digital_salida_t inversa;       //!< Referencia al objeto salida digital para bloquear la puerta
    digital_salida_t alarma;        //!< Referencia al objeto salida digital para alertar de una alarma
    // struct puerta_configuracion_s configuracion;
};

/* === Definiciones de variables internas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/** @brief Busca un nuevo descriptor de entrada sin usar
 *
 * @return Referencia al descriptor del nuevo objeto puerta 
 */
static puerta_t CrearInstancia(void);

/**
 * @brief Funcion para generar e notificar un evento 
 * 
 * @param[in]  self      Referencia al objeto puerta
 * @param[in]  mensaje   Mensaje que se desea notificar
 */
inline static void InformarEvento(puerta_t self, puerta_mensajes_t mensaje) {
    if (self->gestor) self->gestor(mensaje, self, self->referencia);
}

/* === Definiciones de funciones internas ====================================================== */
static puerta_t CrearInstancia(void) {
    puerta_t resultado = NULL;

    static int usadas = 0;
    static struct puerta_s puertas[PUERTAS_CANTIDAD] = {0};
    static struct puerta_tiempos_s tiempos[PUERTAS_CANTIDAD] = {0};
    static struct puerta_opciones_s opciones[PUERTAS_CANTIDAD] = {0};
    // puerta_tiempos_t tiempos = &(struct puerta_tiempos_s)[PUERTAS_CANTIDAD]{0};
    if (usadas < sizeof(puertas) / sizeof(struct puerta_s)) {
        memset(&puertas[usadas], 0, sizeof(struct puerta_s));
        memset(&tiempos[usadas], 0, sizeof(struct puerta_tiempos_s));
        memset(&opciones[usadas], 0, sizeof(struct puerta_opciones_s));
        resultado = &puertas[usadas];
        resultado->tiempos = &tiempos[usadas];
        resultado->opciones = &opciones[usadas];
        usadas++;
    }

    return resultado;
}

static void SinInversionSinSensor(puerta_t self) {
    switch (self->estado) {
        case PUERTA_LIBERADA:
            if (self->espera == 0) {
                SalidaApagar(self->directa);
                self->estado = PUERTA_CERRADA;
                InformarEvento(self, PUERTA_ENTRO);
            } else {
                self->espera--;
            }
        default:
            break;
    };        
}

static void SinInversionConSensor(puerta_t self) {
    switch (self->estado) {
        case PUERTA_CERRADA:
            if (SensorActivo(self->abierta)) {
                self->estado = PUERTA_ALARMA;
                SalidaPrender(self->alarma);
                InformarEvento(self, PUERTA_FORZADA);
            }
            break;
        case PUERTA_LIBERADA:
            if (self->espera == 0) {
                SalidaApagar(self->directa);
                self->estado = PUERTA_CERRADA;
                InformarEvento(self, PUERTA_NO_ABRIO);
            } else if (SensorActivo(self->abierta)) {
                SalidaApagar(self->directa);
                self->espera = self->tiempos->cierre - 1;
                self->estado = PUERTA_ABIERTA;
            } else {
                self->espera--;
            }
            break;
        case PUERTA_ABIERTA:
            if (SensorReposo(self->abierta)) {    
                self->estado = PUERTA_CERRADA;
                InformarEvento(self, PUERTA_ENTRO);
            } else if (self->espera == 0) {
                SalidaPrender(self->alarma);
                InformarEvento(self, PUERTA_NO_CERRO);
                self->estado = PUERTA_ALARMA;
            } else {
                self->espera--;
            }
            break;
        case PUERTA_ALARMA:
            if (SensorReposo(self->abierta)) {
                self->estado = PUERTA_CERRADA;
                InformarEvento(self, PUERTA_CERRO);
                SalidaApagar(self->alarma);
            }        
            break;

        default:
            break;
    }
}

static void ConInversionSinSensor(puerta_t self) {

}

static void ConInversionConSensor(puerta_t self) {
    switch (self->estado) {
        case PUERTA_CERRADA:
            if (SensorActivo(self->abierta)) {
                self->estado = PUERTA_ALARMA;
                SalidaPrender(self->alarma);
                InformarEvento(self, PUERTA_FORZADA);
            }
            break;
        case PUERTA_LIBERANDO:
            if (self->espera == 0) {
                SalidaApagar(self->directa);
                self->espera = self->tiempos->apertura - 1;
                self->estado = PUERTA_LIBERADA;
            } else {
                self->espera--;
            }
            break;

        case PUERTA_LIBERADA:
            if (self->espera == 0) {
                SalidaPrender(self->inversa);
                self->espera = self->tiempos->liberacion - 1;
                self->estado = PUERTA_BLOQUEANDO;
                InformarEvento(self, PUERTA_NO_ABRIO);
            } else if (SensorActivo(self->abierta)) {
                SalidaApagar(self->directa);
                self->espera = self->tiempos->cierre - 1;
                self->estado = PUERTA_ABIERTA;
            } else {
                self->espera--;
            }
            break;

        case PUERTA_BLOQUEANDO:
            if (self->espera == 0) {
                SalidaApagar(self->inversa);
                self->estado = PUERTA_CERRADA;
            } else {
                self->espera--;
            }
            break;

        case PUERTA_ABIERTA:
            if (SensorReposo(self->abierta)) {                
                SalidaPrender(self->inversa);
                self->espera = self->tiempos->liberacion - 1;
                self->estado = PUERTA_BLOQUEANDO;
                InformarEvento(self, PUERTA_ENTRO);
            } else if (self->espera == 0) {
                SalidaPrender(self->alarma);
                InformarEvento(self, PUERTA_NO_CERRO);
                self->estado = PUERTA_ALARMA;
            } else {
                self->espera--;
            }
            break;
        case PUERTA_ALARMA:
            if (SensorReposo(self->abierta)) {
                SalidaApagar(self->alarma);
                SalidaPrender(self->inversa);
                self->espera = self->tiempos->liberacion - 1;
                self->estado = PUERTA_BLOQUEANDO;
                InformarEvento(self, PUERTA_CERRO);
            }        
            break;

        default:
            break;
    }
}

static void PuertaCambios(puerta_t self) {
    if (self->opciones->inversor) {
        if (self->opciones->sensor) {
            ConInversionConSensor(self);
        } else {
            ConInversionSinSensor(self);
        }
    } else {
        if (self->opciones->sensor) {
            SinInversionConSensor(self);
        } else {
            SinInversionSinSensor(self);
        }
    }

    InformarEstado("Estado %u, Espera %u\r\n", self->estado, self->espera);
}

/* === Definiciones de funciones externas ====================================================== */
puerta_t PuertaCrear(puerta_recursos_t recursos, puerta_configuracion_t configuracion) {
    puerta_t self = CrearInstancia();

    if (self != NULL) {
        self->estado = PUERTA_CERRADA;
        self->abierta = recursos->abierta;
        self->liberada = recursos->liberada;
        self->bloqueada = recursos->bloqueada;
        self->directa = recursos->directa;
        self->inversa = recursos->inversa;
        self->alarma = recursos->alarma;
        PuertaConfigurar(self, configuracion);
    }
    return self;
}

void PuertaNotificar(puerta_t self, puerta_evento_t gestor, void * referencia) {
    if (self != NULL) {
        self->gestor = gestor;
        self->referencia = referencia;
    }
}

bool PuertaConfigurar(puerta_t self, puerta_configuracion_t configuracion) {
    bool resultado = false;

    if (self != NULL) {
        memcpy(self->tiempos, &configuracion->tiempos, sizeof(struct puerta_tiempos_s));
        memcpy(self->opciones, &configuracion->opciones, sizeof(struct puerta_opciones_s));
        self->estado = PUERTA_CERRADA;
        resultado = true;
    }
    return resultado;
}

void PuertaEspera(puerta_t self) {
    static int divisor = 0;

    SensorEspera(self->abierta);
    // SensorEspera(self->liberada);
    // SensorEspera(self->bloqueada);
    
    if (divisor == 0) {
        PuertaCambios(self);
        divisor = 9;
    } else {
        divisor--;
    }
}

void PuertaLiberar(puerta_t self) {
    if (self->estado == PUERTA_CERRADA) {
        SalidaPrender(self->directa);
        if (self->opciones->inversor) {
            self->espera = self->tiempos->liberacion - 1;
            self->estado = PUERTA_LIBERANDO;
        } else {
            self->espera = self->tiempos->apertura - 1;
            self->estado = PUERTA_LIBERADA;
        }
    }
}
/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

