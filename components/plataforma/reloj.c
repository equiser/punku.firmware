/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file reloj.c
 ** @brief Implementacion de la clase para gestion del reloj de tiempo real
 **
 ** Libreria para el manejo del reloj de tiempo real como fuente de fecha y hora del sistema.
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.05.30 | evolentini      | Version inicial del archivo de gestion del reloj RTC    |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                  |
 **
 ** @addtogroup plataforma
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include <string.h>
#include <assert.h>
#include "errores.h"
#include "include/reloj.h"
#ifdef TEST
    #include "i2c.h"
    #include "gpio.h"
#else
    #include "driver/i2c.h"
    #include "driver/gpio.h"
#endif

/* === Definicion y Macros ===================================================================== */

//! Puerto I2C a utilizar para la comunicación con el RTC
#define I2C_PUERTO      I2C_NUM_0

//! Numero del terminal que se utiliza para la señal SDA del puerto I2C
#define I2C_GPIO_SDA    14

//! Numero del terminal que se utiliza para la señal SCL del puerto I2C
#define I2C_GPIO_SCL    27

//! Frecuencia a la que opera el puerto I2C
#define I2C_FRECUENCIA  100000

//! Dirección del integrado RTC en el bus I2C
#define I2C_DIRECCION   0x6F

//! Dirección del registro de los segundos actuales en el RTC
#define RTCSEC          0x00

//! Dirección del registro de los minutos actuales en el RTC
#define RTCMIN          0x01

//! Dirección del registro de la hora actual en el RTC
#define RTCHOUR         0x02

//! Dirección del registro del dia actual de la semana en el RTC
#define RTCWKDAY        0x03

//! Dirección del registro del dia actual del mes en el RTC
#define RTCDATE         0x04

//! Dirección del registro del mes actual en el RTC
#define RTCMTH          0x05

//! Dirección del registro del año actual en el RTC
#define RTCYEAR         0x06

//! Bit que informa el estado de funciomiento del RTC
#define ST              (1 << 7)

//! Bit que habilita el oscilador del RTC
#define OSCRUN          (1 << 5)

//! Bit que indica un corte de energia en el RTC
#define PWRFAIL         (1 << 4)

//! Bit que habilita el uso de la bateria de respaldo en el RTC
#define VBATEN          (1 << 3)

/* == Declaraciones de tipos de datos internos ================================================= */

//! Estructura que almacena los atributos de un objeto reloj
struct reloj_s {
    bool configurado;   //!< Indica que el reloj ya fué configurado
};

/* === Definiciones de variables internas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/**
 * @brief Función para convertir un valor binario en un BCD compactado
 * 
 * @param[in]  valor  Valor binario que se desea convertir
 * @return            Valor BCD compactado equivalente al valor recibido
 */
uint8_t DecodificarBCD(uint8_t valor);

/**
 * @brief Función para convertir un valor BCD compactado en un valor binario
 * 
 * @param[in]  valor  Valor BCD compactado que se desea convertir
 * @return            Valor binario equivalente al valor recibido
 */
uint8_t CodificarBCD(uint8_t valor);

/* === Definiciones de funciones internas ====================================================== */
uint8_t DecodificarBCD(uint8_t valor) {
    return ((10 * ((valor & 0xF0) >> 4)) + (valor & 0x0F));
}

uint8_t CodificarBCD(uint8_t valor) {
    return (((valor / 10) % 10) << 4) | (valor % 10);
}

/* === Definiciones de funciones externas ====================================================== */
reloj_t RelojCrear(void) {
    static struct reloj_s reloj = {0};
    reloj_t self = &reloj;

    if (!self->configurado) {
        static const i2c_config_t configuracion = {
            .mode = I2C_MODE_MASTER,
            .sda_io_num = I2C_GPIO_SDA,
            .sda_pullup_en = GPIO_PULLUP_ENABLE,
            .scl_io_num = I2C_GPIO_SCL,
            .scl_pullup_en = GPIO_PULLUP_ENABLE,
            .master.clk_speed = I2C_FRECUENCIA
        };
        i2c_param_config(I2C_PUERTO, &configuracion);
        i2c_driver_install(I2C_PUERTO, I2C_MODE_MASTER, false, false, 0);
        self->configurado = true;
    }
    return self;
};

void RelojActualizar(reloj_t self, const struct tm * fecha) {
    i2c_cmd_handle_t enlace;
    esp_err_t resultado;
    uint8_t datos[7];
    
    assert(self);
    assert(self->configurado);

    datos[RTCSEC] = CodificarBCD(fecha->tm_sec) | ST;
    datos[RTCMIN] = CodificarBCD(fecha->tm_min);
    datos[RTCHOUR] = CodificarBCD(fecha->tm_hour);
    datos[RTCWKDAY] = CodificarBCD(fecha->tm_wday + 1) | OSCRUN | VBATEN;
    datos[RTCDATE] = CodificarBCD(fecha->tm_mday);
    datos[RTCMTH] = CodificarBCD(fecha->tm_mon + 1);
    datos[RTCYEAR] = CodificarBCD(fecha->tm_year - 100);

    enlace = i2c_cmd_link_create();
    i2c_master_start(enlace);
    i2c_master_write_byte(enlace, I2C_DIRECCION << 1 | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(enlace, RTCSEC, true);
    i2c_master_write(enlace, datos, sizeof(datos), I2C_MASTER_LAST_NACK);
    i2c_master_stop(enlace);
    resultado = i2c_master_cmd_begin(I2C_PUERTO, enlace, 100);
    i2c_cmd_link_delete(enlace);
    if (resultado == ESP_OK) {
        Registrar(INFORMACION, "Acutalizacion del reloj de tiempo real");
    } else {
        Registrar(ERROR, "No se pudo escribir el reloj de tiempo real");
    }
};  

void RelojConsultar(reloj_t self, struct tm * fecha) {
    i2c_cmd_handle_t enlace;
    esp_err_t resultado;
    uint8_t datos[7];

    assert(self);
    assert(self->configurado);

    enlace = i2c_cmd_link_create();
    i2c_master_start(enlace);
    i2c_master_write_byte(enlace, I2C_DIRECCION << 1 | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(enlace, RTCSEC, true);
    i2c_master_start(enlace);
    i2c_master_write_byte(enlace, I2C_DIRECCION << 1 | I2C_MASTER_READ, true);
    i2c_master_read(enlace, datos, sizeof(datos), I2C_MASTER_LAST_NACK);
    i2c_master_stop(enlace);
    resultado = i2c_master_cmd_begin(I2C_PUERTO, enlace, 100);
    i2c_cmd_link_delete(enlace);

    if (resultado == ESP_OK) {
        fecha->tm_sec = DecodificarBCD(datos[RTCSEC] & 0x7F);
        fecha->tm_min = DecodificarBCD(datos[RTCMIN]);
        fecha->tm_hour = DecodificarBCD(datos[RTCHOUR] & 0x3F);
        fecha->tm_wday = DecodificarBCD(datos[RTCWKDAY] & 0x07) - 1;
        fecha->tm_mday = DecodificarBCD(datos[RTCDATE] & 0x3F);
        fecha->tm_mon = DecodificarBCD(datos[RTCMTH] & 0x1F) - 1;
        fecha->tm_year = DecodificarBCD(datos[RTCYEAR]) + 100;
    } else {
        Registrar(ERROR, "No se pudo leer el reloj de tiempo real");
    }
};

#ifdef TEST
void RelojDestruir(reloj_t self) {
    self->configurado = false;
}
#endif

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

