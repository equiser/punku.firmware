/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file archivos.c
 ** @brief Declaraciones de la libreria para abstracion del reloj de tiempo real
 **
 ** Libreria para el manejo del reloj de tiempo real como fuente de fecha y hora del sistema.
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.05.30 | evolentini      | Version inicial del archivo para manejo de teclado      |
 **
 ** @addtogroup plataforma
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "archivos.h"
#include "errores.h"
#ifndef TEST
    #include "esp_err.h"
    #include "esp_log.h"
    #include "esp_vfs_fat.h"
    #include "driver/sdmmc_host.h"
    #include "driver/sdspi_host.h"
    #include "sdmmc_cmd.h"
#endif
#include <stdio.h>
#include <string.h>
#include <sys/unistd.h>
#include <sys/stat.h>

/* === Definicion y Macros ===================================================================== */

//! Numero del terminal que se utiliza para la señal MISO del puerto SPI
#define PIN_NUM_MISO 4
//! Numero del terminal que se utiliza para la señal MOSI del puerto SPI
#define PIN_NUM_MOSI 13
//! Numero del terminal que se utiliza para la señal CLK del puerto SPI
#define PIN_NUM_CLK  12
//! Numero del terminal que se utiliza para la señal CS del puerto SPI
#define PIN_NUM_CS   15

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

//! Variable global que indica que el sistema de archivos esta disponible
bool disponible = false;

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de funciones internas ====================================================== */

/* === Definiciones de funciones externas ====================================================== */
bool ArchivosConfigurar(void){
#ifndef TEST
    esp_err_t resultado;

    Registrar(DEPURACION, "Iniciando el sistema de arhivos");

    sdmmc_card_t * card;
    sdmmc_host_t host = SDSPI_HOST_DEFAULT();
    sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();    
    esp_vfs_fat_sdmmc_mount_config_t mount_config = {
        .max_files = 5,
        .format_if_mount_failed = true,
        .allocation_unit_size = 16 * 1024
    };
    host.slot = HSPI_HOST;
    slot_config.gpio_miso = PIN_NUM_MISO;
    slot_config.gpio_mosi = PIN_NUM_MOSI;
    slot_config.gpio_sck  = PIN_NUM_CLK;
    slot_config.gpio_cs   = PIN_NUM_CS;

    resultado = esp_vfs_fat_sdmmc_mount("/sdcard", &host, &slot_config, &mount_config, &card);
    if (resultado != ESP_OK) {
        if (resultado == ESP_FAIL) {
            Error("Error en el sistema de archivos");
        } else {
            // Error("Error en la tarjeta SD (%s)", esp_err_to_name(resultado));
            Error("Error en la tarjeta SD");
        }
    } else {
        Registrar(DEPURACION, "Sistema de archivos inciado correctamente");
        disponible = true;
    }
#else
    disponible = true;
#endif
    return disponible;
}

archivo_t ArchivoAbrir(const char * nombre, const char * modo) {
#ifndef TEST
    char completo[32];
    sprintf(completo, "/sdcard/%s", nombre);
    return fopen(completo, modo);
#else
    return fopen(nombre, modo);
#endif
}

bool ArchivoLeer(archivo_t archivo, void * datos, uint32_t longitud) {
    return (1 == fread(datos, longitud, 1, archivo));
}

bool ArchivoEscribir(archivo_t archivo, const void * datos, uint32_t longitud) {
    return (1 == fwrite(datos, longitud, 1, archivo));
}

bool ArchivoMover(archivo_t archivo, uint32_t desplazamiento, archivo_referencia_t referencia) {
    return (0 == fseek(archivo, desplazamiento, referencia));
}

bool ArchivoCerrar(archivo_t archivo) {
    return (0 == fclose(archivo));
}

bool ArchivoBorrar(const char * nombre) {
#ifndef TEST
    char completo[32];
    sprintf(completo, "/sdcard/%s", nombre);
    return (0 == unlink(completo));
#else
    return (0 == remove(nombre));
#endif
}

bool ArchivoCompletar(archivo_t archivo) {
    return (0 == fflush(archivo));
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */
