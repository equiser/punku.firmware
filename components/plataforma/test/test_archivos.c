/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_archivos.c
 ** @brief Pruebas unitarias de la clase para abstracion del sistema de archivos
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2020.03.20 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup plataforma
 ** @{ */


/* === Inclusiones de cabeceras ================================================================ */
#include <string.h>
#include "unity.h"
#include "archivos.h"

/* === Definicion y Macros ===================================================================== */

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de funciones internas ====================================================== */

/* === Definiciones de funciones externas ====================================================== */

//! Fija las condiciones iniciales comunes a todas las pruebas
void setUp(void) {

}

//! Limpia los resultados y objetos de todas las pruebas
void tearDown(void) {

}

void test_inicio_sistema_de_archivos(void) {
    // Cuando se inicia un sistema de archivos formateado y sin errores
    // Entonces el proceso de inicio es existoso
    TEST_ASSERT_TRUE(ArchivosConfigurar());
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

