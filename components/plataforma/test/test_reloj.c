/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_reloj.c
 ** @brief Pruebas unitarias de la clase para gestion del reloj de tiempo real
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.04.03 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup plataforma
 ** @{ */

/**
 * @todo Revisar que el RTC esta funcionando antes de leer la hora
 * @todo Agregar pruebas para la gestion de errore en la comunicacion
 */

/* === Inclusiones de cabeceras ================================================================ */
#include <string.h>
#include "unity.h"
#include "reloj.h"
#include "mock_i2c.h"
#include "mock_gpio.h"
#include "mock_errores.h"

/* === Definicion y Macros ===================================================================== */

//! Cantidad de registros a leer o escribir en el RTC
#define RTC_REGISTROS  7

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

// Arreglo de estructuras con los valores de fecha y registros del RTC correspondientes
static const struct {
    char * texto;
    struct tm fecha;
    uint8_t registros[RTC_REGISTROS];
} ejemplos[] = {
    {  
        .texto = "2019-11-17 21:34:45",
        .fecha = {
            .tm_sec = 45,
            .tm_min = 34,
            .tm_hour = 21,
            .tm_mday = 17,
            .tm_mon = 10,
            .tm_year = 119,
            .tm_wday = 0,
        },
        .registros = {
            0x45 | (1 << 7),             // RTCSEC  : ST = 1, SECTEN = 4, SECONE = 5
            0x34,                        // RTCMIN  : MINTEN = 3, MINONE = 4
            0x21,                        // RTCHOUR : 12/24 = 0, HRTEN = 2, HRONE = 1
            0x01 | (1 << 5) | (1 << 3),  // RTCWKDAY: OSCRUN = 1, PWRFAIL = 0, VBATEN = 1, WKDAY = 1
            0x17,                        // RTCDATE : DATETEN = 1, DATEONE = 7
            0x11,                        // RTCMONTH: LPYR = 0, MTHTEN = 1, MTHONE = 1
            0x19,                        // RTCYEAR = 19
        } 
    },
};

//! Variable para indicar a las funciones de prueba que datos deben inyectar
static int ejemplo_actual = 0;

//! Variable para almacenar los datos capturados por las funciones de prueba
uint8_t datos_escritos[256] = {0};

//! Variable global cn la refrencia al objeto reloj para ejecutar las pruebas
reloj_t reloj;

/* === Declaraciones de funciones internas ===================================================== */

/**
 * @brief Función para simular la lectura de datos desde el puerto I2C
 * 
 * @param[in]   comandos    Refrencia a la secuencia de comandos por la cual se lee los datos
 * @param[out]  datos       Puntero a la variable donde se almacenaran los datos leidos
 * @param[in]   longitud    Cantidad de datos que se deben leer del bus
 * @param[in]   ack         Indicación de envio del ACK para terminar la operación del bus I2C
 * @return                  Resultado de la operación de lectura del bus I2C
 */
esp_err_t simular_datos_leidos(i2c_cmd_handle_t comandos, uint8_t * datos, size_t longitud, i2c_ack_type_t ack);

/**
 * @brief Función para capturar los datos escritos en el puerto I2C
 * 
 * @param[in]   comandos    Refrencia a la secuencia de comandos por la cual se escriben los datos
 * @param[out]  datos       Puntero a la variable que contiene los datos a escribir en el bus
 * @param[in]   longitud    Cantidad de datos que se deben escribir del bus
 * @param[in]   ack         Indicación de envio del ACK para terminar la operación del bus I2C
 * @return                  Resultado de la operación de escritura del bus I2C
 */
esp_err_t capturar_datos_escritos(i2c_cmd_handle_t comandos, uint8_t * datos, size_t longitud, bool ack);

/* === Definiciones de funciones internas ====================================================== */

esp_err_t simular_datos_leidos(i2c_cmd_handle_t enlace, uint8_t * datos, size_t longitud, i2c_ack_type_t ack) {
    memcpy(datos, &ejemplos[ejemplo_actual].registros, longitud);
    return 0;
}

esp_err_t capturar_datos_escritos(i2c_cmd_handle_t enlace, uint8_t * datos, size_t longitud, bool ack) {
    memcpy(datos_escritos, datos, longitud);
    return 0;
}

/* === Definiciones de funciones externas ====================================================== */

//! Fija las condiciones iniciales comunes a todas las pruebas
void setUp(void) {
    // Limpieza de los resultados de las pruebas anteriores
    FFF_RESET_HISTORY();
    RESET_FAKE(i2c_master_read)
    RESET_FAKE(i2c_master_write);
    RESET_FAKE(i2c_master_write_byte);
    
    // Creación de la instanacia de reloj para la prueba
    reloj = RelojCrear();
}

//! Limpia los resultados y objetos de todas las pruebas
void tearDown(void) {
    // Declaración de la funcion auxiliar para restaurar la configuracion de la variable reloj
    extern void RelojDestruir(reloj_t self);

    // Restauración de la variable reloj al estado inicial de la priemra ejecución
    RelojDestruir(reloj);
}

//! @test Configuracion inicial del puerto I2C
void test_configuracion_reloj_tiempo_real(void) {
    // Verificacion de la creación de una única instancia
    TEST_ASSERT_EQUAL(reloj, RelojCrear());

    // Verficación de la configuración del puerto en el orden correcto
    TEST_ASSERT_SEQUENCE_START(i2c_param_config);
    TEST_ASSERT_SEQUENCE_NEXT(i2c_driver_install);

    // Verificación de la la configuración del puerto I2C una sola vez
    TEST_ASSERT_CALLED_TIMES(1, i2c_param_config);
    TEST_ASSERT_CALLED_TIMES(1, i2c_driver_install);
}

//! @test Verificacion del contenido de la estructura de fecha como texto
void test_formato_de_fecha(void) {
    ejemplo_actual = 0;
    char resultado[64];

    strftime(resultado,sizeof(resultado),  "%Y-%m-%d %H:%M:%S", &ejemplos[ejemplo_actual].fecha);
    TEST_ASSERT_EQUAL_STRING(ejemplos[ejemplo_actual].texto, resultado);
}

//! @test Lectura de la fecha y hora actual del RTC
void test_leer_fecha_ejemplo_actual() {    
    // Seleccion del ejemplo a utilizar en el caso de prueba
    ejemplo_actual = 0;
    // Variable para almacenar la instancia de reloj creada
    struct tm fecha = {0};

    // Definición de las funciones de prueba
    i2c_master_read_fake.custom_fake = simular_datos_leidos;

    // Limpieza de los resultados de las configuracion del puerto
    FFF_RESET_HISTORY();
    // Ejecución del gion de la prueba
    RelojConsultar(reloj, &fecha);

    // Verificación de la fecha devuelta por la función
    TEST_ASSERT_EQUAL_MEMORY(&ejemplos[ejemplo_actual].fecha, &fecha, sizeof(struct  tm));

    // Verficación de llamada a las funciones del bus I2C en el orden correcto
    TEST_ASSERT_SEQUENCE_START(i2c_cmd_link_create);
    // Generación de la señal de START en el bus
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_start);    
    
    // Direccionamiento del RTC para escritura
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_write_byte);
    TEST_ASSERT_EQUAL((0x6F << 1) | I2C_MASTER_WRITE, i2c_master_write_byte_fake.arg1_history[0]);
    TEST_ASSERT(i2c_master_write_byte_fake.arg2_history[0]);
    
    // Selección del primer registro que se desea leer
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_write_byte);
    TEST_ASSERT_EQUAL(0x00, i2c_master_write_byte_fake.arg1_history[1]);
    TEST_ASSERT(i2c_master_write_byte_fake.arg2_history[1]);    
    
    // Verificación de la generación de la señal de START en el bus
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_start);

    // Verificación del direccionamiento del RTC para lectura
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_write_byte);
    TEST_ASSERT_EQUAL((0x6F << 1) | I2C_MASTER_READ, i2c_master_write_byte_fake.arg1_history[2]);
    TEST_ASSERT(i2c_master_write_byte_fake.arg2_history[2]);
    
    // Verificación de la lectura del bloque de registros del RTC
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_read);
    TEST_ASSERT_EQUAL(RTC_REGISTROS, i2c_master_read_fake.arg2_history[0]);
    TEST_ASSERT_EQUAL(I2C_MASTER_LAST_NACK, i2c_master_read_fake.arg3_history[0]);
    
    // Verificación de la generación de la señal de STOP en el bus I2C
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_stop);
    // Verificación de la destruccion del enlace
    TEST_ASSERT_SEQUENCE_NEXT(i2c_cmd_link_delete);
}

//! @test Actualizacion de la fecha y hora en el RTC
void test_ejemplo_actualizar_fecha() {
    // Seleccion del ejemplo a utilizar en el caso de prueba
    ejemplo_actual = 0;
    // Variable para almacenar la fecha a configurar
    struct tm fecha;

    // Inicialización de las variables para las pruebas
    memset(datos_escritos, 0, sizeof(datos_escritos));
    memcpy(&fecha, &ejemplos[ejemplo_actual].fecha, sizeof(struct tm));

    // Definición de las funciones de prueba
    i2c_master_write_fake.custom_fake = capturar_datos_escritos;

    // Limpieza de los resultados de las configuracion del puerto
    FFF_RESET_HISTORY();
    // Ejecución del gion de la prueba
    RelojActualizar(reloj, &fecha);

    // Verificación de los datos escritos en el RTC
    TEST_ASSERT_EQUAL_MEMORY(&ejemplos[ejemplo_actual].registros, &datos_escritos, RTC_REGISTROS);

    // Verficación de llamada a las funciones del bus I2C en el orden correcto
    TEST_ASSERT_SEQUENCE_START(i2c_cmd_link_create);
    // Generación de la señal de START en el bus
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_start);    
    
    // Direccionamiento del RTC para escritura
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_write_byte);
    TEST_ASSERT_EQUAL((0x6F << 1) | I2C_MASTER_WRITE, i2c_master_write_byte_fake.arg1_history[0]);
    TEST_ASSERT(i2c_master_write_byte_fake.arg2_history[0]);
    
    // Selección del primer registro que se desea escribir
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_write_byte);
    TEST_ASSERT_EQUAL(0x00, i2c_master_write_byte_fake.arg1_history[1]);
    TEST_ASSERT(i2c_master_write_byte_fake.arg2_history[1]);    

    // Escritura del bloque de registros del RTC
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_write);

    // Generación de la señal de STOP en el bus I2C
    TEST_ASSERT_SEQUENCE_NEXT(i2c_master_stop);

    // Verificación de la destruccion del enlace
    TEST_ASSERT_SEQUENCE_NEXT(i2c_cmd_link_delete);
}
/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

