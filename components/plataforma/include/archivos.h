/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef ARCHIVOS_H   /*! @cond    */
#define ARCHIVOS_H   /*! @endcond */

/** @file archivos.h
 ** @brief Declaraciones de la libreria para abstracion del sistema de archivos
 **
 ** Libreria para el manejo de un sistema limitado de archivos
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.06.07 | evolentini      | Version inicial del archivo para manejo de archivos     |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                   |
 **
 ** @addtogroup plataforma
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

/* === Cabecera C++ ============================================================================ */

#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

//! Tipo de datos con las referencias para especificar una posicion de escritura
typedef enum {
    ARCHIVO_INICIO = 0,     //!< La nueva posicion se computa desde el inicio del archivo
    ARCHIVO_POSICION = 1,   //!< La nueva posicion se computa desde la posicion actual del archivo
    ARCHIVO_FINAL = 2,      //!< La nueva posicion se computa desde el final del archivo
} archivo_referencia_t;

//! Tipo de datos para almacenar una referencia a un objeto archivo
typedef FILE * archivo_t;

/* === Declaraciones de tipos de datos ========================================================= */

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para configurar el sistema de archivos
 */
bool ArchivosConfigurar(void);

/**
 * @brief Función para la creación de un nuevo archivo o la apertura de uno existente
 * 
 * @param[in]  nombre       Nombre del archivo que se desea abrir o crear 
 * @param[in]  modo         Indicador del modo de apertura del archivo y la creacion del mismo
 * @return     archivo_t    Referencia al nuevo objeto archivo creado
 */
archivo_t ArchivoAbrir(const char * nombre, const char * modo);

/**
 * @brief Función para leer información en un objeto archivo
 * 
 * @param[in]  archivo      Referencia al objeto archivo de donde se desea leer
 * @param[in]  datos        Referencia al bloque de memoria donse se escribiran los datos leidos
 * @param[in]  longitud     Cantidad de bytes que se deben leer del archivo
 * @return     true         El bloque de datos se pudo leer desde el archivo sin errores
 * @return     false        No se pudo pudo leer el bloque de datos desde el archivo
 */
bool ArchivoLeer(archivo_t archivo, void * datos, uint32_t longitud);

/**
 * @brief Función para escribir información en un objeto archivo
 * 
 * @param[in]  archivo      Referencia al objeto archivo donde se desea escribir
 * @param[in]  datos        Referencia al bloque de memoria con los datos a escribir
 * @param[in]  longitud     Cantidad de bytes del bloque de memoria que se deben escribir
 * @return     true         El bloque de datos se pudo escribir en el archivo sin errores
 * @return     false        No se pudo pudo escribir el bloque de datos en el archivo
 */
bool ArchivoEscribir(archivo_t archivo, const void * datos, uint32_t longitud);

/**
 * @brief Función para mover el puntero con la posición actual de un archivo
 * 
 * @param[in]  archivo         Referencia al objeto archivo al cual se desea mover el puntero
 * @param[in]  desplazamiento  Cantidad de bytes que se debe mover el puntero
 * @param[in]  referencia      Referencia desde la cual se determina el desplazamiento
 * @return     true            El puntero con la posicion actual del archivo se pudo mover sin errores
 * @return     false           No se pudo mover el puntero con la posicion actual del archivo
*/
bool ArchivoMover(archivo_t archivo, uint32_t desplazamiento, archivo_referencia_t referencia);

/**
 * @brief Función para borrar un archivo
 * 
 * @param[in]  nombre       Nombre del archivo que se desea borrar
 * @return     true         El archivo se pudo borrar sin errores
 * @return     false        No se pudo borrar el archivo solicitado
 */
bool ArchivoBorrar(const char * nombre);

/**
 * @brief Función para completar las operaciones diferidas en un archivo
 * 
 * @param[in]  archivo      Referencia al objeto archivo que se desea sincronizar
 * @return     true         Las operaciones pendientes del archivo se completaron sin errores
 * @return     false        No se pudieron completar las operacion diferidas pendientes del archivo
 */
bool ArchivoCompletar(archivo_t archivo);

/**
 * @brief Función para cerrar un archivo
 * 
 * @param[in]  archivo      Referencia al objeto archivo que se desea cerrar
 * @return     true         El archivo se pudo cerrar sin errores
 * @return     false        No se pudo cerrar el archivo solicitado
 */
bool ArchivoCerrar(archivo_t archivo);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* ARCHIVOS_H */
