/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef PARLANTES_H   /*! @cond    */
#define PARLANTES_H   /*! @endcond */

/** @file parlantes.h
 ** @brief Declaraciones de la libreria para la gestion de parlantes
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.12.22 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup plataforma
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include <stdint.h>

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos para almacenar una referencia a una lista de tarjetas autorizadas
typedef struct parlante_s * parlante_t;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear una nueva instancia de un parlante
 * 
 * @return     parlante_t       Referencia al nuevo objeto parlante creado
 */
parlante_t ParlanteCrear(uint32_t terminal);

/**
 * @brief Función para emitir un nuevo sonido
 * 
 * @param[in]  parlante         Referencia al objeto para la emision de sonidos
 * @param[in]  fecuencia        Frecuencia del sonido que se desea emitir
 */
void ParlanteEmitir(parlante_t parlante, uint32_t frecuencia);

/**
 * @brief Función para silenciar cualquier sonido en el parlante
 * 
 * @param[in]  parlante         Referencia al objeto para la emision de sonidos
 */
void ParlanteSilenciar(parlante_t parlante);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* PARLANTES_H */
