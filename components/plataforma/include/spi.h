/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef SPI_H   /*! @cond    */
#define SPI_H   /*! @endcond */

/** @file spi.h
 ** @brief Declaraciones de la libreria para abstracion de dispositivos conectados a puertos SPI
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.05.30 | evolentini      | Version inicial del archivo para manejo de auditoria    |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                  |
 **
 ** @addtogroup plataforma
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include <stdint.h>
#include "digitales.h"

/* === Cabecera C++ ============================================================================ */

#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

typedef struct spi_puerto_s * spi_puerto_t;

typedef struct spi_dispositivo_s * spi_dispositivo_t;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

spi_puerto_t SpiPuertoCrear(uint8_t puerto);

spi_dispositivo_t SpiDispositivoCrear(spi_puerto_t puerto, digital_terminal_t selector);

bool SpiTransferir(spi_dispositivo_t dispositivo, uint8_t * datos, uint8_t cantidad);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* SPI_H */
