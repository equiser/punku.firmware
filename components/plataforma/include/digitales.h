/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef DIGITALES_H   /*! @cond    */
#define DIGITALES_H   /*! @endcond */

/** @file digitales.h
 ** @brief Declaraciones de la clase para abstracion de entradas y salidas digitales
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.04.03 | evolentini      | Version inicial del archivo                             |
 **|   2 | 2019.04.23 | evolentini      | Renombrado y generalizacion para entradas digitales     |
 **|   3 | 2019.05.19 | evolentini      | Agregado de notificaciones por interrupciones           |
 **|   4 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                  |
 **
 ** @addtogroup plataforma
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include <stdint.h>
#include <stdbool.h>

/* === Cabecera C++ ============================================================================ */

#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos con la referencia a un terminal digital de microcontrolador
typedef int32_t digital_terminal_t;

//! Tipo de datos con los modos de operación de un terminal digital de salida
typedef enum {
    SALIDA_NORMAL,          //!< La salida opera en modo normal 
    SALIDA_OPENDRAIN,       //!< La salida opera en modo colector abierto
} digital_salida_modo_t;

//! Tipo de datos con los modos de operación de un terminal digital de entrada
typedef enum {
    ENTRADA_FLOTANTE,       //!< La entrada opera sin resistencias de referencia
    ENTRADA_INVERTIDA,      //!< La entrada opera sin resistencias con logica invertida
    ENTRADA_PULLUP,         //!< La entrada opera con una resistencia de referencia a fuente
    ENTRADA_PULLDOWN,       //!< La entrada opera con una resistencia de referencia a masa
} digital_entrada_modo_t;

//! Tipo de datos para almacenar una referencia a un objeto de entrada digital
typedef struct digital_entrada_s * digital_entrada_t;

//! Tipo de datos para almacenar una referencia a un objeto de salida digital
typedef struct digital_salida_s * digital_salida_t;

/**
 * @brief Referencia a una funcion para gestionar los eventos de cambios en las entradas digitales
 * 
 * @param   valor       Valor actual despues del cambio en la entrada digital
 * @param   entrada     Referencia al objeto entrada que notifica el evento de cambio
 * @param   referencia  Referencia del usuario definida al registrar el gestor de eventos
 */
typedef void (*entrada_evento_t)(bool valor, digital_entrada_t entrada, void * referencia);

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear un nuevo objeto con un terminal de entrada digital
 * 
 * @param[in]  terminal             Terminal del microcontrolador a utilizar
 * @param[in]  modo                 Modo de operación del terminal a crear
 * @return     digital_entrada_t    Referencia al nuevo objeto entrada digital creado
 */
digital_entrada_t EntradaCrear(digital_terminal_t terminal, digital_entrada_modo_t modo);

/**
 * @brief Función para leer el estado de un terminal digital de entrada
 * 
 * @param[in]  entrada   Referencia al objeto de terminal digital de entrada a leer
 * @return     true      La entrada se encuentra activa
 * @return     false     La entrada se encuentra en reposo
 */
bool EntradaLeer(digital_entrada_t entrada);

/**
 * @brief Función para solicitar a un objeto entrada digital la notificación de eventos
 * 
 * @param[in]  entrada      Referencia al objeto de terminal digital
 * @param[in]  gestor       Función de callback para informar de los eventos
 * @param[in]  referencia   Referencia de usuario para enviar en los eventos generados
 */
void EntradaNotificar(digital_entrada_t entrada, entrada_evento_t gestor, void * referencia);

/**
 * @brief Función para obtener el terminal de una entrada digital
 * 
 * @param[in]  entrada              Referencia al objeto de terminal digital
 * @return     digital_terminal_t   Terminal del microcontrolador utilizado por la entrada
 */
digital_terminal_t EntradaTerminal(digital_entrada_t entrada);

/**
 * @brief Función para crear un nuevo objeto con un terminal de salida digital
 * 
 * @param[in]  terminal             Terminal del microcontrolador a utilizar
 * @param[in]  modo                 Modo de operación del terminal a crear
 * @return     digital_salida_t     Referencia al nuevo objeto salida digital creado
 */
digital_salida_t SalidaCrear(digital_terminal_t terminal, digital_salida_modo_t modo);

/**
 * @brief Función para leer el estado de un terminal digital de salida
 * 
 * @param[in]  salida   Referencia al objeto de terminal digital de salida a leer
 * @return     true     La salida se encuentra activa
 * @return     false    La salida se encuentra en reposo
 */
bool SalidaLeer(digital_salida_t salida);

/**
 * @brief Función para activar una salida digital
 * 
 * @param[in]  salida   Referencia al objeto de salida digital que se desea activar
 */
void SalidaPrender(digital_salida_t salida);

/**
 * @brief Función para desactivar una salida digital
 * 
 * @param[in]  salida   Referencia al objeto de salida digital que se desea desactivar
 */
void SalidaApagar(digital_salida_t salida);

/**
 * @brief Función para invertir el estado de una salida digital
 * 
 * @param[in]  salida   Referencia al objeto de salida digital que se desea invertir
 */
void SalidaCambiar(digital_salida_t salida);

/**
 * @brief Función para cambiar el estado de una salida digital
 * 
 * @param[in]  salida   Referencia al objeto de salida digital que se desea escribir
 * @param[in]  estado   Nuevo estado que debe tomar la salida digital
 */
void SalidaEscribir(digital_salida_t salida, bool estado);

/**
 * @brief Función para obtener el terminal de una salida digital
 * 
 * @param[in]  salida               Referencia al objeto de salida digital
 * @return     digital_terminal_t   Terminal del microcontrolador utilizado por la salida
 */
digital_terminal_t SalidaTerminal(digital_salida_t salida);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* DIGITALES_H */
