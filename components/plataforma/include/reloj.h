/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef RELOJ_H   /*! @cond    */
#define RELOJ_H   /*! @endcond */

/** @file reloj.h
 ** @brief Declaraciones de la clase para gestion del reloj de tiempo real
 **
 ** Libreria para el manejo del reloj de tiempo real como fuente de fecha y hora del sistema.
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.05.30 | evolentini      | Version inicial del archivo de gestion del reloj RTC    |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                  |
 **
 ** @addtogroup plataforma
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "time.h"

/* === Cabecera C++ ============================================================================ */

#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

//! Tipo de datos para almacenar una referencia a un objeto reloj
typedef struct reloj_s * reloj_t;

/* === Declaraciones de tipos de datos ========================================================= */

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear un objeto reloj de tiempo real
 * 
 * @return  Referencia al nuevo objeto reloj creado 
 */
reloj_t RelojCrear(void);

/**
 * @brief Función para actualizar la fecha y hora de un objeto reloj de tiempo real
 * 
 * @param[in]  reloj  Referencia al objeto reloj que se desea actualizar
 * @param[in]  fecha  Fecha y hora con la que se actualizará el reloj
 */
void RelojActualizar(reloj_t reloj, const struct tm * fecha);

/**
 * @brief Función para consultar la fecha y hora de un objeto reloj de tiempo real
 * 
 * @param[in]  reloj  Referencia al objeto reloj que se desea consultas
 * @param[out] fecha  Fecha y hora actuales del reloj consultado
 */
void RelojConsultar(reloj_t reloj, struct tm * fecha);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* RELOJ_H */
