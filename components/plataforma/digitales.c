/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file digitales.c
 ** @brief Implementacion de la clase para abstracion de entradas y salidas digitales
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.04.03 | evolentini      | Version inicial del archivo                             |
 **|   2 | 2019.04.23 | evolentini      | Renombrado y generalizacion para entradas digitales     |
 **|   3 | 2019.05.19 | evolentini      | Agregado de notificaciones por interrupciones           |
 **|   4 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                  |
 **
 ** @addtogroup plataforma
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include <stdlib.h>
#include <string.h>
#include "include/digitales.h"

#ifdef TEST
    #include "gpio.h"
#else
    #include "esp_attr.h"
    #include "driver/gpio.h"
#endif

//! Macro para permitir la visibilidad de funciones protegidas durante las pruebas unitarias
#ifdef TEST
    #define protected 
#else
    #define protected static
#endif

/* === Definicion y Macros ===================================================================== */

//! Cantidad maxima de instancias de salidas digitales estaticas
#ifndef DIGITALES_SALIDAS_CANTIDAD
    #define DIGITALES_SALIDAS_CANTIDAD   4
#endif

//! Cantidad maxima de instancias de entradas digitales estaticas
#ifndef DIGITALES_ENTRADAS_CANTIDAD
    #define DIGITALES_ENTRADAS_CANTIDAD   4
#endif

/* == Declaraciones de tipos de datos internos ================================================= */

//! Estructura que almacena los atributos de un objeto para un terminal de entrada digital
struct digital_entrada_s {
    gpio_num_t terminal;        //!< Numero del terminal del microcontrolador
    bool invertido;             //!< El terminal opera con lógica invertida
    entrada_evento_t gestor;    //!< Referencia a la función para notificar los eventos
    void * referencia;          //!< Refrencia de usuario que se agrega a los eventos generados
};

//! Estructura que almacena los atributos de un objeto para un terminal de salida digital
struct digital_salida_s {
    gpio_num_t terminal;        //!< Numero del terminal del microcontrolador
};

//! Variable global que indica si ya se instalaron las rutinas de servicio
static bool isr_instalada = false;

/* === Definiciones de variables internas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/**
 * @brief Función para crear una nueva refrencia a un objeto de salida digital
 * 
 * @return Referencia al nuevo objeto creado
 */
static digital_salida_t DigitalNuevaSalida(void);

/**
 * @brief Función para crear una nueva refrencia a un objeto de entrada digital
 * 
 * @return Referencia al nuevo objeto creado
 */
static digital_entrada_t DigitalNuevaEntrada(void);

/**
 * @brief Función para notificar de un evento cuando hay una interrupcion
 * 
 * @param[in] argumento Referencia al objeto entrada que genera la interrupcion
 */
protected void EntradaEventoInterrupcion(void * argumento);

/* === Definiciones de funciones internas ====================================================== */

static digital_salida_t DigitalNuevaSalida(void) {
    digital_salida_t self = NULL;
    self = malloc(sizeof(struct digital_salida_s));
    if (self) memset(self, 0, sizeof(struct digital_salida_s));
    return self;
}

static digital_entrada_t DigitalNuevaEntrada(void) {
    digital_entrada_t self = NULL;
    self = malloc(sizeof(struct digital_entrada_s));
    if (self) memset(self, 0, sizeof(struct digital_entrada_s));
    return self;
}

void EntradaEventoInterrupcion(void * argumento) {
    digital_entrada_t self = argumento;
    bool valor= EntradaLeer(self);
    if (self->gestor) self->gestor(valor, self, self->referencia);
}

/* === Definiciones de funciones externas ====================================================== */
digital_entrada_t EntradaCrear(digital_terminal_t terminal, digital_entrada_modo_t modo) {
    digital_entrada_t self = NULL;
    bool resultado = false;
    bool invertido = false;

    gpio_pad_select_gpio(terminal);
    switch (modo) {
        case ENTRADA_FLOTANTE:
            invertido = false;
            resultado = (gpio_set_direction(terminal, GPIO_MODE_INPUT) == ESP_OK);
            // resultado = resultado && (gpio_set_pull_mode(terminal, GPIO_FLOATING) == ESP_OK);
            break;
        case ENTRADA_INVERTIDA:
            invertido = true;
            // resultado = resultado && (gpio_set_pull_mode(terminal, GPIO_FLOATING) == ESP_OK);
            break;
        case ENTRADA_PULLUP:
            invertido = true;
            resultado = (gpio_set_direction(terminal, GPIO_MODE_INPUT) == ESP_OK);
            resultado = resultado && (gpio_pullup_en(terminal) == ESP_OK);
            break;
        case ENTRADA_PULLDOWN:
            invertido = false;
            resultado = (gpio_set_direction(terminal, GPIO_MODE_INPUT) == ESP_OK);
            resultado = resultado && (gpio_pulldown_en(terminal) == ESP_OK);
            break;
        default:
            resultado = false;
            break;
    }
    if (resultado) {
        self = DigitalNuevaEntrada();
    }
    if (self) {
        self->terminal = terminal;
        self->invertido = invertido;
    }
    return self;
}

bool EntradaLeer(digital_entrada_t self) {
    bool resultado;
    if (self->invertido) {
        resultado = (gpio_get_level(self->terminal) == 0);
    } else {
        resultado = (gpio_get_level(self->terminal) != 0);
    }
    return resultado;
}

void EntradaNotificar(digital_entrada_t self, entrada_evento_t gestor, void * referencia) {
    if (!isr_instalada) {
        // ENTER_CRITICAL();
        // gpio_isr_register(EntradaEvento, NULL, 0, NULL);
        // EXIT_CRITICAL();
        isr_instalada = (gpio_install_isr_service(0) == ESP_OK); 
    }
    self->gestor = gestor;
    self->referencia = referencia;
    gpio_set_intr_type(self->terminal, GPIO_INTR_ANYEDGE);
    gpio_isr_handler_add(self->terminal, EntradaEventoInterrupcion, self);
}

digital_terminal_t EntradaTerminal(digital_entrada_t self){
    return (self->terminal);
}

digital_salida_t SalidaCrear(digital_terminal_t terminal, digital_salida_modo_t modo) {
    digital_salida_t self = NULL;
    bool resultado = false;

    gpio_pad_select_gpio(terminal);
    switch (modo) {
        case SALIDA_NORMAL:
            resultado = (gpio_set_direction(terminal, GPIO_MODE_OUTPUT) == ESP_OK);
            break;
        case SALIDA_OPENDRAIN:
            resultado = (gpio_set_direction(terminal, GPIO_MODE_OUTPUT_OD) == ESP_OK);
            break;
        default:
            resultado = false;
            break;
    }
    if (resultado) {
        self = DigitalNuevaSalida();
    }
    if (self) {
        self->terminal = terminal;
    }
    return self;
}

bool SalidaLeer(digital_salida_t self) {
    return (gpio_get_level(self->terminal) != 0);
}
void SalidaEscribir(digital_salida_t self, bool estado) {
    gpio_set_level(self->terminal, estado);
}

void SalidaPrender(digital_salida_t self) {
    gpio_set_level(self->terminal, true);
}

void SalidaApagar(digital_salida_t self) {
    gpio_set_level(self->terminal, false);
}

void SalidaCambiar(digital_salida_t self) {
    gpio_set_level(self->terminal, (gpio_get_level(self->terminal) != 0));
}

digital_terminal_t SalidaTerminal(digital_salida_t self){
    return (self->terminal);
}
/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

