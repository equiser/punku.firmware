/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file parlantes.c
 ** @brief Declaraciones de la libreria para la gestion de parlantes
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.12.22 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup plataforma
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "parlantes.h"

#include "driver/ledc.h"

#include <stdio.h>

/* === Definicion y Macros ===================================================================== */
#define LEDC_HS_TIMER          LEDC_TIMER_0
#define LEDC_HS_MODE           LEDC_HIGH_SPEED_MODE
#define LEDC_HS_CH0_CHANNEL    LEDC_CHANNEL_0

/* == Declaraciones de tipos de datos internos ================================================= */

//! Estrcutura que describe la información de una puerta
struct parlante_s {
    bool configurado;                       //!< Indica que el parlante ya fué configurado
    ledc_timer_config_t temporizador;       //!< Conserva los parametros del temporizador
    ledc_channel_config_t canal;            //!< Conserva los parametros del canal
};

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de variables internas ====================================================== */

/* === Definiciones de funciones internas ====================================================== */

/* === Definiciones de funciones externas ====================================================== */
parlante_t ParlanteCrear(uint32_t terminal) {
    static struct parlante_s parlante = {0};
    parlante_t self = &parlante;

    if (!self->configurado) {
        self->temporizador.duty_resolution = LEDC_TIMER_8_BIT;
        self->temporizador.freq_hz = 440;
        self->temporizador.speed_mode = LEDC_HS_MODE;
        self->temporizador.timer_num = LEDC_HS_TIMER;
        
        self->canal.channel = LEDC_HS_CH0_CHANNEL;
        self->canal.duty = 0;
        self->canal.gpio_num = terminal;
        self->canal.speed_mode = LEDC_HS_MODE;
        self->canal.timer_sel = LEDC_HS_TIMER;

        ledc_timer_config(&(self->temporizador));
        ledc_channel_config(&(self->canal));

        self->configurado = true;
    }
    return self;
}

void ParlanteEmitir(parlante_t self, uint32_t frecuencia) {
    ledc_set_freq(self->temporizador.speed_mode, self->temporizador.timer_num, frecuencia);
    ledc_set_duty(self->canal.speed_mode, self->canal.channel, 127);
    ledc_update_duty(self->canal.speed_mode, self->canal.channel);
}

void ParlanteSilenciar(parlante_t self) {
    ledc_set_duty(self->canal.speed_mode, self->canal.channel, 0);
    ledc_update_duty(self->canal.speed_mode, self->canal.channel);
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

