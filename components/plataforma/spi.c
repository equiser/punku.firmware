/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file control.c
 ** @brief Declaraciones de la tarea para control de la puerta del equipo
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.05.30 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup aplicacion
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "spi.h"
#include "driver/spi_master.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/timers.h"

#include <string.h>

/* === Definicion y Macros ===================================================================== */
#define PIN_NUM_MISO 19
#define PIN_NUM_MOSI 23
#define PIN_NUM_CLK  18

/* == Declaraciones de tipos de datos internos ================================================= */
struct spi_puerto_s {
    uint32_t puerto;
};

struct spi_dispositivo_s {
    spi_device_handle_t descriptor;
};

/* === Definiciones de variables internas ====================================================== */
spi_device_handle_t spi;

static const  spi_bus_config_t CONFIGURACION = {
    .miso_io_num = PIN_NUM_MISO,
    .mosi_io_num = PIN_NUM_MOSI,
    .sclk_io_num = PIN_NUM_CLK,
    .quadwp_io_num = -1,
    .quadhd_io_num = -1,
    .max_transfer_sz = 64,
};

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de funciones internas ====================================================== */

/* === Definiciones de funciones externas ====================================================== */
spi_puerto_t SpiPuertoCrear(uint8_t puerto) {
    esp_err_t resultado;
    spi_puerto_t descriptor = NULL;

    descriptor = malloc(sizeof(struct spi_puerto_s));
    if (descriptor != NULL) {
        descriptor->puerto = puerto;
        resultado = spi_bus_initialize(descriptor->puerto, &CONFIGURACION, 2);
        if (resultado != ESP_OK) {
            //@todo revisar resultado erroneo
            free(descriptor);
            descriptor = NULL;
        }
    }
    return descriptor;
}

spi_dispositivo_t SpiDispositivoCrear(spi_puerto_t puerto, digital_terminal_t selector) {
    esp_err_t resultado;
    spi_dispositivo_t dispositivo = NULL;
    spi_device_interface_config_t configuracion = {
        .clock_speed_hz=1000*1000, .mode=0, .queue_size=1,
    };

    dispositivo = malloc(sizeof(struct spi_dispositivo_s));
    if (dispositivo != NULL) {
        configuracion.spics_io_num = selector;
        resultado = spi_bus_add_device(puerto->puerto, &configuracion, &dispositivo->descriptor);
        if (resultado != ESP_OK) {
            //@todo revisar resultado erroneo
            free(dispositivo);
            dispositivo = NULL;
        }
    }
    return dispositivo;
}

bool SpiTransferir(spi_dispositivo_t dispositivo, uint8_t * datos, uint8_t cantidad) {
    spi_transaction_t transaccion;
    
    memset(&transaccion, 0, sizeof(transaccion));
    transaccion.length = cantidad << 3;
    transaccion.tx_buffer = datos;
    transaccion.rx_buffer = datos;  

    return (ESP_OK == spi_device_polling_transmit(dispositivo->descriptor, &transaccion));
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

