@startuml
title  PNK-DS003: Lectura de tarjeta de proximidad
    hide footbox

    participant Lectora <<(T,SkyBlue)>>
    participant Mifare <<(C,Gold)>>
    participant Control <<(T,SkyBlue)>>

    participant Autorizados <<(C,Gold)>>
    participant Sonidos <<(T,SkyBlue)>>
    participant Bitacora <<(T,SkyBlue)>>

    == Sistema en reposo ==

    loop 10 * PNK-PO004 
        ... 10 ms ...
        -> Lectora: Planificador
        activate Lectora

        Lectora -> Mifare ++ : PICC_IsNewCardPresent()
        Mifare --> Lectora -- : False

        <-- Lectora
        deactivate Lectora
        ||5||
    end
    ||5||

    == Liberación de la puerta ==

    -> Lectora: Planificador
    activate Lectora

    Lectora -> Mifare ++ : PICC_IsNewCardPresent()
    Mifare --> Lectora -- : True
    Lectora -> Mifare ++ : PICC_ReadCardSerial()
    Mifare --> Lectora -- : Tarjeta

    Lectora -> Control ++ : ControlEventoLectora(Tarjeta)
    Control --> Lectora

    Lectora -> Mifare ++ : PICC_HaltA()
    Mifare --> Lectora --

    <-- Lectora
    deactivate Lectora

    alt Tarjeta autorizada
        Control -> Autorizados ++ : AutorizadosValidar(Tarjeta)
        Autorizados --> Control -- : True

        Control ->: PuertaLiberar(Puerta)
        Control <--
        Control -> Sonidos ++ : SonidoCorrecto()
        Sonidos --> Control

        <-- Control
        deactivate Control
        <-- Sonidos 
        deactivate Sonidos

        ||5||
    else Tareja no autorizada
        Lectora -> Control ++ : ControlEventoLectora(Tarjeta)
        
        Control -> Autorizados ++ : AutorizadosValidar(Tarjeta)
        Autorizados --> Control -- : False

        Control -> Bitacora ++ : BitacoraRegistrar(NoAutorizado)
        Bitacora --> Control
        Control -> Sonidos ++ : SonidoErroneo()
        Sonidos --> Control

        <-- Control
        deactivate Control
        <-- Sonidos 
        deactivate Sonidos
        <-- Bitacora
        deactivate Bitacora
        ||5||
    end
    ||5||
@enduml