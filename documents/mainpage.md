# Punku: Control de Accesos

Sistema de control de accesos autonomo

# Documentacion del proyecto

* [PNK-DO001: Definiciones, abreviaturas y acronimos](@ref PNK-DO001)

* [PNK-DO002: Especificación de requisitos](@ref PNK-DO002)

* [PNK-DO003: Arquitectura y diseño detallado](@ref PNK-DO003)

* [PNK-DO004: Plan maestro de pruebas](@ref PNK-DO004)

* [PNK-DO005: Interfaz de Gestión](@ref PNK-DO005)

# Seguimiento del desarrollo

* [PNK-DO008: Informe de estadisticas de calidad del código](@ref PNK-DO008)

* [PNK-DO009: Informe de cobertura de las pruebas](@ref PNK-DO009)

@defgroup aplicacion Aplicacion 
@brief Modulo con el codigo especifico de la aplicación

@defgroup controladores Controladores 
@brief Modulo con el codigo generico para el control de accesos

@defgroup plataforma Plataforma 
@brief Modulo con el codigo especifico de la plataforma utilizada

