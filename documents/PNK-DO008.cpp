@page PNK-DO008 Estadisticas de calidad del codigo
@brief Informe con las estadisticas de calidad del codigo

[TOC]

@section codigo Codigo fuente del proyecto
#include "codigo.md"

@section prebas Codigo de las pruebas automaticas
#include "pruebas.md"
