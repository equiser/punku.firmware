@page PNK-DO004 Plan maestro de pruebas
@brief Documento con la estrategia general de pruebas del sistema

[TOC]

# Revisiones

| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                                |
|-----|------------|-----------------|-----------------------------------------------------------|
|  A  | 2019.05.04 | E.Volentini     | Versión inicial del documento                             |
|  C  | 2019.05.26 | E.Volentini     | Migración a markdown y publicación con Doxygen            |

# Introducción

## Propósito

Este documento detalla todos los aspectos relacionados con las pruebas que se realizarán sobre el equipo Punku para garantizar la calidad del mismo. Él mismo cubre las bases de las pruebas, las estrategias generales de las mismas y las estrategias por cada nivel de pruebas.
Está dirigido a los desarrolladores que se ocupen de las pruebas del equipo, así como también al quien diseñe e implemente el hardware y el software del mismo.

## Alcance

Las pruebas de aceptación descriptas en este documento abarcan tanto el hardware y el firmware del equipo Punku como así también al protocolo de comunicaciones utilizado para la comunicación con el software de gestión que se ejecuta en un dispositivo móvil.

## Objetivos

Las pruebas de aceptación definidas en este documento tienen por objetivo:
 * Determinar si el sistema desarrollado cumple con los requerimientos definidos en la etapa de análisis.
 * Reportar las diferencias observadas entre el comportamiento del sistema implementado y el descripto en el documento de requerimientos.
 * Generar y documentar herramientas y procesos de prueba  que puedan ser reutilizados en las fases de fabricación y reparación de los equipos una vez en producción.

## Base de las pruebas

* [PNK-DO002: Especificación de Requisitos](@ref PNK-DO002)

* [PNK-DO003: Arquitectura y diseño detallado](@ref PNK-DO003)

## Referencias

* [PNK-DO001: Definiciones, abreviaturas y acronimos](@ref PNK-DO001)

# Estrategias de prueba

## Características de calidad

En la definición del producto se definen cinco características que buscan diferenciar el producto de la oferta existente en el mercado. En la siguiente tabla se muestras estas características con una breve descripción y la importancia relativa de cada una:

|  Característica  | Definición                                                                                |  Importancia  |
|:----------------:|:------------------------------------------------------------------------------------------|:-------------:|
| Funcionalidad    | El equipo cumple con comportamiento esperado                                              |       20      |
| Confiabilidad    | El equipo mantiene su comportamiento aun cuando cambian las condiciones de funcionamiento |       20      |
| Usabilidad       | El equipo presenta al usuario un comportamiento predecible que resulta fácil de entender  |       30      | 
| Respuesta        | El equipo responde a los estímulos externos en un tiempo adecuado                         |       20      |
| Disponibilidad   | El equipo esta disponible para ser utilizado la mayor cantidad de tiempo posible          |       10      |
| ^                | En casos de fallas se puede sustituir por otro equipo en forma rápida y sencilla          |       ^       | 

El objetivo principal de las pruebas descriptas en este documento busca asegurar el cumplimento de estas características

## Niveles de prueba

Se definen los siguientes niveles los siguientes niveles de pruebas:

* Pruebas unitarias: Prueban cada componente en forma separada. Se implementaran en forma automatizada en un ambiente de desarrollo sobre los componentes controladores para verificar funcionalidad y confiabilidad.

* Pruebas de integración: Prueban la integración de los componentes en el sistema general. Se implementarán en forma automatizada en un ambiente de desarrollo sobre los componentes de alto nivel para verificar funcionalidad y confiabilidad.

* Pruebas de aceptación: Prueban el comportamiento del equipo completo. Se intentará implementarlas en forma automatizada sobre el equipo real utilizando equipamiento adicional para generar los estímulos y medir las respuestas. En los casos que no sea posible por la complejidad de la instalación (por ejemplo la lectura de una tarjeta RFID real) se ejecutaran en forma manual a partir de un guion. Estas pruebas buscan garantizar todas las características de calidad.

* Pruebas de campo: Prueba el equipo instalado y utilizado por el cliente final. Se implementarán instalando equipos en clientes seleccionados y relevando los problemas y opiniones de los usuarios del mismo. Estas pruebas buscan garantizar todas las características de calidad.

En el siguiente cuadro se muestra el impacto de cada nivel de pruebas respecto a cada una de las características de calidad deseadas.

| Característica          |  Funcionalidad  |  Confiabilidad  |  Usabilidad  |  Respuesta  |  Disponibilidad  | 
|-------------------------|:---------------:|:---------------:|:------------:|:-----------:|:----------------:|
|                         |       20        |         20      |      30      |      20     |        10        |
| Pruebas unitarias       |      @alta      |      @media     |     Nula     |     Nula    |       Nula       |
| Pruebas de integración  |      @alta      |      @media     |     Nula     |     Nula    |       Nula       |
| Pruebas de aceptación   |      @alta      |      @media     |    @media    |    @alta    |      @media      |
| Pruebas de campo        |      @media     |      @alta      |    @alta     |    @media   |      @alta       |

## Componentes a probar

Según el documento de arquitectura el equipo se divide en un conjunto de controladores que implementan la mayor parte de la lógica del producto. El foco de las pruebas estará por lo tanto en estos módulos y en la clases Control de Acceso y Protocolo de Gestión que integran en funcionamiento de los controladores para generar la funcionalidad del equipo final. Estas partes serían entonces:

1. Pulsador de apertura: Controlador que gestiona la apertura por pulsador para la salida.
2. Control de Puerta: Controlador que gestiona el mecanismo para liberación u bloqueo de la puerta, junto con el sensor de la puerta para mantener el estado de la misma.
3. Lector de proximidad: Controlador que gestiona la apertura por la lectura de una tarjeta.
4. Bitácora de eventos: Controlador que gestiona el almacenamiento y recuperación de los eventos.
5. Lista de autorizados: Controlador que gestiona la consulta y actualización de la lista de personas autorizadas a ingresar.
6. Configuración: Controlador que gestiona la consulta y actualización de los parámetros de configuración del equipo.
7. Control de acceso: Componente de alto nivel que implementa la la funcionalidad autónoma del equipo.
8. Protocolo de gestión: Componente de alto nivel que implementa el protocolo de gestión y configuración del equipo.

En la siguiente tabla se analiza el peso relativo de cada componente de software respecto del total del proyecto y su incidencia relativa en cada uno de las características de calidad deseadas para el equipo:

| Componente           |    |  Funcionalidad  |  Confiabilidad  |  Usabilidad  |  Respuesta  |  Disponibilidad  | 
|----------------------|:--:|:---------------:|:---------------:|:------------:|:-----------:|:----------------:|
|                      |    |       30        |         30      |      10      |      20     |        10        |
| Pulsador de Apertura |  5 |	   @media     |      @baja      |     @baja    |    @media   |       Nula       |
| Control de Puerta    | 20 |      @alta      |      @alta      |     @baja    |    @media   |       Nula       |
| Lector de Proximidad | 10 |      @alta      |      @media     |     @baja    |    @media   |       Nula       |
| Bitácora de Eventos  | 20 |      @media     |      @media     |     @baja    |    @baja    |       Nula       |
| Lista de Autorizados | 20 |      @alta      |      @media     |     @baja    |    @alta    |       @alta      |
| Configuración        |  5 |      @baja      |      Nula       |     @media   |    Nula     |       @alta      |
| Control de Accesos   | 10 |      @alta      |      @alta      |     @alta    |    @media   |       Nula       |
| Protocolo de gestion | 10 |      @baja      |      @baja      |     @alta    |    @baja    |       @media     |

# Pruebas de campo

# Pruebas de aceptación

Se definen las siguientes pruebas de aceptación para el equipo

# Pruebas de integración

# Pruebas unitarias
