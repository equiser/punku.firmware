@page PNK-DO005 Interfaz de Gestión
@brief Documento con la especificación de la API REST para gestion del equipo

[TOC]

# Revisiones

| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                                |
|-----|------------|-----------------|-----------------------------------------------------------|
|  A  | 2019.05.04 | E.Volentini     | Versión inicial del documento                             |
|  C  | 2019.05.26 | E.Volentini     | Migración a markdown y publicación con Doxygen            |

# Introducción

## Propósito

Este documento detalla todos los aspectos relacionados con las pruebas que se realizarán sobre el equipo Punku para garantizar la calidad del mismo. Él mismo cubre las bases de las pruebas, las estrategias generales de las mismas y las estrategias por cada nivel de pruebas.
Está dirigido a los desarrolladores que se ocupen de las pruebas del equipo, así como también al quien diseñe e implemente el hardware y el software del mismo.

## Alcance

Las pruebas de aceptación descriptas en este documento abarcan tanto el hardware y el firmware del equipo Punku como así también al protocolo de comunicaciones utilizado para la comunicación con el software de gestión que se ejecuta en un dispositivo móvil.

## Objetivos

Las pruebas de aceptación definidas en este documento tienen por objetivo:
 * Determinar si el sistema desarrollado cumple con los requerimientos definidos en la etapa de análisis.
 * Reportar las diferencias observadas entre el comportamiento del sistema implementado y el descripto en el documento de requerimientos.
 * Generar y documentar herramientas y procesos de prueba  que puedan ser reutilizados en las fases de fabricación y reparación de los equipos una vez en producción.

## Referencias

* [PNK-DO001: Definiciones, abreviaturas y acronimos](@ref PNK-DO001)

# Recursos

## Personas autorizadas

### Agregar una tarjeta

`PUT /autorizados`

### Elminar una tarjeta

`DELETE /autorizados`

### Consultar una tarjeta

`GET /autorizados`