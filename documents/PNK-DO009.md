@page PNK-DO009 Cobertura de las pruebas
@brief Informe de cobertura generado por la herramientas de pruebas automaticas

@htmlonly[block]
<style>
.iframeclass {
    position: absolute;
    top: 0;
    width: 100%;
    border: none;
}
.iframecontainer {
    position: relative;
    width: 100%;
    height: auto;
    padding-top: 61%;
}
</style>
<div class="iframecontainer">
    <iframe class="iframeclass" scrolling="yes" allowfullscreen src="gcov/GcovCoverageResults.html"  width="100%" height="100%"></iframe>
</div>