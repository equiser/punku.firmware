@page PNK-DO003 Arquitectura y diseño detallado
@brief Documento con la arquitectura y el diseño detallado del firmware

[TOC]

# Revisiones

| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                                |
|-----|------------|-----------------|-----------------------------------------------------------|
|  A  | 2019.04.03 | E.Volentini     | Versión inicial del documento                             |
|  B  | 2019.04.22 | E.Volentini     | Cambios en la arquitectura y diseño detallado             |
|  C  | 2019.05.12 | E.Volentini     | Migración a markdown y publicación con Doxygen            |
|  D  | 2019.06.23 | E.Volentini     | Documentación del primer prototipo de software            |

# Introducción

## Propósito

Este documento describe la arquitectura y el diseño detallado del firmware para el sistema de control de accesos. El mismo incluye al inicio una definición de los patrones arquitectónicos utilizados y posteriormente detalla los componentes del software, sus interfaces y responsabilidades.
Está dirigido a los desarrolladores que se ocupen de la implementación, así como también a quienes desarrollen las pruebas del sistema.

## Ambito del sistema

El equipo llevará el nombre comercial de Punku, y se comercializará como un equipo autónomo de bajo costo destinado a pequeñas oficinas y residencias particulares.

## Referencias

* [PNK-DO001: Definiciones, abreviaturas y acronimos](@ref PNK-DO001)

* [PNK-DO002: Especificación de Requisitos](@ref PNK-DO002)

# Arquitectura

## Patrones

Para este software se emplearan dos patrones arquitectónicos:

- **Arquitectura en capas**: Todo el acceso a los dispositivos se implementa mediante una
capa de abstracción o HAL (Hardware Abstraction Layer). Se utiliza una capa con librerías de terceros para el sistema de archivos y la comunicación con el lector de RFID. Finalmente la aplicación se estructura en dos capas, una que implementa los controladores y otra que implementa la lógica de alto nivel de la aplicación.

- **Control ambiental**: Un proceso de control central será el responsable de concentrar la información proveniente de lector de proximidad, el sensor de puerta abierta, el pulsador de apertura de puerta y los sensores del mecanismo y actuar sobre las salidas de alarma y del mecanismo de la puerta.

@includedoc PNK-DC001.uml

## Componentes

El sistema está formado por los siguientes componentes:

- **Tareas de la aplicación**: contiene las clases que implementan directamente la lógica de funcionamiento del equipo con el mayor nivel de abstracción. Esto permite extender la funcionalidad en forma simple, dado que el código aplica, casi directamente, las reglas de negocio del equipo. Todas las clases de esta capa encapsulan tareas del sistema operativo de tiempo real que se ejecutan en forma concurrente. Las clases que componen esta capa son:
  
  - **Sonidos**: encapsula toda la reproducción de una melodía monofónica en un parlante. Define una secuencia de notas especificando la frecuencia y duración de las mismas, y de esta manera permite generar dos sonidos distintivos para diferenciar los accesos autorizados de los rechazados.

  - **Lectora**: encapsula toda la comunicación con la lectora y, por medio de esta, con la tarjeta de proximidad. Permite recuperar el número de serie único asignado a la tarjeta y genera un evento al control para informar de una nueva lectura.

  - **Control**: encapsula toda la gestión de las reglas para la apertura y supervisión de la puerta. Esta clase implementa un patrón de diseño de software denominado control ambiental. De esta forma convergen en un solo punto los eventos de la clase Puerta, de la clase Sensor, de la clase Lectora y de la clase Autorizados para centralizar la respuesta y el registro a estos eventos.

  - **Bitacora**: encapsula el proceso de registro en un archivo de los eventos generados por el control. Permite efectuar la escritura en el archivo, una operación lenta, en forma diferida para minimizar la interferencia en los tiempos de operación del control de la puerta.

  - **Servidor**: encapsula todo el servicio de comunicación para la gestión del equipo. Permite configurar la interfaz WiFi, iniciar un servidor HTTP y atender los pedidos recibidos interactuando con las clases Reloj, Bitacora, Configuracion y Autorizados para completar las operaciones solicitadas.

- **Controladores**: contiene clases independientes de la plataforma y de la aplicación, y por lo tanto son fácilmente reutilizables en otros proyectos. Para lograr este objetivo, la mayoría de ellas no utilizan los servicios del sistema operativo, e implementan mecanismos de eventos para extraer el código dependiente de la aplicación en otro componente. Las clases que pertenecen a esta capa son: 

  - **Puertas**: encapsula toda la gestión de la puerta. Permite configurar el tipo de cerradura, habilitar o ignorar el sensor de puerta abierta y definir los tiempos correspondientes de liberación, apertura y cierre. Puede generar eventos cuando se completa un ciclo de apertura. 

  - **ArbolB**: encapsula toda la gestión de un índice utilizando la estructura de datos BTree. Permite aprovechar las transferencias de bloques de la tarjeta microSD, para disminuir el tiempo de búsqueda y actualización de una lista ordenada.

  - **Editor**: encapsula toda la gestión de una actualización que involucra múltiples escrituras en un archivo. Permite asegurar que el contenido de un ArbolB no resultará corrupto por una modificación incompleta.

  - **Autorizados**: encapsula toda la gestión de una lista con las tarjetas autorizadas a ingresar.

  - **Sensores**: encapsula toda la gestión de filtro digital asociado a terminales digitales de entrada.

  - **Configuracion**: encapsula toda la gestión de las opciones de configuración del equipo.

- **Plataforma**: Encapsula la funcionalidad dependiente del microcontrolador utilizado para permitir portabilidad al proyecto. Esta formado por los siguientes componentes:

 - **Entradas digitales**: encapsula toda la gestión de un terminal digital utilizado como entrada.

 - **Salidas digitales**: encapsula toda la gestión de un terminal digital utilizado como salida.

 - **Reloj**: encapsula toda la gestión del reloj de tiempo real. Permite ajustar y recuperar la fecha y hora actuales a partir de un reloj de tiempo real alimentado por baterias.
	
 - **Parlantes**:  encapsula la gestión de un parlante conectado a un terminal digital con modulación de ancho de pulso. Permite emitir un tono en el parlante o silenciarlo.

 - **Archivos**: encapsula toda la gestión de un archivo almacenado en la tarjeta microSD del equipo.

- **ESP-Idf**: Encapsula las bibliotecas provistas por el fabricante del microcontrolador. Esta compuesta por los siguientes componentes:

  - **GPIO**: biblioteca para el manejo de los terminales digitales del microcontrolador.

  - **I2C**: biblioteca para el manejo de los puertos de comunicaciones I2C del microcontrolador.

  - **FAT**: biblioteca que implementa el sistema de archivos FAT.

  - **PWM**: biblioteca para generacion de señales moduladas en ancho de pulso.

  - **SPI**: biblioteca para el manejo de los puertos de comunicaciones SPI del microcontrolador.

  - **HTTP**: biblioteca que implenta un servidor HTTP.

## Modos de funcionamiento

Se contemplas cuatro modos de funcionamiento difrentes en función del tipo de cerradura y de la inst

# Diseño detallado

## Diagramas de clases

* En el siguiente diagrama se muestras las clases desarrolladas para la implementación del firmware del equipo.

@includedoc PNK-DC002.uml

## Diagramas de estado

* El control de la puerta se implementa mediante una maquina de estados finitos. El siguiente diagrama de estados describe las entradas y salidas la misma.

@includedoc PNK-DE001.uml

## Diagramas de secuencia

En esta sección se describe la interacción entre los componentes del sistema utilizando diagramas de secuencia para cada uno de los casos de uso identificados en la especificación de requisitos.

### PKN-CU001: Acceso por pulsador

En este caso se uso las interacciones entre los componentes cambian en funcion de los parámetros operativos de describen el tipo de actuador que se utiliza en el sistema y la presencia o no de los sensores de puerta abierta y de estado del mecanismo.

* El primer diagrama de secuencia corresponde a un equipo donde la liberación de la puerta se produce energizando una bobina y no hay instalado un sensor de puerta. 

@includedoc PNK-DS001-A.uml

* El siguiente diagrama de secuencia corresponde a un equipo en donde la liberación de la puerta se produce energizando una bobina y hay instalado un sensor de puerta que informa la apertura y cierre de la misma. 

@includedoc PNK-DS001-B.uml

