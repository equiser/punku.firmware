@page PNK-DO001 Definiciones, abreviaturas y acronimos
@brief Documento con la lista de definiciones de los terminos utilizados en los documentos del proyecto

[TOC]

# Revisiones

| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                                |
|-----|------------|-----------------|-----------------------------------------------------------|
|  A  | 2019.05.26 | E.Volentini     | Versión inicial del documento                             |

# Introducción

## Propósito

Este documento contiene una lista de definiciones de los térmitos utilizados en los documentos de los proyectos, como así tambien una lista de las abreviaturas y acronimos con su correspondiente significado.
Está dirigido a todas las personas involucradas con el proyecto.

# Definiciones

@definicion{usuario,Usuario} Persona que utiliza el sistema para acceder por una puerta controlada utilizando una tarjeta de proximidad.

@definicion{firmware,Firmware} Programa de control del equipo almacenado en forma permanente en el microcontrolador de la placa principal.

@definicion{parametro,Parametro} Cada una de las opciones que se puede configurar en el equipo por el usuario para ajustar el comportamiento del equipo a las necesidades de una instalación en particular.

@definicion{configuracion,Configuración} Conjunto de @ref parametro "parametros" definidos por el usuario que modifican el comportamiento del equipo en una instalación particular.

# Abreviaturas

@definicion{AD,A/D} A definir.

@definicion{NA,N/A} No aplica.

# Acronimos

@definicion{rfid,RFID} Identificación por radiofrecuencia, corresponde a la tecnología utilizada por las tarjetas de proximidad.

# Identificadores

@identificador{PNK-DO000-X} Documento de diseño del proyecto. Los números se asignan consecutivamente para cada nuevo documento del proyecto. Letra del final indica la revisión del mismo. Si un identificador queda obsoleto no se reutiliza.

@identificador{PNK-ES000} Identificador de un puerto una interfase de entrada/salida. Los números se asignan consecutivamente para cada nueva entrada o salida del equipo. Si un identificador queda obsoleto no se reutiliza.

@identificador{PNK-PO000} Identificador de un @ref parametro de @ref configuracion "configuración" del equipo. Los números se asignan consecutivamente a cada nuevo parámetro definido para el equipo. Si un identificador queda obsoleto no se reutiliza.

@identificador{PNK-RS000} Identificador de un requerimiento de software del proyecto. Los números se asignan consecutivamente para cada nuevo requerimiento de software del equipo. Si un identificador queda obsoleto no se reutiliza.

@identificador{PNK-CU000} Identificador de un caso de uso del proyecto. Los números se asignan consecutivamente para cada nuevo caso de uso del equipo. Si un identificador queda obsoleto no se reutiliza.

@identificador{PNK-DS000-A} Identificador de un diagrama de secuencia. Los números debe corresponderse con el caso de uso de uso al cual describe. La letra del final permite describir un mismo caso de uso en varios diagramas de secuencia.
