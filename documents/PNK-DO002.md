@page PNK-DO002 Especificación de requisitos
@brief Documento con la especificación de requisitos del firmware

[TOC]

# Revisiones

| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                                |
|-----|------------|-----------------|-----------------------------------------------------------|
|  A  | 2019.03.12 | E.Volentini     | Versión inicial del ámbito y la descripción del proyecto  |
|  B  | 2019.03.18 | E.Volentini     | Agregado de los requisitos funcionales                    |
|  C  | 2019.03.25 | E.Volentini     | Agregado de los casos de uso                              |
|  D  | 2019.03.31 | E.Volentini     | Mejora en la redacción de los requisitos funcionales      |
|  E  | 2019.04.19 | E.Volentini     | Completado de los requisitos funcionales                  |
|  F  | 2019.05.12 | E.Volentini     | Migración a markdown y publicación con Doxygen            |

# Introducción

## Propósito

Este documento representa la especificación de requerimientos para el firmware de un sistema de control de accesos autónomo que utiliza tarjetas de proximidad como elemento de identificación de los usuarios y que puede ser gestionado desde una computadora o un teléfono inteligente mediante un protocolo de comunicación.
Está dirigido a los desarrolladores que se ocupen del análisis, diseño e implementación, así como también a quienes desarrollen las pruebas del software.

## Ambito del sistema

El equipo llevará el nombre comercial de Punku, y se comercializará como un equipo autónomo de bajo costo destinado a pequeñas oficinas y residencias particulares.
		
## Rerefencias

* [PNK-DO001: Definiciones, abreviaturas y acronimos](@ref PNK-DO001)

## Funciones del producto

El software aquí especificado brindará las siguientes funcionalidades:

1. Control de accesos utilizando tarjetas RFID.

2. Capacidad para supervisar las entradas en una puerta.

3. Capacidad de monitorear la permanencia de la puerta abierta.

4. Almacenamiento de los accesos y aperturas de la puerta.

5. Capacidad para limitar el acceso en base a una lista de tarjetas autorizadas.

6. Capacidad de gestionar la lista de tarjetas autorizadas en forma remota.

7. Capacidad de consultar los eventos registrados en forma remota.

8. Capacidad para gestionar indistintamente una bobina o un motor para permitir el acceso por la puerta.

El software aquí especificado no brindará los servicios de:

1. Control de accesos utilizando sistemas biométricos.

2. Capacidad de registrar las entradas y salidas en una misma puerta.

3. Capacidad de gestionar más de una puerta.

4. Capacidad de consultar el permiso de acceso en un servidor remoto.

## Características de los usuarios

Los usuarios finales de este producto son personas adultas sin ningún tipo de formación previa. También serán usuarios de este técnicos electrónicos o afines capacitados quienes se ocuparán de la instalación y mantenimiento del equipo.

## Restricciones

1. El software debe mantenerse bajo control de versiones GIT.

2. Todo el código desarrollado deberá respetar las plantillas y los estándares de codificación de la empresa.

3. Toda la documentación del código se realizará utilizando comentarios JavaDOC.

4. Todo el código se debe desarrollar siguiendo TDD.

## Suposiciones y dependencias

Se asume que se dispondrá de al menos un prototipo del hardware del equipo desde el comienzo de la fase de desarrollo y hasta la liberación de la última versión de software.

# Descripción general

## Interfaces externas

@identificador{PNK-ES001} Puerto de comunicaciones con el circuito integrado responsable de la comunicación por RFID (Integrado MFRC522 comunicado por una interface SPI).

@identificador{PNK-ES002} Sensor de puerta abierta (Entrada digital opto-aislada)

@identificador{PNK-ES003} Actuador de la cerradura de puerta (Salida digital con capacidad de inversión de la polaridad).

@identificador{PNK-ES004} Sensor del motor de la cerradura que indica puerta liberada (Entrada digital sin aislación).

@identificador{PNK-ES005} Sensor del motor de la cerradura que indica puerta bloqueada (Entrada digital sin aislación).

@identificador{PNK-ES006} Pulsador para apertura manual de puerta (Entrada digital opto-aislada).

@identificador{PNK-ES007} Salida de alarma (Salida digital de contacto seco).

@identificador{PNK-ES008} Indicador sonoro (Salida modulada en frecuencia).

@identificador{PNK-ES009} Indicador luminoso de dos colores (Salida digital con capacidad de inversión de la polaridad).

## Funciones

### Parámetros operativos

@identificador{PNK-PO001} Tiempo de accionamiento del indicador luminoso @ref PNK-ES009 cuando se produce la lectura de una tarjeta. En el rango de los 100ms a los 2500ms.

@identificador{PNK-PO002} Tiempo de accionamiento del indicador sonoro @ref PNK-ES008 cuando se concede el acceso a una tarjeta. En el rango de los 100ms a los 2500ms. **Eliminado en la versión 1.00**

@identificador{PNK-PO003} Tiempo de accionamiento del indicador sonoro @ref PNK-ES008 cuando se deniega el acceso a una tarjeta. En el rango de los 100ms a los 2500ms. **Eliminado en la versión 1.00**

@identificador{PNK-PO004} Tiempo máximo de accionamiento del actuador de la puerta. Corresponde también al tiempo que dispone el usuario para abrir la puerta. En el rango de los 1s a los 10s. 

@identificador{PNK-PO005} Tiempo de accionamiento máximo del motor de cerradura. En el rango de los 100ms a los 2500ms.

@identificador{PNK-PO006} Tiempo máximo de puerta abierta. Corresponde al tiempo que dispone el usuario para volver a cerrar la puerta. En el rango de los 1s a los 60s.

@identificador{PNK-PO007} El sensor de puerta está conectado. Indica que el equipo tiene conectado un sensor para monitorear el estado de la puerta. Valor lógico.

@identificador{PNK-PO008} El actuador requiere inversión de polaridad. Indica que el equipo tiene conectado un motor como actuador para liberar o bloquear la puerta. Valor lógico.

@identificador{PNK-PO009} El actuador tiene fines de carrera. Indica que el equipo tiene conectados sensores para monitorear el estado del motor utilizado para liberar o bloquear la puerta. Valor lógico.

### Control general del acceso {#RS09}

@identificador{PNK-RS001} El software debe recuperar el número de serie de la tarjeta MIFARE presentada ante el lector.

@identificador{PNK-RS002} El software debe indicar la lectura de una tarjeta cambiando el color indicador luminoso durante un tiempo @ref PNK-PO001. Para ello debe activar la salida @ref PNK-ES009 con polaridad inversa. 

@identificador{PNK-RS003} El software debe determinar si la tarjeta leída esta incluida en una lista de personas autorizadas y conceder el acceso, o denegarlo si la misma no está incluida.

@identificador{PNK-RS004} El software debe indicar si se concede el acceso al usuario mediante una melodía de tres tonos ascendentes correspondientes a las frecuencias de 523 Hz, 659 Hz y 784 Hz, todas con una duracción de 100 ms. Para ello debe generar de una señal cuadrada en la interface @ref PNK-ES008 de la frecuencia especificada para cada nota.

@identificador{PNK-RS005} El software debe indicar si no se concede el acceso al usuario mediante una melodía de tres tonos descendentes correspondientes a las frecuencias de 784 Hz, 659 Hz y 523 Hz, todas con una duracción de 100 ms. Para ello debe generar de una señal cuadrada en la interface @ref PNK-ES008 de la frecuencia especificada para cada nota.

@identificador{PNK-RS006} Cuando concede el acceso a un usuario el software debe accionar el actuador de la puerta para liberarla, esperar la apertura de la puerta o el tiempo máximo de espera fijado por el parámetro @ref PNK-PO004, y al ocurrir cualquiera de los dos eventos debe volver a accionar el actuador de la puerta para bloquearla.

@identificador{PNK-RS007} Cuando un usuario abre la puerta después de un acceso autorizado el software debe supervisar que el cierre de la misma, y si esto no ocurre antes del tiempo máximo de espera fijado por el parámetro @ref PNK-PO006, entonces el software debe generar una señal de alarma hasta que ocurra el cierre de a misma. Para ello debe activar la salida @ref PNK-PO007 en forma permanente hasta que finalice la alarma.

@identificador{PNK-RS008} El software debe supervisar permanentemente el estado de la puerta para detectar una apertura no autorizada, y debe generar una señal de alarma hasta que ocurra el cierre de a misma. Para ello debe activar la salida @ref PNK-PO007 en forma permanente hasta que finalice la alarma.

@identificador{PNK-RS009} Cuándo se activa el pulsador de ingreso conectado a la entrada @ref PNK-ES006 el software debe conceder el acceso siguiendo el mismo comportamiento que para una tarjeta autorizada.

@identificador{PNK-RS022} El software debe generar melodías simples para indicar al usuario el resultado de las operación. Para ello debe sintetizar una nota musical mediante la generación de una señal cuadrada en la interface @ref PNK-ES008 con una duración y frecuencias arbitrarias. La frecuencia de la nota estará comprendida entre 400 Hz y 800 Hz, y la duración de cada nota estará comprendida entre los 100 ms y los 800 ms.

### Señales de control para el actuador

@identificador{PNK-RS010} Si el equipo esta configurado para operar sin sensor de puerta (@ref PNK-PO007 = 0) y con un destraba pestillo eléctrico (@ref PNK-PO008 = 0), cuando se concede el acceso el software debe activar la salida @ref PNK-ES003 con polaridad directa durante el tiempo máximo de accionamiento de puerta @ref PNK-PO004. Al finalizar este tiempo de espera el software debe apagar la interface @ref PNK-ES003.

@identificador{PNK-RS011} Si el equipo esta configurado para operar con sensor de puerta (@ref PNK-PO007 = 1) y con un destraba pestillo eléctrico (@ref PNK-PO008 = 0), cuando se concede el acceso el software debe activar la salida @ref PNK-ES003 con polaridad directa hasta que se activa el sensor de puerta abierta @ref PNK-ES002 o hasta que se cumple el tiempo máximo de accionamiento de puerta @ref PNK-PO004. Al finalizar este tiempo de espera el software debe apagar la interface @ref PNK-ES003.

@identificador{PNK-RS012} Si el equipo esta configurado para operar sin sensor de puerta (@ref PNK-PO007 = 0), con un motor como actuador (@ref PNK-PO008 = 1) y sin sensores en el mecanismo del motor (@ref PNK-PO008 = 0), cuando se concede el acceso el software debe activar la salida @ref PNK-ES003 con polaridad directa durante el tiempo máximo de accionamiento del motor @ref PNK-PO005. Al finalizar este tiempo el software debe esperar el tiempo de apertura de la puerta @ref PNK-PO004 y al finalizar el software activar la salida @ref PNK-ES003 con polaridad inversa durante la misma cantidad de tiempo @ref PNK-PO005. Al finalizar este tiempo el software debe apagar la interface @ref PNK-ES003.

@identificador{PNK-RS013} Si el equipo esta configurado para operar sin sensor de puerta (@ref PNK-PO007 = 0), con un motor como actuador (@ref PNK-PO008 = 1) y con sensores en el mecanismo del motor (@ref PNK-PO008 = 0), cuando se concede el acceso el software debe activar la salida @ref PNK-ES003 con polaridad directa hasta que el se activa el sensor que indica que el mecanismo esta liberado @ref PNK-ES004. Después el software debe esperar el tiempo de apertura de la puerta @ref PNK-PO004 y al finalizar el software activar la salida @ref PNK-ES003 con polaridad inversa hasta que se activa el sensor que indica que el mecanismo esta bloqueado @ref PNK-ES005. Al finalizar este tiempo el software debe apagar la interface @ref PNK-ES003. Sí el tiempo que permanece activa la salida @ref PNK-ES003 supera el tiempo máximo @ref PNK-PO004 el software debe apagar la salida y registrar una condición de error.

@identificador{PNK-RS014} Si el equipo esta configurado para operar con sensor de puerta (@ref PNK-PO007 = 1), con un motor como actuador (@ref PNK-PO008 = 1) y sin sensores en el mecanismo del motor (@ref PNK-PO008 = 0), cuando se concede el acceso el software debe activar la salida @ref PNK-ES003 con polaridad directa durante el tiempo máximo de accionamiento del motor @ref PNK-PO005. Al finalizar este tiempo el software debe esperar hasta que se activa el sensor de puerta abierta PKN-ES002 o hasta que se cumple el tiempo máximo de accionamiento de puerta @ref PNK-PO004. Después el software debe activar la salida @ref PNK-ES003 con polaridad inversa durante la misma cantidad de tiempo @ref PNK-PO005. Al finalizar este tiempo el software debe apagar la interface @ref PNK-ES003.

@identificador{PNK-RS015} Si el equipo esta configurado para operar con sensor de puerta (@ref PNK-PO007 = 1), con un motor como actuador (@ref PNK-PO008 = 1) y con sensores en el mecanismo del motor (@ref PNK-PO008 = 0), cuando se concede el acceso el software debe activar la salida @ref PNK-ES003 con polaridad directa hasta que el se activa el sensor que indica que el mecanismo esta liberado @ref PNK-ES004. Después el software debe esperar hasta que se activa el sensor de puerta abierta @ref PNK-ES002 o hasta que se cumple el tiempo máximo de accionamiento de puerta @ref PNK-PO004. Después el software activar la salida @ref PNK-ES003 con polaridad inversa hasta que se activa el sensor que indica que el mecanismo esta bloqueado @ref PNK-ES005. Al finalizar este tiempo el software debe apagar la interface @ref PNK-ES003. Sí el tiempo que permanece activa la salida @ref PNK-ES003 supera el tiempo máximo @ref PNK-PO004 el software debe apagar la salida y registrar una condición de error.

### Gestión del equipo

@identificador{PNK-RS016} En cada lectura de tarjeta el software debe registrar en una bitácora la fecha y la hora de la lectura, el número de la tarjeta leída, si se concedió o no el acceso, si se abrió o no la puerta, si se volvió a cerrar dentro del tiempo establecido y cuánto tiempo permaneció la puerta abierta.

@identificador{PNK-RS017} En cada acceso por pulsador el software debe registrar en una bitácora la fecha y la hora del acceso, si se abrió o no la puerta, si se volvió a cerrar dentro del tiempo establecido y cuánto tiempo permaneció la puerta abierta.

@identificador{PNK-RS018} El software debe permitir agregar o eliminar una tarjeta a la lista de personas autorizadas mediante un protocolo de comunicación que utilice la red WiFi como medio de transporte.

@identificador{PNK-RS019} El software debe permitir borrar completamente la lista de personas autorizadas mediante un protocolo de comunicación que utilice la red WiFi como medio de transporte.

@identificador{PNK-RS020} El software debe permitir recuperar todos los eventos de la bitácora posteriores a una fecha especifica mediante un protocolo de comunicación que utilice la red WiFi como medio de transporte.

@identificador{PNK-RS021} El software debe permitir redefinir todos los parámetros operativos mediante un protocolo de comunicación que utilice la red WiFi como medio de transporte.
Restricciones de diseño

@identificador{PNK-RD001} Se utilizará un modulo ESP8255 como único procesador del sistema.
Atributos del sistema
Mantenibilidad

@identificador{PNK-AM001} El software debe permitir modificar sus parámetros operativos y almacenarlos en una memoria no volátil.
Confiabilidad

@identificador{PNK-AC001} El software debe asegurar su correcto funcionamiento en condiciones normales de operación durante al menos 2 años de uso continuo (sin ser reiniciado).
Seguridad

@identificador{PNK-AS001} El software debe verificar que las operaciones solicitadas por el protocolo de gestión sean realizadas únicamente por personas autorizadas..

## Otros requisitos

N/A.

# Casos de uso

## PKN-CU001: Acceso por pulsador

| Titulo            | Descripción                                                                                           |
|-------------------|-------------------------------------------------------------------------------------------------------|
| Identificador     | PKN-CU001                                                                                             |
| Nombre            | Acceso por pulsador                                                                                   |
| Descripción       | Acceso de una persona utilizando un pulsador                                                          |
| Actor Principal   | Usuario                                                                                               |
| Disparadores      | El usuario presiona el pulsador de salida                                                             |
| Flujo Básico	    | 1. El usuario presiona el pulsador de salida                                                          |
| ^                 | 2. El software informa que concederá el acceso emitiendo un sonido corto y agudo                      |
| ^                 | 3. El software acciona el mecanismo para permitir la apertura de la puerta                            |
| ^                 | 4. El software espera el tiempo accionamiento de cerradura PNK-PO004                                  |
| ^                 | 5. El usuario abre la puerta antes de que se complete el tiempo de cerradura                          |
| ^                 | 6. El software acciona el mecanismo para impedir una nueva apertura de la puerta                      |
| ^                 | 7. El software espera el tiempo de puerta abierta PNK-PO006                                           |
| ^                 | 8. El usuario cierra la puerta antes de que se complete el tiempo de puerta abierta                   |
| ^                 | 9. El software registra el acceso en la bitácora de novedades                                         |
| Flujo Alternativo | 5. El usuario no abre la puerta antes de que se complete el tiempo PNK-PO004                          |
| ^                 | 5.1. El software acciona el mecanismo para impedir una nueva apertura de la puerta                    |
| ^                 | 5.2. El software registra en la bitácora de novedades que el usuario no ingresó                       |
| Flujo Alternativo | 8. El usuario no cierra la puerta antes de que se complete el tiempo PNK-PO006                        |
| ^                 | 8.1. El software informa el error de puerta abierta con un sonido continuo                            |
| ^                 | 8.2. El software espera el cierre de la puerta                                                        |
| ^                 | 8.3. El usuario cierra la puerta                                                                      |
| ^                 | 8.4. El software deja de emitir el sonido                                                             |
| ^                 | 8.5. El software registra en la bitácora de novedades que el usuario dejo la puerta abierta           |
| Precondiciones    | La puerta debe estar cerrada                                                                          |
| Postcondiciones   | La puerta debe estar cerrada                                                                          |

## PKN-CU002: Acceso por tarjeta

| Titulo            | Descripción                                                                                           |
|-------------------|-------------------------------------------------------------------------------------------------------|
| Identificador     | PKN-CU001                                                                                             |
| Nombre            | Acceso por tarjeta                                                                                    |
| Descripción       | Acceso de una persona autorizada utilizando una tarjeta de proximidad                                 |
| Actor Principal   | Usuario con tarjeta de proximidad                                                                     |
| Disparadores      | El usuario presenta la tarjeta delante del lector                                                     |
| Flujo Básico	    | 1. El usuario presenta la tarjeta delante del lector                                                  |
| ^                 | 2. El software verifica que la tarjeta leída esta incluida en la lista de tarjetas autorizadas        |
| ^                 | 3. El software informa que concederá el acceso emitiendo un sonido corto y agudo                      |
| ^                 | 4. El software acciona el mecanismo para permitir la apertura de la puerta                            |
| ^                 | 5. El software espera el tiempo accionamiento de cerradura PNK-PO004                                  |
| ^                 | 6. El usuario abre la puerta antes de que se complete el tiempo de cerradura                          |
| ^                 | 7. El software acciona el mecanismo para impedir una nueva apertura de la puerta                      |
| ^                 | 8. El software espera el tiempo de puerta abierta PNK-PO006                                           |
| ^                 | 9. El usuario cierra la puerta antes de que se complete el tiempo de puerta abierta                   |
| ^                 | 10. El software registra el acceso en la bitácora de novedades                                        |
| Flujo Alternativo | 2. El software determina que la tarjeta leída no está incluida en la lista de tarjetas autorizadas    |
| ^                 | 2.1. El software informa que no concederá acceso emitiendo un sonido largo y grave                    |
| ^                 | 2.2. El software registra en la bitácora de novedades que no se concedió el acceso al usuario         |
| Flujo Alternativo | 6. El usuario no abre la puerta antes de que se complete el tiempo PNK-PO004                          |
| ^                 | 6.1. El software acciona el mecanismo para impedir una nueva apertura de la puerta                    |
| ^                 | 6.2. El software registra en la bitácora de novedades que el usuario no ingresó                       |
| Flujo Alternativo | 9. El usuario no cierra la puerta antes de que se complete el tiempo PNK-PO006                        |
| ^                 | 9.1. El software informa el error de puerta abierta con un sonido continuo                            |
| ^                 | 9.2. El software espera el cierre de la puerta                                                        |
| ^                 | 9.3. El usuario cierra la puerta                                                                      |
| ^                 | 9.4. El software deja de emitir el sonido                                                             |
| ^                 | 9.5. El software registra en la bitácora de novedades que el usuario dejo la puerta abierta           |
| Precondiciones    | La puerta debe estar cerrada                                                                          |
| Postcondiciones   | La puerta debe estar cerrada                                                                          |

## PKN-CU003: Configuración del equipo

| Titulo            | Descripción                                                                                           |
|-------------------|-------------------------------------------------------------------------------------------------------|
| Identificador     | PKN-CU003                                                                                             |
| Nombre            | Configuración del equipo                                                                              |
| Descripción       | Configuración del equipo desde un teléfono celular o una computadora                                  |
| Actor Principal   | Software de gestión                                                                                   |
| Disparadores      | Recepción de un comando de configuración                                                              |
| Flujo Básico	    | 1. El software recibe un comando para actualizar la configuración                                     |
| ^                 | 2. El software recibe los nuevos parámetros de configuración                                          |
| ^                 | 3. El software valida los nuevos parámetros de configuración                                          |
| ^                 | 4. El software aplica los nuevos parámetros de configuración                                          |
| ^                 | 5. El software envía una confirmación informando que los parámetros se aplicaron correctamente        |
| Flujo Alternativo | 3. Los parámetros recibidos son incorrectos                                                           |
| ^                 | 3.1. El software envía una notificación de error informando que los parámetros son incorrectos        |
| Precondiciones    | N/A                                                                                                   |
| Postcondiciones   | N/A                                                                                                   |

## PKN-CU004: Gestión de las personas autorizadas

| Titulo            | Descripción                                                                                           |
|-------------------|-------------------------------------------------------------------------------------------------------|
| Identificador     | PKN-CU004                                                                                             |
| Nombre            | Gestión de las personas autorizadas                                                                   |
| Descripción       | Configuración de la lista de tarjetas autorizadas desde un teléfono celular o una computadora         |
| Actor Principal   | Software de gestión                                                                                   |
| Disparadores      | Recepción de un comando actualización de la lista de tarjetas autorizadas                             |
| Flujo Básico	    | 1. El software recibe un comando para actualizar la lista de tarjetas autorizadas                     |
| ^                 | 2. El software recibe la nueva lista de tarjetas autorizadas                                          |
| ^                 | 3. El software valida la lista de tarjetas autorizadas recibida                                       |
| ^                 | 4. El software reemplaza la lista de tarjetas autorizadas actual por la recibida                      |
| ^                 | 5. El software envía una confirmación informando que se actualizó la lista de tarjetas autorizadas    |
| Flujo Alternativo | 3.  La lista de tarjetas autorizadas no es válida                                                     |
| ^                 | 3.1. El software envía una notificación de error informando que la lista de tarjetas  no es válida    |
| Precondiciones    | N/A                                                                                                   |
| Postcondiciones   | N/A                                                                                                   |

## PKN-CU004: Acceso a la bitácora del equipo

| Titulo            | Descripción                                                                                           |
|-------------------|-------------------------------------------------------------------------------------------------------|
| Identificador     | PKN-CU005                                                                                             |
| Nombre            | Acceso a la bitácora del equipo                                                                       |
| Descripción       | Consulta de la bitácora del equipo desde un teléfono celular o una computadora                        |
| Actor Principal   | Software de gestión                                                                                   |
| Disparadores	    | Recepción de un comando de consulta de la bitácora                                                    |
| Flujo Básico      | 1. El software recibe un comando para recuperar la bitácora de novedades                              |
| ^                 | 2. El software envía la bitácora de eventos                                                           |
| Flujo Alternativo | N/A                                                                                                   |
| Precondiciones    | N/A                                                                                                   |
| Postcondiciones   | N/A                                                                                                   |
