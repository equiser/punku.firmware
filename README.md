# Documentación

La ultima versión de la documentación de diseño puede consultarse en en este [sitio](http://equiser.bitbucket.io/punku/index.html)

# Videos del funcionamiento

* [Componentes:](https://youtu.be/4cCnVDAwbWE) Muestra los componentes del proyecto

* [Puesta en Hora:](https://youtu.be/uHtfQTmcyYk) Muestra la puerta en hora del RTC desde la aplicación movil

* [Funcionamiento:](https://youtu.be/ZVOibsxHT8k) Muestra el funcionamiento del equipo

* [Escritura SD:](https://youtu.be/xfhgHzki1_o) Muestra la escritura en la tarjeta SD de la bitacora de eventos

# Presentaciones

* [RTOS](https://drive.google.com/open?id=1K4b1FIb87nSBkEZEIuGTwJ3eQcLS7Gjg)

* [Protocolos](https://drive.google.com/open?id=1K1k1Dcv7Ok_6OD-tjh-D5IgCJENuLf_y)
