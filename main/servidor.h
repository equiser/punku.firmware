/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef SERVIDOR_H   /*! @cond    */
#define SERVIDOR_H   /*! @endcond */

/** @file servidor.c
 ** @brief Declaraciones de la clase para la gestion del servidor HTTP sobre WiFi
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.12.27 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup aplicacion
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "bitacora.h"
#include "autorizados.h"
#include "json.h"

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos para almacenar una referencia a un objeto control
typedef struct servidor_s * servidor_t;

typedef struct servidor_recursos_s {
    reloj_t reloj;
    bitacora_t bitacora;
    autorizados_t autorizados;
} * servidor_recursos_t;

typedef struct servidor_configuracion_s {
    bool estacion;
    char nombre[32];
    char clave[32];
} * servidor_configuracion_t;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear un objeto de la clase responsable del servidor de comunicaciones
 */
servidor_t ServidorCrear(servidor_recursos_t recursos, void * configuracion);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* SERVIDOR_H */
