/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file control.c
 ** @brief Implementacion de la tarea para control de la puerta del equipo
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.05.30 | evolentini      | Version inicial del archivo para manejo de auditoria    |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                  |
 **
 ** @addtogroup aplicacion
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "control.h"
#include "sensores.h"
// #include "digitales.h"
// #include "eventos.h"
// #include "puertas.h"
// #include "lectora.h"
// #include "reloj.h"
// #include "auditoria.h"
#include "errores.h"

#include <string.h>

#ifdef TEST
    #include "FreeRTOS.h"
    #include "task.h"
    #include "queue.h"
    #include "timers.h"
#else
    #include "freertos/FreeRTOS.h"
    #include "freertos/task.h"
    #include "freertos/queue.h"
    #include "freertos/timers.h"
#endif

/* === Definicion y Macros ===================================================================== */
#define CONTROLES_CANTIDAD      1
#define CONTROL_NOMBRE          "control"
#define CONTROL_PILA            (4 * configMINIMAL_STACK_SIZE)
#define CONTROL_PRIORIDAD       (configMAX_PRIORITIES - 2)
/* == Declaraciones de tipos de datos internos ================================================= */

//! Estructura que describe la información del control de una puerta
struct control_s {
    sensor_t pulsador;       //!< Referencia al objeto sensor pulsador para liberacion manual
    puerta_t puerta;        
    lectora_t lectora; 
    sonido_t sonido;  
    autorizados_t autorizados; 
    bitacora_t bitacora;
    TaskHandle_t tarea;
    tarjeta_t tarjeta;
};

/* === Definiciones de variables internas ====================================================== */

/* === Definiciones de variables externas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/** @brief Busca un nuevo descriptor de entrada sin usar
 *
 * @return Referencia al descriptor del nuevo objeto control de puerta 
 */
static control_t ControlNuevo(void);

static void ControlEventoPulsador(bool estado, bool estable, sensor_t entrada, void * referencia);

static void ControlEventoPuerta(puerta_mensajes_t mensaje, puerta_t puerta, void * referencia);

static void ControlEventoLectora(lectora_mensajes_t mensaje, tarjeta_t tarjeta, lectora_t lectora, void * referencia);

/**
 * @brief Función que implementa la tarea de control de accesos
 * 
 * @param[in]  parametros  Puntero sin requerido por el prototipo
 */
static void ControlTarea (void * parametros);

/* === Definiciones de funciones internas ====================================================== */

static control_t ControlNuevo(void) {
    control_t resultado = NULL;

    static int usados = 0;
    static struct control_s controles[CONTROLES_CANTIDAD] = {0};
    if (usados < sizeof(controles) / sizeof(struct control_s)) {
        memset(&controles[usados], 0, sizeof(struct control_s));
        resultado = &controles[usados];
        usados++;
    }

    return resultado;
}

void ControlEventoPulsador(bool estado, bool estable, sensor_t entrada, void * referencia) {
    control_t self = referencia;
    if (estable && estado) {
        self->tarjeta = ORIGEN_PULSADOR;
        PuertaLiberar(self->puerta);
        SonidoCorrecto(self->sonido);
    }
}

void ControlEventoPuerta(puerta_mensajes_t mensaje, puerta_t puerta, void * referencia) {
    control_t self = referencia;

    if (mensaje == PUERTA_FORZADA) self->tarjeta = ORIGEN_EXTERNO;
    struct bitacora_evento_s evento = {
        .mensaje = (bitacora_mensajes_t) mensaje,
        .tarjeta = self->tarjeta,
    };
    BitacoraRegistrar(self->bitacora, &evento);
    if (mensaje != PUERTA_NO_CERRO) self->tarjeta = ORIGEN_EXTERNO;
}

void ControlEventoLectora(lectora_mensajes_t mensaje, tarjeta_t tarjeta, lectora_t lectora, void * referencia) {
    control_t self = referencia;

    if (mensaje == LECTORA_TARJETA) {
        self->tarjeta = tarjeta;

        Registrar(INFORMACION, "Tarjeta leida 0x%X", self->tarjeta);
        if (AutorizadosValidar(self->autorizados, self->tarjeta)) {
            SonidoCorrecto(self->sonido);
            PuertaLiberar(self->puerta);
        } else {
            struct bitacora_evento_s evento = {
                .mensaje = BITACORA_NO_AUTORIZADA,
                .tarjeta = self->tarjeta,
            };
            SonidoErroneo(self->sonido);
            BitacoraRegistrar(self->bitacora, &evento);
        }
    }
}

void ControlTarea (void * parametros) {
    control_t self = parametros;
    TickType_t temporizador = xTaskGetTickCount();

    while (true) {
        vTaskDelayUntil(&temporizador, pdMS_TO_TICKS(10));
        SensorEspera(self->pulsador);
        PuertaEspera(self->puerta);
    }
}

/* === Definiciones de funciones externas ====================================================== */

control_t ControlCrear(control_recursos_t recursos, control_configuracion_t configuracion) {
    control_t self = ControlNuevo();

    if (self != NULL) {
        self->pulsador = recursos->pulsador;
        SensorNotificar(self->pulsador, ControlEventoPulsador, self);

        self->puerta = recursos->puerta;
        PuertaConfigurar(self->puerta, &configuracion->puerta);
        PuertaNotificar(self->puerta, ControlEventoPuerta, self);

        self->lectora = recursos->lectora;
        LectoraNotificar(self->lectora, ControlEventoLectora, self);

        self->sonido = recursos->sonido;
        self->bitacora = recursos->bitacora;
        self->autorizados = recursos->autorizados;
        xTaskCreate(ControlTarea, CONTROL_NOMBRE, CONTROL_PILA, self, CONTROL_PRIORIDAD, &self->tarea);
    }
    return self;
}

bool ControlConfigurar(control_t self, control_configuracion_t configuracion) {    
    return PuertaConfigurar(self->puerta, &(configuracion->puerta));
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

