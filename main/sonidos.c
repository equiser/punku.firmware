/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file sonidos.c
 ** @brief Declaraciones de la libreria para la gestion de sonidos
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.12.22 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup aplicacion
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "sonidos.h"
#include "parlantes.h"
#include "errores.h"

#include <stdio.h>

#ifdef TEST
    #include "FreeRTOS.h"
    #include "task.h"
#else
    #include "freertos/FreeRTOS.h"
    #include "freertos/task.h"
#endif

/* === Definicion y Macros ===================================================================== */

#define SONIDOS_NOMBRE          "sonidos"

#define SONIDOS_PILA            (configMINIMAL_STACK_SIZE)

#define SONIDOS_PRIORIDAD       (configMAX_PRIORITIES - 4)

/* == Declaraciones de tipos de datos internos ================================================= */
struct sonido_s {
    parlante_t parlante;
    melodia_t melodia;
    TaskHandle_t tarea;
    digital_salida_t acceso;
};

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de variables internas ====================================================== */
static const nota_t MELODIA_CORRECTA[] = {
    {
        .frecuencia = 523 , .duracion = 1,
    }, {
        .frecuencia = 659, .duracion = 1,
    }, {
        .frecuencia = 784, .duracion = 1,
    }, {
        MELODIA_TERMINAR
    }
};

static const nota_t MELODIA_ERRONEA[] = {
    {
        .frecuencia = 784, .duracion = 1,
    }, {
        .frecuencia = 659, .duracion = 1,
    }, {
        .frecuencia = 523 , .duracion = 1,
    }, {
        MELODIA_TERMINAR
    }
};

/* === Definiciones de funciones internas ====================================================== */
void SonidosTarea (void * parametros) {
    sonido_t self = parametros;

    while (true) {
        vTaskSuspend(self->tarea);
        while ((self->melodia->frecuencia != 0) && (self->melodia->duracion != 0)) {
            ParlanteEmitir(self->parlante, self->melodia->frecuencia);
            vTaskDelay(pdMS_TO_TICKS(100 * self->melodia->duracion));
            self->melodia++;
        }
        ParlanteSilenciar(self->parlante);

        vTaskDelay(pdMS_TO_TICKS(1700));
        SalidaApagar(self->acceso);
    }
}

/* === Definiciones de funciones externas ====================================================== */
sonido_t SonidoCrear(uint32_t terminal, digital_salida_t acceso) {
    static struct sonido_s sonido = {0};
    sonido_t self = &sonido;
    
    if (self->tarea == NULL) {
        self->acceso = acceso;
        self->parlante = ParlanteCrear(terminal);
        xTaskCreate(SonidosTarea, SONIDOS_NOMBRE,  SONIDOS_PILA, self, SONIDOS_PRIORIDAD, &self->tarea);
    }
    return self;
}

void SonidoCorrecto(sonido_t self) {
    self->melodia = MELODIA_CORRECTA;
    SalidaPrender(self->acceso);
    vTaskResume(self->tarea);
}

void SonidoErroneo(sonido_t self) {
    self->melodia = MELODIA_ERRONEA;
    vTaskResume(self->tarea);
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

