/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef LECTORAS_H   /*! @cond    */
#define LECTORAS_H   /*! @endcond */

/** @file lectoras.h
 ** @brief Declaraciones de la libreria para la gestion de la lectora de tarjetas RFID
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.09.14 | evolentini      | Version inicial del archivo para manejo de la lectora   |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "tarjeta.h"
#include "spi.h"
#include "digitales.h"

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos para almacenar una referencia a un objeto lectora
typedef struct lectora_s * lectora_t;

//! Tipo de datos con los eventos enviados por la puerta
typedef enum {
    LECTORA_REPOSO,             //!< La lectora se encuentra en reposo sin ninguna tarjeta en alcance
    LECTORA_TARJETA,            //!< La lectora detecta e identifica una tarjeta correctamente
    LECTORA_ERROR,              //!< La lectora econtro un error al tratar de identificar una tarjeta
} lectora_mensajes_t;

//! Tipo de datos para almacenar una referencia a una funcion que gestiona un evento de lectora
typedef void (*lectora_evento_t)(lectora_mensajes_t mensaje, tarjeta_t tarjeta, lectora_t lectora, void * referencia);

//! Tipo de datos para almacenar los recursos de una puerta
typedef struct lectora_recursos_s {
    spi_puerto_t puerto;                //!< Puerto SPI para comunicacion con el chip de la lectora
    digital_terminal_t seleccion;       //!< Salida digital para selecionar el chip de la lectora
    digital_salida_t reinicio;          //!< Salida digital para reiniciar el chip de la lectora
} const * lectora_recursos_t;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear una nueva instancia de un objeto lectora
 * 
 * @param[in]  recursos     Rererencia a los recursos necesarios para crear una lectora
 * @return     lectora_t    Referencia al nuevo objeto lectora creado
 */
lectora_t LectoraCrear(lectora_recursos_t recursos);

/**
 * @brief Función para pedir a la lectora notificación de los eventos que genera
 * 
 * @param[in]  lectora      Referencia al objeto lectora
 * @param[in]  gestor       Función de callback para informar de los eventos
 * @param[in]  referencia   Referencia de usuario para enviar en los eventos generados
 */
void LectoraNotificar(lectora_t lectora, lectora_evento_t gestor, void * referencia);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* LECTORAS_H */
