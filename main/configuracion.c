/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file configuracion.c
 ** @brief Implementacion de la clase para la gestion de la configuraciones del equipo
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.12.27 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup aplicacion
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */

#include "configuracion.h"
#include "servidor.h"
#include "archivos.h"
#include "json.h"

#include <string.h>

/* === Definicion y Macros ===================================================================== */

//! Nombre del archivo con la configuracion del equipo
#define CONFIGURACION_ARCHIVO    "opciones.txt"

#define CONFIGURACION_INICIAL() {           \
    .control = {                            \
        .puerta = {                         \
            .opciones = {                   \
                .sensor = true,             \
                .inversor = false,          \
                .mecanismo = false,         \
            },                              \
            .tiempos = {                    \
                .liberacion = 10,           \
                .apertura = 30,             \
                .cierre = 50,               \
            },                              \
        },                                  \
    },                                      \
    .servidor = {                           \
        .estacion = true,                   \
        .nombre = "Volentini",              \
        .clave = "Sofia1217",               \
    },                                      \
}

/* == Declaraciones de tipos de datos internos ================================================= */

struct configuracion_s {
    configuracion_evento_t gestor;
    void * refrerencia;
    struct control_configuracion_s control;
    struct servidor_configuracion_s servidor;
};

/* === Definiciones de variables internas ====================================================== */

static const struct json_entrada_s PUERTA_TIEMPOS_JSON[] = {
    JSON_CAMPO(puerta_tiempos_s, liberacion, JSON_UNSIGNED),
    JSON_CAMPO(puerta_tiempos_s, apertura, JSON_UNSIGNED),
    JSON_CAMPO(puerta_tiempos_s, cierre, JSON_UNSIGNED),
    JSON_FINAL(),
};

static const struct json_entrada_s PUERTA_OPCIONES_JSON[] = {
    JSON_CAMPO(puerta_opciones_s, sensor, JSON_BOOLEAN),
    JSON_CAMPO(puerta_opciones_s, inversor, JSON_BOOLEAN),
    JSON_CAMPO(puerta_opciones_s, mecanismo, JSON_BOOLEAN),
    JSON_FINAL(),
};

static const struct json_entrada_s PUERTA_CONFIGURACION_JSON[] = {
    JSON_ANIDADO(puerta_configuracion_s, opciones, PUERTA_OPCIONES_JSON),
    JSON_ANIDADO(puerta_configuracion_s, tiempos, PUERTA_TIEMPOS_JSON),
    JSON_FINAL(),
};

static const struct json_entrada_s CONTROL_CONFIGURACION_JSON[] = {
    JSON_ANIDADO(control_configuracion_s, puerta, PUERTA_CONFIGURACION_JSON),
    JSON_FINAL(),
};

static const struct json_entrada_s SERVIDOR_CONFIGURACION_JSON[] = {
    JSON_CAMPO(servidor_configuracion_s, estacion, JSON_BOOLEAN),
    JSON_CAMPO(servidor_configuracion_s, nombre, JSON_STRING),
    JSON_CAMPO(servidor_configuracion_s, clave, JSON_STRING),
    JSON_FINAL(),
};

static const struct json_entrada_s CONFIGURACION_JSON[] = {
    JSON_ANIDADO(configuracion_s, control, CONTROL_CONFIGURACION_JSON),
    JSON_ANIDADO(configuracion_s, servidor, SERVIDOR_CONFIGURACION_JSON),
    JSON_FINAL(),
};

/* === Definiciones de variables externas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de funciones internas ====================================================== */

/* === Definiciones de funciones externas ====================================================== */

configuracion_t ConfiguracionCrear(void) {
    static struct configuracion_s self[1] = {
        CONFIGURACION_INICIAL()
    };
    archivo_t archivo;
    char cadena[256];
    bool resultado = false;

    archivo = ArchivoAbrir(CONFIGURACION_ARCHIVO, "r+");
    if (archivo) {
        if (ArchivoLeer(archivo, cadena, strlen(cadena))) {
            DeserializarJSON(cadena, self, CONFIGURACION_JSON);
            resultado = true;
        }
        ArchivoCerrar(archivo);
    }

    if (!resultado) {
        archivo_t archivo = ArchivoAbrir(CONFIGURACION_ARCHIVO, "w+");
        if (archivo) {
            SerializarJSON(cadena, self, CONFIGURACION_JSON, sizeof(cadena));
            ArchivoEscribir(archivo, cadena, strlen(cadena));
            ArchivoCerrar(archivo);
        }
    }
    return self;
}

control_configuracion_t ConfiguracionControl(configuracion_t self) {
    return &(self->control);
}

void * ConfiguracionServidor(configuracion_t self) {
    return &(self->servidor);
}

int ConfiguracionConsultar(configuracion_t self, char * cadena, size_t espacio) {
    return SerializarJSON(cadena, self, CONFIGURACION_JSON, espacio);
}

bool ConfiguracionActualizar(configuracion_t self, char * cadena) {
    bool resultado = true;
    DeserializarJSON(cadena, self, CONFIGURACION_JSON);

    archivo_t archivo = ArchivoAbrir(CONFIGURACION_ARCHIVO, "w+");
    if (archivo) {
        char actualizada[256];
        SerializarJSON(actualizada, self, CONFIGURACION_JSON, sizeof(actualizada));
        ArchivoEscribir(archivo, actualizada, strlen(actualizada));
        ArchivoCerrar(archivo);
    }

    if (self->gestor != NULL) resultado = self->gestor(self, self->refrerencia);
    return resultado;
}

void ConfiguracionNotificar(configuracion_t self, configuracion_evento_t gestor, void * referencia) {
    self->gestor = gestor;
    self->refrerencia = referencia;
}
/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

