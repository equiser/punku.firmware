/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file lectoras.h
 ** @brief Declaraciones de la libreria para la gestion de la lectora de tarjetas RFID
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.09.14 | evolentini      | Version inicial del archivo para manejo de la lectora   |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "lectoras.h"
#include "mfrc522.h"
#include "digitales.h"

#include <stdio.h>
#include <string.h>

#ifdef TEST
    #include "FreeRTOS.h"
    #include "task.h"
    #include "queue.h"
    #include "timers.h"
#else
    #include "freertos/FreeRTOS.h"
    #include "freertos/task.h"
    #include "freertos/queue.h"
    #include "freertos/timers.h"
#endif

/* === Definicion y Macros ===================================================================== */

//! Cantidad maxima de lectoras de tarjetas que se reservan en forma estatica
#ifndef LECTORAS_CANTIDAD
    #define LECTORAS_CANTIDAD                1
#endif

#define LECTORA_NOMBRE          "lectora"

#define LECTORA_PILA            (4 * configMINIMAL_STACK_SIZE)

#define LECTORA_PRIORIDAD       (configMAX_PRIORITIES - 3)


/* == Declaraciones de tipos de datos internos ================================================= */

//! Estrcutura que describe la información de una puerta
struct lectora_s {
    spi_puerto_t puerto;            //!< Referencia al puerto SPI utilizado para la comunicacion
    spi_dispositivo_t mfrc522;      //!< Referencia al dispositivo en el bus SPI
    lectora_evento_t gestor;        //!< Funcion para informar un evento de puerta
    void * referencia;              //!< Puntero para enviar una referencia al gestor de eventos
    TaskHandle_t tarea;
};

/* === Definiciones de variables internas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/** @brief Busca un nuevo descriptor de entrada sin usar
 *
 * @return Referencia al descriptor del nuevo objeto lectora 
 */
static lectora_t LectoraNueva(void);

/**
 * @brief Funcion para generar e notificar un evento 
 * 
 * @param[in]  self      Referencia al objeto puerta
 * @param[in]  mensaje   Mensaje que se desea notificar
 * @param[in]  tarjeta   Numero de la tarjeta leida, si corresponde
 */
inline static void InformarEvento(lectora_t self, lectora_mensajes_t mensaje, tarjeta_t tarjeta) {
    if (self->gestor) self->gestor(mensaje, tarjeta, self, self->referencia);
}

/* === Definiciones de funciones internas ====================================================== */

static lectora_t LectoraNueva(void) {
    lectora_t resultado = NULL;

    static int usadas = 0;
    static struct lectora_s lectoras[LECTORAS_CANTIDAD] = {0};
    if (usadas < sizeof(lectoras) / sizeof(struct lectora_s)) {
        memset(&lectoras[usadas], 0, sizeof(struct lectora_s));
        resultado = &lectoras[usadas];
        usadas++;
    }

    return resultado;
}

void LectoraTarea (void * parametros) {
    lectora_t self = parametros;
    TickType_t temporizador = xTaskGetTickCount();
    tarjeta_t tarjeta;
    PICC_UID_TYPE uid;


    while (true) {
        vTaskDelayUntil(&temporizador, pdMS_TO_TICKS(100));
        if (PICC_IsNewCardPresent() && PICC_ReadCardSerial(&uid)) {
            PICC_HaltA();
        }
        if (uid.size == 4) {
            tarjeta = (uid.data[0] << 24) | (uid.data[1] << 16) | (uid.data[2] << 8) | uid.data[0];
            InformarEvento(self, LECTORA_TARJETA, tarjeta);
            memset(&uid, 0, sizeof(PICC_UID_TYPE));
        }
    }
}

/* === Definiciones de funciones externas ====================================================== */

lectora_t LectoraCrear(lectora_recursos_t recursos) {
    lectora_t self = LectoraNueva();

    if (self != NULL) {
        self->puerto = recursos->puerto;
        self->mfrc522 = SpiDispositivoCrear(self->puerto, recursos->seleccion);
        PCD_Init(self->mfrc522, recursos->reinicio);

        xTaskCreate(LectoraTarea, LECTORA_NOMBRE, LECTORA_PILA, self, LECTORA_PRIORIDAD, self->tarea);
    }
    return self;
}

void LectoraNotificar(lectora_t self, lectora_evento_t gestor, void * referencia) {
    if (self != NULL) {
        self->gestor = gestor;
        self->referencia = referencia;
    }
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

