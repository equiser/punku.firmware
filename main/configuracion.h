/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef CONFIGURACION_H   /*! @cond    */
#define CONFIGURACION_H   /*! @endcond */

/** @file configuracion.h
 ** @brief declaraciones de la clase para la gestion de la configuraciones del equipo
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.12.27 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup aplicacion
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "control.h"
// #include "servidor.h"

/* === Cabecera C++ ============================================================================ */

#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

//! Terminal del procesador al que se conecta el pulsador de liberación de la puerta
#define GPIO_PUERTA_PULSADOR            25

//! Terminal del procesador al que se conecta el sensor de puerta abierta
#define GPIO_PUERTA_ABIERTA             26

//! Terminal del procesador que se utiliza para liberar la puerta
#define GPIO_PUERTA_DIRECTA             33

//! Terminal del procesador que se utiliza para bloquear la puerta
#define GPIO_PUERTA_INVERSA             32

//! Terminal del procesador que se utiliza para informar una alarma
#define GPIO_PUERTA_ALARMA              2

//! Terminal del procesador que se utiliza para la autorizacion de ingreso
#define GPIO_AUTORIZADO                 0

//! Terminal del procesador que se utiliza para la emision de sonidos al usuario
#define GPIO_PARLANTE                   21

//! Terminal del procesador que se utiliza para la emision de sonidos al usuario
#define GPIO_PUERTA_LIBERADA            0xFFFFFFFF

//! Terminal del procesador que se utiliza para la emision de sonidos al usuario
#define GPIO_PUERTA_BLOQUEADA           0xFFFFFFFF

//! Puerto SPI utilizado por el chip MFRC-522 de la lectora mifare
#define SPI_PUERTO_LECTORA              2

//! Terminal digital utilizado para seleccionar el chip MFRC-522 de la lectora mifare
#define GPIO_LECTORA_SELECCION          5

//! Terminal digital utilizado para reiniciar el chip MFRC-522 de la lectora mifare
#define GPIO_LECTORA_REINICIO           22

//! Cantidad de muestras consecutivas de un sensor de puerta para producir un cambio de estado
#define SENSORES_HISTERESIS             8

//! Cantidad de muestras consecutivas de un pulsador producir un cambio de estado
#define PULSADORES_HISTERESIS           16

/* === Declaraciones de tipos de datos ========================================================= */

typedef struct configuracion_s * configuracion_t;

//! Tipo de datos para almacenar una referencia a una funcion que gestiona un evento de lectora
typedef bool (*configuracion_evento_t)(configuracion_t configuracion, void * referencia);

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Funcion para crear un objeto condiguracion que gestione las opciones del equipo
 * 
 * @return                 Referencia al objeto configuracion que se acaba de crear
 */
configuracion_t ConfiguracionCrear(void);

/**
 * @brief Funcion para obtener la estructura para configurar el control de la puerta
 * 
 * @param  configuracion   Referencia al objeto configuracion que se desea consultar
 * @return                 Puntero a la estructura con la configuracion del control
 */
control_configuracion_t ConfiguracionControl(configuracion_t configuracion);

/**
 * @brief Funcion para obtener la estructura para configurar el servidor HTTP
 * 
 * @param  configuracion   Referencia al objeto configuracion que se desea consultar
 * @return                 Puntero a la estructura con la configuracion del servidor
 */
void * ConfiguracionServidor(configuracion_t configuracion);

/**
 * @brief Funcion obtener la configuracion actual en una cadena de texto
 * 
 * @param   configuracion   Referencia al objeto configuracion que se desea consultar
 * @param   cadena          Puntero a la cadena de caracteres donde se devuelve la configuracion actual
 * @param   espacio         Cantidad de caracteres reservados en la cadena de resultado
 * @return int              Longitud de la cadena generada, o -1 si no hay espacio suficiente
 */
int ConfiguracionConsultar(configuracion_t configuracion, char * cadena, size_t espacio);

/**
 * @brief Funcion para actualizar la configuracion desde una cadena de texto
 * 
 * @param   configuracion   Referencia al objeto configuracion que se desea actualizar
 * @param   cadena          Puntero a la cadena de caracteres desde la que se realizar la actualizacion
 */
bool ConfiguracionActualizar(configuracion_t configuracion, char * cadena);

/**
 * @brief Funcion para pedir a la configuracion notificacion por cambios
 * 
 * @param[in]  configuracion    Referencia al objeto configuracion que genera el evento
 * @param[in]  gestor           Función de callback para informar de los eventos
 * @param[in]  referencia       Referencia de usuario para enviar en los eventos generados
 */
void ConfiguracionNotificar(configuracion_t configuracion, configuracion_evento_t gestor, void * referencia);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* CONTROL_H */
