/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file punku.c
 ** @brief Programa principal del firmware el equipo
 **
 ** Programa principal del firmware del equipo
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.06.03 | evolentini      | Version inicial del archivo para manejo de teclado      |
 **
 ** @addtogroup aplicacion
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "archivos.h"
#include "configuracion.h"
#include "errores.h"
#include "control.h"
#include "servidor.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <string.h>

/* === Definicion y Macros ===================================================================== */

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de funciones internas ====================================================== */
bool EventoConfigurar(configuracion_t configuracion, void * referencia) {
    control_t control = referencia;
    bool resultado;

    resultado = ControlConfigurar(control, ConfiguracionControl(configuracion));
    if (resultado) {
        Registrar(INFORMACION, "Se actualizo la configuracion del control");
    } else {
        Alerta("No se pudo actualizar la configuracion del control");
    }
    return resultado;
}

/* === Definiciones de funciones externas ====================================================== */

void app_main(void * parametros) {
    ArchivosConfigurar();
    configuracion_t configuracion = ConfiguracionCrear();

    sensor_t pulsador = SensorCrear(EntradaCrear(GPIO_PUERTA_PULSADOR, ENTRADA_PULLUP), PULSADORES_HISTERESIS); 

    reloj_t reloj = RelojCrear();
    
    sonido_t sonido = SonidoCrear(GPIO_PARLANTE, SalidaCrear(GPIO_AUTORIZADO, SALIDA_NORMAL));
    bitacora_t bitacora = BitacoraCrear(reloj);

    autorizados_t autorizados = AutorizadosCrear();
    if (autorizados == NULL) {
        Error("No se pudo crear la lista de tarjetas autorizadas");
    }
    
    puerta_t puerta = PuertaCrear(
        &(struct puerta_recursos_s) {
            .abierta = SensorCrear(EntradaCrear(GPIO_PUERTA_ABIERTA, ENTRADA_PULLDOWN), SENSORES_HISTERESIS),
            // .liberada = SensorCrear(EntradaCrear(GPIO_PUERTA_LIBERADA, ENTRADA_PULLUP), SENSORES_HISTERESIS),
            // .bloqueada = SensorCrear(EntradaCrear(GPIO_PUERTA_BLOQUEADA, ENTRADA_PULLUP), SENSORES_HISTERESIS),
            .directa = SalidaCrear(GPIO_PUERTA_DIRECTA, SALIDA_NORMAL),
            .inversa = SalidaCrear(GPIO_PUERTA_INVERSA, SALIDA_NORMAL),
            .alarma = SalidaCrear(GPIO_PUERTA_ALARMA, SALIDA_NORMAL),
        }, &(ConfiguracionControl(configuracion)->puerta)
    );

    lectora_t lectora = LectoraCrear(
        &(struct lectora_recursos_s) {
            .puerto = SpiPuertoCrear(SPI_PUERTO_LECTORA),
            .seleccion = GPIO_LECTORA_SELECCION,
            .reinicio = SalidaCrear(GPIO_LECTORA_REINICIO, SALIDA_NORMAL),
        }
    );
    
    control_t control = ControlCrear( 
        &(struct control_recursos_s) {
            .lectora = lectora,
            .puerta = puerta,
            .pulsador = pulsador,
            .sonido = sonido,
            .bitacora = bitacora,
            .autorizados = autorizados,
        }, ConfiguracionControl(configuracion)
    );

    ServidorCrear(
        &(struct servidor_recursos_s) {
            .reloj = reloj,
            .bitacora = bitacora,
            .autorizados = autorizados,
        }, configuracion
    );

    ConfiguracionNotificar(configuracion, EventoConfigurar, control);
}
/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

