/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file servidor.c
 ** @brief Implementacion de la libreria para la gestion del servidor HTTP sobre WiFi
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.12.27 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup aplicacion
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "servidor.h"
#include <stddef.h>

#ifdef TEST
    #include "esp_http_server.h"
#else
    #include <esp_wifi.h>
    #include <esp_event_loop.h>
    #include <esp_log.h>
    #include <esp_system.h>
    #include <nvs_flash.h>
    #include <sys/param.h>

    #include <esp_http_server.h>
#endif
#include "json.h"
#include "tarjeta.h"
#include "errores.h"
#include "configuracion.h"

/* === Definicion y Macros ===================================================================== */

/* == Declaraciones de tipos de datos internos ================================================= */
struct servidor_s {
    reloj_t reloj;
    bitacora_t bitacora;
    autorizados_t autorizados;
    httpd_handle_t servidor;
    configuracion_t configuracion;
};

typedef struct cadena_fecha_hora_s {
    char fecha[15];
    char hora[9];
} * cadena_fecha_hora_t;

/* === Declaraciones de funciones internas ===================================================== */

static void ConfigurarRed(void * parametro);

static bool ServidorIniciar(servidor_t self);

static esp_err_t ServidorEvento(void * contexto, system_event_t * evento);

static tarjeta_t ObtenerTarjeta(httpd_req_t * request);

static esp_err_t ConsultarReloj(httpd_req_t * request);

static esp_err_t ActualizarReloj(httpd_req_t * request);

static esp_err_t ConsultarTarjeta(httpd_req_t * request);

static esp_err_t AgregarTarjeta(httpd_req_t * request);

static esp_err_t BorrarTarjeta(httpd_req_t * request);

static esp_err_t ObtenerLista(httpd_req_t * request);

static esp_err_t BorrarLista(httpd_req_t * request);

static esp_err_t ConsultarBitacora(httpd_req_t * request);

static esp_err_t ConsultarConfiguracion(httpd_req_t * request);

static esp_err_t ActualizarConfiguracion(httpd_req_t * request);

static bool ListarTarjeta(uint16_t indice, tarjeta_t tarjeta, void * referencia);

// static esp_err_t ConfigurarServidor(httpd_req_t * request);

/* === Definiciones de variables internas ====================================================== */

static const char correcto[] = "{\"resultado\": true}";

static const char erroneo[] = "{\"resultado\": false}";

static const httpd_uri_t direcciones[] = {
    {
        .uri    = "/reloj",
        .method    = HTTP_GET,
        .handler   = ConsultarReloj,
        .user_ctx  = NULL,
    },
    {
        .uri    = "/reloj",
        .method    = HTTP_PUT,
        .handler   = ActualizarReloj,
        .user_ctx  = NULL,
    },
    {
        .uri    = "/autorizados",
        .method    = HTTP_GET,
        .handler   = ConsultarTarjeta,
        .user_ctx  = NULL,
    },
    {
        .uri    = "/autorizados",
        .method    = HTTP_PUT,
        .handler   = AgregarTarjeta,
        .user_ctx  = NULL,
    },
    {
        .uri    = "/autorizados",
        .method    = HTTP_DELETE,
        .handler   = BorrarTarjeta,
        .user_ctx  = NULL,
    },
    {
        .uri    = "/autorizados/lista",
        .method    = HTTP_GET,
        .handler   = ObtenerLista,
        .user_ctx  = NULL,
    },
    {
        .uri    = "/autorizados/lista",
        .method    = HTTP_DELETE,
        .handler   = BorrarLista,
        .user_ctx  = NULL,
    },
    {
        .uri    = "/bitacora",
        .method    = HTTP_GET,
        .handler   = ConsultarBitacora,
        .user_ctx  = NULL,
    },
    {
        .uri    = "/configuracion",
        .method    = HTTP_GET,
        .handler   = ConsultarConfiguracion,
        .user_ctx  = NULL,
    },
    {
        .uri    = "/configuracion",
        .method    = HTTP_PUT,
        .handler   = ActualizarConfiguracion,
        .user_ctx  = NULL,
    },
};

/* === Definiciones de variables internas ====================================================== */

/* === Definiciones de funciones internas ====================================================== */

static void ConfigurarRed(void * parametro) {
    servidor_t self = parametro;

    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(ServidorEvento, self));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));

    wifi_config_t configuracion= {
        .sta = {
            .ssid = "FliaVolentini",
            .password = "Sofia1217",
        },
    };

    servidor_configuracion_t opciones = ConfiguracionServidor(self->configuracion);
    if (opciones->estacion){
    //     strcpy((char *) configuracion.sta.ssid, opciones->nombre);
    //     strcpy((char *) configuracion.sta.password, opciones->clave);
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
        ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &configuracion));
    } else {
        strcpy((char *) configuracion.ap.ssid, opciones->nombre);
        strcpy((char *) configuracion.ap.password, opciones->clave);
        configuracion.ap.ssid_len = strlen(opciones->nombre);
        configuracion.ap.max_connection = 1;
        configuracion.ap.ssid_hidden = false;
        configuracion.ap.channel = 8;
        configuracion.ap.authmode = WIFI_AUTH_WPA_WPA2_PSK;
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
        ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &configuracion));
    }
    ESP_ERROR_CHECK(esp_wifi_start());
}

static bool ServidorIniciar(servidor_t self) {
    httpd_config_t configuracion = HTTPD_DEFAULT_CONFIG();
    
    configuracion.max_uri_handlers = sizeof(direcciones)/sizeof(httpd_uri_t);
    if (httpd_start(&self->servidor, &configuracion) == ESP_OK) {
        httpd_uri_t direccion[1];
        for (int indice = 0; indice < sizeof(direcciones) / sizeof(httpd_uri_t); indice++) {
            memcpy(direccion, &direcciones[indice], sizeof(httpd_uri_t));
            direccion->user_ctx = self;
            if (httpd_register_uri_handler(self->servidor, direccion) != ESP_OK) {
                Alerta("No se pudo registrar la ruta %s", direccion->uri);
            };
        }
        Registrar(INFORMACION, "Servidor de coumicaciones iniciado correctamente");
    } else {
        self->servidor = NULL;
        Alerta("No se pudo iniciar el servidor de comunicaciones");
    }
    return (self->servidor != NULL);
}

static esp_err_t ServidorEvento(void * contexto, system_event_t * evento) {
    static system_event_id_t last_event;
    servidor_t self = contexto;
    tcpip_adapter_ip_info_t direccion;

    switch(evento->event_id) {
        case SYSTEM_EVENT_STA_START:
            if (esp_wifi_connect() != ESP_OK) {
                Alerta("Error al conectarse a la red WiFi");
            } else {
                Registrar(INFORMACION, "Conectado a la red WiFi en modo estacion");
            }
            break;
        case SYSTEM_EVENT_AP_START:
            tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_AP, &direccion);
    
            servidor_configuracion_t configuracion = ConfiguracionServidor(self->configuracion);
            Registrar(INFORMACION, "Punto de acceso a la red WiFi %s disponible", configuracion->nombre);
            Registrar(INFORMACION, "El servidor tiene asignada la direccion IP %s", ip4addr_ntoa(&direccion.ip));
            if (self->servidor == NULL) ServidorIniciar(self);
            break;
        case SYSTEM_EVENT_STA_GOT_IP:
            Registrar(INFORMACION, "El servidor tiene asignada la direccion IP %s", 
                ip4addr_ntoa(&evento->event_info.got_ip.ip_info.ip));
            if (self->servidor == NULL) ServidorIniciar(self);
            break;
        case SYSTEM_EVENT_STA_DISCONNECTED:
            if ((esp_wifi_connect() != ESP_OK) && (last_event != SYSTEM_EVENT_STA_DISCONNECTED)) {
                Error("Estacion desconectada de la red WiFi");
            }
            if (self->servidor) {
                httpd_stop(self->servidor);
                self->servidor = NULL;
                Alerta("Se detuvo el servidor de comunicaciones");
            }
            break;
        default:
            break;
    }
    last_event = evento->event_id;
    return ESP_OK;
}

static servidor_t ServidorNuevo(void) {
    servidor_t resultado = NULL;

    static int usadas = 0;
    static struct servidor_s servidores[1] = {0};
    if (usadas < sizeof(servidores) / sizeof(struct servidor_s)) {
        memset(&servidores[usadas], 0, sizeof(struct servidor_s));
        resultado = &servidores[usadas];
        usadas++;
    }

    return resultado;
}

static tarjeta_t ObtenerTarjeta(httpd_req_t * request) {
    static struct json_entrada_s campos[] = {
        {
            .tipo = JSON_UNSIGNED,
            .etiqueta = "numero",
            .desplazamiento = 0,
            .espacio = sizeof(tarjeta_t)
        },{
            .tipo = JSON_END_DATA,
        }
    };
    tarjeta_t tarjeta = 0;
    char parametros[64];
    
    if (request->content_len < sizeof(parametros)) {
        if (httpd_req_recv(request, parametros, request->content_len) > 0) {
            DeserializarJSON(parametros, &tarjeta, campos);
        }
    }

    httpd_resp_set_type(request, HTTPD_TYPE_JSON);
    if (tarjeta == 0) {
        httpd_resp_set_status(request, HTTPD_400);
        httpd_resp_send(request, erroneo, strlen(erroneo));
    }
    return tarjeta;
}

static bool InterpretarFecha(char * cadena, struct tm * fecha) {
    static const char dias[][4] = {
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };    
    bool resultado = (strlen(cadena) == 14);

    if (resultado) {
        for (fecha->tm_wday = 0; fecha->tm_wday < 7; fecha->tm_wday++) {
            resultado = (strncasecmp(cadena, dias[fecha->tm_wday], 3) == 0);
            if (resultado) break;
        }
    }
    if (resultado) {
        cadena[8] = 0;
        fecha->tm_year = atoi(&cadena[4]) - 1900;
        resultado = (fecha->tm_year > 0);
    }
    if (resultado) {
        cadena[11] = 0;
        fecha->tm_mon = atoi(&cadena[9]) - 1;
        resultado = (fecha->tm_mon >= 0) && (fecha->tm_mon <= 11);
    }
    if (resultado) {
        fecha->tm_mday = atoi(&cadena[12]);
        resultado = (fecha->tm_mday >= 1) && (fecha->tm_mday <= 31);
    }
    return resultado;
}

static bool InterpretarHora(char * cadena, struct tm * hora) {
    bool resultado = (strlen(cadena) == 8);

    if (resultado) {
        cadena[2] = 0;
        hora->tm_hour = atoi(&cadena[0]);
        resultado = (hora->tm_hour >= 0) && (hora->tm_hour <= 23);
    }
    if (resultado) {
        cadena[5] = 0;
        hora->tm_min = atoi(&cadena[3]);
        resultado = (hora->tm_min >= 0) && (hora->tm_min <= 59);
    }
    if (resultado) {
        hora->tm_sec = atoi(&cadena[6]);
        resultado = (hora->tm_sec >= 0) && (hora->tm_sec <= 59);
    }
    return resultado;
}

static bool ObtenerFechaHora(httpd_req_t * request, struct tm * fecha) {
    static struct json_entrada_s campos[] = {
        {
            .tipo = JSON_STRING,
            .etiqueta = "fecha",
            .desplazamiento = offsetof(struct cadena_fecha_hora_s, fecha),
            .espacio = sizeof(((cadena_fecha_hora_t)0)->fecha),
        },{
            .tipo = JSON_STRING,
            .etiqueta = "hora",
            .desplazamiento = offsetof(struct cadena_fecha_hora_s, hora),
            .espacio = sizeof(((cadena_fecha_hora_t)0)->hora),
        },{
            .tipo = JSON_END_DATA,
        }
    };
    bool resultado;
    char parametros[64];
    struct cadena_fecha_hora_s valores[1] = {0};
    
    if (request->content_len < sizeof(parametros)) {
        if (httpd_req_recv(request, parametros, request->content_len) > 0) {
            DeserializarJSON(parametros, valores, campos);
        }
    }

    httpd_resp_set_type(request, HTTPD_TYPE_JSON);

    resultado = InterpretarFecha(valores->fecha, fecha);
    if (resultado) {
        resultado = InterpretarHora(valores->hora, fecha);
    }
    strftime(parametros, sizeof(parametros), "%a %Y-%m-%d %H:%M:%S", fecha);
    Registrar(INFORMACION, parametros);

    if (!resultado) {
        httpd_resp_set_status(request, HTTPD_400);
        httpd_resp_send(request, erroneo, strlen(erroneo));
    }
    return resultado;
}

static esp_err_t ConsultarReloj(httpd_req_t * request) {
    static const char formato[] = "{"
        "\"fecha\":\"%a %Y-%m-%d\",\"hora\":\"%H:%M:%S\""
    "}";
    servidor_t self = request->user_ctx;
    struct tm actual;
    char fecha[64];

    RelojConsultar(self->reloj, &actual);
    strftime(fecha, sizeof(fecha), formato, &actual);
    httpd_resp_set_type(request, HTTPD_TYPE_JSON);
    httpd_resp_send(request, fecha, strlen(fecha));

    return ESP_OK;
}

static esp_err_t ActualizarReloj(httpd_req_t * request) {
    servidor_t self = request->user_ctx;
    struct tm fecha;

    if (ObtenerFechaHora(request, &fecha)) {
        RelojActualizar(self->reloj, &fecha);
        Alerta("Se actualizo la fecha y hora del reloj RTC");
        return ConsultarReloj(request);
    }
    return ESP_OK;
}

esp_err_t AgregarTarjeta(httpd_req_t * request) {
    servidor_t self = request->user_ctx;
    tarjeta_t tarjeta = ObtenerTarjeta(request);

    if (tarjeta) {
        httpd_resp_set_status(request, HTTPD_200);
        if (AutorizadosAgregar(self->autorizados, tarjeta)) {
            Registrar(INFORMACION, "Se agrego la tarjeta 0x%08X a la lista de autorizados", tarjeta);
            httpd_resp_send(request, correcto, strlen(correcto));
        } else {
            Alerta("No se puedo agregar la tarjeta 0x%08X a la lista de autorizados", tarjeta);
            httpd_resp_send(request, erroneo, strlen(erroneo));
        }
    }
    return ESP_OK;
}

esp_err_t ConsultarTarjeta(httpd_req_t * request) {
    servidor_t self = request->user_ctx;
    tarjeta_t tarjeta = ObtenerTarjeta(request);

    if (tarjeta) {
        httpd_resp_set_status(request, HTTPD_200);
        if (AutorizadosValidar(self->autorizados, tarjeta)) {
            Registrar(INFORMACION, "La tarjeta 0x%08X esta autorizada", tarjeta);
            httpd_resp_send(request, correcto, strlen(correcto));
        } else {
            Registrar(INFORMACION, "La tarjeta 0x%08X no esta autorizada", tarjeta);
            httpd_resp_send(request, erroneo, strlen(erroneo));
        }
    }
    return ESP_OK;
}

esp_err_t BorrarTarjeta(httpd_req_t * request) {
    servidor_t self = request->user_ctx;
    tarjeta_t tarjeta = ObtenerTarjeta(request);

    if (tarjeta) {
        httpd_resp_set_status(request, HTTPD_200);
        if (AutorizadosBorrar(self->autorizados, tarjeta)) {
            Registrar(INFORMACION, "Se borro la tarjeta 0x%08X de la lista de autorizados", tarjeta);
            httpd_resp_send(request, correcto, strlen(correcto));
        } else {
            Alerta("No se puedo borrar la tarjeta 0x%08X de la lista de autorizados", tarjeta);
            httpd_resp_send(request, erroneo, strlen(erroneo));
        }
    }
    return ESP_OK;
}

esp_err_t BorrarLista(httpd_req_t * request) {
    servidor_t self = request->user_ctx;

    httpd_resp_set_status(request, HTTPD_200);
    if (AutorizadosLimpiar(self->autorizados)) {
        Registrar(INFORMACION, "Se borro la lista de autorizados");
        httpd_resp_send(request, correcto, strlen(correcto));
    } else {
        Alerta("No se puedo borrar la lista de autorizados");
        httpd_resp_send(request, erroneo, strlen(erroneo));
    }
    return ESP_OK;
}

esp_err_t ObtenerLista(httpd_req_t * request) {
    servidor_t self = request->user_ctx;

    httpd_resp_set_type(request, HTTPD_TYPE_JSON);
    httpd_resp_set_status(request, HTTPD_200);

    httpd_resp_send_chunk(request, "[", 1);
    AutorizadosListar(self->autorizados, ListarTarjeta, request);
    httpd_resp_send_chunk(request, "]", 1);
    httpd_resp_send_chunk(request, NULL, 0);
    return ESP_OK;
}

bool ListarTarjeta(uint16_t indice, tarjeta_t tarjeta, void * referencia) {
    httpd_req_t * request = referencia;
    char cadena[32];

    if (indice == 0) {
        sprintf(cadena, "{\"numero\":\"0x%08X\"}", tarjeta);
    } else {
        sprintf(cadena, ",{\"numero\":\"0x%08X\"}", tarjeta);
    }
    Registrar(INFORMACION, cadena);
    httpd_resp_send_chunk(request, cadena, strlen(cadena));
    
    return true;
}

bool ListarBitacora(uint16_t indice, bitacora_entrada_t entrada, void * referencia) {
    httpd_req_t * request = referencia;
    char cadena[150];

    if (indice > 0) httpd_resp_send_chunk(request, ",", 1);
    BitacoraCadena(entrada, cadena, sizeof(cadena));

    Registrar(INFORMACION, cadena);
    httpd_resp_send_chunk(request, cadena, strlen(cadena));
    return true;
}

esp_err_t ConsultarBitacora(httpd_req_t * request) {
    servidor_t self = request->user_ctx;

    httpd_resp_set_type(request, HTTPD_TYPE_JSON);
    httpd_resp_set_status(request, HTTPD_200);

    httpd_resp_send_chunk(request, "[", 1);
    BitacoraObtener(self->bitacora, ListarBitacora, request);
    httpd_resp_send_chunk(request, "]", 1);
    httpd_resp_send_chunk(request, NULL, 0);

    return ESP_OK;
}

esp_err_t ConsultarConfiguracion(httpd_req_t * request) {
    servidor_t self = request->user_ctx;
    char cadena[256];

    if (ConfiguracionConsultar(self->configuracion, cadena, sizeof(cadena)) > 0) {
        httpd_resp_set_status(request, HTTPD_200);
        httpd_resp_set_type(request, HTTPD_TYPE_JSON);
        httpd_resp_send(request, cadena, strlen(cadena));
    } else {
        httpd_resp_set_status(request, HTTPD_500);
    }
    return ESP_OK;
}

esp_err_t ActualizarConfiguracion(httpd_req_t * request) {
    servidor_t self = request->user_ctx;
    char cadena[400];

    cadena[0] = 0;
    if (request->content_len < sizeof(cadena)) {
        httpd_req_recv(request, cadena, request->content_len);
    }

    if (cadena[0]) {
        httpd_resp_set_status(request, HTTPD_200);
        httpd_resp_set_type(request, HTTPD_TYPE_JSON);

        if (ConfiguracionActualizar(self->configuracion, cadena)) {
            if (ConfiguracionConsultar(self->configuracion, cadena, sizeof(cadena)) > 0) {
                httpd_resp_set_status(request, HTTPD_200);
                httpd_resp_set_type(request, HTTPD_TYPE_JSON);
                httpd_resp_send(request, cadena, strlen(cadena));
            } else {
                httpd_resp_set_status(request, HTTPD_500);
            }
        } else {
            Alerta("No se puedo actualizar la configuracion del equipo");
            httpd_resp_send(request, erroneo, strlen(erroneo));
        }
    } else {
        httpd_resp_set_status(request, HTTPD_400);
        httpd_resp_send(request, erroneo, strlen(erroneo));
    }
    return ESP_OK;
}

/* === Definiciones de funciones externas ====================================================== */

servidor_t ServidorCrear(servidor_recursos_t recursos, void * configuracion) {
    servidor_t self = ServidorNuevo();

    self->configuracion = configuracion;
    self->autorizados = recursos->autorizados;
    self->bitacora = recursos->bitacora;
    self->reloj = recursos->reloj;

    ESP_ERROR_CHECK(nvs_flash_init());
    ConfigurarRed(self);
    return self;
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

