/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef SONIDOS_H   /*! @cond    */
#define SONIDOS_H   /*! @endcond */

/** @file parlantes.h
 ** @brief Declaraciones de la libreria para la gestion de parlantes
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.12.22 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup aplicacion
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "digitales.h"
#include <stdint.h>

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */
#define MELODIA_TERMINAR    0

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos para almacenar una referencia a una lista de tarjetas autorizadas
typedef struct nota_s {
    uint16_t frecuencia;        //!< Frecuencia de la nota que se debe ejecutar
    uint8_t duracion;           //!< Duracion en decimas de segundo que debe sonar la nota
} nota_t;

typedef nota_t const * melodia_t;

//! Tipo de datos para almacenar una referencia a una interfase para emitir sonidos
typedef struct sonido_s * sonido_t;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear una nueva instancia de un parlante
 * 
 * @param[in]  parlante         Referencia al objeto para la emision de sonidos
 * @return     parlante_t       Referencia al nuevo objeto parlante creado
 */
sonido_t SonidoCrear(uint32_t terminal, digital_salida_t acceso);

/**
 * @brief Función para emitir un nuevo sonido
 * 
 * @param[in]  parlante         Referencia al objeto para la emision de sonidos
 * @param[in]  fecuencia        Frecuencia del sonido que se desea emitir
 */
void SonidoCorrecto(sonido_t sonido);

/**
 * @brief Función para silenciar cualquier sonido en el parlante
 * 
 * @param[in]  parlante         Referencia al objeto para la emision de sonidos
 */
void SonidoErroneo(sonido_t sonido);


/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* SONIDOS_H */
