/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef CONTROL_H   /*! @cond    */
#define CONTROL_H   /*! @endcond */

/** @file control.h
 ** @brief Declaraciones de la tarea para control de la puerta del equipo
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.05.30 | evolentini      | Version inicial del archivo para manejo de auditoria    |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                  |
 **
 ** @addtogroup aplicacion
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "puertas.h"
#include "lectoras.h"
#include "sonidos.h"
#include "autorizados.h"
#include "bitacora.h"
#include "json.h"

/* === Cabecera C++ ============================================================================ */

#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

/* === Declaraciones de tipos de datos ========================================================= */

//! Tipo de datos para almacenar una referencia a un objeto control
typedef struct control_s * control_t;

//! Estructura con las opciones de funcionamiento del control
typedef struct control_configuracion_s {
    struct puerta_configuracion_s puerta;
} * control_configuracion_t;

typedef struct control_recursos_s {
    sensor_t pulsador;              //!< Sensor ditigal para liberacion manual de la puerta
    puerta_t puerta;                //!< Refrencia al objeto puerta que se desea controlar
    lectora_t lectora;              //!< Refrecnia al objeto lectora que libera la puerta
    sonido_t sonido;
    bitacora_t bitacora;
    autorizados_t autorizados;
} * control_recursos_t;

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear una nueva instancia de un objeto de contro de una puerta
 * 
 * @param   terminales      Refrecnia a la estructura con los terminales utilizados en el contro de la puerta
 * @param   configuracion   Referencia a la estructura con la configuracion inical del control de la puerta
 * @return  puerta_t        Referencia al nuevo objeto contrl de puerta creado
 */
control_t ControlCrear(control_recursos_t recursos, control_configuracion_t configuracion);

/**
 * @brief Función para configurar los tiempos y opciones de una puerta
 * 
 * @param   control         Referencia al objeto control de puerta que se desea configurar
 * @param   configuracion   Referencia a la estructura con la configuracion del control de puerta
 * @return  true            El control de puerta se configuro correctamente con los nuevos valores
 * @return  false           No se pudo configurar el control de puerta porque alguno de los valores es erroneo
 */
bool ControlConfigurar(control_t control, control_configuracion_t configuracion);

/**
 * @brief Función para recuperar la configuración actual del control de accesos
 * 
 * @param  self        Puntero al objeto devuelto en la creación del control de accesos
 * @param  opciones    Puntero a la estructura donse de copian los valores actuales 
 */
// void ControlConfiguracion(control_t self, control_opciones_t * opciones);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* CONTROL_H */
