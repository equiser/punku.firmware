/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file bitacora.h
 ** @brief Declaraciones de la libreria para la gestion de la bitacora de eventos
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.06.03 | evolentini      | Version inicial del archivo para manejo de bitacora     |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                   |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include <stdint.h>
#include <string.h>
#include "bitacora.h"
#include "archivos.h"
#include "reloj.h"
#include "errores.h"

#ifdef TEST
    #include "FreeRTOS.h"
    #include "task.h"
    #include "queue.h"
#else
    #include "freertos/FreeRTOS.h"
    #include "freertos/task.h"
    #include "freertos/queue.h"
#endif

/* === Definicion y Macros ===================================================================== */

//! Nombre del archivo de la bitacora de accesos
#define BITACORA_ARCHIVO    "bitacora.txt"

#define BITACORA_NOMBRE          "control"
#define BITACORA_PILA            (4 * configMINIMAL_STACK_SIZE)
#define BITACORA_PRIORIDAD       (configMAX_PRIORITIES - 1)
#define BITACORA_COLA_CANTIDAD   4

#define ENTRADAS            64
/* == Declaraciones de tipos de datos internos ================================================= */

//! Estructura con los miembros de un objeto bitacora
struct bitacora_s {
    reloj_t reloj;                  //!< Referencia a reloj para obtener la fecha y hora actuales
    TaskHandle_t tarea;
    QueueHandle_t cola;
};

struct bitacora_entrada_s {
    struct tm fecha;
    bitacora_mensajes_t mensaje;
    uint32_t tarjeta;
};

struct almacenamiento_s {
    int ultimo;
    int cantidad;
    struct bitacora_entrada_s entradas[ENTRADAS];
};

/* === Definiciones de variables internas ====================================================== */

static const char * mensajes[] = {
    "Liberacion sin apertura de la puerta",
    "Apertura y cierre de la puerta",
    "Apertura sin cierre de la puerta",
    "Cierre de la puerta",
    "Apertura forzada de la puerta",
    "Tarjeta no autorizada",
    "Sistema reiniciado",
};

static const char * formato[] = {
    "{"
    "\"fecha\":\"%s\","
    "\"hora\":\"%s\","
    "\"origen\":\"%s\","
    "\"evento\":%d,"
    "\"descripcion\":\"%s\""
    "}"
,
    "{"
    "\"fecha\":\"%s\","
    "\"hora\":\"%s\","
    "\"origen\":\"0x%08X\","
    "\"evento\":%d,"
    "\"descripcion\":\"%s\""
    "}"
};

static struct almacenamiento_s almacenamiento = {0};

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de funciones internas ====================================================== */

void PonerHora(bitacora_t self) {
    // DOM 2019-12-22 18:35:30
    struct tm fecha = {
        .tm_sec = 30,
        .tm_min = 35,
        .tm_hour = 18,
        .tm_mday = 22,
        .tm_mon = 11,
        .tm_year = 119,
        .tm_wday = 0,
    };
    RelojActualizar(self->reloj, &fecha);
}

void BitacoraCadena(bitacora_entrada_t entrada, char * cadena, size_t espacio) {
    char fecha[15];
    char hora[10];

    strftime(fecha, sizeof(fecha),  "%a %Y-%m-%d", &entrada->fecha);
    strftime(hora, sizeof(hora),  "%H:%M:%S", &entrada->fecha);
    if (entrada->tarjeta == ORIGEN_EXTERNO) {
        snprintf(cadena, espacio, formato[0], fecha, hora, "externo", entrada->mensaje, mensajes[entrada->mensaje]);
    } else if (entrada->tarjeta == ORIGEN_PULSADOR) {
        snprintf(cadena, espacio, formato[0], fecha, hora, "pulsador", entrada->mensaje, mensajes[entrada->mensaje]);
    } else  {
        snprintf(cadena, espacio, formato[1], fecha, hora, entrada->tarjeta, entrada->mensaje, mensajes[entrada->mensaje]);
    }
}

void BitacoraTarea(void * parametros) {
    bitacora_t self = parametros;
    // archivo_t archivo;
    char cadena[150];
    bitacora_entrada_t entrada;
    
    almacenamiento.ultimo = 0;
    almacenamiento.cantidad = 0;
    while(true) {
        entrada = &almacenamiento.entradas[almacenamiento.ultimo];
        xQueueReceive(self->cola, entrada, portMAX_DELAY);

        almacenamiento.ultimo = (almacenamiento.ultimo + 1) & (ENTRADAS - 1);
        if (almacenamiento.cantidad < ENTRADAS) almacenamiento.cantidad++;

        BitacoraCadena(entrada, cadena, sizeof(cadena));

        // archivo = ArchivoAbrir(BITACORA_ARCHIVO, "a");
        // ArchivoMover(archivo, 0, ARCHIVO_FINAL);
        // ArchivoEscribir(archivo, cadena, strlen(cadena));
        // ArchivoCerrar(archivo);

        Registrar(INFORMACION, cadena);
    }
}
/* === Definiciones de funciones externas ====================================================== */

bitacora_t BitacoraCrear(reloj_t reloj) {
    static struct bitacora_s bitacora = {0};
    bitacora_t self = &(bitacora);

    struct bitacora_evento_s evento = {
        .mensaje = BITACORA_REINICIO,
    };

    self->reloj = reloj;
    self->cola = xQueueCreate(BITACORA_COLA_CANTIDAD, sizeof(struct bitacora_entrada_s));
    xTaskCreate(&BitacoraTarea, BITACORA_NOMBRE, BITACORA_PILA, self, BITACORA_PRIORIDAD, self->tarea);

    BitacoraRegistrar(self, &evento);
    return self;
}

void BitacoraRegistrar(bitacora_t self, const bitacora_evento_t evento) {
    struct bitacora_entrada_s entrada[1];

    entrada->mensaje = evento->mensaje;
    entrada->tarjeta = evento->tarjeta;
    RelojConsultar(self->reloj, &entrada->fecha);
    xQueueSend(self->cola, entrada, 0);
}

void BitacoraObtener(bitacora_t self, bitacora_listar_t gestor, void * referencia) {
    int indice;
    
    for(int cantidad = 0; cantidad < almacenamiento.cantidad; cantidad++) {
        indice = (almacenamiento.ultimo - cantidad - 1);
        if (indice < 0) indice += ENTRADAS;
        gestor(cantidad, &almacenamiento.entradas[indice], referencia);
    }
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

