/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

#ifndef BITACORA_H   /*! @cond    */
#define BITACORA_H   /*! @endcond */

/** @file bitacora.h
 ** @brief Declaraciones de la libreria para la gestion de la bitacora de eventos
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.06.03 | evolentini      | Version inicial del archivo para manejo de bitacora     |
 **|   2 | 2019.06.23 | evolentini      | Mejora en la documentación del archivo                   |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de archivos externos ======================================================== */
#include "reloj.h"
#include "tarjeta.h"
#include <stdbool.h>

/* === Cabecera C++ ============================================================================ */
#ifdef __cplusplus
extern "C" {
#endif

/* === Definicion y Macros ===================================================================== */

//! Tipo de datos para almacenar un referencia a un objeto bitacora
typedef struct bitacora_s * bitacora_t;

typedef struct bitacora_entrada_s * bitacora_entrada_t;

//! Tipo de datos con los eventos conocidgos por la bitacora
typedef enum bitacora_mensajes_e {
    BITACORA_NO_ABRIO = 0,    //!< La puerta se liberó pero no se abrió en el tiempo esperado
    BITACORA_ENTRO,           //!< La puerta se liberó, se abrió y se cerró correctamente
    BITACORA_NO_CERRO,        //!< La puerta se liberó, se abrió pero no se cerro en el tiempo esperado
    BITACORA_CERRO,           //!< La puerta se cerró despues de ser forzada o quedar abierta
    BITACORA_FORZADA,         //!< La puerta se abrió sin una liberación previa del equipo
    BITACORA_NO_AUTORIZADA,   //!< La tarjeta leida no tiene autorizacion para abrir la puerta
    BITACORA_REINICIO,        //!< El sistema fue reiniciado
} bitacora_mensajes_t;

typedef struct bitacora_evento_s {
    bitacora_mensajes_t mensaje;
    uint32_t tarjeta;
} * bitacora_evento_t;

//! Prototipo de la funcion para procesar cada entrada al listar las entradas de la bitacora
typedef bool (*bitacora_listar_t)(uint16_t indice, bitacora_entrada_t entrada, void * referencia);

/* === Declaraciones de tipos de datos ========================================================= */

/* === Declaraciones de variables externas ===================================================== */

/* === Declaraciones de funciones externas ===================================================== */

/**
 * @brief Función para crear una nueva instancia de un objeto bitacora
 * 
 * @return  bitacora_t  Referencia al nuevo objeto bitacora creado
 */
bitacora_t BitacoraCrear(reloj_t reloj);

/**
 * @brief Función para recibir las notificaciones de eventos externos
 * 
 * @param   bitacora    Referencia al obtenida al crear el objeto bitacora 
 * @param   evento      Referencia al objeto evento que se desea informar
 */
void BitacoraRegistrar(bitacora_t self, const bitacora_evento_t evento);

void BitacoraCadena(const bitacora_entrada_t entrada, char * cadena, size_t espacio);

void BitacoraObtener(bitacora_t self, bitacora_listar_t gestor, void * referencia);

/* === Ciere de documentacion ================================================================== */
#ifdef __cplusplus
}
#endif

/** @} Final de la definición del modulo para doxygen */

#endif   /* BITACORA_H */
