#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := punku

include $(IDF_PATH)/make/project.mk

.PHONY: coverage

coverage: 
	@ceedling clobber
	@ceedling gcov:all
	@ceedling utils:gcov

statics:
	@cloc --vcs=git --by-file-by-lang --not-match-d="test" --md --report-file=build/artifacts/pruebas.md
	# @cloc --vcs=git --by-file-by-lang --match-d="test" --md --report-file=build/artifacts/pruebas.md
	@cloc --vcs=git --by-file-by-lang --match-d="test" --md --report-file=build/artifacts/codigo.md
#	@cloc --vcs=git --by-file-by-lang --json --report-file=build/artifacts/estadisticas.json

documents/PNK-DO008.md: statics
	@$(CC) -E -x c -P -C -Ibuild/artifacts -traditional-cpp documents/PNK-DO008.cpp > documents/PNK-DO008.md

# document: coverage documents/PNK-DO008.md
document: documents/PNK-DO008.md
	@doxygen Doxyfile
	@cp -r build/artifacts/gcov ../../documentacion/punku