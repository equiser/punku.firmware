/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_lectoras.c
 ** @brief Pruebas unitarias del componente para gestion de las lectoras de tarjetas
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.12.13 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "unity.h"
#include "configuracion.h"
#include "json.h"
/* === Definicion y Macros ===================================================================== */

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de funciones internas ====================================================== */

/* === Definiciones de funciones externas ====================================================== */

//! Fija las condiciones iniciales comunes a todas las pruebas
void setUp(void) {
    // lectora = LectoraCrear();
    // LectoraNotificar(lectora, registrar_evento, referencia);
}

//! Limpia los resultados y objetos de todas las pruebas
void tearDown(void) {
    // limpiar_eventos();
}

    char EJEMPLO[] = "{"
        "\"entero\": 37345,"
        "\"logico\": false,"
        "\"cadena\": \"prueba\","
    "}";

//! @test Se autoriza el ingreso al agregar una tarjeta en una lista vacia"
void test_serializar_configuracion_defecto(void) {
    const char RESULTADO[] = "{"
        "\"control\":{"
            "\"puerta\":{"
                "\"opciones\":{"
                    "\"sensor\":true,"
                    "\"inversor\":false,"
                    "\"mecanismo\":false"
                "},"
                "\"tiempos\":{"
                    "\"liberacion\":10,"
                    "\"apertura\":30,"
                    "\"cierre\":50"
                "}"
            "}"
        "},"
        "\"servidor\":{"
            "\"estacion\":true,"
            "\"nombre\":\"Volentini\","
            "\"clave\":\"Sofia1217\""
        "}"
    "}";

    configuracion_t configuracion = ConfiguracionCrear();
    char cadena [256];

    TEST_ASSERT_NOT_EQUAL(0, ConfiguracionConsultar(configuracion, cadena, sizeof(cadena)));
    TEST_ASSERT_EQUAL_STRING(RESULTADO, cadena);
}

void test_configurar_sensor_puerta(void) {
    char cadena[128] = "{\"control\":{\"puerta\":{\"opciones\":{\"sensor\":false}}}}";
    configuracion_t configuracion = ConfiguracionCrear();

    TEST_ASSERT(ConfiguracionActualizar(configuracion, cadena));
    TEST_ASSERT_FALSE(ConfiguracionControl(configuracion)->puerta.opciones.sensor);
}

void test_configurar_inversion(void) {
    char cadena[128] = "{\"control\":{\"puerta\":{\"opciones\":{\"inversor\":true}}}}";
    configuracion_t configuracion = ConfiguracionCrear();

    TEST_ASSERT(ConfiguracionActualizar(configuracion, cadena));
    TEST_ASSERT(ConfiguracionControl(configuracion)->puerta.opciones.inversor);
}

void test_configurar_tiempo_apertura(void) {
    char cadena[128] = "{\"control\":{\"puerta\":{\"tiempos\":{\"apertura\":90}}}}";
    configuracion_t configuracion = ConfiguracionCrear();

    TEST_ASSERT(ConfiguracionActualizar(configuracion, cadena));
    TEST_ASSERT_EQUAL(90, ConfiguracionControl(configuracion)->puerta.tiempos.apertura);
}

void test_configurar_tiempo_cierre(void) {
    char cadena[128] = "{\"control\":{\"puerta\":{\"tiempos\":{\"cierre\":90}}}}";
    configuracion_t configuracion = ConfiguracionCrear();

    TEST_ASSERT(ConfiguracionActualizar(configuracion, cadena));
    TEST_ASSERT_EQUAL(90, ConfiguracionControl(configuracion)->puerta.tiempos.cierre);
}

void test_configurar_tiempo_liberacion(void) {
    char cadena[128] = "{\"control\":{\"puerta\":{\"tiempos\":{\"liberacion\":90}}}}";
    configuracion_t configuracion = ConfiguracionCrear();

    TEST_ASSERT(ConfiguracionActualizar(configuracion, cadena));
    TEST_ASSERT_EQUAL(90, ConfiguracionControl(configuracion)->puerta.tiempos.liberacion);
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

