/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_bitacora.c
 ** @brief Pruebas unitarias de la libreria para el registro de 
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.04.03 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "unity.h"
#include "bitacora.h"

#include "mock_reloj.h"
#include "mock_errores.h"
#include "mock_archivos.h"

#include <string.h>
/* === Definicion y Macros ===================================================================== */
#define TEST_ASSERT_FILE_WRITE_MESSAGE(mensaje)                                 \
        TEST_ASSERT_EQUAL_MESSAGE(1, ArchivoAbrir_fake.call_count, mensaje);    \
        TEST_ASSERT_EQUAL_MESSAGE(1, ArchivoMover_fake.call_count, mensaje);    \
        TEST_ASSERT_EQUAL_MESSAGE(1, ArchivoEscribir_fake.call_count, mensaje); \
        TEST_ASSERT_EQUAL_MESSAGE(1, ArchivoCerrar_fake.call_count, mensaje)

#define RESET_TEST_FAKES(mensaje, indice)                                       \
        registro[0] = 0;                                                        \
        FFF_RESET_HISTORY();                                                    \
        RESET_FAKE(ArchivoAbrir);                                               \
        RESET_FAKE(ArchivoMover);                                               \
        RESET_FAKE(ArchivoEscribir);                                            \
        RESET_FAKE(ArchivoCerrar)                                               \
        sprintf(mensaje, "Ejemplo %d", ejemplo_actual + 1);                     \
        ArchivoEscribir_fake.custom_fake = CapturarEscritura;                   \
        RelojConsultar_fake.custom_fake = SimularReloj

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

// //!< Estructura con los ejempos para las pruebas
// static const struct ejemplo_s {     
//     char * fecha;                   //!< Cadena de texto con la fecha y hora del reloj
//     bitacora_acceso_t acceso;       //!< Evento que se desea registrar
//     uint32_t tarjeta;               //!< Tarjeta que origina el evento
//     char * registro;                //!< Cadena de texto con el registro del evento
// } ejemplos[] = {                    //!< Arreglo de valores con los ejemplos para las pruebas
//     {
//         .fecha = "2018-01-14 02:18:23",
//         .acceso.evento = BITACORA_NO_ABRIO,
//         .acceso.tarjeta = 0,
//         .registro = "2018-01-14,02:18:23,PULSADOR,0,Liberacion sin apertura de la puerta\r\n",
//     },{
//         .fecha = "2019-12-11 22:35:47",
//         .acceso.evento = BITACORA_ENTRO,
//         .acceso.tarjeta = 0,
//         .registro = "2019-12-11,22:35:47,PULSADOR,1,Apertura y cierre de la puerta\r\n",
//     },{
//         .fecha = "2018-01-14 02:18:23",
//         .acceso.evento = BITACORA_NO_CERRO,
//         .acceso.tarjeta = 0,
//         .registro = "2018-01-14,02:18:23,PULSADOR,2,Apertura sin cierre de la puerta\r\n",
//     },{
//         .fecha = "2018-01-14 02:18:23",
//         .acceso.evento = BITACORA_FORZADA,
//         .acceso.tarjeta = 0,
//         .registro = "2018-01-14,02:18:23,PULSADOR,4,Apertura forzada de la puerta\r\n",
//     },
// };

// Variable para indicar a las funciones de prueba que datos deben inyectar
// static int ejemplo_actual = 0;

// bitacora_t bitacora;

// char registro[128];


/* === Declaraciones de funciones internas ===================================================== */
// bool CapturarEscritura(archivo_t archivo, const void * datos, uint32_t longitud);

/* === Definiciones de funciones internas ====================================================== */
// bool CapturarEscritura(archivo_t archivo, const void * datos, uint32_t longitud) {
//     memcpy(registro, datos, longitud);
//     registro[longitud] = 0;
//     return true;
// }

// void SimularReloj(reloj_t reloj, struct tm * fecha) {
//     strptime(ejemplos[ejemplo_actual].fecha, "%Y-%m-%d %H:%M:%S", fecha);
// }



/* === Definiciones de funciones externas ====================================================== */

void setUp(void) {
    // bitacora = BitacoraCrear(RelojCrear());
}

void tearDown(void) {
}

void test_registrar_acceso(void) {
    // char mensaje[20];

    // for(ejemplo_actual = 0; ejemplo_actual < sizeof(ejemplos) / sizeof(struct ejemplo_s); ejemplo_actual++) {
    //     RESET_TEST_FAKES(mensaje, ejemplo_actual);

    //     BitacoraRegistrarAcceso(bitacora, &ejemplos[ejemplo_actual].acceso);

    //     TEST_ASSERT_EQUAL_STRING_MESSAGE(ejemplos[ejemplo_actual].registro, registro, mensaje);
    // }
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

