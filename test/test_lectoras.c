/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file test_lectoras.c
 ** @brief Pruebas unitarias del componente para gestion de las lectoras de tarjetas
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.12.13 | evolentini      | Version inicial del archivo                             |
 **
 ** @addtogroup controladores
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "unity.h"
#include "lectoras.h"
#include "mock_errores.h"
#include "mock_digitales.h"
#include "mock_spi.h"
#include "mock_picc.h"
#include "mock_mifare.h"
#include "mock_mfrc522.h"

/* === Definicion y Macros ===================================================================== */

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

//! Variable global con la referencia al objeto puerta bajo prueba
static lectora_t lectora;

//! Variable global con una referencia a utilizar en los eventos generados por la puerta
static void * referencia = (void *) 4;

/* === Declaraciones de funciones internas ===================================================== */

/* === Definiciones de funciones internas ====================================================== */

/* === Definiciones de funciones externas ====================================================== */

//! Fija las condiciones iniciales comunes a todas las pruebas
void setUp(void) {
    // lectora = LectoraCrear();
    // LectoraNotificar(lectora, registrar_evento, referencia);
}

//! Limpia los resultados y objetos de todas las pruebas
void tearDown(void) {
    // limpiar_eventos();
}

//! @test Se autoriza el ingreso al agregar una tarjeta en una lista vacia"
void test_lectura_de_una_tarjeta(void) {
    // evento_t evento = {
    //     .mensaje = EVENTO_CONTAR_TIEMPO,
    // };
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definición del modulo para doxygen */

