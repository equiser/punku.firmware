/**************************************************************************************************
 ** (c) Copyright 2019: Esteban VOLENTINI <evolentini@gmail.com>
 ** ALL RIGHTS RESERVED, DON'T USE OR PUBLISH THIS FILE WITHOUT AUTORIZATION
 *************************************************************************************************/

/** @file puertas.h
 ** @brief Declaraciones de la libreria para gestion de la puerta
 **
 **| REV | YYYY.MM.DD | Autor           | Descripción de los cambios                              |
 **|-----|------------|-----------------|---------------------------------------------------------|
 **|   1 | 2019.04.03 | evolentini      | Version inicial del archivo                             |
 **
 ** @defgroup PdM Programacion de Microcontroladores
 ** @brief Trabajo Practico para Programacion de Microcontroladores 
 ** @{ */

/* === Inclusiones de cabeceras ================================================================ */
#include "unity.h"

#include "configuracion.h"
#include "control.h"
#include "sensores.h"
#include "puertas.h"
#include "lectoras.h"
#include "mock_bitacora.h"
#include "mock_digitales.h"

#include "mock_gpio.h"
#include "mock_reloj.h"
#include "mock_mfrc522.h"
#include "mock_picc.h"
#include "mock_spi.h"
#include "mock_autorizados.h"
#include "mock_errores.h"
#include "mock_archivos.h"
#include "mock_sonidos.h"

#include "FreeRTOS.h"
#include "task.h"

/* === Definicion y Macros ===================================================================== */
// #define TEST_ABORT() return

/* == Declaraciones de tipos de datos internos ================================================= */

/* === Definiciones de variables internas ====================================================== */

//! Variable global con la referencia al objeto puerta bajo prueba
static control_t control = NULL;

static PICC_UID_TYPE uid;

//! Variable global para simular el estado actual de los sensores
static struct {
    bool directa;
    bool inversa;
    bool alarma;
    bool liberada;
    bool bloqueada;
    bool abierta;
    bool pulsador;
} gpio = {0};

static struct control_configuracion_s configuracion = {
    .puerta = {
        .opciones = {
            .sensor = false, .inversor = false, .mecanismo = false
        },
        .tiempos = {
            .liberacion = 10, .apertura = 30, .cierre = 50  
        }
    }
};

/* === Declaraciones de funciones internas ===================================================== */

extern void SensorEventoEntrada(void * argumento);

// extern digital_entrada_t EspiarPulsador(control_t self);

// extern digital_entrada_t EspiarSensor(control_t self);

// extern digital_salida_t EspiarAutorizado(control_t self);

/* === Definiciones de funciones internas ====================================================== */
bool simular_entrada(digital_entrada_t entrada) {
    bool * puntero = (void *) entrada;
    bool resultado = false;

    if ((puntero >=  &gpio.liberada) && (puntero <= &gpio.pulsador)) {
        resultado = *puntero;
    }
    return resultado;
}

void simular_escritura(digital_salida_t salida, bool estado) {
    *((bool *)salida) = estado;
}

void simular_prender(digital_salida_t salida) {
    *((bool *)salida) = true;
}

void simular_apagar(digital_salida_t salida) {
    *((bool *)salida) = false;
}

void simular_lectura(tarjeta_t tarjeta) {
    uid.data[0] = (uint8_t) ((tarjeta >> 24) & 0xFF);
    uid.data[1] = (uint8_t) ((tarjeta >> 24) & 0xFF);
    uid.data[2] = (uint8_t) ((tarjeta >> 24) & 0xFF);
    uid.data[3] = (uint8_t) ((tarjeta >> 24) & 0xFF);
    uid.size = 4;
    PICC_IsNewCardPresent_fake.return_val = true;
}

bool tarjeta_leida(PICC_UID_TYPE * resultado) {
    memcpy(resultado, &uid, sizeof(PICC_UID_TYPE));
    return STATUS_OK;
}

StatusCode terminar_lectura() {
    PICC_IsNewCardPresent_fake.return_val = false;
    return STATUS_OK;
}

int SimularLectura(gpio_num_t terminal) {
    uint32_t resultado = 0;

    // if (terminal == GPIO_PULSADOR) {
    //     resultado = estado_terminales.pulsador;
    // } else if (terminal == GPIO_SENSOR) {
    //     resultado = estado_terminales.sensor;
    // } else if (terminal == GPIO_DIRECTA) {
    //     resultado = estado_terminales.directa;
    // } else if (terminal == GPIO_INVERSA) {
    //     resultado = estado_terminales.inversa;
    // } else if (terminal == GPIO_ALARMA) {
    //     resultado = estado_terminales.alarma;
    // }
    return resultado;
}

esp_err_t RegistrarEscritura(gpio_num_t terminal, uint32_t nivel) {
    // if (terminal == GPIO_PULSADOR) {
    //     TEST_FAIL_MESSAGE("Escritura en un terminal de entrada");
    // } else if (terminal == GPIO_SENSOR) {
    //     TEST_FAIL_MESSAGE("Escritura en un terminal de entrada");
    // } else if (terminal == GPIO_DIRECTA) {
    //     estado_terminales.directa = nivel;
    // } else if (terminal == GPIO_INVERSA) {
    //     estado_terminales.inversa = nivel;
    // } else if (terminal == GPIO_ALARMA) {
    //     estado_terminales.alarma = nivel;
    // }
    return ESP_OK;
}

/* === Definiciones de funciones externas ====================================================== */
void suiteSetUp(void) {
    puerta_t puerta = PuertaCrear(
        &(struct puerta_recursos_s) {
            .abierta = SensorCrear((digital_entrada_t) &gpio.abierta, SENSORES_HISTERESIS),
            .liberada = SensorCrear((digital_entrada_t) &gpio.liberada, SENSORES_HISTERESIS),
            .bloqueada = SensorCrear((digital_entrada_t) &gpio.bloqueada, SENSORES_HISTERESIS),
            .directa = (digital_salida_t) &gpio.directa,
            .inversa = (digital_salida_t) &gpio.inversa,
            .alarma = (digital_salida_t) &gpio.alarma,
        }, &configuracion.puerta
    );

    control = ControlCrear(
        &(struct control_recursos_s) {
            .lectora = LectoraCrear(&(struct lectora_recursos_s) {0}),
            .puerta = puerta,
            .pulsador = SensorCrear((digital_entrada_t) &gpio.pulsador, SENSORES_HISTERESIS),
            .sonido = NULL,
            .bitacora = BitacoraCrear(NULL),
            .autorizados = AutorizadosCrear(),
        }, &configuracion
    );
    // control = ControlCrear();
}

void setUp(void) {
    vTaskDelay(pdMS_TO_TICKS(1));

    FFF_RESET_HISTORY();
    EntradaLeer_fake.custom_fake = simular_entrada;
    SalidaEscribir_fake.custom_fake = simular_escritura;
    SalidaPrender_fake.custom_fake = simular_prender;
    SalidaApagar_fake.custom_fake = simular_apagar;
    PICC_ReadCardSerial_fake.custom_fake = tarjeta_leida;
    PICC_HaltA_fake.custom_fake = terminar_lectura;
}

void tearDown(void) {

}

//! @test Abrir la puerta sin autorizacion del equipo"
void test_violacion_de_puerta(void) {
    // Dado un equipo con el sensor de puerta instalado
    configuracion.puerta.opciones.sensor = true;
    ControlConfigurar(control, &configuracion);
    vTaskDelay(pdMS_TO_TICKS(1));

    // Cuando la puerta se abre sin haber sin haber sido liberada
    gpio.abierta = true;
    vTaskDelay(pdMS_TO_TICKS(200));
    // CambiarEntrada(GPIO_SENSOR, 0);

    // Entonces se activa la salida de alarma
    TEST_ASSERT_TRUE(gpio.alarma);

    // Y se genera registra en la bitacora que la puerta fue forzada

    // Cuando despues de un tiempo arbitrario se cierra la puerta
    vTaskDelay(pdMS_TO_TICKS(300));
    gpio.abierta = false;
    vTaskDelay(pdMS_TO_TICKS(200));

    // Entonces se apaga la salida de alarma
    TEST_ASSERT_FALSE(gpio.alarma);

    // Y se genera registra en la bitacora el cierre de la puerta
    // TEST_ASSERT_EVENT(PUERTA_CERRO);
}

//! @test Liberacion de la puerta por pulsador sin apertura"
void test_liberacion_pulsador_sin_apertura(void) {

    // // Dado un equipo sin inversor y con el tiempo de apertura en 20 decimas 
    // // Y sin la opción de invsersión de giro
    // const control_opciones_t opciones = {
    //     .puerta.tiempos.apertura = 20,
    //     .puerta.opciones.inversor = false,
    // };
    // ControlConfigurar(control, &opciones);
    // vTaskDelay(pdMS_TO_TICKS(1));

    // // Cuando se activa la entrada para el pulsador de apertura
    // CambiarEntrada(GPIO_PULSADOR, 0);

    // // Entonces la salida para liberar la puerta se encuentra activa
    // vTaskDelay(pdMS_TO_TICKS(1));
    // TEST_ASSERT_NOT_EQUAL(0, estado_terminales.directa);

    // // Cuando transcurren 5 decimas de segundo
    // vTaskDelay(pdMS_TO_TICKS(500));

    // // Y se libera la entrada para el pulsador de apertura
    // CambiarEntrada(GPIO_PULSADOR, 1);

    // // Entonces la salida para liberar la puerta se encuentra activa
    // TEST_ASSERT_NOT_EQUAL(0, estado_terminales.directa);

    // // Cuando transcurren 15 decimas de segundo
    // vTaskDelay(pdMS_TO_TICKS(1500));

    // // Entonces la salida de alarma del equipo se encuentra activa
    // TEST_ASSERT_NOT_EQUAL(0, estado_terminales.directa);

    // // Cuando transcurren 1 decimas de segundo
    // vTaskDelay(pdMS_TO_TICKS(100));

    // // Entonces la salida de alarma del equipo se encuentra en reposo
    // TEST_ASSERT_EQUAL(0, estado_terminales.directa);
}

//! @test Apertura y cierre de la puerta por pulsador"
void test_apertura_cierre_pulsador(void) {
    // Dado un equipo sin inversor y con el sensor de puerta instalado
    configuracion.puerta.opciones.inversor = false,
    configuracion.puerta.opciones.sensor = true;
    // Y con el tiempo de apertura configurado en 3 segundos
    configuracion.puerta.tiempos.apertura = 30;
    // Y con el tiempo de cierre configurado en 5 segundos
    configuracion.puerta.tiempos.cierre = 50,
    // Y sin la opción de invsersión de giro
    configuracion.puerta.opciones.inversor = false;
    ControlConfigurar(control, &configuracion);
    vTaskDelay(pdMS_TO_TICKS(1));
    
    // Cuando se activa la entrada para el pulsador de apertura
    gpio.pulsador = true;
    vTaskDelay(pdMS_TO_TICKS(200));

    // Entonces la salida para liberar la puerta se encuentra activa
    TEST_ASSERT_TRUE(gpio.directa);

    // Cuando transcurren 5 decimas de segundo
    vTaskDelay(pdMS_TO_TICKS(500));

    // Y se libera la entrada para el pulsador de apertura
    gpio.pulsador = false;
    vTaskDelay(pdMS_TO_TICKS(200));

    // Entonces la salida para liberar la puerta se encuentra activa
    TEST_ASSERT_TRUE(gpio.directa);

    // Cuando transcurren 15 decimas de segundo
    vTaskDelay(pdMS_TO_TICKS(1500));

    // Y el sensor informa la apertura de la puerta
    gpio.abierta = true;
    vTaskDelay(pdMS_TO_TICKS(200));

    // Entonces la salida para liberar la puerta se encuentra en reposo
    TEST_ASSERT_FALSE(gpio.directa);

    // Y la salida de alarma del equipo se encuentra en reposo
    TEST_ASSERT_FALSE(gpio.alarma);

    // Cuando transcurren 30 decimas de segundo
    vTaskDelay(pdMS_TO_TICKS(3000));

    // Y el sensor informa el cierre de la puerta
    gpio.abierta = false;
    vTaskDelay(pdMS_TO_TICKS(200));

    // Entonces la salida de alarma del equipo se encuentra en reposo
    TEST_ASSERT_FALSE(gpio.alarma);
}

//! @test Apertura por pulsador sin cierre de la puerta"
void test_apertura_pulsador_sin_cierre(void) {
    // // Dado un equipo con el tiempo de apertura configurado en 30 decimas de segundo

    // // Dado un equipo sin inversor y con el tiempo de apertura en 30 decimas 
    // // Y con el tiempo de cierre configurado en 40 decimas de segundo
    // // Y sin la opción de invsersión de giro
    // const control_opciones_t opciones = {
    //     .puerta.tiempos.apertura = 30,
    //     .puerta.tiempos.cierre = 40,
    //     .puerta.opciones.inversor = false,
    // };
    // ControlConfigurar(control, &opciones);
    // vTaskDelay(pdMS_TO_TICKS(1));

    // // Cuando se activa la entrada para el pulsador de apertura
    // CambiarEntrada(GPIO_PULSADOR, 0);

    // // Entonces la salida para liberar la puerta se encuentra activa
    // vTaskDelay(pdMS_TO_TICKS(1));
    // TEST_ASSERT_NOT_EQUAL(0, estado_terminales.directa);

    // // Cuando transcurren 5 decimas de segundo
    // vTaskDelay(pdMS_TO_TICKS(500));

    // // Y se libera la entrada para el pulsador de apertura
    // CambiarEntrada(GPIO_PULSADOR, 1);

    // // Entonces la salida para liberar la puerta se encuentra activa
    // TEST_ASSERT_NOT_EQUAL(0, estado_terminales.directa);

    // // Cuando transcurren 15 decimas de segundo
    // vTaskDelay(pdMS_TO_TICKS(1500));

    // // Y el sensor informa la apertura de la puerta
    // CambiarEntrada(GPIO_SENSOR, 0);
    // vTaskDelay(pdMS_TO_TICKS(1));

    // // Entonces la salida para liberar la puerta se encuentra en reposo
    // TEST_ASSERT_EQUAL(0, estado_terminales.directa);

    // // Y la salida de alarma del equipo se encuentra en reposo
    // TEST_ASSERT_EQUAL(0, estado_terminales.alarma);

    // // Cuando transcurren 40 decimas de segundo
    // vTaskDelay(pdMS_TO_TICKS(4000));

    // // Entonces la salida para liberar la puerta se encuentra en reposo
    // TEST_ASSERT_EQUAL(0, estado_terminales.directa);

    // // Y la salida de alarma del equipo se encuentra activa
    // TEST_ASSERT_NOT_EQUAL(0, estado_terminales.alarma);

    // // Cuando transcurren 10 decimas de segundo
    // vTaskDelay(pdMS_TO_TICKS(1000));

    // // Y el sensor informa el cierre de la puerta
    // CambiarEntrada(GPIO_SENSOR, 1);
    // vTaskDelay(pdMS_TO_TICKS(1));

    // // Entonces la salida de alarma del equipo se encuentra en reposo
    // TEST_ASSERT_EQUAL(0, estado_terminales.alarma);
}

void test_tarjeta_no_autorizada(void) {
    simular_lectura(15551112);
    vTaskDelay(pdMS_TO_TICKS(200));
}

/* === Ciere de documentacion ================================================================== */

/** @} Final de la definicion del modulo para doxygen */

